FROM php:7.4-apache
RUN apt-get update && apt-get upgrade -y
RUN a2enmod rewrite
COPY . /var/www/html/
EXPOSE 80