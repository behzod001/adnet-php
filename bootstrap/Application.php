<?php

namespace bootstrap;

use app\exceptions\DataNotFoundException;
use directapi\DirectApiService;
use FacebookAds\Api;
use FacebookAds\Logger\CurlLogger;
use FacebookAds\Object\AdAccount;
use helpers\Alert;
use models\Account;
use PDO;
use PDOException;
use Symfony\Component\Validator\Exception\UnsupportedMetadataException;

class Application
{
    public array $url;

    public ?PDO $db;

    private array $config;

    private ?DirectApiService $direct;
    private ?Api $facebook;

    /**
     * Application constructor.
     * @param $config array()
     */
    public function __construct()
    {
        $this->db = null;
        $uri = $routePath = $_SERVER['REQUEST_URI'];
        $folder = $page = "";
        if (strpos($uri, "?") > 0) {
            $query = str_split($uri, strpos($uri, "?"));
            $queryParam = $query[1];
            $path = explode("/", $query[0]);
            $routePath = $query[0];
        } else {
            $path = explode("/", $uri);
        }
        if (isset($path[3])) {
            $folder = $path[1] . DIRECTORY_SEPARATOR . $path[2];
            $page = $path[3];
        } else {
            $folder = $path[1];
        }

        $page = array_key_exists($routePath, CONFIG['routes']) ? DIRECTORY_SEPARATOR . CONFIG['routes'][$routePath] : "/index";

        $this->url = [
            "page" => $page,
            "folder" => $folder,
        ];
    }

    public function run()
    {
        // fb start
        $api = Api::init(CONFIG['fbConfig']['app_id'], CONFIG['fbConfig']['app_secret'], CONFIG['fbConfig']['access_token']);
        $api->setLogger(new CurlLogger());
        $api->setDefaultGraphVersion(CONFIG['fbConfig']['api_version']);
        $this->facebook = $api;

        // yandex start
        $this->direct = new DirectApiService(CONFIG['direct']['token'], CONFIG['direct']['client_login'], CONFIG['direct']['query_logger'], CONFIG['direct']['logger'], CONFIG['direct']['user_sandbox']);

        //get layout
        $content = ROOT_PATH . '/public/views/' . $this->url['folder'] . $this->url['page'] . '.php';
        define('$content', $content);

        if ($this->url['folder'] == "sign")
            require_once ROOT_PATH . '/public/layouts/sign.php';
        if ($this->url['folder'] != "sign")
            require_once ROOT_PATH . '/public/layouts/main.php';
    }

    public function getIdentity($redirect = false)
    {
        if ($redirect && isset($_SESSION['identity'])) {
            header("Location: /sign/in", true, 200);
        }
        return $_SESSION['identity'] ?? null;
    }

    /**
     * @return Api
     */
    public function getFacebook(): Api
    {
        if ($this->facebook == null) {
            $this->facebook = Api::init(CONFIG['fbConfig']['app_id'], CONFIG['fbConfig']['app_secret'], CONFIG['fbConfig']['access_token']);
            $this->facebook->setLogger(new CurlLogger());
            $this->facebook->setDefaultGraphVersion(CONFIG['fbConfig']['api_version']);
        }
        return $this->facebook;
    }

    /**
     * @throws DataNotFoundException
     */
    public function getFacebookActiveAccount(): AdAccount
    {
        if (isset($_SESSION['FACEBOOK']['account_id']))
            return new AdAccount($_SESSION['FACEBOOK']['account_id'], null, $this->getFacebook());
        else
            throw new DataNotFoundException("Account not selected");
    }

    /**
     * @return PDO|null
     */
    public function getDb(): PDO
    {
        if ($this->db == null) {
            try {
                $this->db = new PDO("mysql:host=" . CONFIG['db']['host'] . ";dbname=" . CONFIG['db']['dbName'], CONFIG['db']['user'], CONFIG['db']['password']);
            } catch (PDOException $e) {
                Alert::error($e->getMessage());
                die();
            }
        }
        return $this->db;
    }

    /**
     * @return DirectApiService
     * @throws DataNotFoundException
     */
    public function getDirect(): DirectApiService
    {
        if ($this->direct == null) {
            $this->direct = new DirectApiService(CONFIG['direct']['token'], CONFIG['direct']['client_login'], CONFIG['direct']['query_logger'], CONFIG['direct']['logger'], CONFIG['direct']['user_sandbox']);
        }
        if (isset($_SESSION['YANDEX']['client_login'])) {
            $this->direct->setClientLogin($_SESSION['YANDEX']['client_login']);
            return $this->direct;
        } else throw new DataNotFoundException("Yandex default account not selected");

    }

    public function getDirectWithClient($clientId): DirectApiService
    {
        if ($this->direct == null) {
            return new DirectApiService(CONFIG['direct']['token'], $clientId, CONFIG['direct']['query_logger'], CONFIG['direct']['logger'], CONFIG['direct']['user_sandbox']);
        }
        $this->direct->setClientLogin($clientId);
        return $this->direct;
    }

    /**
     * @param DirectApiService $direct
     */
    public function setDirect(DirectApiService $direct): void
    {
        $this->direct = $direct;
    }

    /**
     * @param Api $facebook
     */
    public function setFacebook(Api $facebook): void
    {
        $this->facebook = $facebook;
    }

    /**
     * getting all local registrred acounts
     * @var $result Account[]
     *
     */
    public function getLocalAccounts(): array
    {
        if (!isset($_SESSION['identity']->id)) throw new UnsupportedMetadataException("User not logged in");
        $stmt = $this->getDb()->prepare("Select * from `accounts` where `user_id`= :user_id");
        $stmt->bindValue(":user_id", $_SESSION['identity']->id);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        unset($stmt);
        return $result;
    }

    public function logout()
    {
        if (isset($_SESSION['identity'])) unset($_SESSION['identity']);

        if (isset($_SESSION['YANDEX'])) unset($_SESSION['YANDEX']);
        if (isset($_SESSION['FACEBOOK'])) unset($_SESSION['FACEBOOK']);
        $this->facebook = null;
        $this->direct = null;
    }
}