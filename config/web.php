<?php
return [
    'db' => [
        'host' => 'localhost',
        'dbName' => 'adnet',
        'user' => 'root',
        'password' => ''
    ],
    'direct' => [
        'master_token' => '212e1b27c45d696bdb26e889b9283873',
        'token' => 'AQAAAABVYl0iAAcpt9unH6qwW0rnsaYg2_xbLcI',
        'client_login' => 'bexzodxayrullayev',
        'query_logger' => null,
        'logger' => null,
        'user_sandbox' => true
    ],
    "fbConfig" => [
        'app_id' => '613840796261147',
        'app_secret' => 'a7ee2dd5a7518273a127808a5ba8c542',
        'access_token' => 'EAAIuSPKcGxsBAHYQgLwZAnkHZAiZBv6Ydfn6jT9twrN5PmtDr4ZAKX0hZASRZAVW0kfZAdySQsIEEfh6ZAQqf2vlECHu9Xae4pIKGbf5ayEKPeH3HGKfoUpcp4QhETmyFrbulDOlcTclj6NZAmZCsrheJfSl3gt0lIgj1DqJ26WASn5rTPbI6TKOGi',
        'account_id' => '113756654299964',
        'api_version' => '11.0'
    ],
//    "fbConfig" => [
//        'app_id' => '613840796261147',
//        'app_secret' => 'a7ee2dd5a7518273a127808a5ba8c542',
//        'access_token' => 'EAAIuSPKcGxsBAFapgQl5uuNzyTSqavtRo7gqJPi99LLlX565atCX3YQaNjNwhqq1IQSsfDJva2ayxGpcLJNa0FnEE4hEAP1D1nYyFj0q3J6teCPNQLQ99ZB2iBV2npdlZCGxMVzRRnTDxAdS0umaC7GszhZAfYTewdrauDovZCIzVRZClPRCA6A5ZAPvjNZCoUZD',
//        'account_id' => '100069968905201',
//        'api_version' => '11.0'
//    ],
    'routes' => [
        '/google/upload' => 'upload',
        '/google/chart' => 'chart',
        '/google/index' => 'index',
        '/google/delete' => 'delete',
        '/google/types' => 'types',

        '/campaigns/create' => 'create',
        '/campaigns/index' => 'index',
        '/campaigns/view' => 'view',
        '/campaigns/update' => 'update',
        '/campaigns/delete' => 'delete',
        '/campaigns/insights' => 'insights',

        '/adimages/create' => 'create',
        '/adimages/view' => 'view',
        '/adimages/update' => 'update',
        '/adimages/delete' => 'delete',

        '/creatives/create' => 'create',
        '/creatives/view' => 'view',
        '/creatives/update' => 'update',
        '/creatives/delete' => 'delete',

        '/adsets/create' => 'create',
        '/adsets/view' => 'view',
        '/adsets/update' => 'update',
        '/adsets/delete' => 'delete',

        '/ads/create' => 'create',
        '/ads/view' => 'view',
        '/ads/update' => 'update',
        '/ads/delete' => 'delete',

        '/sign/in' => 'in',
        '/sign/up' => 'up',
        '/sign/privacy' => 'privacy',
        '/sign/terms' => 'terms',
        '/sign/out' => 'out',

        '/accounts/create' => 'create',
        '/accounts/profile' => 'view',
        '/accounts/update' => 'update',
        '/accounts/delete' => 'delete',


//module direct
        '/direct/accounts/add' => 'add',
        '/direct/accounts/view' => 'view',
        '/direct/accounts/update' => 'update',
        '/direct/accounts/delete' => 'delete',

        '/direct/campaigns/create' => 'create',
        '/direct/campaigns/view' => 'view',
        '/direct/campaigns/update' => 'update',
        '/direct/campaigns/delete' => 'delete',
        '/direct/campaigns/network' => 'network',

        '/direct/adset/create' => 'create',
        '/direct/adset/update' => 'update',
        '/direct/adset/view' => 'view',

        '/direct/ads/create' => 'create',
        '/direct/ads/update' => 'update',
        '/direct/ads/view' => 'view',

        '/default/set' => 'set',
        '/home/help' => 'help',
        '/home/accounts' => 'accounts',
        '/home/campaigns' => 'campaigns',
        '/home/creatives' => 'creatives',
        '/home/files' => 'files',
        '/home/groups' => 'groups',
        '/home/removed' => 'removed',
        '/home/start' => 'start',
        '/home/structure' => 'structure',
        '/home/users' => 'users',
        '/home/videos' => 'videos',
    ]
];