CREATE TABLE `adnet`.`accounts`
(
    `id`         INT(11)     NOT NULL AUTO_INCREMENT,
    `user_id`    INT(11)     NOT NULL,
    `created_at` INT(11)     NOT NULL,
    `updated_at` INT(11)     NOT NULL,
    `account_id` VARCHAR(50) NOT NULL,
    `service_id` INT(1)      NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `idx-user-user_id` (`user_id`)
) ENGINE = InnoDB;

ALTER TABLE `accounts`
    ADD UNIQUE (`account_id`);