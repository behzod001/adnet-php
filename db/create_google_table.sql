CREATE TABLE `adnet`.`google_stats`
(
    `id`         INT(11)     NOT NULL AUTO_INCREMENT,
    `user_id`    INT(11)     NOT NULL,
    `created_at` INT(11)     NOT NULL,
    `updated_at` INT(11)     NOT NULL,
    `type` INT(11)     NOT NULL,
    `name` VARCHAR(512)       NOT NULL,
    `file` VARCHAR(512)       NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `idx-user-user_id` (`user_id`)
) ENGINE = InnoDB;
