
create table users
(
    login      varchar(50)  not null,
    username      varchar(50)  not null,
    created_at int          null,
    updated_at int          null,
    password   varchar(256) null,
    id         int auto_increment,
    constraint users_pk primary key (id)
);

create
    index users_id_index on users (id);




