<?php

namespace app\exceptions;

use Exception;

class FileNotReadableException extends \Exception
{

    public function __construct(Exception $exception)
    {
        $this->message = $exception->getMessage();
    }
}