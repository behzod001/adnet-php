<?php


namespace helpers;


class Alert
{

    private static string $stdClassValues = "rounded-md flex items-center px-5 py-4 mb-2 text-white";

    public const SUCCESS = "success";
    public const ERROR = "error";
    public const WARNING = "warning";

    private static array $themes = [
        self::SUCCESS => "bg-theme-9",
        self::ERROR => "bg-theme-6",
        self::WARNING => "bg-theme-12"
    ];

    private static array $icons = [
        self::SUCCESS => "check-square",
        self::WARNING => "alert-circle",
        self::ERROR => "alert-circle",
    ];

    public static function success($message)
    {
        echo self::generate(self::SUCCESS, self::SUCCESS, $message);
    }


    public static function error(string $message): void
    {
        echo self::generate(self::ERROR, self::ERROR, $message);
    }

    public static function warning(string $message): void
    {
        echo self::generate(self::ERROR, self::ERROR, $message);
    }

    public static function replace(array $replaceValues)
    {
        $str = " <div class='{stdClassValues} {class}'>  <i data-feather='{icon}' class='w-6 h-6 mr-2'></i> {message} <i data-feather='x' class='w-4 h-4 ml-auto'></i></div>";
        array_push($replaceValues, self::$stdClassValues);
        return str_replace(['{class}', '{icon}', '{message}', '{stdClassValues}'], $replaceValues, $str);
    }

    public static function setFlush(string $key, string $message): void
    {
        $_SESSION[$key] = $message;
    }

    public static function getFlush(string $key): string
    {

        if (isset($_SESSION[$key])) {
            $flush = $_SESSION[$key];
            unset($_SESSION[$key]);
            return $flush;
        }
        return "";
    }

    public static function generateMessage(string $theme, string $message): string
    {
        return self::generate($theme, $theme, $message);
    }

    /**
     * @param $theme
     * @param $icon
     * @param $message
     * @return string
     */
    private static function generate($theme, $icon, $message): string
    {
        return self::replace([self::$themes[$theme], self::$icons[$icon], $message]);
    }

}