<?php

namespace app\helpers;

class Chart
{
    /**
     * @var array
     * chart labels
     * @example  ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug']
     *
     */
    private array $labels;

    /**
     * @var array $color
     * @example ['#FF8B26','#FFC533','#285FD3','#ff9933','#ccff33','#a3a375']
     */
    private array $color;
    private array $data;


}