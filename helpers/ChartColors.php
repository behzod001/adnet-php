<?php

namespace app\helpers;

class ChartColors
{
    private array $colors = [
        "#FF8B26",
        "#FFC533",
        "#285FD3",
        "#ff9933",
        "#ccff33",
        "#a3a375",
        "#6b7216",
        "#90f4b4",
        "#1a9340",
        "#52b6d4",
        "#e3d695",
        "#80481d",
        "#f0bda9",
        "#c3df6d",
        "#95ae0f",
        "#566d34",
        "#989332",
        "#4083ec",
        "#259435",
        "#b98265",
        "#16ed08",
        "#b9f69b",
        "#c1ed7b",
        "#28c6da",
        "#bea9d5",
        "#f3c0d5",
        "#bcfd71",
        "#405bde",
        "#f523b3",
        "#7b79e9",
        "#9a5648"
    ];

    public function getColorByIndex(int $colorIndex): string
    {
        return $this->colors[$colorIndex];
    }

    public function getColorRandom(): string
    {
        return $this->colors[rand(0, count($this->colors))];
    }

    public function getColorByLength(int $length): array
    {
        return array_splice($this->colors, 0, $length);
    }

}