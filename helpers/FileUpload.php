<?php

namespace app\helpers;


class FileUpload
{
    private array $errors = array();
    private string $tmp;
    private string $uploadedFilePath;

    public function __construct(string $tmp)
    {
        $this->tmp = $tmp;
    }

    /**
     * Moves an uploaded file to a new location
     * @link https://php.net/manual/en/function.move-uploaded-file.php
     *
     * @param string $temporaryFilePath <p>
     * The filename of the uploaded file.
     * </p>
     * @return bool If filename is not a valid upload file,
     * then no action will occur, and
     * uploadFile will return
     * false.
     * </p>
     * <p>
     * If filename is a valid upload file, but
     * cannot be moved for some reason, no action will occur, and
     * uploadFile will return
     * false. Additionally, a warning will be issued.
     */
    public function uploadFile(string $uploadFilePath, string $fileName): bool
    {
        $fileType = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));
        $storePath = "/uploads" . $uploadFilePath . md5($fileName) . "." . $fileType;
        $uPath = ROOT_PATH . $storePath;

        if (move_uploaded_file($this->tmp, $uPath)) {
            $this->uploadedFilePath = $uPath;
            return true;
        }
        return false;
    }

    public function getUploadedPath(): string
    {
        return $this->uploadedFilePath;
    }

    public function validate(array $validateAttribute)
    {
        $uploadedFileType = strtolower(pathinfo($this->tmp, PATHINFO_EXTENSION));
        if (false) {
            $this->errors['file'] = ["File must not be a null"];
        }
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}