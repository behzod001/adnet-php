<?php

namespace app\helpers;

use FacebookAds\Object\Link;


/**
 * @example
 * $linkPager = LinkPager::widget([
 *      "total"=> $page->getTotalElementsCount(),
 *      "page"=>$page,
 *      "size"=>$size
 * ])
 *
 * echo $linkPager->print();
 *
 */
class LinkPager
{
    /**
     * @var string
     * this item need to replace url
     * {page} page value
     * @see LinkPager::$page
     *
     * {size} need set to page elements size
     * @see LinkPager::$size
     */
    public string $link = "/google/index?page={page}&size={size}";

    /**
     * @var string
     * if you want change url you can set template like this
     *  "/google/index?page={page}&size={size}";
     */
    public string $linkTemplate = "";

    /**
     * @var string
     * this item set to link items if link active
     * @see LinkPager::$linkItem
     */
    public string $linkActive = "pagination__link--active";

    /**
     * @var string
     * this page number
     * @see LinkPager::$link
     */
    public string $page;

    /**
     * @var string
     * set size elements of page
     * @see LinkPager::$link
     */
    public string $size;

    /**
     * @var string
     * total elements of result set
     */
    public string $total;

    /**
     * @var string
     * LinkPager link item like this
     *
     * {active} need replace to active class
     * @see LinkPager::$linkActive
     *
     * {link} need to replace generated pages items url
     * @see LinkPager::$link
     *
     * {icon} need replace to icon item
     * icon item maybe like this
     * @see LinkPager::$firstPage
     * @see LinkPager::$lastPage
     * @see LinkPager::$prev
     * @see LinkPager::$next
     *
     */
    private string $linkItem = '<a class="pagination__link {active}" href="{link}"> {icon} </a>';

    /**
     * @var string
     * First page of LinkPager
     */
    private string $firstPage = ' <i class="w-4 h-4" data-feather="chevrons-left"></i> ';

    /**
     * @var string
     * Last page of LinkPager
     */
    private string $lastPage = '<i class="w-4 h-4" data-feather="chevrons-right"></i>';

    /**
     * @var string
     * previous page in LinkPager
     */
    private string $prev = ' <i class="w-4 h-4" data-feather="chevron-left"></i>';

    /**
     * @var string
     * Next page of LinkPager
     */
    private string $next = ' <i class="w-4 h-4" data-feather="chevron-right"></i> ';

    public function __construct(array $config)
    {
        if (is_null($config["total"]) || is_null($config["page"]) || is_null($config["size"])) {
            throw new \InvalidArgumentException("total, page and size required config elements");
        }

        $this->page = $config["page"];
        $this->size = $config["size"];
        $this->total = $config["total"];
        $this->linkTemplate = $config["linkTemplate"];


    }

    /**
     * @param bool $active
     * @param string $link
     * @param string $icon
     * @example <a class='pagination__link {active}' href='{link}'> {icon} </a>
     */
    private function getListItem(string $link, string $icon, bool $active = false): string
    {
        $active = $active ? $this->linkActive : "";
        $a = str_replace(["{active}", "{link}", "{icon}"], [$active, $link, $icon], $this->linkItem);
        return " <li> \n" . $a . " </li> \n";
    }

    /**
     * @param $page
     * @param $size
     * @return string
     * @example  "/google/index?page={page}&size={size}"
     */
    private function generateLink($page, $size): string
    {
        $template = ($this->linkTemplate !== "") ? $this->linkTemplate : $this->link;
        return str_replace(["{page}", "{size}"], [$page, $size], $template);
    }

    private function printList()
    {
        $totalLink = ceil($this->total / $this->size);

        $linkFirst = $this->generateLink(1, $this->size);
        echo $this->getListItem($linkFirst, $this->firstPage);

        $linkPrev = $this->generateLink($this->page, $this->size);
        echo $this->getListItem($linkPrev, $this->prev);

        for ($i = 0; $i < $totalLink; ) {

            $active = ($this->page == $i);
            $link = $this->generateLink($i, $this->size);
            echo $this->getListItem($link, ++$i, $active);
        }

        $nextPage = $this->generateLink($this->page, $this->size);
        echo $this->getListItem($nextPage, $this->next);

        $lastPage = $this->generateLink($totalLink, $this->size);
        echo $this->getListItem($lastPage, $this->lastPage);
    }

    public function print()
    {
        $this->printList();
    }

    /**
     *
     */
    public function __toString(): string
    {

        return sprintf("Testing print pageTotal %d, page %d, size %d", $this->total, $this->page, $this->size);
    }


}