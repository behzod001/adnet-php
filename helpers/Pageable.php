<?php

namespace app\helpers;

class Pageable
{
    private array $rows;
    private int $total;

    /**
     * @param object $rows
     * @param int $total
     */
    public function __construct(array $rows, int $total)
    {
        $this->rows = $rows;
        $this->total = $total;
    }

    /**
     * @return array|object
     */
    public function getRows()
    {
        return $this->rows;
    }

    /**
     * @param array|object $rows
     */
    public function setRows($rows): void
    {
        $this->rows = $rows;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal(int $total): void
    {
        $this->total = $total;
    }


}