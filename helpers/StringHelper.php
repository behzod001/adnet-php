<?php

namespace helpers;

class StringHelper
{
    public static function underscoreToCamelCase($string, $capitalizeFirstCharacter = false): string
    {
        return ucfirst(strtolower(str_replace('_', ' ', $string)));
    }

}