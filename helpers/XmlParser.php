<?php

namespace helpers;

use Aimeos\Map;
use SimpleXMLElement;

class XmlParser
{
    private SimpleXMLElement $xmlData;
    private string $jsonStringContent;
    private bool $bOptimize;
    private Map $headerRows;
    private Map $rowColumnsData;
    private string $reportName;
    private string $dateRange;

    /**
     * @return string
     */
    public function getReportName(): string
    {
        return $this->reportName;
    }

    /**
     * @param string $reportName
     */
    public function setReportName(string $reportName): void
    {
        $this->reportName = $reportName;
    }

    /**
     * @return string
     */
    public function getDateRange(): string
    {
        return $this->dateRange;
    }

    /**
     * @param string $dateRange
     */
    public function setDateRange(string $dateRange): void
    {
        $this->dateRange = $dateRange;
    }

    /**
     * @depends  \Aimeos\Map
     * create empty $headerRows and $rowColumnsData
     */
    public function __construct()
    {
        $this->headerRows = new Map();
        $this->rowColumnsData = new Map();
    }

    /**
     * @return SimpleXMLElement
     */
    public function getXmlData(): SimpleXMLElement
    {
        return $this->xmlData;
    }

    /**
     * @return Map
     */
    public function getRowColumnsData(): Map
    {
        return $this->rowColumnsData;
    }


    /**
     * @param $xmlData
     * @return array
     */
    public function parse($xmlData): array
    {
        $this->xmlData = $xmlData;
        $tableRows = $this->xmlData->xpath("table")[0];
        return array_merge($tableRows->xpath("row"), $tableRows->xpath("summary-row"));
    }

    public function getTablesJsonParsedContent()
    {
        $json = json_decode($this->jsonStringContent);
        $this->setReportName($json->{"report-name"});
        $this->setDateRange($json->{"date-range"});
        return $json->{"table"};

    }

    public function parseTableRows(): void
    {
        $tables = $this->getTablesJsonParsedContent();
        $rowIndex = 0;
        foreach ($tables->row as $tableRowItem => $cellItems) {
            $rowItem = $cellItems->cell;
            $rowItemMap = new Map();
            for ($j = 0; $j < count($rowItem); $j++) {
                $key = $rowItem[$j]->key;
                $value = $rowItem[$j]->value;
                if (is_object($value))
                    continue;
                else {
                    // generate keys hash value
                    $keyHash = md5($key);

                    //if not exists add to headersRowMap by hash value key
                    $this->setHeaderRows($keyHash, $key);

                    // if exists then add value to items map by hash value
                    $rowItemMap->set($keyHash, $value);
                }
            }
            $this->rowColumnsData->set($rowIndex++, $rowItemMap);
        }
        unset($rowItemMap, $key, $value, $tables);
    }

    /**
     * @return Map
     */
    public function getHeaderRows(): Map
    {
        return $this->headerRows;
    }


    /**
     * @param string $headerRowsHashKey
     * @param string $headerRowsValue
     */
    public function setHeaderRows(string $headerRowsHashKey, string $headerRowsValue): void
    {
        // check exists keyword in headersRowMap by hash value key
        if (!$this->headerRows->has($headerRowsHashKey))
            //if not exists add to headersRowMap by hash value key
            $this->headerRows->set($headerRowsHashKey, $headerRowsValue);
    }

    /**
     * @param String $xmlFilePath
     * @example C:\OpenServer\domains\adnets/uploads/g_reports/b4c59c2f90f13423eabac12a2ebe0529.xml
     */
    public function parseFileContent(string $xmlFilePath): XmlParser
    {
        $xml = simplexml_load_file($xmlFilePath);
        $this->parseContent($xml);
        $this->parseTableRows();
        return $this;
    }

    public function parseContent(SimpleXMLElement $xml)
    {
        $this->jsonStringContent = json_encode($xml, JSON_UNESCAPED_UNICODE);
        return $this->jsonStringContent;
    }

}