<?php

use bootstrap\Application;
use FacebookAds\Api;
use FacebookAds\Logger\CurlLogger;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
$config = require 'config/web.php';

define("ROOT_PATH", dirname(__FILE__));
define('CONFIG', $config);
require ROOT_PATH . '/vendor/autoload.php';

$app = new Application();
$app->run();


