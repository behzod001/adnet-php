<?php


namespace models;


use PDO;

class Account extends Audit
{
    const SERVICE_ID_FACEBOOK = 1;
    const SERVICE_ID_YANDEX = 2;
    const SERVICE_ID_GOOGLE = 3;

    const SERVICE_ID = 'service_id';
    const USER_ID = 'user_id';
    const ACCOUNT_ID = 'account_id';

    public function __construct()
    {
        $this->errors = [
            self::ACCOUNT_ID => '',
            self::SERVICE_ID => '',
            self::USER_ID => ''
        ];
    }

    public function requiredFields(): array
    {
        return [
            self::SERVICE_ID,
            self::USER_ID,
            self::ACCOUNT_ID
        ];
    }

    public function validate(): bool
    {
        global $app;
        $valid = true;
        $account_id = $this->fields[self::ACCOUNT_ID];
        $stmt = $app->getDb()->prepare("SELECT * from `account` where `account_id`= :account_id;");
        $stmt->bindValue(":account_id", $account_id);
        $stmt->execute();
        $exists = $stmt->fetch(PDO::FETCH_OBJ);
        if (is_null($exists)) {
            $this->errors[self::ACCOUNT_ID] = "Account id already registered";
            $valid = false;
        }

        return $valid;
    }

    public function create(): bool
    {
        global $app;
        $time = time();
        $stmt = $app->getDb()->prepare("INSERT INTO `accounts` (`user_id`, `created_at`, `updated_at`, `account_id`, `service_id`) VALUES (?,?,?,?,?)");
        $stmt->bindParam(1, $this->fields[self::USER_ID]);
        $stmt->bindParam(2, $time);
        $stmt->bindParam(3, $time);
        $stmt->bindParam(4, $this->fields[self::ACCOUNT_ID]);
        $stmt->bindParam(5, $this->fields[self::SERVICE_ID]);
        return $stmt->execute();
    }
}