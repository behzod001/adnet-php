<?php

namespace models;

use http\Exception\InvalidArgumentException;

class Audit
{
    public array $errors;
    public array $fields;

    const CREATED_AT = "created_at";
    const UPDATED_AT = "updated_at";

    /**
     * @return bool
     */
    public function hasError(): bool
    {
        foreach ($this->errors as $error) {
            if ($error != "") {
                return true;
            }
        }
        return false;
    }

    /**
     * @return array|string[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param array
     * @see SignUp
     *
     * load all fields values from $_POST[SignUp]
     *
     */
    public function load($modelData)
    {
        if (count($this->requiredFields())) {
            foreach ($this->requiredFields() as $requiredField) {
                if (in_array($requiredField, $modelData)) {
                    throw new InvalidArgumentException("{$requiredField} is required. Cannot be a blank", 409);
                }
            }
        }
        foreach ($modelData as $key => $value) {
            $this->fields[$key] = trim($value);
        }
    }

    public function requiredFields(): array
    {
        return [];
    }
}