<?php

namespace app\models;

class CampaignItem
{
    private String $name;
    private String $currency;
    private String $balance;
    private String $type_budget;
    private String $status;
    private String $type;
    private String $reach;
    private String $average;
    private String $cost;
    private String $strategy;

    /**
     * @param String $name
     * @param String $currency
     * @param String $balance
     * @param String $type_budget
     * @param String $status
     * @param String $type
     * @param String $reach
     * @param String $average
     * @param String $cost
     * @param String $strategy
     */
    public function __construct(string $name, string $currency, string $balance, string $type_budget, string $status, string $type, string $reach, string $average, string $cost, string $strategy)
    {
        $this->name = $name;
        $this->currency = $currency;
        $this->balance = $balance;
        $this->type_budget = $type_budget;
        $this->status = $status;
        $this->type = $type;
        $this->reach = $reach;
        $this->average = $average;
        $this->cost = $cost;
        $this->strategy = $strategy;
    }

    /**
     * @return String
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param String $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return String
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param String $currency
     */
    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return String
     */
    public function getBalance(): string
    {
        return $this->balance;
    }

    /**
     * @param String $balance
     */
    public function setBalance(string $balance): void
    {
        $this->balance = $balance;
    }

    /**
     * @return String
     */
    public function getTypeBudget(): string
    {
        return $this->type_budget;
    }

    /**
     * @param String $type_budget
     */
    public function setTypeBudget(string $type_budget): void
    {
        $this->type_budget = $type_budget;
    }

    /**
     * @return String
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param String $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return String
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param String $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return String
     */
    public function getReach(): string
    {
        return $this->reach;
    }

    /**
     * @param String $reach
     */
    public function setReach(string $reach): void
    {
        $this->reach = $reach;
    }

    /**
     * @return String
     */
    public function getAverage(): string
    {
        return $this->average;
    }

    /**
     * @param String $average
     */
    public function setAverage(string $average): void
    {
        $this->average = $average;
    }

    /**
     * @return String
     */
    public function getCost(): string
    {
        return $this->cost;
    }

    /**
     * @param String $cost
     */
    public function setCost(string $cost): void
    {
        $this->cost = $cost;
    }

    /**
     * @return String
     */
    public function getStrategy(): string
    {
        return $this->strategy;
    }

    /**
     * @param String $strategy
     */
    public function setStrategy(string $strategy): void
    {
        $this->strategy = $strategy;
    }

    public function __toString()
    {
        return "CampaignItem";
    }


}