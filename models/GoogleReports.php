<?php

namespace app\models;

use app\helpers\Pageable;
use models\Audit;
use PDO;

class GoogleReports extends Audit
{
    const FILE_NAME = "name";
    const FILE = "file";
    const USER = "user_id";
    const TYPE = "type";

    public static function findAll()
    {
        global $app;
        $stmt = $app->getDb()->prepare("SELECT * FROM `google_stats`");
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }


    public static function findAllPageable(int $page, int $size, int $type): Pageable
    {
        global $app;
        $stmt = $app->getDb()
            ->prepare("SELECT * FROM `google_stats` where  `type` = :type order by id desc LIMIT :limit OFFSET :offset");
        $stmt->bindValue(':type', (int)$type, PDO::PARAM_INT);
        $stmt->bindValue(':limit', (int)$size, PDO::PARAM_INT);
        $stmt->bindValue(':offset', (int)$page * $size, PDO::PARAM_INT);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $total = $app->getDb()
            ->query("SELECT count(`id`) as sum  FROM `google_stats` where `type` = {$type}")->fetch();
        return new Pageable($result, $total['sum']);
    }

    public function requiredFields(): array
    {
        return [
            self::FILE, self::FILE_NAME, self::CREATED_AT, self::UPDATED_AT, self::TYPE
        ];
    }

    /**
     * @return array|bool
     */
    public function create(): bool
    {
        global $app;
        $time = time();
        $res = $app->getDb()
            ->prepare("INSERT INTO `google_stats` (`name`,`file`, `updated_at`, `created_at`,`user_id`,`type`) VALUES ( ?,?,?,?,?,?);")
            ->execute([
                $this->fields[self::FILE_NAME],
                $this->fields[self::FILE],
                $time,
                $time,
                $this->fields[self::USER],
                $this->fields[self::TYPE]
            ]);
        return $res;
    }

    public function validate(): bool
    {
        return true;
    }

    public function findById($reportId)
    {
        global $app;
        $stmt = $app->getDb()->prepare("SELECT * FROM `google_stats` WHERE `id`= ? ");
        $stmt->execute([$reportId]);
        $result = $stmt->fetchAll(PDO::FETCH_CLASS);
        return $result[0];
    }

    public function deleteById($reportId): bool
    {
        global $app;
        return $app->getDb()->prepare("DELETE FROM `google_stats` WHERE `id`= ? ")
            ->execute([$reportId]);
    }

    public function findLastByType(int $getTypesValue)
    {
        global $app;
        $stmt = $app->getDb()
            ->prepare("SELECT * FROM `google_stats` where  `type` = :type order by id desc LIMIT :limit ");
        $stmt->bindValue(':type', (int)$getTypesValue, PDO::PARAM_INT);
        $stmt->bindValue(':limit', 1, PDO::PARAM_INT);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $result[0];
    }

}