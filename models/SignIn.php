<?php


namespace models;


class SignIn extends Audit
{
    public array $errors;
    public array $fields;
    const LOGIN = 'login';
    const PASSWORD = 'password';

    public function __construct()
    {
        $this->errors = [
            self::LOGIN => '',
            self::PASSWORD => ''
        ];
    }

    /**
     * @param $hash
     * @return bool
     */
    public function verifyPassword($hash): bool
    {
        return password_verify($this->fields[self::PASSWORD], $hash);
    }


    /**
     * @return bool
     */
    public function validate(): bool
    {
        global $app;
        $valid = true;
        if (empty($this->fields[self::LOGIN])) {
            $this->errors[self::LOGIN] = "Login can not be blank!";
            $valid = false;
        }
        if (empty($this->fields[self::PASSWORD])) {
            $this->errors[self::PASSWORD] = "Password can not be blank!";
            $valid = false;
        } else {
            $stmt = $app->getDb()->prepare("SELECT * from `users` where `login`=?;");
            $stmt->bindParam(1, $this->fields[SignUp::LOGIN]);
            $stmt->execute();
            $resObj = $stmt->fetchObject();
            if (is_null($resObj)) {
                $this->errors[self::LOGIN] = "Username or password invalid";
                $valid = false;
            } else {
                if (!$this->verifyPassword($resObj->password)) {
                    $this->errors[self::LOGIN] = "Username or password invalid";
                    $valid = false;
                } else {
                    $_SESSION['identity'] = null;
                    $_SESSION['identity'] = $resObj;
                }
            }
        }


        return $valid;
    }
}