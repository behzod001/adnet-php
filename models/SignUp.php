<?php

namespace models;

class SignUp extends Audit
{
    public array $errors;
    public array $fields;
    const USER_NAME = "username";
    const LOGIN = "login";
    const PASSWORD = "password";
    const CONFIRM = "confirm";

    /**
     * SignUp constructor.
     */
    public function __construct()
    {
        $this->errors = [
            self::USER_NAME => '',
            self::LOGIN => '',
            self::PASSWORD => '',
            self::CONFIRM => '',
        ];
    }


    /**
     * @return array
     */
    public function getFields(): array
    {
        $this->fields = [
            self::USER_NAME,
            self::LOGIN,
            self::PASSWORD,
            self::CONFIRM
        ];
        return $this->fields;
    }

    /**
     * @return bool
     */
    public function validate(): bool
    {
        global $app;
        $valid = true;
        $stmt = $app->getDb()->prepare("Select * from `users` where `login`=?;");
        $stmt->bindParam(1, $this->fields[SignUp::LOGIN]);
        $stmt->execute();
        $resObj=$stmt->fetchObject();
        if(is_null($resObj)){
            $this->errors[self::LOGIN] = "Login already exists";
            $valid = false;
        }
        if (empty($this->fields[self::USER_NAME])) {
            $this->errors[self::USER_NAME] = "Username can not be blank!";
            $valid = false;
        }
        if (empty($this->fields[self::PASSWORD])) {
            $this->errors[self::PASSWORD] = "Password can not be blank!";
            $valid = false;
        }
        if (empty($this->fields[self::LOGIN])) {
            $this->errors[self::LOGIN] = "Login can not be blank!";
            $valid = false;
        }
        if (empty($this->fields[self::CONFIRM])) {
            $this->errors[self::CONFIRM] = "Confirm can not be blank!";
            $valid = false;
        }
        if ($this->fields[self::CONFIRM] !== $this->fields[self::PASSWORD]) {
            $this->errors[self::CONFIRM] = "Password and confirm be equal";
            $valid = false;
        }
        return $valid;
    }

    /**
     * @param array
     * @see SignUp
     *
     * load all fields values from $_POST[SignUp]
     *
     */
    public function load($SignUp)
    {
        foreach ($SignUp as $key => $value) {
            $this->fields[$key] = trim($value);
        }
    }

    public function setPassword()
    {
        $this->fields[self::PASSWORD] = password_hash($this->fields[self::PASSWORD],PASSWORD_BCRYPT,['cost'=>12]);
    }

}