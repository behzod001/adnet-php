<?php


use FacebookAds\Api;
use FacebookAds\Logger\CurlLogger;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\User;


if(!isset($_SESSION['user'])) {
    Api::init(CONFIG['app_id'], CONFIG['app_secret'], CONFIG['access_token']);
    $api = Api::instance();
    $api->setLogger(new CurlLogger());

//read all accounts
    $user = new User(CONFIG['account_id']);
//$user->getAdAccounts(array(AdAccountFields::ID));

    $accounts = $user->getAdAccounts(array(
        AdAccountFields::ID,
        AdAccountFields::NAME,
    ));

// Print out the accounts
    foreach ($accounts as $account) {
        $_SESSION['user'][AdAccountFields::ID] = $account->{AdAccountFields::ID};
        $_SESSION['user'][AdAccountFields::NAME] = $account->{AdAccountFields::NAME};
        break;
    }
}
?>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="/">Facebook Ads Api </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText"
                aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="/home">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/accounts">Accounts</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/campaigns">Campaigns</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/adimages">Ad Images</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/adcreative">Ad Creatives</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/adsets">Ad Sets</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/ads">Ads</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/targets">Targets</a>
                </li>
            </ul>
            <span class="navbar-text">
                <?= $_SESSION['user'][AdAccountFields::NAME] ?? ""; ?>
                <?= $_SESSION['user'][AdAccountFields::ID] ?? ""; ?>
            </span>

        </div>
    </div>
</nav>
