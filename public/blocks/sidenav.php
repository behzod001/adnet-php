<?php


use FacebookAds\Api;
use FacebookAds\Logger\CurlLogger;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\User;

//
//
//if (!isset($_SESSION['user'])) {
//
//
//    Api::init(CONFIG['fbConfig']['app_id'], CONFIG['fbConfig']['app_secret'], CONFIG['fbConfig']['access_token']);
//    $api = Api::instance();
//    $api->setLogger(new CurlLogger());
//
////read all accounts
//    $user = new User(CONFIG['fbConfig']['account_id']);
////$user->getAdAccounts(array(AdAccountFields::ID));
//
//    $accounts = $user->getAdAccounts(array(
//        AdAccountFields::ID,
//        AdAccountFields::NAME,
//    ));
//
//// Print out the accounts
//    foreach ($accounts as $account) {
//        $_SESSION['user'][AdAccountFields::ID] = $account->{AdAccountFields::ID};
//        $_SESSION['user'][AdAccountFields::NAME] = $account->{AdAccountFields::NAME};
//        break;
//    }
//}

global $app;
$url = $app->url;
$accountSelected = false;
if (isset($_SESSION['FACEBOOK']) || isset($_SESSION['YANDEX'])) $accountSelected = true;
?>
<!-- BEGIN: Side Menu -->
<nav class="side-nav">
    <a href="" class="intro-x flex items-center pl-5 pt-4">
        <img alt="AdNet" class="w-6" src="/resources/dist/images/logo.svg">
        <span class="hidden xl:block text-white text-lg ml-3"> Ad<span class="font-medium">Net</span> </span>
    </a>
    <div class="side-nav__devider my-6"></div>
    <ul>
        <li>
            <a href="/home" class="side-menu <?= (in_array("home", $url)) ? "side-menu--active" : "" ?>">
                <div class="side-menu__icon"><i data-feather="home"></i></div>
                <div class="side-menu__title"> Dashboard</div>
            </a>
        </li>
        <li>
            <a href="/google/types"
               class="side-menu <?= (in_array("report", $url)) ? "side-menu--active" : "" ?>">
                <div class="side-menu__icon"><i data-feather="inbox"></i></div>
                <div class="side-menu__title"> Google Report</div>
            </a>
        </li>
        <li>
            <a href="/accounts"
               class="side-menu <?= (in_array("accounts", $url)) ? "side-menu--active" : "" ?>">
                <div class="side-menu__icon"><i data-feather="inbox"></i></div>
                <div class="side-menu__title"> Accounts</div>
            </a>
        </li>
        <li>
            <a href="<?= ($accountSelected) ? "/campaigns" : "javascript: " ?>"
                <?= ($accountSelected) ? "" : ' data-toggle="modal" data-target="#select-account"'; ?>
               class="side-menu <?= (in_array("campaigns", $url)) ? " side-menu--active " : "" ?> ">
                <div class="side-menu__icon"><i data-feather="hard-drive"></i></div>
                <div class="side-menu__title <?= ($accountSelected) ? "" : " text-gray-600" ?>">Campaigns</div>
            </a>
        </li>
        <li>
            <a  href="<?= ($accountSelected) ? "/adsets" : "javascript: " ?>"
                <?= ($accountSelected) ? "" : ' data-toggle="modal" data-target="#select-account"'; ?>
               class="side-menu <?= (in_array("adsets", $url) || in_array("direct/adset", $url)) ? "side-menu--active" : "" ?>">
                <div class="side-menu__icon"><i data-feather="credit-card"></i></div>
                <div class="side-menu__title  <?= ($accountSelected) ? "" : " text-gray-600" ?>"> Ad groups</div>
            </a>
        </li>
        <li>
            <a href="<?= ($accountSelected) ? "/creatives" : "javascript: " ?>"
               <?= ($accountSelected) ? "" : ' data-toggle="modal" data-target="#select-account"'; ?>
               class="side-menu <?= (in_array("creatives", $url)) ? "side-menu--active" : "" ?>">
                <div class="side-menu__icon"><i data-feather="message-square"></i></div>
                <div class="side-menu__title  <?= ($accountSelected) ? "" : " text-gray-600" ?>"> Creatives</div>
            </a>
        </li>
        <li>
            <a  href="<?= ($accountSelected) ? "/adimages" : "javascript: " ?>"
               <?= ($accountSelected) ? "" : ' data-toggle="modal" data-target="#select-account"'; ?>
               class="side-menu <?= (in_array("adimages", $url)) ? "side-menu--active" : "" ?>">
                <div class="side-menu__icon"><i data-feather="file-text"></i></div>
                <div class="side-menu__title  <?= ($accountSelected) ? "" : " text-gray-600" ?>"> Ad Images</div>
            </a>
        </li>
        <li>
            <a  href="<?= ($accountSelected) ? "/ads" : "javascript: " ?>"
                <?= ($accountSelected) ? "" : ' data-toggle="modal" data-target="#select-account"'; ?>
               class="side-menu <?= (in_array("ads", $url) || in_array("direct/ads", $url)) ? "side-menu--active" : "" ?>">
                <div class="side-menu__icon"><i data-feather="edit"></i></div>
                <div class="side-menu__title  <?= ($accountSelected) ? "" : " text-gray-600" ?>"> Ads</div>
            </a>
        </li>
        <li class="side-nav__devider my-6"></li>
        <li>
            <a href="/home/start" class="side-menu <?= (in_array("/start", $url)) ? "side-menu--active" : "" ?>">
                <div class="side-menu__icon"><i data-feather="file-text"></i></div>
                <div class="side-menu__title"> Гид пользователя</div>
            </a>
        </li>
        <li>
            <a href="/home/videos" class="side-menu <?= (in_array("/videos", $url)) ? "side-menu--active" : "" ?>">
                <div class="side-menu__icon"><i data-feather="file-text"></i></div>
                <div class="side-menu__title"> Видео уроки</div>
            </a>
        </li>
        <li>
            <a href="/home/accounts" class="side-menu <?= (in_array("/accounts", $url)) ? "side-menu--active" : "" ?>">
                <div class="side-menu__icon"><i data-feather="file-text"></i></div>
                <div class="side-menu__title"> Как создаётся компания ?</div>
            </a>
        </li>
    </ul>
</nav>
<!-- END: Side Menu -->