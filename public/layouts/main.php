<?php
ob_start();

?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN: Head -->
<head>
    <meta charset="utf-8">
    <link href="/resources/dist/images/logo.svg" rel="shortcut icon">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description"
          content="Midone admin is super flexible, powerful, clean & modern responsive tailwind admin template with unlimited possibilities.">
    <meta name="keywords"
          content="admin template, Midone admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="LEFT4CODE">
    <title>Dashboard - AdNet - Admin Panel</title>
    <!-- BEGIN: CSS Assets-->
    <link rel="stylesheet" href="/resources/dist/css/app.css"/>
    <!-- END: CSS Assets-->
</head>
<?php

global $app;
if (isset($_GET['account'])) {

    if ($_GET['service'] == 'fb') $_SESSION['FACEBOOK']['account_id'] = $_GET['account'];
    if ($_GET['service'] == 'ya') $_SESSION['YANDEX']['client_login'] = $_GET['account'];
}

if (!isset($_SESSION['identity'])) {
    header("Location: /sign/in", true, 200);
}


?>
<!-- END: Head -->
<body class="app">
<div id="fb-root"></div>
<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
        crossorigin="anonymous"></script>

<?php if (isset($_GET['unset'])) {

    if ($_GET['service'] == 'fb') unset($_SESSION['FACEBOOK']);
    if ($_GET['service'] == 'ya') unset($_SESSION['YANDEX']);
}
require_once dirname(__DIR__) . "/blocks/mobile.php" ?>
<!-- Mobile nav -->
<div class="flex">
    <!--    Sidenav     -->
    <?php require_once dirname(__DIR__) . "/blocks/sidenav.php" ?>
    <!-- BEGIN: Content -->
    <div class="content">
        <!-- BEGIN: Content -->
        <!-- BEGIN: Top Bar -->
        <?php require_once dirname(__DIR__) . "/blocks/nav.php" ?>
        <!-- END: Top Bar -->
        <?php
        require_once ROOT_PATH . '/public/views/' . $app->url['folder'] . $app->url['page'] . '.php'; ?>
    </div>
</div>
<!-- BEGIN: Google not working Modal -->
<div class="modal" id="add-google-account">
    <div class="modal__content">
        <div class="p-5 text-center">
            <i data-feather="x-circle" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i>
            <div class="text-3xl mt-5">Идёт разработка</div>
            <div class="text-gray-600 mt-2">
                К сожалению сейчас идёт разработка интеграция с Гуглом. Скоро вы можете работать с Гуглом
            </div>
        </div>
        <div class="px-5 pb-8 text-center">
            <button type="button" data-dismiss="modal" class="button w-24 border text-gray-700 mr-1">Ok</button>
        </div>
    </div>
</div>
<!-- END: Google not working Modal -->

<!-- BEGIN: Select account  Modal -->
<div class="modal" id="select-account">
    <div class="modal__content">
        <div class="p-5 text-center">
            <i data-feather="x-circle" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i>
            <div class="text-3xl mt-5"> Аккаунт не выбран</div>
            <div class="text-gray-600 mt-2">
                Сначала выберите аккаунт для просмотр детальный информация
            </div>
        </div>
        <div class="px-5 pb-8 text-center">
            <a href="/accounts" class="button w-auto border bg-theme-1 text-white  mr-1"> Выбрать аккаунт </a>
            <button type="button" data-dismiss="modal" class="button w-auto border bg-theme-6 text-white mr-1">Закрыт
                модаль
            </button>
        </div>
    </div>
</div>
<!-- END: Select account Modal -->

<!-- BEGIN: JS Assets-->

<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=['your-google-map-api']&libraries=places"></script>
<script src="/resources/dist/js/app.js"></script>
<script>
    var confirmDelete = function (element) {
        console.log(element.getAttribute('data-delete'));
        document.getElementById("deleteBtn").setAttribute('href', element.getAttribute('data-delete'));
    }
</script>
<!-- END: JS Assets-->
</body>
</html>