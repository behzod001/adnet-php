<!DOCTYPE html>
<html lang="en">
<!-- BEGIN: Head -->
<head>
    <meta charset="utf-8">
    <link href="/resources/dist/images/logo.svg" rel="shortcut icon">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description"
          content="AdNet admin is super flexible, powerful, clean & modern responsive tailwind admin template with unlimited possibilities.">
    <meta name="keywords"
          content="admin template, Midone admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="LEFT4CODE">
    <title>Dashboard - AdNet - Admin Panel</title>
    <!-- BEGIN: CSS Assets-->
    <link rel="stylesheet" href="/resources/dist/css/app.css"/>
    <!-- END: CSS Assets-->
</head>
<!-- END: Head -->
<body class="login">
<?php

global $app;
require ROOT_PATH . '/public/views/' . $app->url['folder'] . $app->url['page'] . '.php'; ?>
<!-- BEGIN: JS Assets-->
<script src="/resources/dist/js/app.js"></script>
<!-- END: JS Assets-->
</body>
</html>