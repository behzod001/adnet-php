<?php

use helpers\Alert;
use models\Account;

$model = new Account();

global $app;

if (isset($_POST['Account'])) {
    $post[Account::USER_ID] = $_SESSION['identity']->id;
    $post[Account::ACCOUNT_ID] = $_POST['Account']['account_id'];
    $post[Account::SERVICE_ID] = $_POST['Account']['service_id'];
    $model->load($post);

    if ($model->validate() && $model->create()) {
        Alert::success(' Service account successfully added to user account ');
    } else {
        Alert::error(' Something went wrong. Service account not added.');
    }
}
?>


<div class="intro-y flex items-center mt-8">
    <h2 class="text-lg font-medium mr-auto">
        Add new service account
    </h2>
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
        <div class="hidden md:block mx-auto text-gray-600"></div>
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <a href="/accounts" class="button text-white bg-theme-1 shadow-md mr-2">Go back</a>
        </div>
    </div>
</div>
<div class="grid grid-cols-12 gap-6 mt-5 box">

    <div class="intro-y col-span-12 lg:col-span-6 ">
        <div class="intro-y  p-5">
            <h2 class="text-lg font-medium mr-auto">if you already have an account fill out the form</h2>
            <form action="/accounts/create" method="post" class="mt-5">
                <div class="mb-3">
                    <label for="<?= Account::SERVICE_ID ?>"> Account service select one</label>
                    <select id="<?= Account::SERVICE_ID ?>" name="Account[<?= Account::SERVICE_ID ?>]"
                            class="select2 w-full"
                            aria-label="Default select example">
                        <option disabled selected>Account service select one</option>
                        <option value="<?= Account::SERVICE_ID_FACEBOOK ?>">Facebook account</option>
                        <option disabled value="<?= Account::SERVICE_ID_GOOGLE ?>">Google account</option>
                        <option value="<?= Account::SERVICE_ID_YANDEX ?>">Yandex account</option>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="<?= Account::SERVICE_ID ?>">Account id</label>
                    <input type="text" class="input w-full border mt-2" name="Account[<?= Account::ACCOUNT_ID ?>]"
                           id="<?= Account::ACCOUNT_ID ?>">
                </div>
                <div class="text-right mt-5">
                    <button type="reset" class="button w-24 border text-gray-700 mr-1">Cancel</button>
                    <button type="submit" class="button w-24 bg-theme-1 text-white">Save</button>
                </div>
            </form>
        </div>
    </div>
    <div class="intro-y col-span-12 lg:col-span-6 ">
        <div class="intro-y  p-5">
            <div class="mb-3">
                <a class="flex items-center text-theme-6" href="javascript:" data-toggle="modal"
                   data-target="#add-google-account">
                    Create new Ads account from GOOGLE
                </a>


            </div>
            <div class="mb-3">
                Create new Ads account from <a target="_blank" class="text-theme-1"
                                               href="https://passport.yandex.ru/auth?retpath=%2F%2Fyandex.ru%2Fsupport%2Fdirect%2Falternative-interfaces%2Fapi.html">
                    YANDEX DIRECT </a>
            </div>
            <div class="mb-3">
                Create new Ads account from <a target="_blank" class="text-theme-1"
                                               href="https://www.facebook.com/business/tools/ads-manager"> FACEBOOK
                    MARKETING </a>
            </div>
        </div>
    </div>
</div>

