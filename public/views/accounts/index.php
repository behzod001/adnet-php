<?php

use app\exceptions\DataNotFoundException;
use directapi\common\enum\clients\ClientFieldEnum;
use directapi\DirectApiService;
use directapi\services\agencyclients\criterias\AgencyClientsSelectionCriteria;
use FacebookAds\Http\Exception\AuthorizationException;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\User;
use GuzzleHttp\Exception\GuzzleException;
use helpers\Alert;
use models\Account;

global $app;


$results = $app->getLocalAccounts();
$accounts = [];

if (isset($results))
    foreach ($results as $item) {

        if ($item[Account::SERVICE_ID] == Account::SERVICE_ID_FACEBOOK) {
            try {
                $fbAccount = new stdClass();
                $fbAccount->data = (new User($item['account_id']))
                    ->getAdAccounts([AdAccountFields::ID, AdAccountFields::ACCOUNT_ID, AdAccountFields::NAME, AdAccountFields::BALANCE])
                    ->getLastResponse()->getContent()['data'];

                $fbAccount->service_id = Account::SERVICE_ID_FACEBOOK;
                array_push($accounts, $fbAccount);

                unset($fbAccount);
            } catch (AuthorizationException $e) {
                Alert::error($e->getMessage());
            }
        }
        if ($item[Account::SERVICE_ID] == Account::SERVICE_ID_YANDEX) {
            $direct = new DirectApiService(CONFIG['direct']['token'], CONFIG['direct']['client_login'], CONFIG['direct']['query_logger'], CONFIG['direct']['logger'], CONFIG['direct']['user_sandbox']);
            //add  yandex direct accounts
            $fields = [
                ClientFieldEnum::ARCHIVED,
                ClientFieldEnum::LOGIN,
                ClientFieldEnum::ACCOUNT_QUALITY,
                ClientFieldEnum::CLIENT_ID,
                ClientFieldEnum::COUNTRY_ID,
                ClientFieldEnum::GRANTS,
                ClientFieldEnum::LOGIN,
                ClientFieldEnum::NOTIFICATION,
                ClientFieldEnum::PHONE,
                ClientFieldEnum::CLIENT_INFO
            ];

            try {
                $criteria = new AgencyClientsSelectionCriteria();
                $criteria->Logins = [$item[Account::ACCOUNT_ID]];
                $criteria->Archived = 'NO';
                $directAccount = $app->getDirectWithClient(Account::ACCOUNT_ID)->getAgencyClientsService()->get(
                    $criteria, $fields
                );
                $std = new stdClass();
                $idx = 0;
                while (count($directAccount) > $idx) {
                    $std = $directAccount[$idx++];
                    $std->service_id = Account::SERVICE_ID_YANDEX;
                    array_push($accounts, $std);
                }
                unset($directAccount);
            } catch (GuzzleException $e) {
                Alert::error("Something went wrong! <a href='/accounts' class='mx-1 text-theme-1'> Reload this page </a> ");
            } catch (DataNotFoundException $e) {
                Alert::warning(' У вас есть Yandex Direct аккаунт ? Тогда выберите ваш аккаунт. Если нет создайте и  <a href="/accounts" class="text-theme-1 mx-1"> регистрируйте. </a> ');
            }

        }
    }

?>

<h2 class="intro-y text-lg font-medium mt-10">
    User accounts
</h2>

<div class="grid grid-cols-12 gap-6 mt-5">

    <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
        <button class="button text-white bg-theme-1 shadow-md mr-2">Add New Service account</button>
        <div class="dropdown relative ">
            <button class="dropdown-toggle button px-2 box text-gray-700">
            <span class="w-5 h-5 flex items-center justify-center">
                <i class="w-4 h-4" data-feather="plus"></i>
            </span>
            </button>
            <div class="dropdown-box mt-10 absolute top-0 left-0 z-20" style="width: 250px">
                <div class="dropdown-box__content box p-2">
                    <a href="/accounts/create"
                       class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                        <i data-feather="facebook" class="w-4 h-4 mr-2"></i> Add existing Facebook/Yandex account </a>
                    <a href="/direct/accounts/add"
                       class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                        <i data-feather="shield" class="w-4 h-4 mr-2"></i> Create Yandex Direct ad </a>
                    <a href="javascript:" data-toggle="modal"
                       data-target="#add-google-account"
                       class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                        <i data-feather="map-pin" class="w-4 h-4 mr-2"></i>Google Ads account add </a>
                </div>
            </div>
        </div>
        <div class="hidden md:block mx-auto text-gray-600">Showing 1 to 10 of 150 entries</div>
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <div class="w-56 relative text-gray-700">
                <input type="text" class="input w-56 box pr-10 placeholder-theme-13" placeholder="Search...">
                <i class="w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0" data-feather="search"></i>
            </div>
        </div>
    </div>

    <!-- BEGIN: Users Layout -->
    <?php $idx = 0;

    foreach ($accounts as $account) { ?>
        <?php if ($account->service_id == Account::SERVICE_ID_YANDEX) { ?>
            <div class="intro-y col-span-12 md:col-span-6">
                <div class="box">
                    <div class="flex flex-col lg:flex-row items-center p-5 border-b border-gray-200">
                        <div class="w-24 h-24 lg:w-12 lg:h-12 image-fit lg:mr-1">
                            <img alt="Adnet" class="rounded-full" src="/resources/yandex-direct.png">
                        </div>
                        <div class="lg:ml-2 lg:mr-auto text-center lg:text-left mt-3 lg:mt-0">
                            <a href="" class="font-medium"><?= $account->{ClientFieldEnum::CLIENT_INFO} ?></a>
                            <div class="text-gray-600 text-xs">
                                <div class="text-left w-auto">
                                    <span class="bg-theme-17 text-theme-11 rounded px-2 mt-1 text-center">Yandex direct</span>
                                    id <?= $account->{ClientFieldEnum::LOGIN} ?>
                                </div>
                            </div>
                        </div>
                        <div class="flex -ml-2 lg:ml-0 lg:justify-end mt-3 lg:mt-0">
                            <a href=""
                               class="w-8 h-8 rounded-full flex items-center justify-center border ml-2 text-gray-500 zoom-in tooltip"
                               title="Yandex Direct">
                                <i class="w-3 h-3 fill-current" data-feather="shield"></i>
                            </a>
                        </div>
                    </div>
                    <div class="flex flex-wrap lg:flex-no-wrap items-center justify-center p-5">
                        <div class="w-full lg:w-1/2 mb-4 lg:mb-0 mr-auto">
                            <div class="flex">
                                <div class="text-gray-600 text-xs mr-auto">Progress</div>
                                <div class="text-xs font-medium"><?= rand(1, 100) ?></div>
                            </div>
                            <div class="w-full h-1 mt-2 bg-gray-400 rounded-full">
                                <div class="w-1/4 h-full bg-theme-1 rounded-full"></div>
                            </div>
                        </div>
                        <?php if (isset($_SESSION['YANDEX']['client_login']) && $_SESSION['YANDEX']['client_login'] == $account->{ClientFieldEnum::LOGIN}) { ?>
                            <a href="/accounts/index?service=ya&unset=<?= $account->{ClientFieldEnum::LOGIN} ?>"
                               class="button button--sm text-white bg-theme-1 mx-2"> Unset default account </a>
                        <?php } else { ?>
                            <a href="/accounts/index?service=ya&account=<?= $account->{ClientFieldEnum::LOGIN} ?>"
                               class="button button--sm text-gray-700 border border-gray-300 mx-2"> Set default </a>
                        <?php } ?>

                        <a href="/campaigns?service=ya&account=<?= $account->{ClientFieldEnum::LOGIN} ?>"
                           class="button button--sm text-white bg-theme-1 mx-2"> Campaigns </a>
                        <a class="button button--sm text-gray-700 border border-gray-300 " href="javascript:"
                           data-toggle="modal"
                           data-target="#add-yandex-account"> Profile</a>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php
        if ($account->service_id == Account::SERVICE_ID_FACEBOOK) {
            foreach ($account->data as $act) { ?>
                <div class="intro-y col-span-12 md:col-span-6">
                    <div class="box">
                        <div class="flex flex-col lg:flex-row items-center p-5 border-b border-gray-200">
                            <div class="w-24 h-24 lg:w-12 lg:h-12 image-fit lg:mr-1">
                                <img alt="Adnet" class="rounded-full" src="/resources/facebook.png">
                            </div>
                            <div class="lg:ml-2 lg:mr-auto text-center lg:text-left mt-3 lg:mt-0">
                                <a href="" class="font-medium"><?= $act[AdAccountFields::NAME] ?></a>
                                <div class="text-gray-600 text-xs">
                                    <div class="text-left w-auto">
                                        <span class="bg-theme-14 text-theme-10 rounded px-2 mt-1 text-center">Facebook</span>
                                        id <?= $act[AdAccountFields::ACCOUNT_ID] ?>
                                    </div>
                                </div>
                            </div>
                            <div class="flex -ml-2 lg:ml-0 lg:justify-end mt-3 lg:mt-0">
                                <a href=""
                                   class="w-8 h-8 rounded-full flex items-center justify-center border ml-2 text-gray-500 zoom-in tooltip"
                                   title="Facebook">
                                    <i class="w-3 h-3 fill-current" data-feather="facebook"></i>
                                </a>
                            </div>
                        </div>
                        <div class="flex flex-wrap lg:flex-no-wrap items-center justify-center p-5">
                            <div class="w-full lg:w-1/2 mb-4 lg:mb-0 mr-auto">
                                <div class="flex">
                                    <div class="text-gray-600 text-xs mr-auto"> Balance </div>
                                    <div class="text-xs font-medium"> $ <?= $act[AdAccountFields::BALANCE]; ?></div>
                                </div>
                                <div class="w-full h-1 mt-2 bg-gray-400 rounded-full">
                                    <div class="w-2/4 h-full bg-theme-1 rounded-full"></div>
                                </div>
                            </div>
                            <?php if (isset($_SESSION['FACEBOOK']['account_id']) && $_SESSION['FACEBOOK']['account_id'] == 'act_' . $act[AdAccountFields::ACCOUNT_ID]) { ?>
                                <a href="/accounts/index?service=fb&unset=act_<?= $act[AdAccountFields::ACCOUNT_ID] ?>"
                                   class="button button--sm text-white bg-theme-1 mx-2">Unset default account</a>
                            <?php } else { ?>
                                <a href="/accounts/index?service=fb&account=act_<?= $act[AdAccountFields::ACCOUNT_ID] ?>"
                                   class="button button--sm text-gray-700 border border-gray-300 mx-2">Set default</a>
                            <?php } ?>

                            <a href="/campaigns?service=fb&account=act_<?= $act[AdAccountFields::ACCOUNT_ID] ?>"
                               class="button button--sm text-white bg-theme-1 mx-2"> Campaigns </a>
                            <!--                        <a href="/accounts/profile" class="button button--sm text-gray-700 border border-gray-300 ">-->
                            <!--                            Profile</a>-->
                        </div>
                    </div>
                </div>
            <?php } ?>
        <?php } ?>
    <?php } ?>
    <!-- END: Users Layout -->
    <!-- BEGIN: Pagination -->
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-row sm:flex-no-wrap items-center">
        <ul class="pagination">
            <li>
                <a class="pagination__link" href=""> <i class="w-4 h-4" data-feather="chevrons-left"></i> </a>
            </li>
            <li>
                <a class="pagination__link" href=""> <i class="w-4 h-4" data-feather="chevron-left"></i> </a>
            </li>
            <li><a class="pagination__link" href="">...</a></li>
            <li><a class="pagination__link pagination__link--active" href="">1</a></li>
            <li><a class="pagination__link " href="">2</a></li>
            <li><a class="pagination__link" href="">3</a></li>
            <li><a class="pagination__link" href="">...</a></li>
            <li>
                <a class="pagination__link" href=""> <i class="w-4 h-4" data-feather="chevron-right"></i> </a>
            </li>
            <li>
                <a class="pagination__link" href=""> <i class="w-4 h-4" data-feather="chevrons-right"></i> </a>
            </li>
        </ul>
        <select class="w-20 input box mt-3 sm:mt-0">
            <option>10</option>
            <option>25</option>
            <option>35</option>
            <option>50</option>
        </select>
    </div>
    <!-- END: Pagination -->
</div>


<!-- BEGIN: Delete Confirmation Modal -->
<div class="modal" id="add-yandex-account">
    <div class="modal__content">
        <div class="p-5 text-center">
            <i data-feather="x-circle" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i>
            <div class="text-3xl mt-5">Идёт разработка</div>
            <div class="text-gray-600 mt-2">
                К сожалению сейчас идёт разработка интеграция с Яндекс Директ . Скоро вы можете работать с этим методом.
            </div>
        </div>
        <div class="px-5 pb-8 text-center">
            <button type="button" data-dismiss="modal" class="button w-24 border text-gray-700 mr-1">Ok</button>
        </div>
    </div>
</div>
<!-- END: Delete Confirmation Modal -->