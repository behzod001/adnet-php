<?php


use directapi\common\enum\clients\ClientFieldEnum;
use helpers\Alert;

global $app;
$fields = [
    ClientFieldEnum::ARCHIVED,
    ClientFieldEnum::LOGIN,
    ClientFieldEnum::ACCOUNT_QUALITY,
    ClientFieldEnum::CLIENT_INFO
];

try {
    $ac = $app->getDirect()->getClientsService()->get($fields);
} catch (Exception $e) {
    Alert::error($e->getMessage());
}
