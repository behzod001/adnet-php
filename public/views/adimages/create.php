<?php


use directapi\common\containers\Base64Binary;
use directapi\services\adimages\enum\AdImageFieldEnum;
use directapi\services\adimages\models\AdImageAddItem;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\Fields\AdImageFields;
use helpers\Alert;

global $api, $app;

if (isset($_POST['AdImage'])) {

    if ($_POST['AdImage']['Target'] == 'Facebook') {
        $target_file = ROOT_PATH . "/uploads/" . basename($_FILES['AdImage']['name'][AdImageFields::URL]);
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

        $tmpFile = $_FILES['AdImage']['tmp_name'][AdImageFields::URL];

        move_uploaded_file($tmpFile, $target_file);
        $fields = [
            AdImageFields::ID,
            AdImageFields::HASH,
            AdImageFields::ACCOUNT_ID,
            AdImageFields::NAME,
            AdImageFields::FILENAME,
        ];
        $params = [
            AdImageFields::ACCOUNT_ID => $_SESSION['user'][AdAccountFields::ID],
            AdImageFields::FILENAME => $target_file,
            AdImageFields::NAME => $_POST['AdImage'][AdImageFields::NAME],
        ];
        $account = new AdAccount($_SESSION['user'][AdAccountFields::ID], null, $api);
        try {
            $image = $account->createAdImage($fields, $params);
            $hash = $image->exportAllData()['images'];
            $h = $hash[$_FILES['AdImage']['name']['url']]['hash'];

            Alert::success(' <i data-feather="check-square" class="w-6 h-6 mr-2"></i> AdImage successfully created. For view <a class="text-theme-1 mx-1" href="/adimages/view?hash=' . $h . '"> checkout this page </a>');
        } catch (Exception $e) {
            Alert::error('<i data-feather="alert-circle" class="w-6 h-6 mr-2"></i> ' . $e->getTraceAsString());
        }
    }

    if ($_POST['AdImage']['Target'] == "Direct") {
        $target_file = ROOT_PATH . "/uploads/" . basename($_FILES['AdImage']['name']['url']);
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
        $tmpFile = $_FILES['AdImage']['tmp_name']['url'];
        move_uploaded_file($tmpFile, $target_file);
        try {
            $adImageItem = new AdImageAddItem([
                "ImageData" => (new Base64Binary($target_file))->jsonSerialize(),
                AdImageFieldEnum::NAME => $_POST['AdImage'][AdImageFields::NAME],
            ]);

            $app->getDirect()->setClientLogin("testinglogin2");
            $image = $app->getDirect()->getAdImagesService()->add([$adImageItem]);
            if (!is_null($image[0]->Errors)) {
                $exceptionNotification = $image[0]->Errors[0];
                Alert::error('<i data-feather="check-square" class="w-6 h-6 mr-2"></i> ' . $exceptionNotification->Message . '  ' . $exceptionNotification->Details . '  <a href="/adimages/create">Back to create new</a>');
            } else Alert::success('<i data-feather="alert-circle" class="w-6 h-6 mr-2"></i> AdImage successfully created. For view <a href="/adimages/view?service=direct&hash=' . $image[0]->AdImageHash . '">checkout this page</a> ');
        } catch (Exception $e) {
            Alert::error(' <i data-feather="alert-circle" class="w-6 h-6 mr-2"></i> ' . $image[0]->Errors[0]->Message . '. ' . $image[0]->Errors[0]->Details . '  <a href="/adimages/create">Back to create new</a>');
        }
    }
}

?>
<div class="intro-y flex items-center mt-8">
    <h2 class="text-lg font-medium mr-auto">
        Create new Ad Image
    </h2>
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
        <div class="hidden md:block mx-auto text-gray-600"></div>
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <a href="/adimages" class="button text-white bg-theme-1 shadow-md mr-2">Go back</a>
        </div>
    </div>
</div>
<div class="grid grid-cols-12 gap-6 mt-5 box">

    <div class="intro-y col-span-12 lg:col-span-6 ">
        <div class="intro-y  p-5">
            <form action="/adimages/create" method="post" class="mt-5" enctype="multipart/form-data">
                <div class="mb-3">
                    <label for="formFile" class="form-label">Select image from device.</label>

                    <input class="input w-full border mt-2" type="file" name="AdImage[<?= AdImageFields::URL ?>]"
                           id="formFile">
                </div>
                <div class="mb-3">
                    <label for="name" class="form-label">Image name</label>
                    <input type="text" class="input w-full border mt-2" name="AdImage[<?= AdImageFields::NAME ?>]"
                           id="name">
                </div>
                <div class="mb-3">
                    <label for="name" class="form-label">Target service</label>
                    <select name="AdImage[Target]" class="select2 w-full " id="target">
                        <option value="Direct">Yandex direct</option>
                        <option value="Facebook">Facebook</option>
                    </select>
                </div>
                <div class="text-right mt-5">
                    <button type="reset" class="button w-24 border text-gray-700 mr-1">Cancel</button>
                    <button type="submit" class="button w-24 bg-theme-1 text-white">Save</button>
                </div>
            </form>
        </div>
    </div>
    <div class="intro-y col-span-12 lg:col-span-6 ">
        <div class="intro-y  p-5">
            <label for="" class="text-lg font-medium mr-auto">Select image from device for Yandex Direct. For example,
                provide an image file such
                as GIF, JPEG or PNG formats</label>
            <div class="text-xs text-gray-600 mt-2">For Yandex Direct Image size — at least 450 pixels in length and
                width for Text & Image ads, and at least 1,080 pixels for mobile app ads.
            </div>
            <br>
            <label for="formFile" class="text-lg font-medium mr-auto">Select image from device for Facebook. For
                example, provide an image
                file such as .bmp, .jpeg, or .gif:</label>

        </div>
    </div>
</div>