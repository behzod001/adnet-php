<?php

use directapi\services\adimages\criterias\AdImageIdsCriteria;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\Fields\AdImageFields;
use helpers\Alert;

global $api, $app;

try {
    $service = $_GET['service'];
    if ($service == "fb") {
        $account = new AdAccount($_SESSION['user'][AdAccountFields::ID], null, $api);
        $account->deleteAdImages([], [AdImageFields::HASH => $_GET['hash']]);
    }
    if ($service == "direct") {
        $criteria = new AdImageIdsCriteria();
        $criteria->AdImageHashes = [$_GET['hash']];
        $app->getDirect()->setClientLogin("testinglogin2");
        $app->getDirect()->getAdImagesService()->delete($criteria);
    }
    Alert::success(' <i data-feather="check-square" class="w-6 h-6 mr-2 "></i> AdImage successfully deleted. For continue <a href="/adimages" class=" mx-1 text-theme-1"> checkout this page </a>');
} catch (Exception $e) {
    Alert::error('<i data-feather="alert-circle" class="w-6 h-6 mr-2"></i> ' . $e->getMessage());
}
