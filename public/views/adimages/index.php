<?php


use app\exceptions\DataNotFoundException;
use directapi\common\criterias\LimitOffset;
use directapi\exceptions\DirectAccountNotExistException;
use directapi\exceptions\DirectApiException;
use directapi\exceptions\DirectApiNotEnoughUnitsException;
use directapi\exceptions\RequestValidationException;
use directapi\exceptions\UnknownPropertyException;
use directapi\services\adimages\criterias\AdImageSelectionCriteria;
use directapi\services\adimages\enum\AdImageFieldEnum;
use FacebookAds\Object\Fields\AdImageFields;
use GuzzleHttp\Exception\GuzzleException;
use helpers\Alert;
use helpers\StringHelper;

global $app;
$fields = [
    AdImageFields::ID,
    AdImageFields::FILENAME,
    AdImageFields::HASH,
    AdImageFields::NAME,
    AdImageFields::ACCOUNT_ID,
    AdImageFields::UPDATED_TIME,
    AdImageFields::STATUS,
    AdImageFields::CREATED_TIME,
    AdImageFields::BYTES,
    AdImageFields::URL,
];
$params = [

];
try {
    $account = $app->getFacebookActiveAccount();
    $images = $account->getAdImages($fields, $params);
} catch (DataNotFoundException $e) {
    Alert::warning(' У вас есть Фасебоок аккаунт ? Тогда выберите ваш аккаунт. Если нет создайте и  <a href="/accounts" class="text-theme-1 mx-1"> регистрируйте. </a> ');
}


try {

    $fieldNames = [
        AdImageFieldEnum::AD_IMAGE_HASH,
        AdImageFieldEnum::ORIGINAL_URL,
        AdImageFieldEnum::PREVIEW_URL,
        AdImageFieldEnum::NAME,
        AdImageFieldEnum::TYPE,
        AdImageFieldEnum::SUBTYPE,
        AdImageFieldEnum::ASSOCIATED
    ];
    try {
        $limit = new LimitOffset(
            [
                "Limit" => 10000,
                "Offset" => 0
            ]
        );
    } catch (UnknownPropertyException $e) {
        Alert::error($e->getMessage() . '  <a href="/adimages"> reload this page </a>');
    }

    $criteria = new AdImageSelectionCriteria();
//    $criteria->AdImageHashes = [$_GET['hash']];
//    $criteria->Associated = "NO";
    $params = [
        "SelectionCriteria" => $criteria,
        "FieldNames" => $fieldNames,
        "Page" => $limit
    ];
    $adImages = [];
    $adImages = $app->getDirect()->call("adimages", "get", $params, true);

}  catch (DataNotFoundException $e) {
    Alert::warning(' У вас есть Yandex Direct аккаунт ? Тогда выберите ваш аккаунт. Если нет создайте и  <a href="/accounts"  class="mx-1 text-theme-1 mx-1"> регистрируйте. </a> ');
}catch (GuzzleException $e) {
    Alert::error($e->getMessage() . ' <a href="/adimages/create"> Back to create new </a>');

} catch (DirectAccountNotExistException | DirectApiNotEnoughUnitsException | RequestValidationException | DirectApiException $e) {
    Alert::error($e->getMessage() . ' <a href="/adimages/create"> Back to create new </a>');
}
?>

<h2 class="intro-y text-lg font-medium mt-10">
    Data List Ad Images
</h2>
<div class="grid grid-cols-12 gap-6 mt-5">
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
        <button class="button text-white bg-theme-1 shadow-md mr-2">Add New Ad Image</button>
        <div class="dropdown relative">
            <a href="/adimages/create">
                <button class="dropdown-toggle button px-2 box text-gray-700">
                <span class="w-5 h-5 flex items-center justify-center">
                    <svg xmlns="http://www.w3.org/2000/svg"
                         width="24" height="24" viewBox="0 0 24 24"
                         fill="none" stroke="currentColor"
                         stroke-width="1.5" stroke-linecap="round"
                         stroke-linejoin="round"
                         class="feather feather-plus w-4 h-4"><line
                                x1="12" y1="5" x2="12" y2="19"></line><line x1="5" y1="12" x2="19" y2="12"></line></svg> </span>
                </button>
            </a>
        </div>
        <div class="hidden md:block mx-auto text-gray-600">Showing 1 to 10 of 150 entries</div>
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <div class="w-56 relative text-gray-700">
                <input type="text" class="input w-56 box pr-10 placeholder-theme-13" placeholder="Search...">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                     stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
                     class="feather feather-search w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0">
                    <circle cx="11" cy="11" r="8"></circle>
                    <line x1="21" y1="21" x2="16.65" y2="16.65"></line>
                </svg>
            </div>
        </div>
    </div>
    <!-- BEGIN: Users Layout -->
    <?php if (isset($images)) foreach ($images as $image) { ?>
        <div class="intro-y col-span-12 md:col-span-6 lg:col-span-4">
            <div class="box">
                <div class="flex items-start px-5 pt-5">

                    <div class="absolute right-0 top-0 dropdown relative">
                        <img alt="Ad Net" class="rounded-md"
                             src="<?= $image->{AdImageFields::URL} ?>">
                    </div>
                </div>
                <div class="text-center lg:text-left p-5">
                    <div class="my-5">It is a long established fact that a reader will be distracted by the readable
                        content of a
                        page when looking at its layout. The point of using Lorem
                    </div>
                    <div class="flex items-center justify-center lg:justify-start text-gray-600 mt-5">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-activity">
                            <path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"></path>
                        </svg>
                        <span class="bg-theme-14 text-theme-10 rounded px-2 mt-1 text-center">Facebook</span>
                    </div>
                    <div class="flex items-center justify-center lg:justify-start text-gray-600 mt-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-mail w-3 h-3 mr-2">
                            <path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path>
                            <polyline points="22,6 12,13 2,6"></polyline>
                        </svg>
                        <?= StringHelper::underscoreToCamelCase(AdImageFields::ID) . ": " . $image->{AdImageFields::ID} ?>
                    </div>
                    <div class="flex items-center justify-center lg:justify-start text-gray-600 mt-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-instagram w-3 h-3 mr-2">
                            <rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect>
                            <path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path>
                            <line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line>
                        </svg>
                        <?= StringHelper::underscoreToCamelCase(AdImageFields::NAME) . ": " . $image->{AdImageFields::NAME} ?>
                    </div>
                    <div class="flex items-center justify-center lg:justify-start text-gray-600 mt-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-instagram w-3 h-3 mr-2">
                            <rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect>
                            <path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path>
                            <line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line>
                        </svg>
                        <?= StringHelper::underscoreToCamelCase(AdImageFields::CREATED_TIME) . ": " . $image->{AdImageFields::CREATED_TIME} ?>
                    </div>
                    <div class="flex items-center justify-center lg:justify-start text-gray-600 mt-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-instagram w-3 h-3 mr-2">
                            <rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect>
                            <path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path>
                            <line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line>
                        </svg>
                        <?= StringHelper::underscoreToCamelCase(AdImageFields::UPDATED_TIME) . ": " . $image->{AdImageFields::UPDATED_TIME} ?>
                    </div>
                </div>
                <div class="text-center lg:text-right p-5 border-t border-gray-200">
                    <a href="/adimages/view?service=fb&hash=<?= $image->{AdImageFields::HASH} ?>"
                       class="button button--sm text-white bg-theme-1 mr-2">View</a>
                    <a data-delete="/adimages/delete?service=fb&id=<?= (int)$image->{AdImageFields::ID} ?>&hash=<?= $image->{AdImageFields::HASH} ?>"
                       onclick="confirmDelete(this);" data-toggle="modal" data-target="#delete-confirmation-modal"
                       class="button button--sm text-gray-700 border border-gray-300">Delete</a>
                </div>
            </div>
        </div>
    <?php } ?>

    <?php if (isset($adImages->AdImages)) foreach ($adImages->AdImages as $adImage) { ?>
        <div class="intro-y col-span-12 md:col-span-6 lg:col-span-4">
            <div class="box">
                <div class="flex items-start px-5 pt-5">

                    <div class="absolute right-0 top-0 dropdown relative">
                        <img alt="Ad Net" class="rounded-md"
                             src="<?= $adImage->{AdImageFieldEnum::PREVIEW_URL} ?>">
                    </div>
                </div>
                <div class="text-center lg:text-left p-5">
                    <div class="my-5">
                        It is a long established fact that a reader will be distracted by the readable
                        content of a
                        page when looking at its layout. The point of using Lorem
                    </div>
                    <div class="flex items-center justify-center lg:justify-start text-gray-600 mt-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-activity">
                            <path d="M19 21l-7-5-7 5V5a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2z"></path>
                        </svg>
                        <span class="bg-theme-17 text-theme-11 rounded px-2 mt-1 text-center">Yandex</span>
                    </div>
                    <div class="flex items-center justify-center lg:justify-start text-gray-600 mt-5">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-mail w-3 h-3 mr-2">
                            <path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path>
                            <polyline points="22,6 12,13 2,6"></polyline>
                        </svg>
                        <?= StringHelper::underscoreToCamelCase(AdImageFieldEnum::AD_IMAGE_HASH) . ": " . $adImage->{AdImageFieldEnum::AD_IMAGE_HASH} ?>
                    </div>
                    <div class="flex items-center justify-center lg:justify-start text-gray-600 mt-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-instagram w-3 h-3 mr-2">
                            <rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect>
                            <path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path>
                            <line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line>
                        </svg>
                        <?= StringHelper::underscoreToCamelCase(AdImageFieldEnum::NAME) . ": " . $adImage->{AdImageFieldEnum::NAME} ?>
                    </div>
                    <div class="flex items-center justify-center lg:justify-start text-gray-600 mt-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-instagram w-3 h-3 mr-2">
                            <rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect>
                            <path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path>
                            <line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line>
                        </svg>
                        <?= StringHelper::underscoreToCamelCase(AdImageFieldEnum::ORIGINAL_URL) . ": " . $adImage->{AdImageFieldEnum::ORIGINAL_URL} ?>
                    </div>

                </div>
                <div class="text-center lg:text-right p-5 border-t border-gray-200">
                    <a href="/adimages/view?service=direct&hash=<?= $adImage->{AdImageFieldEnum::AD_IMAGE_HASH} ?>"
                       class="button button--sm text-white bg-theme-1 mr-2">View</a>
                    <a data-delete="/adimages/delete?service=direct&hash=<?= $adImage->{AdImageFieldEnum::AD_IMAGE_HASH} ?>"
                       onclick="confirmDelete(this);" data-toggle="modal" data-target="#delete-confirmation-modal"
                       class="button button--sm text-gray-700 border border-gray-300">Delete</a>
                </div>
            </div>
        </div>
    <?php } ?>
    <!-- END: Users Layout -->
    <!-- BEGIN: Pagination -->
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-row sm:flex-no-wrap items-center">
        <ul class="pagination">
            <li>
                <a class="pagination__link" href="">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                         stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
                         class="feather feather-chevrons-left w-4 h-4">
                        <polyline points="11 17 6 12 11 7"></polyline>
                        <polyline points="18 17 13 12 18 7"></polyline>
                    </svg>
                </a>
            </li>
            <li>
                <a class="pagination__link" href="">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                         stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
                         class="feather feather-chevron-left w-4 h-4">
                        <polyline points="15 18 9 12 15 6"></polyline>
                    </svg>
                </a>
            </li>
            <li><a class="pagination__link" href="">...</a></li>
            <li><a class="pagination__link  pagination__link--active" href="">1</a></li>
            <li><a class="pagination__link" href="">2</a></li>
            <li><a class="pagination__link" href="">3</a></li>
            <li><a class="pagination__link" href="">...</a></li>
            <li>
                <a class="pagination__link" href="">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                         stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
                         class="feather feather-chevron-right w-4 h-4">
                        <polyline points="9 18 15 12 9 6"></polyline>
                    </svg>
                </a>
            </li>
            <li>
                <a class="pagination__link" href="">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                         stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
                         class="feather feather-chevrons-right w-4 h-4">
                        <polyline points="13 17 18 12 13 7"></polyline>
                        <polyline points="6 17 11 12 6 7"></polyline>
                    </svg>
                </a>
            </li>
        </ul>
        <select class="w-20 input box mt-3 sm:mt-0">
            <option>10</option>
            <option>25</option>
            <option>35</option>
            <option>50</option>
        </select>
    </div>
    <!-- END: Pagination -->
</div>


<!-- BEGIN: Delete Confirmation Modal -->
<div class="modal" id="delete-confirmation-modal">
    <div class="modal__content">
        <div class="p-5 text-center">
            <i data-feather="x-circle" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i>
            <div class="text-3xl mt-5">Are you sure?</div>
            <div class="text-gray-600 mt-2">Do you really want to delete these records? This process cannot be
                undone.
            </div>
        </div>
        <div class="px-5 pb-8 text-center">
            <button type="button" data-dismiss="modal" class="button w-24 border text-gray-700 mr-1">Cancel</button>
            <a href="" id="deleteBtn" type="button" class="button w-24 bg-theme-6 text-white">Delete</a>
        </div>
    </div>
</div>
<!-- END: Delete Confirmation Modal -->