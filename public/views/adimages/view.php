<?php


use directapi\common\criterias\LimitOffset;
use directapi\exceptions\DirectAccountNotExistException;
use directapi\exceptions\DirectApiException;
use directapi\exceptions\DirectApiNotEnoughUnitsException;
use directapi\exceptions\RequestValidationException;
use directapi\exceptions\UnknownPropertyException;
use directapi\services\adimages\criterias\AdImageSelectionCriteria;
use directapi\services\adimages\enum\AdImageFieldEnum;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\Fields\AdImageFields;
use GuzzleHttp\Exception\GuzzleException;
use helpers\Alert;
use helpers\StringHelper;

global $api, $app;
$images = $adImages = null;
if (isset($_GET['service']) && $_GET['service'] == 'fb') {
    $account = new AdAccount($_SESSION['user'][AdAccountFields::ID], null, $api);
    $fields = [
        AdImageFields::ID,
        AdImageFields::FILENAME,
        AdImageFields::HASH,
        AdImageFields::NAME,
        AdImageFields::ACCOUNT_ID,
        AdImageFields::UPDATED_TIME,
        AdImageFields::STATUS,
        AdImageFields::CREATED_TIME,
        AdImageFields::BYTES,
        AdImageFields::URL,
    ];
    $params = [
        'hashes' => [
            $_GET['hash']
        ]
    ];
    $images = $account->getAdImages($fields, $params);

}
if (isset($_GET['service']) && $_GET['service'] == 'direct') {
    $fieldNames = [
        AdImageFieldEnum::AD_IMAGE_HASH,
        AdImageFieldEnum::ORIGINAL_URL,
        AdImageFieldEnum::PREVIEW_URL,
        AdImageFieldEnum::NAME,
        AdImageFieldEnum::TYPE,
        AdImageFieldEnum::SUBTYPE,
        AdImageFieldEnum::ASSOCIATED
    ];
    try {
        $limit = new LimitOffset(
            [
                "Limit" => 10000,
                "Offset" => 0
            ]
        );
        $criteria = new AdImageSelectionCriteria();
        $criteria->AdImageHashes = [$_GET['hash']];
        $params = [
            "SelectionCriteria" => $criteria,
            "FieldNames" => $fieldNames
        ];
        $app->getDirect()->setClientLogin("testinglogin2");
        $adImages = $app->getDirect()->call("adimages", "get", $params, true)->AdImages;
    } catch (GuzzleException | UnknownPropertyException | DirectAccountNotExistException | DirectApiNotEnoughUnitsException | RequestValidationException | DirectApiException $e) {
        Alert::error($e->getMessage() . '  <a href="/adimages/create">Back to create new</a>');
    }
}
?>


<div class="grid grid-cols-1 gap-1 mt-5">
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
        <h2 class="intro-y text-lg font-medium my-10">
            View Ad Image
        </h2>
        <div class="hidden md:block mx-auto text-gray-600">

        </div>
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <div class="w-56 relative text-gray-700">
                <a class="button text-white bg-theme-1 shadow-md mr-2" href="/adimages">Go back</a>
            </div>
        </div>
    </div>
    <!-- BEGIN: Data List -->
    <?php if (!is_null($images)) foreach ($images as $image) { ?>
        <div class="intro-y col-span-1 md:col-span-1 lg:col-span-1">
            <div class="box">
                <div class="flex items-start px-5 pt-5">

                    <div class="absolute right-0 top-0 dropdown relative">
                        <img alt="Ad Net" class="rounded-md"
                             src="<?= $image->{AdImageFields::URL} ?>">
                    </div>
                </div>
                <div class="text-center lg:text-left p-5">
                    <div class="my-5">It is a long established fact that a reader will be distracted by the readable
                        content of a
                        page when looking at its layout. The point of using Lorem
                    </div>
                    <div class="flex items-center justify-center lg:justify-start text-gray-600 mt-5">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-mail w-3 h-3 mr-2">
                            <path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path>
                            <polyline points="22,6 12,13 2,6"></polyline>
                        </svg>
                        <?= StringHelper::underscoreToCamelCase(AdImageFields::ID) . ": " . $image->{AdImageFields::ID} ?>
                    </div>
                    <div class="flex items-center justify-center lg:justify-start text-gray-600 mt-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-instagram w-3 h-3 mr-2">
                            <rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect>
                            <path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path>
                            <line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line>
                        </svg>
                        <?= StringHelper::underscoreToCamelCase(AdImageFields::NAME) . ": " . $image->{AdImageFields::NAME} ?>
                    </div>
                    <div class="flex items-center justify-center lg:justify-start text-gray-600 mt-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-instagram w-3 h-3 mr-2">
                            <rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect>
                            <path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path>
                            <line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line>
                        </svg>
                        <?= StringHelper::underscoreToCamelCase(AdImageFields::CREATED_TIME) . ": " . $image->{AdImageFields::CREATED_TIME} ?>
                    </div>
                    <div class="flex items-center justify-center lg:justify-start text-gray-600 mt-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-instagram w-3 h-3 mr-2">
                            <rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect>
                            <path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path>
                            <line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line>
                        </svg>
                        <?= StringHelper::underscoreToCamelCase(AdImageFields::UPDATED_TIME) . ": " . $image->{AdImageFields::UPDATED_TIME} ?>
                    </div>
                </div>
                <div class="text-center lg:text-right p-5 border-t border-gray-200">
                    <a data-delete="/adimages/delete?id=<?= (int)$image->{AdImageFields::ID} ?>&hash=<?= $image->{AdImageFields::HASH} ?>"
                       data-toggle="modal"
                       data-target="#delete-confirmation-modal"
                       class="button button--sm text-gray-700 border border-gray-300">Delete</a>
                </div>
            </div>
        </div>

    <?php } ?>
    <?php if (!is_null($adImages)) foreach ($adImages as $adImage) { ?>
        <div class="intro-y col-span-12 md:col-span-6 lg:col-span-4">
            <div class="box">
                <div class="flex items-start px-5 pt-5">

                    <div class="absolute right-0 top-0 dropdown relative">
                        <img alt="Ad Net" class="rounded-md"
                             src="<?= $adImage->{AdImageFieldEnum::PREVIEW_URL} ?>">
                    </div>
                </div>
                <div class="text-center lg:text-left p-5">
                    <div class="my-5">It is a long established fact that a reader will be distracted by the readable
                        content of a page when looking at its layout. The point of using Lorem
                    </div>
                    <div class="flex items-center justify-center lg:justify-start text-gray-600 mt-5">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-mail w-3 h-3 mr-2">
                            <path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path>
                            <polyline points="22,6 12,13 2,6"></polyline>
                        </svg>
                        <?= StringHelper::underscoreToCamelCase(AdImageFieldEnum::AD_IMAGE_HASH) . ": " . $adImage->{AdImageFieldEnum::AD_IMAGE_HASH} ?>
                    </div>
                    <div class="flex items-center justify-center lg:justify-start text-gray-600 mt-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-instagram w-3 h-3 mr-2">
                            <rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect>
                            <path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path>
                            <line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line>
                        </svg>
                        <?= StringHelper::underscoreToCamelCase(AdImageFieldEnum::NAME) . ": " . $adImage->{AdImageFieldEnum::NAME} ?>
                    </div>
                    <div class="flex items-center justify-center lg:justify-start text-gray-600 mt-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-instagram w-3 h-3 mr-2">
                            <rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect>
                            <path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path>
                            <line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line>
                        </svg>
                        <?= StringHelper::underscoreToCamelCase(AdImageFieldEnum::ORIGINAL_URL) . ": " . $adImage->{AdImageFieldEnum::ORIGINAL_URL} ?>
                    </div>

                </div>
                <div class="text-center lg:text-right p-5 border-t border-gray-200">
                    <a data-delete="/adimages/delete?service=direct&hash=<?= $adImage->{AdImageFieldEnum::AD_IMAGE_HASH} ?>"
                       onclick="confirmDelete(this);" data-toggle="modal" data-target="#delete-confirmation-modal"
                       class="button button--sm text-gray-700 border border-gray-300">Delete</a>
                </div>
            </div>
        </div>
    <?php } ?>
</div>

<!-- BEGIN: Delete Confirmation Modal -->
<div class="modal" id="delete-confirmation-modal">
    <div class="modal__content">
        <div class="p-5 text-center">
            <i data-feather="x-circle" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i>
            <div class="text-3xl mt-5">Are you sure?</div>
            <div class="text-gray-600 mt-2">Do you really want to delete these records? This process cannot be
                undone.
            </div>
        </div>
        <div class="px-5 pb-8 text-center">
            <button type="button" data-dismiss="modal" class="button w-24 border text-gray-700 mr-1">Cancel</button>
            <a href="" id="deleteBtn" type="button" class="button w-24 bg-theme-6 text-white">Delete</a>
        </div>
    </div>
</div>
<!-- END: Delete Confirmation Modal -->

