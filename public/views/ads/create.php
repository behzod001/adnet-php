<?php

use app\exceptions\DataNotFoundException;
use FacebookAds\Http\Exception\RequestException;
use FacebookAds\Object\AdCreative;
use FacebookAds\Object\AdSet;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\AdCreativeFields;
use FacebookAds\Object\Fields\AdFields;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\Values\AdSetStatusValues;
use helpers\Alert;

/**
 * @var $campaigns Campaign
 * @var $adSets AdSet
 * @var $adCreatives AdCreative
 */

global $app;
try {
    $account = $app->getFacebookActiveAccount();
    // get campaigns
    $fields = array(CampaignFields::ID, CampaignFields::NAME);
    $params = array('effective_status' => array('ACTIVE', 'PAUSED'));
    $campaigns = $account->getCampaigns($fields, $params);

    // get ad creative
    $fields = array(AdCreativeFields::ID, AdCreativeFields::NAME);
    $params = [];
    $adCreatives = $account->getAdCreatives($fields, $params);

    // ad sets
    $fields = [AdSetFields::ID, AdSetFields::NAME];
    $params = array(
        AdSetFields::EFFECTIVE_STATUS => [
            AdSetStatusValues::ACTIVE,
            AdSetStatusValues::PAUSED
        ]
    );
    $adSets = $account->getAdSets($fields, $params);

    if (isset($_POST['Ad'])) {
        try {
            $params = [
                AdFields::NAME => $_POST["Ad"][AdFields::NAME],
                AdFields::ADSET_ID => (int)$_POST["Ad"][AdFields::ADSET_ID],
                AdFields::CREATIVE => ['creative_id' => (int)$_POST["Ad"][AdFields::CREATIVE]],
                AdFields::STATUS => "PAUSED"
            ];
            $fields = [AdFields::ID];
            $ad = $account->createAd($fields, $params);
            Alert::success('  Ad successfully created for view ad <a  href="/ads/view' . $ad->{AdFields::ID} . '"> checkout this page</a> ');
        } catch (RequestException $e) {
            Alert::error('<div> ' . $e->getErrorUserTitle() . ' </div>' . $e->getErrorUserMessage() . ' <a href="/ads">checkout this page</a> ');
        }
    }

} catch (DataNotFoundException $e) {
    Alert::warning(' У вас есть Фасебоок аккаунт ? Тогда выберите ваш аккаунт. Если нет создайте и  <a href="/accounts" class="text-theme-1 mx-1"> регистрируйте. </a> ');
}
?>
<div class="intro-y flex items-center mt-8">
    <h2 class="text-lg font-medium mr-auto">
        Create new Ads
    </h2>
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
        <div class="hidden md:block mx-auto text-gray-600"></div>
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <a href="/ads" class="button text-white bg-theme-1 shadow-md mr-2">Go back</a>
        </div>
    </div>
</div>

<div class="grid grid-cols-12 gap-6 mt-5 box">

    <div class="intro-y col-span-12 lg:col-span-6 ">
        <div class="intro-y  p-5">
            <form action="/ads/create" method="post">

                <div class="mb-3">
                    <label for="adname" class="form-label">Ad name</label>
                    <input type="text" name="Ad[<?= AdFields::NAME ?>]" class="input w-full border mt-2" id="adname">
                </div>
                <div class="mb-3">
                    <label for="adset" class="form-label">Select Ad Set</label>
                    <select name="Ad[<?= AdFields::ADSET_ID ?>]" class="select2 w-full"
                            aria-label="Default select example">
                        <option disabled selected>Select ad sets</option>
                        <?php if (isset($adSet))
                            foreach ($adSets as $adSet) {
                                echo '<option value="' . $adSet->{AdSetFields::ID} . '">' . $adSet->{AdSetFields::NAME} . '</option>';
                            }
                        ?>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="adcreative" class="form-label">Select Ad creative</label>
                    <select class="select2 w-full" name="Ad[<?= AdFields::CREATIVE ?>]"
                            aria-label="Default select example">
                        <option disabled selected>Select one of ad creatives</option>
                        <?php if (isset($adCreatives))
                            foreach ($adCreatives as $creative) {
                                echo '<option value="' . $creative->{AdSetFields::ID} . '">' . $creative->{AdSetFields::NAME} . '</option>';
                            }
                        ?>
                    </select>
                </div>
                <div class="text-right mt-5">
                    <button type="reset" class="button w-24 border text-gray-700 mr-1">Cancel</button>
                    <button type="submit" class="button w-24 bg-theme-1 text-white">Save</button>
                </div>

            </form>
        </div>
    </div>
</div>


