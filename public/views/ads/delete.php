<?php

use FacebookAds\Object\Ad;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdAccountFields;
use helpers\Alert;

global $api;

$account = new AdAccount($_SESSION['user'][AdAccountFields::ID]);
if ((new Ad($_GET['id'], null, $api))->deleteSelf()) {
    Alert::success(' <i data-feather="check-square" class="w-6 h-6 mr-2"></i> Ad successfully deleted for view ad <a href="/ads"> checkout this page</a> ');
}

