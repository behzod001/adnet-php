<?php

use app\exceptions\DataNotFoundException;
use directapi\exceptions\DirectAccountNotExistException;
use directapi\exceptions\DirectApiException;
use directapi\exceptions\DirectApiNotEnoughUnitsException;
use directapi\exceptions\RequestValidationException;
use directapi\services\ads\criterias\AdsSelectionCriteria;
use directapi\services\ads\enum\AdFieldEnum;
use directapi\services\ads\enum\TextAdFieldEnum;
use directapi\services\ads\enum\TextAdPriceExtensionFieldEnum;
use directapi\services\campaigns\criterias\CampaignsSelectionCriteria;
use directapi\services\campaigns\enum\CampaignFieldEnum;
use FacebookAds\Object\Fields\AdFields;
use FacebookAds\Object\Fields\TargetingFields;
use GuzzleHttp\Exception\GuzzleException;
use helpers\Alert;
use helpers\StringHelper;

//getting facebook ads
global $app;

try {
    $account = $app->getFacebookActiveAccount();
    $fields = [
        AdFields::ID,
        AdFields::NAME,
        AdFields::STATUS,
        AdFields::RECOMMENDATIONS,
        AdFields::TARGETING,
        AdFields::EFFECTIVE_STATUS,
        AdFields::BID_AMOUNT
    ];
    $params = [];
    $ads = $account->getAds($fields, $params);


} catch (DataNotFoundException $e) {
    Alert::warning(' У вас есть Фасебоок аккаунт ? Тогда выберите ваш аккаунт. Если нет создайте и  <a href="/accounts" class="text-theme-1 mx-1"> регистрируйте. </a> ');
}


try {
    //getting yandex ads

    $fieldNames = [
        AdFieldEnum::ID,
        AdFieldEnum::STATUS,
        AdFieldEnum::AD_CATEGORIES,
        AdFieldEnum::STATE,
        AdFieldEnum::AD_GROUP_ID,
        AdFieldEnum::AGE_LABEL,
    ];

    $adGroups = $directAds = null;
    $campaignFieldNames = [
        CampaignFieldEnum::NAME,
        CampaignFieldEnum::ID
    ];
    $campaigns = $app->getDirect()->getCampaignsService()->get(new CampaignsSelectionCriteria(), $campaignFieldNames);
    $CampaignIds = [];
    foreach ($campaigns as $campaign) {
        array_push($CampaignIds, $campaign->{CampaignFieldEnum::ID});
    }
    if (count($CampaignIds) > 0) {
        $criteria = new AdsSelectionCriteria();
        $criteria->CampaignIds = $CampaignIds;

        $directAds = $app->getDirect()->getAdsService()->get($criteria, $fieldNames, [
            TextAdFieldEnum::TITLE,
            TextAdFieldEnum::TITLE2,
            TextAdFieldEnum::TEXT,
            TextAdFieldEnum::AD_IMAGE_HASH,
            TextAdFieldEnum::AD_IMAGE_HASH,
            TextAdFieldEnum::DISPLAY_URL_PATH,
            TextAdFieldEnum::HREF,
        ], [], [], [], [], [], [], [
//        TextAdPriceExtensionFieldEnum::PRICE_CURRENCY,
            TextAdPriceExtensionFieldEnum::PRICE,
            TextAdPriceExtensionFieldEnum::PRICE_QUALIFIER,
            TextAdPriceExtensionFieldEnum::OLD_PRICE,
        ]);
    }

} catch (GuzzleException | DirectAccountNotExistException | DirectApiNotEnoughUnitsException | RequestValidationException | DirectApiException $e) {
    Alert::error($e->getMessage());
} catch (DataNotFoundException $e) {
    Alert::warning(' У вас есть Yandex Direct аккаунт ? Тогда выберите ваш аккаунт. Если нет создайте и  <a href="/accounts" class="text-theme-1 mx-1"> регистрируйте. </a> ');
}

?>


<h2 class="intro-y text-lg font-medium mt-10">
    Data List Ads
</h2>
<div class="grid grid-cols-12 gap-6 mt-5">
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
        <button class="button text-white bg-theme-1 shadow-md mr-2">Create new add</button>
        <div class="dropdown relative ">
            <button class="dropdown-toggle button px-2 box text-gray-700">
            <span class="w-5 h-5 flex items-center justify-center">
                <i class="w-4 h-4" data-feather="plus"></i>
            </span>
            </button>
            <div class="dropdown-box mt-10 absolute top-0 left-0 z-20" style="width: 250px">
                <div class="dropdown-box__content box p-2">
                    <a href="/ads/create"
                       class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                        <i data-feather="facebook" class="w-4 h-4 mr-2"></i> Facebook ad </a>
                    <a href="/direct/ads/create"
                       class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                        <i data-feather="shield" class="w-4 h-4 mr-2"></i>Yandex Direct ad </a>
                    <a href="javascript:" data-toggle="modal"
                       data-target="#add-google-account"
                       class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                        <i data-feather="map-pin" class="w-4 h-4 mr-2"></i>Google Ads ad </a>
                </div>
            </div>
        </div>
        <div class="hidden md:block mx-auto text-gray-600">Showing 1 to 10 of 150 entries</div>
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <div class="w-56 relative text-gray-700">
                <input type="text" class="input w-56 box pr-10 placeholder-theme-13" placeholder="Search...">
                <i class="w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0" data-feather="search"></i>
            </div>
        </div>
    </div>
    <!-- BEGIN: Data List -->
    <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
        <table class="table table-report -mt-2">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name Ads</th>
                <?php
                if (isset($fields)) {
                    unset($fields[0], $fields[1], $fields[2]);
                    foreach ($fields as $field) {if (is_array($field) )continue; ?>
                        <th scope="col"><?= StringHelper::underscoreToCamelCase($field); ?></th>
                    <?php }
                } ?>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
            <?php $idx = 0;
            if (isset($ads)) {
                foreach ($ads as $ad) { ?>
                    <tr>
                        <td>  <?= ++$idx; ?> </td>
                        <td>
                            <a href="/ads/view?id=<?= $ad->{AdFields::ID} ?>"
                               class="font-medium whitespace-no-wrap"> <?= StringHelper::underscoreToCamelCase($ad->{AdFields::NAME}); ?>
                                <div class="text-gray-600 text-xs whitespace-no-wrap"><?= $ad->{AdFields::ID} ?></div>
                            </a>
                        </td>
                        <?php if(isset($fields)) foreach ($fields as $field) {
                            if ($field === AdFields::RECOMMENDATIONS && !is_null($ad->{AdFields::RECOMMENDATIONS})) {
                                $recommendation = $ad->{$field};
                                for ($i = 0; $i < count($recommendation); $i++) {
                                    echo "<td> <span>" . $recommendation[$i]['title'] . " <i class='bi bi-info-circle-fill' data-bs-toggle='tooltip' data-bs-placement='right'  title='" . $recommendation[$i]['message'] . "'  /> </span></td>";
                                }
                                continue;
                            }
                            if ($field == AdFields::TARGETING && !is_null($ad->{$field})) {
                                $targeting = $ad->{AdFields::TARGETING};
                                echo "<td>";
                                echo StringHelper::underscoreToCamelCase(TargetingFields::AGE_MAX) . " : " . $ad->{$field}[TargetingFields::AGE_MAX] . "<br />";
                                echo StringHelper::underscoreToCamelCase(TargetingFields::AGE_MIN) . " : " . $ad->{$field}[TargetingFields::AGE_MIN] . "<br />";
                                if(isset($targeting[TargetingFields::GEO_LOCATIONS]['countries'])){
                                    echo StringHelper::underscoreToCamelCase(TargetingFields::GEO_LOCATIONS) . " : ";
                                    $loc = $targeting[TargetingFields::GEO_LOCATIONS]['countries'];
                                    for ($it = 0; $it < count($loc) - 1; $it++) {
                                        echo ' <span class="badge bg-success">' . $loc[$it] . ' </span>';
                                    }
                                }
                                if(isset($targeting[TargetingFields::GEO_LOCATIONS]['places'])){
                                    echo "Places : ";
                                    $places = $targeting[TargetingFields::GEO_LOCATIONS]['places'];
                                    foreach ($places[0] as $keyLoc => $valueLoc) {
                                        echo ' <span class="badge bg-success">' . StringHelper::underscoreToCamelCase($keyLoc).": ".$valueLoc . ' </span>';
                                    }
                                }
                                if(isset($targeting[TargetingFields::FACEBOOK_POSITIONS])){
                                    echo StringHelper::underscoreToCamelCase(TargetingFields::FACEBOOK_POSITIONS) . " : ";
                                    $positions = $targeting[TargetingFields::FACEBOOK_POSITIONS];
                                    for ($indx = 0;$indx<count($positions); $indx++) {
                                        echo ' <span class="badge bg-success">' . StringHelper::underscoreToCamelCase($positions[$indx] ) . ' </span>';
                                    }
                                }
                                if(isset($targeting[TargetingFields::DEVICE_PLATFORMS])){
                                    echo StringHelper::underscoreToCamelCase(TargetingFields::DEVICE_PLATFORMS) . " : ";
                                    $device = $targeting[TargetingFields::DEVICE_PLATFORMS];
                                    for ($indx = 0;$indx<count($device);$indx++) {
                                        echo ' <span class="badge bg-success">' . StringHelper::underscoreToCamelCase($device[$indx] ) . ' </span>';
                                    }
                                }
                                if(isset($targeting[TargetingFields::PUBLISHER_PLATFORMS])){
                                    echo StringHelper::underscoreToCamelCase(TargetingFields::PUBLISHER_PLATFORMS) . " : ";
                                    $publisher = $targeting[TargetingFields::PUBLISHER_PLATFORMS];
                                    for ($indx=0; $indx<count($publisher);$indx++) {
                                        echo ' <span class="badge bg-success">' . StringHelper::underscoreToCamelCase($publisher[$indx] ) . ' </span>';
                                    }
                                }
                                echo "</td>";
                                continue;
                            }
                            ?>
                            <td><?= is_null($ad->{$field}) ? " Not installed " : StringHelper::underscoreToCamelCase($ad->{$field}); ?></td>
                        <?php } ?>
                        <td class="w-40">
                            <div class="flex items-center justify-center text-theme-6">
                                <i data-feather="check-square" class="w-4 h-4 mr-2"></i>
                                <?= $ad->{AdFields::STATUS} ?>
                            </div>
                        </td>
                        <td class="table-report__action w-56">
                            <div class="flex justify-center items-center">
                                <a class="flex items-center mr-3" href="/ads/view?id=<?= $ad->{AdFields::ID} ?>"> <i
                                            data-feather="eye" class="w-4 h-4 mr-1"></i> View </a>
                                <a class="flex items-center mr-3" href="/ads/update?id=<?= $ad->{AdFields::ID} ?>"> <i
                                            data-feather="check-square" class="w-4 h-4 mr-1"></i> Edit </a>
                                <a class="flex items-center text-theme-6" href="javascript:" data-toggle="modal"
                                   data-delete="/ads/delete?id=<?= $ad->{AdFields::ID} ?>"
                                   onclick="confirmDelete(this);"
                                   data-target="#delete-confirmation-modal">
                                    <i data-feather="trash-2" class="w-4 h-4 mr-1"></i>
                                    Delete </a>
                            </div>
                        </td>
                    </tr>
                <?php }
            } ?>
            <?php unset($fieldNames[0], $fieldNames[1], $fieldNames[2]);
            if (!is_null($directAds))
                foreach ($directAds as $directAd) { ?>
                    <tr>
                        <td>  <?= ++$idx; ?> </td>
                        <td>
                            <a href="/ads/view?id=<?= $directAd->{AdFieldEnum::ID} ?>"
                               class="font-medium whitespace-no-wrap">
                                <?= StringHelper::underscoreToCamelCase($directAd->TextAd->{TextAdFieldEnum::TITLE}); ?>
                                ,
                                <?= StringHelper::underscoreToCamelCase($directAd->TextAd->{TextAdFieldEnum::TITLE2}); ?>
                                <div class="text-gray-600 text-xs whitespace-no-wrap"><?= $directAd->{AdFieldEnum::ID} ?></div>
                            </a>
                        </td>
                        <td>  <?= $directAd->TextAd->{TextAdFieldEnum::TEXT}; ?> </td>
                        <?php foreach ($fieldNames as $field) { ?>
                            <td><?= is_null($directAd->{$field}) ? " Not installed " : StringHelper::underscoreToCamelCase($directAd->{$field}); ?></td>
                        <?php } ?>
                        <td class="w-40">
                            <div class="flex items-center justify-center text-theme-6">
                                <i data-feather="check-square" class="w-4 h-4 mr-2"></i>
                                <?= $directAd->{AdFieldEnum::STATUS} ?>
                            </div>
                        </td>
                        <td class="table-report__action w-56">
                            <div class="flex justify-center items-center">
                                <a class="flex items-center mr-3"
                                   href="/direct/ads/view?id=<?= $directAd->{AdFieldEnum::ID} ?>"> <i
                                            data-feather="eye" class="w-4 h-4 mr-1"></i> View </a>
                                <a class="flex items-center mr-3"
                                   href="/direct/ads/update?id=<?= $directAd->{AdFieldEnum::ID} ?>"> <i
                                            data-feather="check-square" class="w-4 h-4 mr-1"></i> Edit </a>
                                <a class="flex items-center text-theme-6" href="javascript:" data-toggle="modal"
                                   data-delete="/ads/delete?id=<?= $directAd->{AdFieldEnum::ID} ?>"
                                   onclick="confirmDelete(this);"
                                   data-target="#delete-confirmation-modal">
                                    <i data-feather="trash-2" class="w-4 h-4 mr-1"></i>
                                    Delete </a>
                            </div>
                        </td>
                    </tr>
                <?php } ?>

            </tbody>
        </table>
    </div>
</div>
<!-- BEGIN: Delete Confirmation Modal -->
<div class="modal" id="delete-confirmation-modal">
    <div class="modal__content">
        <div class="p-5 text-center">
            <i data-feather="x-circle" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i>
            <div class="text-3xl mt-5">Are you sure?</div>
            <div class="text-gray-600 mt-2">Do you really want to delete these records? This process cannot be
                undone.
            </div>
        </div>
        <div class="px-5 pb-8 text-center">
            <button type="button" data-dismiss="modal" class="button w-24 border text-gray-700 mr-1">Cancel</button>
            <a href="" id="deleteBtn" type="button" class="button w-24 bg-theme-6 text-white">Delete</a>
        </div>
    </div>
</div>
<!-- END: Delete Confirmation Modal -->

