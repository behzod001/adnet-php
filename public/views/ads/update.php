<?php

use FacebookAds\Object\Ad;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\AdCreative;
use FacebookAds\Object\AdSet;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\Fields\AdFields;
use helpers\Alert;

/**
 * @var $campaigns Campaign
 * @var $adSets AdSet
 * @var $adCreatives AdCreative
 */

global $api;
$account = new AdAccount($_SESSION['user'][AdAccountFields::ID]);
$ad = new Ad($_GET['id'], null, $api);
$adSelf = $ad->getSelf([AdFields::ID, AdFields::NAME]);
if (isset($_POST['Ad'])) {
    try {
        $params = [
            AdFields::NAME => $_POST["Ad"][AdFields::NAME],
        ];
        $fields = [AdFields::ID];
        $ad->updateSelf($fields, $params);
        Alert::success(' <i data-feather="check-square" class="w-6 h-6 mr-2"></i>  Ads successfully updated for view <a class="mx-1" href="/ads/view?id=' . $ad->{AdFields::ID} . '"> checkout this page</a>');
    } catch (Exception $exception) {
        Alert::error(' <i data-feather="alert-circle" class="w-6 h-6 mr-2"></i> ' . $exception->getMessage());
    }
}

?>

<div class="intro-y flex items-center mt-8">
    <h2 class="text-lg font-medium mr-auto">
        Create new Ads
    </h2>
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
        <div class="hidden md:block mx-auto text-gray-600"></div>
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <a href="/ads" class="button text-white bg-theme-1 shadow-md mr-2">Go back</a>
        </div>
    </div>
</div>
<div class="grid grid-cols-12 gap-6 mt-5 box">
    <div class="intro-y col-span-12 lg:col-span-6 ">
        <div class="intro-y  p-5">
            <form action="/ads/update?id=<?= $_GET['id']; ?>" method="post">
                <div class="mb-3">
                    <label for="adname" class="form-label">Ad name</label>
                    <input type="text" name="Ad[<?= AdFields::NAME ?>]" value="<?= $adSelf->{AdFields::NAME}; ?>"
                           class="input w-full border mt-2" id="adname">
                </div>
                <div class="text-right mt-5">
                    <button type="reset" class="button w-24 border text-gray-700 mr-1">Cancel</button>
                    <button type="submit" class="button w-24 bg-theme-1 text-white">Save</button>
                </div>

            </form>
        </div>
    </div>
</div>

