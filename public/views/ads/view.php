<?php


use FacebookAds\Object\Ad;
use FacebookAds\Object\Fields\AdFields;
use FacebookAds\Object\Fields\TargetingFields;
use FacebookAds\Object\Values\AdSetStatusValues;
use helpers\StringHelper;

global $api;

if (isset($_GET['publish'])) {
    $fields = [
        AdFields::ID,
        AdFields::NAME,
        AdFields::STATUS,
        AdFields::RECOMMENDATIONS,
        AdFields::TARGETING,
        AdFields::EFFECTIVE_STATUS,
        AdFields::BID_AMOUNT
    ];
    $params = array(
        AdFields::STATUS => $_GET['publish'] == 'true' ? AdSetStatusValues::ACTIVE : AdSetStatusValues::PAUSED
    );
    $ad = (new Ad($_GET['id']))->updateSelf(
        $fields,
        $params
    );
}
$fields = [
    AdFields::ID,
    AdFields::NAME,
    AdFields::STATUS,
    AdFields::RECOMMENDATIONS,
    AdFields::TARGETING,
    AdFields::EFFECTIVE_STATUS,
    AdFields::BID_AMOUNT
];
$params = array(
    AdFields::ID => $_GET['id']
);
$ad = (new Ad($_GET['id']))->getSelf(
    $fields,
    $params
);
$themeColor = [
    AdSetStatusValues::ACTIVE=>9,
    AdSetStatusValues::DELETED=>1,

];

?>


<div class="grid grid-cols-12 gap-6 mt-5">
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
        <h2 class="intro-y text-lg font-medium mt-10">
            View Ads "<?= $ad->{AdFields::NAME}; ?>"
        </h2>
        <button class="hiddden button  "></button>
        <div class="hidden md:block mx-auto text-gray-600"></div>
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <div class="w-86 relative text-gray-700">
                <a class="button text-white bg-theme-9 shadow-md mr-2"
                   href="/ads/view?publish=true&id=<?= $ad->{AdFields::ID}; ?>">
                    Publish
                </a>

                <a class="button text-white bg-theme-12 shadow-md mr-2"
                   href="/ads/view?publish=false&id=<?= $ad->{AdFields::ID}; ?>"> Stop </a>

                <a class="button text-white  text-gray-600 shadow-md mr-2"
                   href="/ads/update?id=<?= $ad->{AdFields::ID} ?>"> Update </a>
                <a class="button text-white bg-theme-1 shadow-md mr-2" href="/ads">Go back</a>
            </div>
        </div>
    </div>
    <!-- BEGIN: Data List -->
    <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
        <table class="table table-report -mt-2">
            <thead>
            <tr>
                <th scope="col">Field name</th>
                <th scope="col">Field value</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($fields as $field) { ?>
                <tr class="intro-x">
                    <td><?= StringHelper::underscoreToCamelCase($field); ?></td>
                    <?php
                    if ($field === AdFields::RECOMMENDATIONS && !is_null($ad->{AdFields::RECOMMENDATIONS})) {
                        $recommendation = $ad->{$field};
                        echo "<td> ";
                        for ($i = 0; $i < count($recommendation); $i++) {
                            echo " <span>" . $recommendation[$i]['title'] . " <i class='bi bi-info-circle-fill' data-bs-toggle='tooltip' data-bs-placement='right'  title='" . $recommendation[$i]['message'] . "'  /> </span>";
                        }
                        echo "</td>";
                        continue;
                    }
                    if ($field == AdFields::TARGETING && !is_null($ad->{$field})) {
                        echo "<td>";

                        echo StringHelper::underscoreToCamelCase(TargetingFields::AGE_MAX) . " : " . $ad->{$field}[TargetingFields::AGE_MAX] . "<br />";
                        echo StringHelper::underscoreToCamelCase(TargetingFields::AGE_MIN) . " : " . $ad->{$field}[TargetingFields::AGE_MIN] . "<br />";

                        if (isset($ad->{$field}[TargetingFields::GEO_LOCATIONS]['countries'])) {
                            echo StringHelper::underscoreToCamelCase(TargetingFields::GEO_LOCATIONS) . " : ";
                            $loc = $ad->{$field}[TargetingFields::GEO_LOCATIONS]['countries'];
                            for ($it = 0; $it < count($loc) - 1; $it++) {
                                echo ' <span class="badge bg-success">' . $loc[$it] . ' </span>';
                            }
                        }
                        if (isset($ad->{$field}[TargetingFields::GEO_LOCATIONS]['places'])) {
                            echo "Places : ";
                            $places = $ad->{$field}[TargetingFields::GEO_LOCATIONS]['places'];
                            foreach ($places[0] as $keyLoc => $valueLoc) {
                                echo ' <span class="badge bg-success">' . StringHelper::underscoreToCamelCase($keyLoc) . ": " . $valueLoc . ' </span>';
                            }
                        }
                        if (isset($targeting[TargetingFields::FACEBOOK_POSITIONS])) {
                            echo StringHelper::underscoreToCamelCase(TargetingFields::FACEBOOK_POSITIONS) . " : ";
                            $positions = $targeting[TargetingFields::FACEBOOK_POSITIONS];
                            for ($indx = 0; $indx < count($positions); $indx++) {
                                echo ' <span class="badge bg-success">' . StringHelper::underscoreToCamelCase($positions[$indx]) . ' </span>';
                            }
                        }
                        if (isset($targeting[TargetingFields::DEVICE_PLATFORMS])) {
                            echo StringHelper::underscoreToCamelCase(TargetingFields::DEVICE_PLATFORMS) . " : ";
                            $device = $targeting[TargetingFields::DEVICE_PLATFORMS];
                            for ($indx = 0; $indx < count($device); $indx++) {
                                echo ' <span class="badge bg-success">' . StringHelper::underscoreToCamelCase($device[$indx]) . ' </span>';
                            }
                        }
                        if (isset($targeting[TargetingFields::PUBLISHER_PLATFORMS])) {
                            echo StringHelper::underscoreToCamelCase(TargetingFields::PUBLISHER_PLATFORMS) . " : ";
                            $publisher = $targeting[TargetingFields::PUBLISHER_PLATFORMS];
                            for ($indx = 0; $indx < count($publisher); $indx++) {
                                echo ' <span class="badge bg-success">' . StringHelper::underscoreToCamelCase($publisher[$indx]) . ' </span>';
                            }
                        }
                        echo "</td>";
                        continue;
                    }

                    ?>
                    <td><?= $ad->{$field} ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>

</div>