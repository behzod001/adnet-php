<?php

use app\exceptions\DataNotFoundException;
use FacebookAds\Http\Exception\RequestException;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\AdSet;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\Fields\TargetingFields;
use FacebookAds\Object\Targeting;
use FacebookAds\Object\Values\AdEffectiveStatusValues;
use FacebookAds\Object\Values\AdSetBillingEventValues;
use FacebookAds\Object\Values\AdSetDestinationTypeValues;
use FacebookAds\Object\Values\AdSetOptimizationGoalValues;
use FacebookAds\Object\Values\CampaignSpecialAdCategoryCountryValues;
use helpers\Alert;
use helpers\StringHelper;

/**
 * @var $campaigns Campaign[]
 * @var $targeting Targeting[]
 * @var $account AdAccount[]
 */
global $app;

//all campaigns for select
try {
    $campaigns = $app->getFacebookActiveAccount()->getCampaigns(
        [
            //create fields
            CampaignFields::ID,
            CampaignFields::NAME
        ],
        [
            // create params
            'effective_status' => [
                AdEffectiveStatusValues::ACTIVE,
                AdEffectiveStatusValues::PAUSED
            ]
        ]
    );

    if (isset($_POST['Ad'])) {
        // crate targeting
        $targeting = (new Targeting())->setData([
            TargetingFields::GEO_LOCATIONS => [
                'countries' => $_POST['Ad']['AdSet'][AdSetFields::TARGETING]['countries']
            ],

            TargetingFields::FACEBOOK_POSITIONS => ['feed'],
            TargetingFields::AGE_MAX => $_POST['Ad']['AdSet'][TargetingFields::AGE_MAX],
            TargetingFields::AGE_MIN => $_POST['Ad']['AdSet'][TargetingFields::AGE_MIN],
        ]);


        $fields = [
            AdSetFields::ID,
        ];
        $params = [
            AdSetFields::NAME => $_POST['Ad']['AdSet'][AdSetFields::NAME],
            AdSetFields::DAILY_BUDGET => $_POST['Ad']['AdSet'][AdSetFields::DAILY_BUDGET],
            AdSetFields::BID_AMOUNT => $_POST['Ad']['AdSet'][AdSetFields::BID_AMOUNT],
            AdSetFields::BILLING_EVENT => $_POST['Ad']['AdSet'][AdSetFields::BILLING_EVENT],
            AdSetFields::OPTIMIZATION_GOAL => $_POST['Ad']['AdSet'][AdSetFields::OPTIMIZATION_GOAL],
            AdSetFields::CAMPAIGN_ID => $_POST['Ad']['AdSet'][AdSetFields::CAMPAIGN_ID],
            AdSetFields::DESTINATION_TYPE => $_POST['Ad']['AdSet'][AdSetFields::DESTINATION_TYPE],
            AdSetFields::STATUS => $_POST['Ad']['AdSet'][AdSetFields::STATUS],
            AdSetFields::START_TIME => (new DateTime($_POST['Ad']['AdSet'][AdSetFields::START_TIME]))->format("Y-m-d H:i:s T"),
            AdSetFields::END_TIME => (new DateTime($_POST['Ad']['AdSet'][AdSetFields::END_TIME]))->format("Y-m-d H:i:s T"),
            AdSetFields::PROMOTED_OBJECT => array('page_id' => $_POST['Ad']['AdSet']['page_id']),//"103324265290960"
            AdSetFields::TARGETING => $targeting
        ];
        $response = $app->getFacebookActiveAccount()->createAdSet($fields, $params);
        Alert::success(' Ad set successfully created for view <a class="mx-1" href="/adsets/view?id=' . $response->{AdSetFields::ID} . '"> checkout this page </a>');
    }
} catch (RequestException $e) {
    Alert::error($e->getErrorUserMessage() . ' <a  class="mx-1" href="/adsets"> checkout this page </a>');
} catch (DataNotFoundException $e) {
    Alert::warning(' У вас есть Yandex Direct аккаунт ? Тогда выберите ваш аккаунт. Если нет создайте и  <a href="/accounts"  class="mx-1 text-theme-1 mx-1"> регистрируйте. </a> ');
}

?>
<div class="intro-y flex items-center mt-8">
    <h2 class="text-lg font-medium mr-auto">
        Create new Ad Set
    </h2>
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
        <div class="hidden md:block mx-auto text-gray-600"></div>
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <a href="/campaigns" class="button text-white bg-theme-1 shadow-md mr-2">Go back</a>
        </div>
    </div>
</div>

<div class="grid grid-cols-12 gap-6 mt-5 box">

    <div class="intro-y col-span-12 lg:col-span-6 ">
        <div class="intro-y  p-5">
            <form action="/adsets/create" method="post">
                <div class="my-3">
                    <label for="<?= AdSetFields::NAME ?>"><?= StringHelper::underscoreToCamelCase(AdSetFields::NAME); ?></label>
                    <input type="text" name="<?= "Ad[AdSet][" . AdSetFields::NAME . "]" ?>"
                           class="input w-full border mt-2"
                           id="<?= AdSetFields::NAME ?>" aria-describedby=" emailHelp">
                </div>
                <div class="my-3">
                    <label for="<?= AdSetFields::CAMPAIGN_ID ?>">Select campaign</label>
                    <select name="Ad[AdSet][<?= AdSetFields::CAMPAIGN_ID ?>]"
                            id="<?= AdSetFields::CAMPAIGN_ID ?>" class="select2 w-full"
                            aria-label="Default select example">
                        <option disabled selected>Select Campaign</option>
                        <?php foreach ($campaigns as $value) { ?>
                            <option value='<?= $value->{CampaignFields::ID} ?>'> <?= $value->{CampaignFields::NAME} ?> </option>
                        <?php } ?>
                    </select>
                </div>
                <div class="my-3">
                    <label for="<?= AdSetFields::DAILY_BUDGET ?>"><?= StringHelper::underscoreToCamelCase(AdSetFields::DAILY_BUDGET) ?></label>
                    <input type="text"
                           name="<?= "Ad[AdSet][" . AdSetFields::DAILY_BUDGET . "]" ?>"
                           class="input w-full border mt-2"
                           id="<?= AdSetFields::DAILY_BUDGET ?>"
                           aria-describedby="<?= AdSetFields::DAILY_BUDGET ?>">
                    <div id="<?= AdSetFields::DAILY_BUDGET ?>"> Your ad set budget is the daily or lifetime
                        amount you want to spend on this ad set.
                        A daily budget is the average you'll spend every day. A lifetime budget is the maximum
                        you'll
                        spend during the lifetime of your ad set.
                    </div>
                </div>
                <!--                <div class="col-span-1 sm:col-span-1">-->
                <!--                    <label for="--><? //= CampaignFieldEnum::START_DATE ?><!--">From</label>-->
                <!--                    <input class="datepicker input w-full border mt-2" id="-->
                <? //= CampaignFieldEnum::START_DATE ?><!--"-->
                <!--                           name="Campaigns[--><? //= CampaignFieldEnum::START_DATE ?><!--]">-->
                <!---->
                <!--                    <div class="text-xs text-gray-600 mt-2">Дата начала показов объявлений в формате YYYY-MM-DD.-->
                <!--                        Должна быть не меньше текущей даты. Показы объявлений начинаются в 00:00 по московскому-->
                <!--                        времени (независимо от значения параметра TimeZone). На время начала показов влияют-->
                <!--                        настройки временного таргетинга (параметр TimeTargeting). Показы объявлений возможны при-->
                <!--                        условии, что хотя бы одно объявление принято модерацией и внесены средства на кампанию или-->
                <!--                        на общий счет.-->
                <!--                    </div>-->
                <!--                </div>-->

                <div class="my-3 ">
                    <label class="input-group-text"> <?= StringHelper::underscoreToCamelCase(AdSetFields::START_TIME) ?></label>
                    <input class="datepicker input w-full border mt-2" id="<?= AdSetFields::START_TIME ?>"
                           name="Ad[AdSet][<?= AdSetFields::START_TIME ?>]">
                </div>
                <div class="mb-3">
                    <label for="<?= AdSetFields::OPTIMIZATION_GOAL ?>"
                           class=" form-label"><?= StringHelper::underscoreToCamelCase(AdSetFields::OPTIMIZATION_GOAL) ?></label>
                    <select name="<?= "Ad[AdSet][" . AdSetFields::OPTIMIZATION_GOAL . "]" ?>"
                            id="<?= AdSetFields::OPTIMIZATION_GOAL ?>" class="select2 w-full"
                            aria-label="Default select example">
                        <option disabled selected> Open this select menu</option>
                        <?php foreach (AdSetOptimizationGoalValues::getInstance()->getValues() as $value) {
                            echo "<option value='" . $value . "'>" . StringHelper::underscoreToCamelCase($value) . "</option>";
                        } ?>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="<?= AdSetFields::STATUS ?>"
                    ><?= StringHelper::underscoreToCamelCase(AdSetFields::STATUS) ?></label>
                    <select name="<?= "Ad[AdSet][" . AdSetFields::STATUS . "]" ?>"
                            id="<?= AdSetFields::STATUS ?>" class="select2 w-full"
                            aria-label="Default select example">
                        <option disabled selected>Select audience location</option>
                        <option value='<?= AdEffectiveStatusValues::ACTIVE ?>'><?= StringHelper::underscoreToCamelCase(AdEffectiveStatusValues::ACTIVE )?></option>
                        <option value='<?= AdEffectiveStatusValues::PAUSED ?>'> <?= StringHelper::underscoreToCamelCase(AdEffectiveStatusValues::PAUSED) ?></option>
                    </select>
                </div>
                <div class="mb-3">
                    <label class="input-group-text"> <?= StringHelper::underscoreToCamelCase(TargetingFields::AGE_MAX) ?></label>
                    <input class="input w-full border mt-2" type="number" id="<?= TargetingFields::AGE_MAX ?>"
                           name="Ad[AdSet][<?= TargetingFields::AGE_MAX ?>]">
                </div>
        </div>
    </div>

    <div class="intro-y col-span-12 lg:col-span-6">

        <div class="intro-y p-5">
            <!--        Form BEGIN  -->

                <div class="my-3">
                    <label for="page_id">Page id </label>
                    <input type="text" class="input w-full border mt-2"
                           name="<?= "Ad[AdSet][page_id]" ?>"
                           value="103324265290960"
                           id="page_id" aria-describedby="page_id">
                    <div id="page_id">For Ad Network manager "103324265290960"</div>
                </div>

                <div class="my-3">
                    <label for="<?= AdSetFields::DESTINATION_TYPE ?>"
                    >Select <?= StringHelper::underscoreToCamelCase(AdSetFields::DESTINATION_TYPE) ?></label>
                    <select name="Ad[AdSet][<?= AdSetFields::DESTINATION_TYPE ?>]"
                            id="<?= AdSetFields::DESTINATION_TYPE ?>" class="select2 w-full"
                            aria-label="Default select example">
                        <option selected disabled>
                            Select <?= StringHelper::underscoreToCamelCase(AdSetFields::DESTINATION_TYPE) ?></option>
                        <?php foreach (AdSetDestinationTypeValues::getInstance()->getValues() as $value) { ?>
                            <option value='<?= $value ?>'> <?= StringHelper::underscoreToCamelCase($value) ?> </option>
                        <?php } ?>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="<?= AdSetFields::BID_AMOUNT ?>"
                    ><?= StringHelper::underscoreToCamelCase(AdSetFields::BID_AMOUNT) ?></label>
                    <input type="text"
                           name="<?= "Ad[AdSet][" . AdSetFields::BID_AMOUNT . "]" ?>"
                           class="input w-full border mt-2"
                           id="<?= AdSetFields::BID_AMOUNT ?>" aria-describedby=" emailHelp">
                </div>

                <div class="my-3 ">
                    <label class="input-group-text"> <?= StringHelper::underscoreToCamelCase(AdSetFields::END_TIME) ?></label>
                    <input class="datepicker input w-full border mt-2" id="<?= AdSetFields::END_TIME ?>"
                           name="Ad[AdSet][<?= AdSetFields::END_TIME ?>]">
                </div>

                <div class="mb-3">
                    <label for="<?= AdSetFields::BILLING_EVENT ?>"
                           class=" form-label"><?= StringHelper::underscoreToCamelCase(AdSetFields::BILLING_EVENT) ?></label>
                    <select name="<?= "Ad[AdSet][" . AdSetFields::BILLING_EVENT . "]" ?>"
                            id="<?= AdSetFields::BILLING_EVENT ?>" class="select2 w-full"
                            aria-label="Default select example">
                        <option disabled selected>Open this select menu</option>
                        <?php foreach (AdSetBillingEventValues::getInstance()->getValues() as $value) {
                            echo "<option value='" . $value . "'>" . StringHelper::underscoreToCamelCase($value) . "</option>";
                        } ?>
                    </select>
                </div>


                <div class="mb-3">
                    <label for="<?= AdSetFields::TARGETING ?>"
                           class=" form-label"><?= StringHelper::underscoreToCamelCase(TargetingFields::GEO_LOCATIONS) ?></label>
                    <select multiple aria-label="multiple select example"
                            name="<?= "Ad[AdSet][" . AdSetFields::TARGETING . "][countries][]" ?>"
                            id="<?= AdSetFields::TARGETING ?>" class="select2 w-full"
                            aria-label="Default select example">
                        <option disabled> Select audience location</option>
                        <?php foreach (CampaignSpecialAdCategoryCountryValues::getInstance()->getValues() as $value) {
                            echo "<option value='" . $value . "'>" . StringHelper::underscoreToCamelCase($value) . "</option>";
                        } ?>
                    </select>
                </div>
                <label class="form-label" for="<?= TargetingFields::AGE_MAX ?>">Select the minimum and maximum
                    age of the people who will find your ad relevant.</label>

                <div class="mb-3">
                    <label class="input-group-text"> <?= StringHelper::underscoreToCamelCase(TargetingFields::AGE_MIN) ?></label>
                    <input class="input w-full border mt-2" type="number" id="<?= TargetingFields::AGE_MIN ?>"
                           name="Ad[AdSet][<?= TargetingFields::AGE_MIN ?>]">
                </div>


                <div class="text-right mt-5">
                    <button type="reset" class="button w-24 border text-gray-700 mr-1">Cancel</button>
                    <button type="submit" class="button w-24 bg-theme-1 text-white">Save</button>
                </div>
            </form>
        </div>
    </div>

</div>
