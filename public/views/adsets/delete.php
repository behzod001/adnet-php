<?php

global $api;

use FacebookAds\Object\AdSet;
use helpers\Alert;

try {
    $adSet = new AdSet($_GET['id'], null, $api);
    if ($adSet->deleteSelf())
        Alert::success(' AdSet successfully deleted. For continue <a class="mx-1" href="/adsets"> checkout this page </a>');
} catch (Exception $e) {
    Alert::error($e->getMessage() . ' <a class="mx-1" href="/adsets"> checkout this page </a> ');
}