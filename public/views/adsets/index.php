<?php

use app\exceptions\DataNotFoundException;
use directapi\exceptions\DirectAccountNotExistException;
use directapi\exceptions\DirectApiException;
use directapi\exceptions\DirectApiNotEnoughUnitsException;
use directapi\exceptions\RequestValidationException;
use directapi\exceptions\UnknownPropertyException;
use directapi\services\adgroups\criterias\AdGroupsSelectionCriteria;
use directapi\services\adgroups\enum\AdGroupFieldEnum;
use directapi\services\campaigns\criterias\CampaignsSelectionCriteria;
use directapi\services\campaigns\enum\CampaignFieldEnum;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Fields\TargetingFields;
use FacebookAds\Object\Values\AdSetStatusValues;
use GuzzleHttp\Exception\GuzzleException;
use helpers\Alert;
use helpers\StringHelper;

global $app;
$adGroupsPrint = $adSetPrint = false;

try {
    $fields = [
        AdSetFields::ID,
        AdSetFields::NAME,
        AdSetFields::STATUS,
        AdSetFields::RECOMMENDATIONS,
        AdSetFields::TARGETING,
        AdSetFields::DAILY_BUDGET,
        AdSetFields::BID_AMOUNT,
    ];
    $params = array(
        AdSetFields::EFFECTIVE_STATUS => [
            AdSetStatusValues::ACTIVE,
            AdSetStatusValues::PAUSED
        ]
    );
    $ads = $app->getFacebookActiveAccount()->getAdSets(
        $fields,
        $params
    );

// getting yandex direct ad groups
    $campaignFieldNames = [
        CampaignFieldEnum::NAME,
        CampaignFieldEnum::ID
    ];
    $campaigns = $app->getDirect()->getCampaignsService()->get(new CampaignsSelectionCriteria(), $campaignFieldNames);
    $CampaignIds = [];
    foreach ($campaigns as $campaign) {
        array_push($CampaignIds, $campaign->{CampaignFieldEnum::ID});
    }

    if (count($CampaignIds) > 0) {
        $selectionCriteria = new AdGroupsSelectionCriteria([
            "CampaignIds" => $CampaignIds,
            "Statuses" => ["ACCEPTED", "DRAFT", "MODERATION", "PREACCEPTED", "REJECTED"]
        ]);
        $fieldNames = [
            AdGroupFieldEnum::ID,
            AdGroupFieldEnum::NAME,
            AdGroupFieldEnum::STATUS,
            AdGroupFieldEnum::CAMPAIGN_ID,
            AdGroupFieldEnum::REGION_IDS,
            AdGroupFieldEnum::STATUS,
            AdGroupFieldEnum::NEGATIVE_KEYWORDS,

        ];
        $adGroups = $app->getDirect()->getAdGroupsService()->get($selectionCriteria, $fieldNames);
        $adGroupsPrint = true;
    }

} catch (DataNotFoundException $e) {
    Alert::warning(' У вас есть Yandex direct аккаунт ? Тогда выберите ваш аккаунт. Если нет создайте и  <a href="/accounts" class="text-theme-1 mx-1"> регистрируйте. </a> ');
} catch (UnknownPropertyException $e) {
    Alert::error($e->getMessage() . ' Unknown property set <a href="/adsets" class="text-theme-1 mx-1"> reload this page </a> ');
} catch (GuzzleException | DirectAccountNotExistException | DirectApiNotEnoughUnitsException | RequestValidationException | DirectApiException $e) {
    Alert::error($e->getMessage() . ' <a href="/adsets" class="text-theme-1 mx-1"> reload this page </a> ');
}


?>
<h2 class="intro-y text-lg font-medium mt-10">
    Data List Ad Sets
</h2>
<div class="grid grid-cols-12 gap-6 mt-5">
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
        <button class="button text-white bg-theme-1 shadow-md mr-2">Add New Service account</button>
        <div class="dropdown relative ">
            <button class="dropdown-toggle button px-2 box text-gray-700">
            <span class="w-5 h-5 flex items-center justify-center">
                <i class="w-4 h-4" data-feather="plus"></i>
            </span>
            </button>
            <div class="dropdown-box mt-10 absolute top-0 left-0 z-20" style="width: 250px">
                <div class="dropdown-box__content box p-2">
                    <a href="/adsets/create"
                       class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                        <i data-feather="facebook" class="w-4 h-4 mr-2"></i> Facebook adset </a>
                    <a href="/direct/adset/create"
                       class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                        <i data-feather="shield" class="w-4 h-4 mr-2"></i>Yandex Direct ad group</a>
                    <a href="javascript:" data-toggle="modal"
                       data-target="#add-google-account"
                       class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                        <i data-feather="map-pin" class="w-4 h-4 mr-2"></i>Google Ads ad group </a>
                </div>
            </div>
        </div>
        <div class="hidden md:block mx-auto text-gray-600">Showing 1 to 10 of 150 entries</div>
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <div class="w-56 relative text-gray-700">
                <input type="text" class="input w-56 box pr-10 placeholder-theme-13" placeholder="Search...">
                <i class="w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0" data-feather="search"></i>
            </div>
        </div>
    </div>

    <!-- BEGIN: Data List -->
    <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
        <table class="table table-report -mt-2">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name Ad Set</th>
                <?php unset($fields[0], $fields[1], $fields[2]);
                foreach ($fields as $field) { ?>
                    <th scope="col"><?= StringHelper::underscoreToCamelCase($field); ?></th>
                <?php } ?>
                <th scope="col">Status</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>

            <?php if (isset($ads)) {
                $idx = 0;
                foreach ($ads as $adSet) { ?>
                    <tr class="intro-x">
                        <td class="w-40">
                            <div class="flex">
                                <div class="w-10 h-10 image-fit zoom-in">
                                    <img alt="Ad Network testing project" class="tooltip rounded-full"
                                         src="/resources/dist/images/preview-4.jpg" title="Uploaded at 17 July 2021">
                                </div>
                                <div class="w-10 h-10 image-fit zoom-in -ml-5">
                                    <img alt="Ad Network testing project" class="tooltip rounded-full"
                                         src="/resources/dist/images/preview-4.jpg" title="Uploaded at 17 July 2021">
                                </div>
                                <div class="w-10 h-10 image-fit zoom-in -ml-5">
                                    <img alt="Ad Network testing project" class="tooltip rounded-full"
                                         src="/resources/dist/images/preview-10.jpg" title="Uploaded at 17 July 2021">
                                </div>
                            </div>
                        </td>
                        <td>
                            <a href="/adsets/view?id=<?= $adSet->{AdSetFields::ID} ?>"
                               class="font-medium whitespace-no-wrap"> <?= StringHelper::underscoreToCamelCase($adSet->{AdSetFields::NAME}); ?>
                                <div class="text-gray-600 text-xs whitespace-no-wrap">
                                    <span class="bg-theme-14 text-theme-10 rounded px-2 mt-1 text-center"> Facebook </span>
                                    <?= $adSet->{AdSetFields::ID} ?>
                                </div>
                            </a>
                        </td>
                        <?php foreach ($fields as $field) {
                            if ($field == AdSetFields::RECOMMENDATIONS && is_array($adSet->{$field})) {
                                echo " <td>";
                                $itemIdx = 0;
                                foreach ($adSet->{$field} as $r) {
                                    echo ++$itemIdx . ')  <span> ' . $r['title'] . ' <i data-feather="alert-circle" data-theme="light" class="tooltip inline-block text-black" title="' . $r['message'] . '"> </i></span> <br />';
                                }
                                echo "</td>";
                                continue;
                            }
                            if ($field == AdSetFields::TARGETING && is_array($adSet->{$field})) {
                                echo " <td>";
                                $itemIdx = 0;
                                $item = $adSet->{$field};
                                echo StringHelper::underscoreToCamelCase(TargetingFields::AGE_MAX) . " <span> " . $item[TargetingFields::AGE_MAX] . "</span><br />";
                                echo StringHelper::underscoreToCamelCase(TargetingFields::AGE_MIN) . " <span> " . $item[TargetingFields::AGE_MIN] . "</span><br />";
                                if($item[TargetingFields::GEO_LOCATIONS]){echo StringHelper::underscoreToCamelCase(TargetingFields::GEO_LOCATIONS) . " <span> " . implode(", ", $item[TargetingFields::GEO_LOCATIONS]['location_types']) . "</span><br />";}
                                if(isset($item[TargetingFields::GEO_LOCATIONS][TargetingFields::COUNTRIES]))echo StringHelper::underscoreToCamelCase(TargetingFields::GEO_LOCATIONS) . " <span> " . implode(", ", $item[TargetingFields::GEO_LOCATIONS][TargetingFields::COUNTRIES]) . "</span><br />";
                                if (isset($item[TargetingFields::FACEBOOK_POSITIONS])) echo StringHelper::underscoreToCamelCase(TargetingFields::FACEBOOK_POSITIONS) . " <span> " . implode(", ", $item[TargetingFields::FACEBOOK_POSITIONS]) . "</span><br />";
                                echo "</td>";
                                continue;
                            }
                            ?>
                            <td><?= StringHelper::underscoreToCamelCase($adSet->{$field}); ?></td>
                        <?php } ?>
                        <td class="w-40">
                            <div class="flex items-center justify-center text-theme-6">
                                <i data-feather="check-square" class="w-4 h-4 mr-2"></i>
                                <?= $adSet->{AdSetFields::STATUS} ?>
                            </div>
                        </td>
                        <td class="table-report__action w-56">
                            <div class="flex justify-center items-center">
                                <a class="flex items-center mr-3" href="/adsets/view?id=<?= $adSet->id ?>"> <i
                                            data-feather="eye" class="w-4 h-4 mr-1"></i> View </a>
                                <a class="flex items-center mr-3" href="/adsets/update?id=<?= $adSet->id ?>"> <i
                                            data-feather="check-square" class="w-4 h-4 mr-1"></i> Edit </a>
                                <a class="flex items-center text-theme-6" href="javascript:" data-toggle="modal"
                                   data-delete="/adsets/delete?id=<?= $adSet->{AdSetFields::ID} ?>"
                                   onclick="confirmDelete(this);"
                                   data-target="#delete-confirmation-modal"> <i data-feather="trash-2"
                                                                                class="w-4 h-4 mr-1"></i>
                                    Delete </a>
                            </div>
                        </td>
                    </tr>
                <?php }
            } ?>
            <?php $idx = 0;
            if (isset($fieldNames, $adGroups)) {
                unset($fieldNames[0], $fieldNames[1], $fieldNames[2]);
                foreach ($adGroups as $adGroup) { ?>
                    <tr>
                        <td class="w-40">
                            <div class="flex">
                                <div class="w-10 h-10 image-fit zoom-in">
                                    <img alt="Ad Network testing project" class="tooltip rounded-full"
                                         src="/resources/dist/images/preview-4.jpg" title="Uploaded at 17 July 2021">
                                </div>
                                <div class="w-10 h-10 image-fit zoom-in -ml-5">
                                    <img alt="Ad Network testing project" class="tooltip rounded-full"
                                         src="/resources/dist/images/preview-4.jpg" title="Uploaded at 17 July 2021">
                                </div>
                                <div class="w-10 h-10 image-fit zoom-in -ml-5">
                                    <img alt="Ad Network testing project" class="tooltip rounded-full"
                                         src="/resources/dist/images/preview-10.jpg" title="Uploaded at 17 July 2021">
                                </div>
                            </div>
                        </td>
                        <td>
                            <a href="/direct/adset/view?id=<?= $adGroup->{AdGroupFieldEnum::ID} ?>"
                               class="font-medium whitespace-no-wrap"> <?= StringHelper::underscoreToCamelCase($adGroup->{AdGroupFieldEnum::NAME}); ?>
                                <div class="text-gray-600 text-xs whitespace-no-wrap">
                                    <span class="bg-theme-17 text-theme-11 rounded px-2 mt-1 text-center">Yandex direct</span>
                                    <?= $adGroup->{AdGroupFieldEnum::ID} ?>
                                </div>
                            </a>
                        </td>
                        <?php foreach ($fieldNames as $field) {
                            if ($field == AdGroupFieldEnum::REGION_IDS && is_array($adGroup->{$field})) {
                                echo " <td>";
                                $itemIdx = 0;
                                while (isset($adGroup->{$field}[$itemIdx])) {
                                    $regionId = $adGroup->{$field}[$itemIdx];
                                    echo " <span class='badge bg-warning text-dark' >" . $regionId . " <i class='bi bi-info-circle-fill m-2' data-bs-toggle='tooltip' data-bs-placement='right' title='" . $regionId . "' ></i> </span> ";
                                    $itemIdx++;
                                }
                                echo "</td>";
                                continue;
                            }
                            if ($field == AdGroupFieldEnum::NEGATIVE_KEYWORDS && !is_null($adGroup->{$field})) {
                                echo " <td>";
                                $items = $adGroup->{$field}->Items;
                                $keyIdx = 0;
                                while (isset($items[$keyIdx]))
                                    echo " <span class='badge bg-warning text-dark' >" . $items[$keyIdx++] . " </span> ";
                                echo "</td>";
                                continue;
                            }
                            ?>
                            <td><?= StringHelper::underscoreToCamelCase($adGroup->{$field}); ?></td>
                        <?php } ?>
                        <td class="w-40">
                            <div class="flex items-center justify-center text-theme-6">
                                <i data-feather="check-square" class="w-4 h-4 mr-2"></i>
                                <?= $adGroup->{AdGroupFieldEnum::STATUS} ?>
                            </div>
                        </td>
                        <td class="table-report__action w-56">
                            <div class="flex justify-center items-center">
                                <a class="flex items-center mr-3"
                                   href="/direct/adset/view?id=<?= $adGroup->{CampaignFieldEnum::ID} ?>"> <i
                                            data-feather="eye" class="w-4 h-4 mr-1"></i> View </a>
                                <a class="flex items-center mr-3"
                                   href="/direct/adset/update?id=<?= $adGroup->{CampaignFieldEnum::ID} ?>"> <i
                                            data-feather="check-square" class="w-4 h-4 mr-1"></i> Edit </a>
                                <a class="flex items-center text-theme-6" href="javascript:" data-toggle="modal"
                                   data-delete="/adsets/delete?id=<?= $adGroup->{CampaignFieldEnum::ID} ?>"
                                   onclick="confirmDelete(this);"
                                   data-target="#delete-confirmation-modal">
                                    <i data-feather="trash-2" class="w-4 h-4 mr-1"></i>
                                    Delete
                                </a>
                            </div>
                        </td>
                    </tr>
                <?php }
            } ?>
            </tbody>
        </table>
    </div>
    <!-- END: Data List -->
    <!-- BEGIN: Pagination -->
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-row sm:flex-no-wrap items-center">
        <ul class="pagination">
            <li>
                <a class="pagination__link" href=""> <i class="w-4 h-4" data-feather="chevrons-left"></i> </a>
            </li>
            <li>
                <a class="pagination__link" href=""> <i class="w-4 h-4" data-feather="chevron-left"></i> </a>
            </li>
            <li><a class="pagination__link" href="">...</a></li>
            <li><a class="pagination__link pagination__link--active" href="">1</a></li>
            <li><a class="pagination__link" href="">2</a></li>
            <li><a class="pagination__link" href="">3</a></li>
            <li><a class="pagination__link" href="">...</a></li>
            <li>
                <a class="pagination__link" href=""> <i class="w-4 h-4" data-feather="chevron-right"></i> </a>
            </li>
            <li>
                <a class="pagination__link" href=""> <i class="w-4 h-4" data-feather="chevrons-right"></i> </a>
            </li>
        </ul>
        <select class="w-20 input box mt-3 sm:mt-0">
            <option>10</option>
            <option>25</option>
            <option>35</option>
            <option>50</option>
        </select>
    </div>
    <!-- END: Pagination -->
</div>
<!-- BEGIN: Delete Confirmation Modal -->
<div class="modal" id="delete-confirmation-modal">
    <div class="modal__content">
        <div class="p-5 text-center">
            <i data-feather="x-circle" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i>
            <div class="text-3xl mt-5">Are you sure?</div>
            <div class="text-gray-600 mt-2">Do you really want to delete these records? This process cannot be undone.
            </div>
        </div>
        <div class="px-5 pb-8 text-center">
            <button type="button" data-dismiss="modal" class="button w-24 border text-gray-700 mr-1">Cancel</button>
            <a href="" id="deleteBtn" type="button" class="button w-24 bg-theme-6 text-white">Delete</a>
        </div>
    </div>
</div>
<!-- END: Delete Confirmation Modal -->

