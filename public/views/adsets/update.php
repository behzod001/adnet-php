<?php

use FacebookAds\Http\Exception\ClientException;
use FacebookAds\Http\Exception\RequestException;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\Fields\AdSet;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\Fields\TargetingFields;
use FacebookAds\Object\Targeting;
use FacebookAds\Object\Values\AdEffectiveStatusValues;
use FacebookAds\Object\Values\AdSetBillingEventValues;
use FacebookAds\Object\Values\AdSetDestinationTypeValues;
use FacebookAds\Object\Values\AdSetOptimizationGoalValues;
use FacebookAds\Object\Values\CampaignSpecialAdCategoryCountryValues;
use helpers\Alert;
use helpers\StringHelper;

/**
 * @var $campaigns Campaign[]
 * @var $targeting Targeting[]
 * @var $account AdAccount[]
 */
global $api;
$fields = [
    AdSetFields::ID,
    AdSetFields::NAME,
    AdSetFields::STATUS,
    AdSetFields::RECOMMENDATIONS,
    AdSetFields::TARGETING,
    AdSetFields::DAILY_BUDGET,
    AdSetFields::EFFECTIVE_STATUS,
    AdSetFields::BID_AMOUNT,
    AdSetFields::BILLING_EVENT,
    AdSetFields::PROMOTED_OBJECT,
];

$set = new \FacebookAds\Object\AdSet($_GET['id']);

try {
    $adSet = $set->getSelf($fields);


    if (isset($_POST['Ad'])) {
        // crate targeting
        $targeting = (new Targeting())->setData([
            TargetingFields::GEO_LOCATIONS => [
                'countries' => $_POST['Ad']['AdSet'][AdSetFields::TARGETING]['countries']
            ],

            TargetingFields::FACEBOOK_POSITIONS => ['feed'],
            TargetingFields::AGE_MAX => $_POST['Ad']['AdSet'][TargetingFields::AGE_MAX],
            TargetingFields::AGE_MIN => $_POST['Ad']['AdSet'][TargetingFields::AGE_MIN],
        ]);
        $fields = [
            AdSetFields::ID,
        ];
        $params = [
            AdSetFields::NAME => $_POST['Ad']['AdSet'][AdSetFields::NAME],
            AdSetFields::CAMPAIGN_ID => $adSet->{AdSetFields::CAMPAIGN_ID},
            AdSetFields::DAILY_BUDGET => $_POST['Ad']['AdSet'][AdSetFields::DAILY_BUDGET],
            AdSetFields::BID_AMOUNT => $_POST['Ad']['AdSet'][AdSetFields::BID_AMOUNT],
            AdSetFields::BILLING_EVENT => $_POST['Ad']['AdSet'][AdSetFields::BILLING_EVENT],
            AdSetFields::OPTIMIZATION_GOAL => $_POST['Ad']['AdSet'][AdSetFields::OPTIMIZATION_GOAL],
            AdSetFields::DESTINATION_TYPE => $_POST['Ad']['AdSet'][AdSetFields::DESTINATION_TYPE],
            AdSetFields::STATUS => $_POST['Ad']['AdSet'][AdSetFields::STATUS],
            AdSetFields::PROMOTED_OBJECT => array('page_id' => $_POST['Ad']['AdSet']['page_id']),//"103324265290960"
            AdSetFields::TARGETING => $targeting
        ];
        $response = $adSet->updateSelf($fields, $params);
        Alert::success(' Ad set successfully updated for view <a href="/adsets/view?id=' . $response->{AdSetFields::ID} . '">checkout this page</a>');
    }
} catch (RequestException $e) {
    Alert::error($e->getErrorUserMessage());
}
?>

<div class="intro-y flex items-center mt-8">
    <h2 class="text-lg font-medium mr-auto">
        Update new Ad Set "<?= $adSet->{AdSetFields::NAME} ?>"
    </h2>
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
        <div class="hidden md:block mx-auto text-gray-600"></div>
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <a href="/adsets" class="button text-white bg-theme-1 shadow-md mr-2">Go back</a>
        </div>
    </div>
</div>

<div class="grid grid-cols-12 gap-6 mt-5 box">

    <div class="intro-y col-span-12 lg:col-span-6 ">
        <div class="intro-y  p-5">
            <form action="/adsets/update?id=<?= $_GET['id'] ?>" method="post">

                <div class="my-3">
                    <label for="<?= AdSetFields::NAME ?>"><?= StringHelper::underscoreToCamelCase(AdSetFields::NAME); ?></label>
                    <input type="text" name="<?= "Ad[AdSet][" . AdSetFields::NAME . "]" ?>"
                           class="input w-full border mt-2" value="<?= $adSet->{AdSetFields::NAME} ?>"
                           id="<?= AdSetFields::NAME ?>">
                </div>

                <div class="my-3">
                    <label for="<?= AdSetFields::DAILY_BUDGET ?>"><?= StringHelper::underscoreToCamelCase(AdSetFields::DAILY_BUDGET) ?></label>
                    <input type="text" name="<?= "Ad[AdSet][" . AdSetFields::DAILY_BUDGET . "]" ?>"
                           class="input w-full border mt-2" value="<?= $adSet->{AdSetFields::DAILY_BUDGET} ?>"
                           id="<?= AdSetFields::DAILY_BUDGET ?>">
                    <div class="text-xs text-gray-600 mt-2"> Your ad set budget is the daily or lifetime
                        amount you want to spend on this ad set.
                        A daily budget is the average you'll spend every day. A lifetime budget is the maximum
                        you'll
                        spend during the lifetime of your ad set.
                    </div>
                </div>

                <div class="mb-3">
                    <label for="<?= AdSetFields::OPTIMIZATION_GOAL ?>">
                        <?= StringHelper::underscoreToCamelCase(AdSetFields::OPTIMIZATION_GOAL) ?>
                    </label>
                    <select name="<?= "Ad[AdSet][" . AdSetFields::OPTIMIZATION_GOAL . "]" ?>"
                            id="<?= AdSetFields::OPTIMIZATION_GOAL ?>" class="select2 w-full">
                        <option disabled> Open this select menu</option>
                        <?php foreach (AdSetOptimizationGoalValues::getInstance()->getValues() as $value) { ?>
                            <option value='<?= $value ?>'
                                <?= ($adSet->{AdSetFields::OPTIMIZATION_GOAL} == $value) ? 'selected' : "" ?> >
                                <?= $value ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="<?= AdSetFields::STATUS ?>">
                        <?= StringHelper::underscoreToCamelCase(AdSetFields::STATUS) ?>
                    </label>
                    <select name="<?= "Ad[AdSet][" . AdSetFields::STATUS . "]" ?>"
                            id="<?= AdSetFields::STATUS ?>" class="select2 w-full">
                        <option disabled>Select audience location</option>
                        <option <?= ($adSet->{AdSetFields::STATUS} == AdEffectiveStatusValues::ACTIVE) ? "selected" : ""; ?>
                                value='<?= AdEffectiveStatusValues::ACTIVE ?>'><?= AdEffectiveStatusValues::ACTIVE ?></option>
                        <option <?= ($adSet->{AdSetFields::STATUS} == AdEffectiveStatusValues::PAUSED) ? "selected" : ""; ?>
                                value='<?= AdEffectiveStatusValues::PAUSED ?>'>
                            <?= AdEffectiveStatusValues::PAUSED ?>
                        </option>
                    </select>
                </div>
                <div class="mb-3">
                    <label class="input-group-text"> <?= StringHelper::underscoreToCamelCase(TargetingFields::AGE_MAX) ?></label>
                    <input class="input w-full border mt-2" type="number" id="<?= TargetingFields::AGE_MAX ?>"
                           name="Ad[AdSet][<?= TargetingFields::AGE_MAX ?>]"
                           value="<?= $adSet->{AdSetFields::TARGETING}[TargetingFields::AGE_MAX] ?>">
                </div>
        </div>
    </div>

    <div class="intro-y col-span-12 lg:col-span-6">
        <div class="intro-y p-5">
            <!--        Form BEGIN  -->
            <div class="my-3">
                <label for="page_id">Page id </label>
                <input type="text" class="input w-full border mt-2"
                       name="<?= "Ad[AdSet][page_id]" ?>"
                       value="<?= $adSet->{AdSetFields::PROMOTED_OBJECT}['page_id'] ?>"
                       id="page_id">
                <div class="text-xs text-gray-600 mt-2">For Ad Network manager "103324265290960"</div>
            </div>

            <div class="my-3">
                <label for="<?= AdSetFields::DESTINATION_TYPE ?>">
                    Select <?= StringHelper::underscoreToCamelCase(AdSetFields::DESTINATION_TYPE) ?>
                </label>
                <select name="Ad[AdSet][<?= AdSetFields::DESTINATION_TYPE ?>]" id="<?= AdSetFields::DESTINATION_TYPE ?>"
                        class="select2 w-full">
                    <option disabled>
                        Select <?= StringHelper::underscoreToCamelCase(AdSetFields::DESTINATION_TYPE) ?>
                    </option>
                    <?php foreach (AdSetDestinationTypeValues::getInstance()->getValues() as $value) { ?>
                        <option <?= ($adSet->{AdSetFields::DESTINATION_TYPE} == $value) ?>
                                value='<?= $value ?>'> <?= $value ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
            <div class="mb-3">
                <label for="<?= AdSetFields::BID_AMOUNT ?>">
                    <?= StringHelper::underscoreToCamelCase(AdSetFields::BID_AMOUNT) ?>
                </label>
                <input type="text" name="<?= "Ad[AdSet][" . AdSetFields::BID_AMOUNT . "]" ?>"
                       class="input w-full border mt-2" value="<?= $adSet->{AdSetFields::BID_AMOUNT} ?>"
                       id="<?= AdSetFields::BID_AMOUNT ?>">
            </div>

            <div class="mb-3">
                <label for="<?= AdSetFields::BILLING_EVENT ?>">
                    <?= StringHelper::underscoreToCamelCase(AdSetFields::BILLING_EVENT) ?>
                </label>
                <select name="<?= "Ad[AdSet][" . AdSetFields::BILLING_EVENT . "]" ?>"
                        id="<?= AdSetFields::BILLING_EVENT ?>" class="select2 w-full">
                    <option disabled selected>Open this select menu</option>
                    <?php foreach (AdSetBillingEventValues::getInstance()->getValues() as $value) { ?>
                        <option <?= ($adSet->{AdSetFields::BILLING_EVENT} == $value) ? "" : "" ?> value='<?= $value ?>'>
                            <?= $value ?>
                        </option>
                    <?php } ?>
                </select>
            </div>

            <div class="mb-3">
                <label for="<?= AdSetFields::TARGETING ?>">
                    <?= StringHelper::underscoreToCamelCase(TargetingFields::GEO_LOCATIONS) ?>
                </label>
                <select multiple name="<?= "Ad[AdSet][" . AdSetFields::TARGETING . "][countries][]" ?>"
                        id="<?= AdSetFields::TARGETING ?>" class="select2 w-full">
                    <option disabled> Select audience location</option>
                    <?php foreach (CampaignSpecialAdCategoryCountryValues::getInstance()->getValues() as $value) { ?>
                        <option <?= (in_array($value, $adSet->{AdSetFields::TARGETING}[TargetingFields::GEO_LOCATIONS][TargetingFields::COUNTRIES])) ? "selected" : "" ?>
                                value='<?= $value ?>'><?= $value ?></option>
                    <?php } ?>
                </select>
            </div>
            <label class="form-label" for="<?= TargetingFields::AGE_MAX ?>">
                Select the minimum and maximum age of the people who will find your ad relevant.
            </label>

            <div class="mb-3">
                <label class="input-group-text"> <?= StringHelper::underscoreToCamelCase(TargetingFields::AGE_MIN) ?></label>
                <input class="input w-full border mt-2" type="number" id="<?= TargetingFields::AGE_MIN ?>"
                       name="Ad[AdSet][<?= TargetingFields::AGE_MIN ?>]"
                       value="<?= $adSet->{AdSetFields::TARGETING}[TargetingFields::AGE_MIN]; ?>">
            </div>

            <div class="text-right mt-5">
                <button type="reset" class="button w-24 border text-gray-700 mr-1">Cancel</button>
                <button type="submit" class="button w-24 bg-theme-1 text-white">Save</button>
            </div>
            </form>
        </div>
    </div>

</div>
