<?php


global $app;

use app\exceptions\DataNotFoundException;
use FacebookAds\Cursor;
use FacebookAds\Http\Exception\ClientException;
use FacebookAds\Http\Exception\RequestException;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Fields\TargetingFields;
use helpers\Alert;
use helpers\StringHelper;

/**
 * @var $adSet Cursor
 * @var $idx int index of array
 */
$fields = [
    AdSetFields::ID,
    AdSetFields::NAME,
    AdSetFields::STATUS,
    AdSetFields::RECOMMENDATIONS,
    AdSetFields::TARGETING,
    AdSetFields::DAILY_BUDGET,
    AdSetFields::EFFECTIVE_STATUS,
    AdSetFields::BID_AMOUNT,
    AdSetFields::BILLING_EVENT,
    AdSetFields::BILLING_EVENT,
];


$params = array();
try {
    $adSet = $app->getFacebookActiveAccount()->getAdSets(
        $fields,
        $params
    )->current()->exportAllData();
} catch (DataNotFoundException $e) {
    Alert::warning(' У вас есть Facebook аккаунт ? Тогда выберите ваш аккаунт. Если нет создайте и  <a href="/accounts" class="text-theme-1 mx-1"> регистрируйте. </a> ');
} catch (RequestException | ClientException $e) {
    Alert::error($e->getErrorUserMessage());
}
$idx = 0;

?>

<div class="grid grid-cols-12 gap-6 mt-5">
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
        <h2 class="intro-y text-lg font-medium ">
            View Ad Set "<?= $adSet[AdSetFields::NAME]; ?>"
        </h2>
        <div class="hidden md:block mx-auto text-gray-600"></div>
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <div class="w-56 relative text-gray-700">
                <a class="button text-white bg-theme-1 shadow-md mr-2"
                   href="/adsets/update?id=<?= $adSet[AdSetFields::ID] ?>">Update</a>
                <a class="button text-white bg-theme-1 shadow-md mr-2" href="/adsets">Go back</a>
            </div>
        </div>
    </div>
    <!-- BEGIN: Data List -->
    <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
        <table class="table table-report -mt-2">
            <thead>
            <tr>
                <th scope="col">Field name</th>
                <th scope="col">Field value</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($fields as $field) { ?>
                <tr class="intro-x">
                    <td><?= StringHelper::underscoreToCamelCase($field); ?></td>
                    <td>
                        <?php
                        if( !key_exists($field,$adSet)){
                            echo " Not installed ";
                            continue;
                        }
                        if ($field == AdSetFields::RECOMMENDATIONS && key_exists(AdSetFields::RECOMMENDATIONS,$adSet) && is_array($adSet[$field])) {
                            $itemIdx = 0;
                            foreach ($adSet[$field] as $r) {
                                echo ++$itemIdx . ')  <span> ' . $r['title'] . ' <i data-feather="alert-circle" data-theme="light" class="tooltip inline-block text-black" title="' . $r['message'] . '"> </i></span>';
                            }
                            continue;
                        }
                        if ($field == AdSetFields::TARGETING && is_array($adSet[$field])) {
                            $item = $adSet[$field];
                            echo StringHelper::underscoreToCamelCase(TargetingFields::AGE_MAX) . " <span> " . $item[TargetingFields::AGE_MAX] . "</span><br />";
                            echo StringHelper::underscoreToCamelCase(TargetingFields::AGE_MIN) . " <span> " . $item[TargetingFields::AGE_MIN] . "</span><br />";
                            echo StringHelper::underscoreToCamelCase(TargetingFields::GEO_LOCATIONS) . " <span> " . implode(", ", $item[TargetingFields::GEO_LOCATIONS]['location_types']) . "</span><br />";
                            echo StringHelper::underscoreToCamelCase(TargetingFields::GEO_LOCATIONS) . " <span> " . implode(", ", $item[TargetingFields::GEO_LOCATIONS][TargetingFields::COUNTRIES]) . "</span><br />";
                            if (isset($item[TargetingFields::FACEBOOK_POSITIONS])) echo StringHelper::underscoreToCamelCase(TargetingFields::FACEBOOK_POSITIONS) . " <span> " . implode(", ", $item[TargetingFields::FACEBOOK_POSITIONS]) . "</span><br />";
                            continue;
                        }

                        echo is_null($adSet[$field]) ? " Not installed " : StringHelper::underscoreToCamelCase($adSet[$field]);
                        ?>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>

</div>
