<?php


use app\exceptions\DataNotFoundException;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\Values\CampaignObjectiveValues;
use FacebookAds\Object\Values\CampaignSpecialAdCategoriesValues;
use FacebookAds\Object\Values\CampaignSpecialAdCategoryCountryValues;
use FacebookAds\Object\Values\CampaignStatusValues;
use helpers\Alert;
use helpers\StringHelper;


try {
    if (isset($_POST['Campaign'])) {
        global $app;
        $account = $app->getFacebookActiveAccount();
        $fields = array();
        $params = array(
            'name' => $_POST['Campaign'][CampaignFields::NAME],
            'objective' => $_POST['Campaign'][CampaignFields::OBJECTIVE],
            'status' => $_POST['Campaign'][CampaignFields::STATUS],
            'special_ad_categories' => [$_POST['Campaign'][CampaignFields::SPECIAL_AD_CATEGORIES]],
        );
        $response = $account->createCampaign($fields, $params);
        Alert::success(' Campaign successfully created for view <a class="mx-1" href="/campaigns/view?service=fb&id=' . $response->{CampaignFields::ID} . '"> checkout this page </a>');
    }
} catch (DataNotFoundException $e) {
    Alert::warning(' У вас есть Yandex Direct аккаунт ? Тогда выберите ваш аккаунт. Если нет создайте и  <a href="/accounts" class="text-theme-1 mx-1"> регистрируйте. </a> ');
}
$campaignObjective = [
    "Awareness" => [
        CampaignObjectiveValues::BRAND_AWARENESS => "Brand awareness",
        CampaignObjectiveValues::REACH => "Reach",
    ],
    "Consideration" => [
        CampaignObjectiveValues::LINK_CLICKS => "Traffic",
        CampaignObjectiveValues::POST_ENGAGEMENT => "Engagement",
        CampaignObjectiveValues::APP_INSTALLS => "App installs",
        CampaignObjectiveValues::VIDEO_VIEWS => "Video views",
        CampaignObjectiveValues::LEAD_GENERATION => "Lead generation",
        CampaignObjectiveValues::MESSAGES => "Messages",
    ],
    "Conversion" => [
        CampaignObjectiveValues::CONVERSIONS => "Conversions",
        CampaignObjectiveValues::PRODUCT_CATALOG_SALES => "Catalog sales",
        CampaignObjectiveValues::STORE_VISITS => "Store traffic",
    ],
];

?>
<div class="intro-y flex items-center mt-8">
    <h2 class="text-lg font-medium mr-auto">
        Create new Campaign
    </h2>
</div>
<div class="grid grid-cols-12 gap-6 mt-5">
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
        <div class="hidden md:block mx-auto text-gray-600"></div>
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <a href="/campaigns" class="button text-white bg-theme-1 shadow-md mr-2">Go back</a>
        </div>
    </div>
    <div class="intro-y col-span-12 lg:col-span-6">
        <div class="intro-y box p-5">
            <form method="post" action="/campaigns/create">
                <div class="my-3 ">
                    <div class="grid grid-cols-2 gap-6 ">
                        <label for="name" class=" col-span-1">Company name </label>
                        <a class="text-theme-1 col-span-1 text-right" target="_blank"
                           href="https://www.facebook.com/ads/manager/account_settings/account_name_template/?act=810619469873809&pid=p1&page=account_settings&tab=account_name_template">Create
                            Name Template</a>
                    </div>
                    <input type="text" name="Campaign[<?= CampaignFields::NAME ?>]" class="input w-full border mt-2"
                           id="name">

                </div>
                <div class="my-3">
                    <label for="">Select campaign status</label>
                    <select data-placeholder="Select your favorite actors" class="select2 w-full"
                            name="Campaign[<?= CampaignFields::STATUS ?>]"
                            aria-label="Default select example">
                        <option>Select campaign status</option>
                        <option value="<?= CampaignStatusValues::PAUSED ?>"><?= StringHelper::underscoreToCamelCase(CampaignStatusValues::PAUSED) ?></option>
                        <option selected
                                value="<?= CampaignStatusValues::ACTIVE ?>"><?= StringHelper::underscoreToCamelCase(CampaignStatusValues::ACTIVE) ?></option>
                    </select>
                </div>
                <div class="my-3">
                    <label for="">Select campaign objective</label>
                    <select name="Campaign[<?= CampaignFields::OBJECTIVE ?>]" class="select2 w-full"
                            aria-label="Default select example">
                        <option selected>Select campaign objective</option>
                        <?php foreach ($campaignObjective as $groupLabel => $val) { ?>
                            <optgroup label="<?= $groupLabel ?>">
                                <?php foreach ($val as $option => $label) {
                                    echo '<option value="' . $option . '">' . $label . '</option>';
                                } ?>
                            </optgroup>
                        <?php } ?>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="">Select campaign Special Ad Categories</label>
                    <select name="Campaign[<?= CampaignFields::SPECIAL_AD_CATEGORIES ?>]" class="select2 w-full"
                            aria-label="Default select example">
                        <option selected><?=StringHelper::underscoreToCamelCase(CampaignSpecialAdCategoriesValues::NONE)?></option>
                        <option ><?=StringHelper::underscoreToCamelCase(CampaignSpecialAdCategoriesValues::CREDIT)?></option>
                        <option ><?=StringHelper::underscoreToCamelCase(CampaignSpecialAdCategoriesValues::EMPLOYMENT)?></option>
                        <option ><?=StringHelper::underscoreToCamelCase(CampaignSpecialAdCategoriesValues::HOUSING)?></option>
                        <option ><?=StringHelper::underscoreToCamelCase(CampaignSpecialAdCategoriesValues::ISSUES_ELECTIONS_POLITICS)?></option>
                    </select>
                    <div class="text-xs text-gray-600 mt-2">You're required to declare if your ads are related to
                        credit, employment or housing
                        opportunities or related to social issues, elections or politics. <a class="mx-1 text-theme-1"
                                                                                             target="_blank"
                                                                                             href="https://www.facebook.com/business/help/298000447747885"">
                        Learn More</a>
                    </div>
                </div>

                <div class="text-right mt-5">
                    <button type="reset" class="button w-24 border text-gray-700 mr-1">Cancel</button>
                    <button type="submit" class="button w-24 bg-theme-1 text-white">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
