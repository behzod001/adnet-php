<?php

use app\exceptions\DataNotFoundException;
use directapi\common\criterias\IdsCriteria;
use directapi\exceptions\DirectAccountNotExistException;
use directapi\exceptions\DirectApiException;
use directapi\exceptions\DirectApiNotEnoughUnitsException;
use directapi\exceptions\RequestValidationException;
use directapi\services\campaigns\enum\CampaignFieldEnum;
use FacebookAds\Object\Campaign;
use GuzzleHttp\Exception\GuzzleException;
use helpers\Alert;

global $api;

?>

<h2 class="intro-y text-lg font-medium mt-10">
    Deleting Campaign
</h2>
<div class="grid grid-cols-12 gap-6 mt-5">
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
        <div class="hidden md:block mx-auto text-gray-600"></div>
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <div class="w-56 relative text-gray-700">
                <a class="button text-white bg-theme-1 shadow-md mr-2" href="/campaigns">Go back</a>
            </div>
        </div>
    </div>
    <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">

        <?php
        if (isset($_GET['service']) && $_GET['service'] == 'fb') {
            $campaign = new Campaign($_GET['id'], null, $api);
            try {
                $campaignResponse = $campaign->deleteSelf();
                $success = json_decode($campaignResponse->getBody(), true);
                if ($success) {
                    Alert::success(' Campaign successfully deleted for view <a class="mx-1" href="/campaigns">checkout this page</a>');
                } else {
                    Alert::error('Something went wrong <a class="mx-1" href="/campaigns">checkout this page</a> ');
                }
            } catch (FacebookAds\Http\Exception\AuthorizationException $e) {
                Alert::error($e->getMessage() . ' <a class="mx-1" href="/campaigns">checkout this page</a> ');
            }
        }

        if (isset($_GET['service']) && $_GET['service'] == 'direct') {

            global $app;
            $fields = [
                CampaignFieldEnum::NAME,
                CampaignFieldEnum::ID,
                CampaignFieldEnum::STATUS,
                CampaignFieldEnum::DAILY_BUDGET,
                CampaignFieldEnum::START_DATE,
                CampaignFieldEnum::CURRENCY,
            ];
            $criteria = new IdsCriteria();
            $criteria->Ids = [$_GET['id']];
            try {
                $response = $app->getDirect()->getCampaignsService()->delete($criteria)[0];
                if ($response->Errors != null) {
                    $idx = 0;
                    while ($response->Errors[$idx])
                        Alert::error($response->Errors[$idx++]->Message . '<a class="mx-1" href="/campaigns">Back to main page</a>');
                } else {
                    Alert::success(' Campaign successfully created for view <a class="mx-1" href="/campaigns">checkout this page</a>');
                    echo '<h2 class="container">This is campaign deleted ' . $_GET['id'] . '</h2>';
                }

            } catch (DataNotFoundException $e) {
                Alert::warning(' У вас есть Yandex Direct аккаунт ? Тогда выберите ваш аккаунт. Если нет создайте и  <a href="/accounts" class="text-theme-1 mx-1"> регистрируйте. </a> ');

            } catch (DirectAccountNotExistException | DirectApiNotEnoughUnitsException | RequestValidationException | DirectApiException $e) {
                Alert::error('Something went wrong <a class="mx-1" href="/campaigns">Back to main page</a>');
            }
        }
        ?>

    </div>
</div>
