<?php

use app\exceptions\DataNotFoundException;
use directapi\exceptions\DirectAccountNotExistException;
use directapi\exceptions\DirectApiException;
use directapi\exceptions\DirectApiNotEnoughUnitsException;
use directapi\exceptions\RequestValidationException;
use directapi\services\campaigns\criterias\CampaignsSelectionCriteria;
use directapi\services\campaigns\enum\CampaignFieldEnum;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\Fields\CampaignFields;
use GuzzleHttp\Exception\GuzzleException;
use helpers\Alert;
use helpers\StringHelper;

global $api, $app;
if (isset($_GET['account']) && isset($_GET['service'])) {
    if ($_GET['service'] == 'fb') $_SESSION['FACEBOOK']['account_id'] = $_GET['account'];
    if ($_GET['service'] == 'ya') $_SESSION['YANDEX']['client_login'] = $_GET['account'];
}
/** @var Campaign $campaigns */
$fields = array(
    CampaignFields::ID,
    CampaignFields::NAME,
    CampaignFields::RECOMMENDATIONS,
    CampaignFields::CAN_USE_SPEND_CAP,
    CampaignFields::ACCOUNT_ID,
    CampaignFields::STATUS,
);
$params = array(
    'effective_status' => array('ACTIVE', 'PAUSED', 'IN_PROCESS'),
);

try {
    $campaigns = $app->getFacebookActiveAccount()->getCampaigns(
        $fields,
        $params
    );
} catch (DataNotFoundException $e) {
    Alert::warning(' У вас есть Фасебоок аккаунт ? Тогда выберите ваш аккаунт. Если нет создайте и  <a href="/accounts" class="text-theme-1 mx-1"> регистрируйте. </a> ');
}

// direct campaigns
$directFields = [
    CampaignFieldEnum::NAME,
    CampaignFieldEnum::ID,
    CampaignFieldEnum::STATUS,
    CampaignFieldEnum::START_DATE,
    CampaignFieldEnum::END_DATE,
    CampaignFieldEnum::DAILY_BUDGET,
    CampaignFieldEnum::TYPE,
];

$criteria = new CampaignsSelectionCriteria();
try {
    $directCampaigns = $app->getDirect()->getCampaignsService()->get($criteria, $directFields);
} catch (DataNotFoundException $e) {
    Alert::warning(' У вас есть Yandex Direct аккаунт ? Тогда выберите ваш аккаунт. Если нет создайте и  <a href="/accounts" class="text-theme-1 mx-1"> регистрируйте. </a> ');
} catch (GuzzleException | DirectAccountNotExistException | DirectApiNotEnoughUnitsException | RequestValidationException | DirectApiException $e) {
    if ($e->getCode() == 8000) {
        Alert::warning(' У вас есть Yandex Direct аккаунт ? Тогда выберите ваш аккаунт. Если нет создайте и  <a href="/accounts" class="text-theme-1 mx-1"> регистрируйте. </a> ');
    } else
        Alert::error($e->getMessage() . ' Something went wrong <a href="/campaigns" class="text-theme-1 mx-1"> reload this page </a> ');
}

?>
    <h2 class="intro-y text-lg font-medium mt-10">
        Data List Campaigns
    </h2>

    <div class="grid grid-cols-12 gap-6 mt-5">
        <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
            <button class="button text-white bg-theme-1 shadow-md mr-2">Add New Campaign</button>
            <div class="dropdown relative ">
                <button class="dropdown-toggle button px-2 box text-gray-700">
            <span class="w-5 h-5 flex items-center justify-center">
                <i class="w-4 h-4" data-feather="plus"></i>
            </span>
                </button>
                <div class="dropdown-box mt-10 absolute top-0 left-0 z-20" style="width: 250px">
                    <div class="dropdown-box__content box p-2">
                        <a href="/campaigns/create"
                           class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                            <i data-feather="facebook" class="w-4 h-4 mr-2"></i> Facebook campaign </a>
                        <a href="/direct/campaigns/create"
                           class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                            <i data-feather="shield" class="w-4 h-4 mr-2"></i>Yandex Direct campaign </a>
                        <a href="javascript:" data-toggle="modal"
                           data-target="#add-google-account"
                           class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                            <i data-feather="map-pin" class="w-4 h-4 mr-2"></i>Google Ads campaign </a>
                    </div>
                </div>
            </div>
            <div class="hidden md:block mx-auto text-gray-600">Showing 1 to 10 of 150 entries</div>
            <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
                <div class="w-56 relative text-gray-700">
                    <input type="text" class="input w-56 box pr-10 placeholder-theme-13" placeholder="Search...">
                    <i class="w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0" data-feather="search"></i>
                </div>
            </div>
        </div>

        <!-- BEGIN: Data List -->
        <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
            <table class="table table-report -mt-2">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Campaign name</th>
                    <?php unset($fields[0], $fields[1]);
                    foreach ($fields as $field) { ?>
                        <th scope="col"><?= StringHelper::underscoreToCamelCase($field); ?></th>
                    <?php } ?>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php if (isset($campaigns)) {
                    $idx = 0;
                    foreach ($campaigns as $campaign) { ?>
                        <tr class="intro-x">
                            <td class="w-40">
                                <div class="flex">
                                    <div class="w-10 h-10 image-fit zoom-in">
                                        <img alt="AdNet" class="tooltip rounded-full"
                                             src="/resources/dist/images/preview-4.jpg"
                                             title="Uploaded at 17 July 2021">
                                    </div>
                                    <div class="w-10 h-10 image-fit zoom-in -ml-5">
                                        <img alt="AdNet" class="tooltip rounded-full"
                                             src="/resources/dist/images/preview-4.jpg"
                                             title="Uploaded at 17 July 2021">
                                    </div>
                                    <div class="w-10 h-10 image-fit zoom-in -ml-5">
                                        <img alt="AdNet" class="tooltip rounded-full"
                                             src="/resources/dist/images/preview-10.jpg"
                                             title="Uploaded at 17 July 2021">
                                    </div>
                                </div>
                            </td>
                            <td>
                                <a href="/campaigns/view?service=fb&id=<?= $campaign->{AdAccountFields::ID} ?>"
                                   class="font-medium whitespace-no-wrap"><?= $campaign->{CampaignFields::NAME} ?></a>
                                <div class="text-gray-600 text-xs whitespace-no-wrap">
                                    <span class="bg-theme-14 text-theme-10 rounded px-2 mt-1 text-center"> Facebook </span>
                                    <?= $campaign->{CampaignFields::ID} ?>
                                </div>
                            </td>
                            <td class="text-center"><?= $campaign->{CampaignFields::RECOMMENDATIONS} ?></td>
                            <td><?= $campaign->{CampaignFields::CAN_USE_SPEND_CAP} ?></td>
                            <td><?= $campaign->{CampaignFields::ACCOUNT_ID} ?></td>

                            <td class="w-40">
                                <div class="flex items-center justify-center text-theme-6">
                                    <i data-feather="check-square" class="w-4 h-4 mr-2"></i>
                                    <?= $campaign->{CampaignFields::STATUS} ?>
                                </div>
                            </td>
                            <td class="table-report__action w-auto">
                                <div class="flex justify-center items-center">
                                    <a href="/campaigns/view?service=fb&id=<?= $campaign->{AdAccountFields::ID} ?>"
                                       class="flex items-center mr-3">
                                        <i data-feather="eye" class=" w-4 h-4 mr-1"></i>
                                        View
                                    </a>
                                    <a class="flex items-center mr-3"
                                       href="/campaigns/update?service=fb&id=<?= $campaign->{AdAccountFields::ID} ?>">
                                        <i data-feather="check-square" class="w-4 h-4 mr-1"></i>
                                        Edit
                                    </a>
                                    <a class="flex items-center text-theme-6" href="javascript:" data-toggle="modal"
                                       data-target="#delete-confirmation-modal" onclick="confirmDelete(this);"
                                       data-delete="/campaigns/delete?service=fb&id=<?= $campaign->{CampaignFields::ID} ?>">
                                        <i data-feather="trash-2" class="w-4 h-4 mr-1"></i>
                                        Delete
                                    </a>
                                </div>
                            </td>
                        </tr>
                    <?php }
                } ?>
                <?php if (isset($directCampaigns)) {
                    $idx = 0;
                    foreach ($directCampaigns as $item) { ?>
                        <tr>
                            <td class="w-40">
                                <div class="flex">
                                    <div class="w-10 h-10 image-fit zoom-in">
                                        <img alt="AdNet" class="tooltip rounded-full"
                                             src="/resources/dist/images/preview-4.jpg"
                                             title="Uploaded at 17 July 2021">
                                    </div>
                                    <div class="w-10 h-10 image-fit zoom-in -ml-5">
                                        <img alt="AdNet" class="tooltip rounded-full"
                                             src="/resources/dist/images/preview-4.jpg"
                                             title="Uploaded at 17 July 2021">
                                    </div>
                                    <div class="w-10 h-10 image-fit zoom-in -ml-5">
                                        <img alt="AdNet" class="tooltip rounded-full"
                                             src="/resources/dist/images/preview-10.jpg"
                                             title="Uploaded at 17 July 2021">
                                    </div>
                                </div>
                            </td>
                            <td>
                                <a href="/campaigns/view?id=<?= $item->{CampaignFieldEnum::ID} ?>"
                                   class="font-medium whitespace-no-wrap"><?= $item->{CampaignFieldEnum::NAME} ?></a>
                                <div class="text-gray-600 text-xs whitespace-no-wrap">
                                    <span class="bg-theme-17 text-theme-11 rounded px-2 mt-1 text-center"> Yandex Direct </span>
                                    <?= $item->{CampaignFieldEnum::ID} ?>
                                </div>
                            </td>
                            <td><?= $item->{CampaignFieldEnum::START_DATE} ?></td>
                            <td><?= $item->{CampaignFieldEnum::END_DATE} ?></td>
                            <td><?= StringHelper::underscoreToCamelCase($item->{CampaignFieldEnum::TYPE}) ?></td>
                            <td class="w-40">
                                <div class="flex items-center justify-center text-theme-6">
                                    <i data-feather="check-square" class="w-4 h-4 mr-2"></i>
                                    <?= $item->{CampaignFieldEnum::STATUS} ?>
                                </div>
                            </td>
                            <td class="table-report__action w-auto">
                                <div class="flex justify-center items-center">
                                    <a href="/campaigns/view?service=direct&id=<?= $item->{CampaignFieldEnum::ID} ?>"
                                       class="flex items-center mr-3">
                                        <i data-feather="eye" class=" w-4 h-4 mr-1"></i>
                                        View
                                    </a>
                                    <a class="flex items-center mr-3"
                                       href="/direct/campaigns/update?id=<?= $item->{CampaignFieldEnum::ID} ?>">
                                        <i data-feather="check-square" class="w-4 h-4 mr-1"></i>
                                        Edit
                                    </a>
                                    <a class="flex items-center text-theme-6" href="javascript:" data-toggle="modal"
                                       data-target="#delete-confirmation-modal" onclick="confirmDelete(this);"
                                       data-delete="/campaigns/delete?service=direct&id=<?= $item->{CampaignFieldEnum::ID} ?>">
                                        <i data-feather="trash-2" class="w-4 h-4 mr-1"></i>
                                        Delete
                                    </a>
                                </div>
                            </td>
                        </tr>
                    <?php }
                } ?>
                </tbody>
            </table>
        </div>
        <!-- END: Data List -->
        <!-- BEGIN: Pagination -->
        <div class="intro-y col-span-12 flex flex-wrap sm:flex-row sm:flex-no-wrap items-center">
            <ul class="pagination">
                <li>
                    <a class="pagination__link" href=""> <i class="w-4 h-4" data-feather="chevrons-left"></i> </a>
                </li>
                <li>
                    <a class="pagination__link" href=""> <i class="w-4 h-4" data-feather="chevron-left"></i> </a>
                </li>
                <li><a class="pagination__link" href="">...</a></li>
                <li><a class="pagination__link pagination__link--active" href="">1</a></li>
                <li><a class="pagination__link" href="">2</a></li>
                <li><a class="pagination__link" href="">3</a></li>
                <li><a class="pagination__link" href="">...</a></li>
                <li>
                    <a class="pagination__link" href=""> <i class="w-4 h-4" data-feather="chevron-right"></i> </a>
                </li>
                <li>
                    <a class="pagination__link" href=""> <i class="w-4 h-4" data-feather="chevrons-right"></i> </a>
                </li>
            </ul>
            <select class="w-20 input box mt-3 sm:mt-0">
                <option>10</option>
                <option>25</option>
                <option>35</option>
                <option>50</option>
            </select>
        </div>
        <!-- END: Pagination -->
    </div>
    <!-- BEGIN: Delete Confirmation Modal -->
    <div class="modal" id="delete-confirmation-modal">
        <div class="modal__content">
            <div class="p-5 text-center">
                <i data-feather="x-circle" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i>
                <div class="text-3xl mt-5">Are you sure?</div>
                <div class="text-gray-600 mt-2">Do you really want to delete these records? This process cannot be
                    undone.
                </div>
            </div>
            <div class="px-5 pb-8 text-center">
                <button type="button" data-dismiss="modal" class="button w-24 border text-gray-700 mr-1">Cancel</button>
                <a href="" id="deleteBtn" type="button" class="button w-24 bg-theme-6 text-white">Delete</a>
            </div>
        </div>
    </div>
    <!-- END: Delete Confirmation Modal -->

<?php /*
<div class="container">
    <table class="table caption-top">
        <caption>List of Campaigns</caption>
        <a href="/campaigns/create" class="btn btn-primary">Create Campaign</a>
        <thead>
        <tr>
            <th scope="col">#</th>
            <?php foreach ($fields as $field) { ?>
                <th scope="col"><?= ucfirst(str_replace("_", " ", $field)); ?></th>
            <?php } ?>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        <?php $idx = 0;
        foreach ($campaigns as $create) { ?>
            <tr>
                <th scope="row"><?= ++$idx ?></th>
                <?php foreach ($fields as $field) { ?>
                    <td><?= $create->{$field} ?></td>
                <?php } ?>
                <td>
                    <a href="/campaigns/view?id=<?= $create->id ?>" class="btn btn-primary">View</a>
                    <a href="/campaigns/update?id=<?= $create->id ?>" class="btn btn-warning">Update</a>
                    <a href="/campaigns/delete?id=<?= $create->id ?>" class="btn btn-danger">Delete</a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

 */ ?>