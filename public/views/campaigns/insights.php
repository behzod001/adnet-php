<?php
global $app;

use app\exceptions\DataNotFoundException;
use FacebookAds\Http\Request;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\AdsActionStatsFields;
use FacebookAds\Object\Fields\AdsInsightsFields;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\Values\AdsInsightsActionBreakdownsValues;
use FacebookAds\Object\Values\AdsInsightsDatePresetValues;
use helpers\Alert;
use helpers\StringHelper;

//
//$fieldPars = [
////    "clicks", "account_name", "ad_id", "ad_name", "adset_id", "campaign_id", "campaign_name", "unique_clicks", "cpc", "ctr", "cpm", "account_currency", "account_id",
//    "action_values{" . AdsActionStatsFields::ACTION_DEVICE, AdsActionStatsFields::ACTION_DESTINATION, AdsActionStatsFields::ACTION_REACTION, AdsActionStatsFields::ACTION_TARGET_ID . "}," .
//    "actions{" . AdsActionStatsFields::ACTION_DEVICE, AdsActionStatsFields::ACTION_DESTINATION, AdsActionStatsFields::ACTION_REACTION, AdsActionStatsFields::ACTION_TARGET_ID . "}",
//];
//
//$params = array(
//    "date_preset" => AdsInsightsDatePresetValues::LAST_7D,
////    "level" => "campaign",
////    "action_attribution_windows" => "7d_click",
//    "action_breakdowns" => AdsInsightsActionBreakdownsValues::ACTION_DESTINATION, AdsInsightsActionBreakdownsValues::ACTION_CANVAS_COMPONENT_NAME,
//    AdsInsightsActionBreakdownsValues::ACTION_CAROUSEL_CARD_ID, AdsInsightsActionBreakdownsValues::ACTION_CAROUSEL_CARD_NAME,
//    AdsInsightsActionBreakdownsValues::ACTION_DESTINATION, AdsInsightsActionBreakdownsValues::ACTION_REACTION, AdsInsightsActionBreakdownsValues::ACTION_TARGET_ID,
//    AdsInsightsActionBreakdownsValues::ACTION_TYPE, AdsInsightsActionBreakdownsValues::ACTION_VIDEO_SOUND, AdsInsightsActionBreakdownsValues::ACTION_VIDEO_TYPE,
//);
//$data = null;
//try {
//    if (isset($_GET['fields'])) {
//        $ff = $_GET['fields'];
//        $fieldPars = $_GET['fields'];
//    }
//    $fb = $app->getFacebookActiveAccount()->getInsights($fieldPars, $params);
////    var_dump($fb);
//    $data = $fb->getLastResponse()->getContent()['data'][0];
//
//} catch (DataNotFoundException $e) {
//    Alert::error("Something went wrong " . $e->getMessage());
//}
//
//$report = [
//    AdsInsightsFields::ACCOUNT_ID => "The ID number of your ad account, which groups your advertising activity. Your ad account includes your campaigns, ads and billing.",
//    AdsInsightsFields::ACCOUNT_CURRENCY => "Currency that is used by your ad account.",
//    AdsInsightsFields::ACCOUNT_NAME => "The name of your ad account, which groups your advertising activity. Your ad account includes your campaigns, ads and billing.",
//    AdsInsightsFields::AD_ID => "The unique ID of the ad you're viewing in reporting.",
//    AdsInsightsFields::AD_NAME => "The name of the ad you're viewing in reporting.",
//    AdsInsightsFields::ADSET_ID => "The unique ID of the ad set you're viewing in reporting. An ad set is a group of ads that share the same budget, schedule, delivery optimization and targeting.",
//    AdsInsightsFields::ADSET_NAME => "The name of the ad set you're viewing in reporting. An ad set is a group of ads that share the same budget, schedule, delivery optimization and targeting.",
//    AdsInsightsFields::ATTRIBUTION_SETTING => "The default attribution window to be used when the attribution result is calculated. Each ad set has its own attribution setting value. The attribution setting for a campaign or an account is calculated based on existing ad sets. Returns mixed, indicating multiple attribution settings, skan, indicating SKAdNetwork attribution of iOS 14 app install campaigns, and na, indicating no ad set in a campaign or account.
//The attribution_setting field only returns values when use_unified_attribution_setting is set to true.",
//    AdsInsightsFields::BUYING_TYPE => "The method by which you pay for and target ads in your campaigns: through dynamic auction bidding, fixed-price bidding, or reach and frequency buying. This field is currently only visible at the campaign level.",
//    AdsInsightsFields::CAMPAIGN_ID => "The unique ID number of the ad campaign you're viewing in reporting. Your campaign contains ad sets and ads.",
//    AdsInsightsFields::CAMPAIGN_NAME => "The name of the ad campaign you're viewing in reporting. Your campaign contains ad sets and ads.",
//    AdsInsightsFields::CLICKS => "The number of clicks on your ads.",
//    AdsInsightsFields::COST_PER_INLINE_LINK_CLICK => "The average cost of each inline link click.",
//    AdsInsightsFields::COST_PER_INLINE_POST_ENGAGEMENT => "The average cost of each inline post engagement.",
//    AdsInsightsFields::COST_PER_UNIQUE_CLICK => "The average cost for each unique click (all). This metric is estimated.",
//    AdsInsightsFields::COST_PER_UNIQUE_INLINE_LINK_CLICK => "The average cost of each unique inline link click. This metric is estimated.",
//    //    AdsInsightsFields::COST_PER_UNIQUE_OUTBOUND_CLICK=>"The average cost for each unique outbound click. This metric is estimated.",
//    AdsInsightsFields::CPP => "The average cost to reach 1,000 people. This metric is estimated.",
//    AdsInsightsFields::CTR => "The percentage of times people saw your ad and performed a click (all).",
//    AdsInsightsFields::DATE_START => "The start date for your data. This is controlled by the date range you've selected for your reporting view.",
//    AdsInsightsFields::DATE_STOP => "The end date for your data. This is controlled by the date range you've selected for your reporting view.",
//    AdsInsightsFields::FREQUENCY => "The average number of times each person saw your ad. This metric is estimated.",
//    AdsInsightsFields::FULL_VIEW_IMPRESSIONS => "The number of Full Views on your Page's posts as a result of your ad.",
//    AdsInsightsFields::FULL_VIEW_REACH => "The number of people who performed a Full View on your Page's post as a result of your ad.",
//    AdsInsightsFields::IMPRESSIONS => "The number of times your ads were on screen.",
//    AdsInsightsFields::INLINE_LINK_CLICK_CTR => "The percentage of time people saw your ads and performed an inline link click.",
//    AdsInsightsFields::INLINE_LINK_CLICKS => "The number of clicks on links to select destinations or experiences, on or off Facebook-owned properties. Inline link clicks use a fixed 1-day-click attribution window.",
//    AdsInsightsFields::INLINE_POST_ENGAGEMENT => "The total number of actions that people take involving your ads. Inline post engagements use a fixed 1-day-click attribution window.",
//    AdsInsightsFields::INSTANT_EXPERIENCE_CLICKS_TO_OPEN => "The number of clicks on your ad that open an Instant Experience. This metric is in development.",
//    AdsInsightsFields::INSTANT_EXPERIENCE_CLICKS_TO_START => "The number of times an interactive component in an Instant Experience starts. This metric is in development.",
//    AdsInsightsFields::OBJECTIVE => "The objective reflecting the goal you want to achieve with your advertising. It may be different from the selected objective of the campaign in some cases.",
//    AdsInsightsFields::OPTIMIZATION_GOAL => "The optimization goal you selected for your ad or ad set. Your optimization goal reflects what you want to optimize for the ads.",
//    AdsInsightsFields::PLACE_PAGE_NAME => "The name of the place page involved in impression or click. Has to be used together with Business Locations breakdown.",
//];

// report single chart CTR hourly
$chartData = [
    'labels' => [
        "00-07" => 0,
        "07-12" => 0,
        "12-17" => 0,
        "17-24" => 0
    ],
    'clicks' => 0,
    'ctr' => 0,
    'cpc' => 0,

];
$dataHourly = [0, 0, 0, 0];
try {

    $reportCtrChartData = $app->getFacebookActiveAccount()
        ->getInsights(["clicks", "cpc", "ctr"], ["breakdowns" => "hourly_stats_aggregated_by_audience_time_zone", "level" => "ad"])
        ->getLastResponse()->getContent()['data'];

//    $minCost = $maxCost = 0;
    $cost = [];
    foreach ($reportCtrChartData as $reportCtrChartDatum) {
        $chartData['clicks'] += $reportCtrChartDatum['clicks'];
        $chartData['ctr'] += $reportCtrChartDatum['ctr'] ?? 0;
        $chartData['cpc'] += $reportCtrChartDatum['cpc'] ?? 0;

        // getting times labels  from facebook response
        $pattern = '/(^[\d]{2}):[\d]{2}:[\d]{2} - ([\d]{2}):[\d]{2}:[\d]{2}/';
        $match = [];
        preg_match($pattern, $reportCtrChartDatum['hourly_stats_aggregated_by_audience_time_zone'], $match);
        array_push($chartData['labels'], $match[1]);


        if ($match[1] < 7) {
            $dataHourly[0] += $reportCtrChartDatum['ctr'];
        }
        if ($match[1] > 7 && $match[1] < 12) {
            $dataHourly[1] += $reportCtrChartDatum['ctr'];
        }

        if ($match[1] > 12 && $match[1] < 17) {
            $dataHourly[2] += $reportCtrChartDatum['ctr'];
        }
        if ($match[1] > 17 && $match[1] < 24) {
            $dataHourly[3] += $reportCtrChartDatum['ctr'];
        }
        // push ctr to cost labels array
        if (isset($reportCtrChartDatum['ctr'])) {
            array_push($cost, $reportCtrChartDatum['ctr']);
        }
    }

} catch (DataNotFoundException $e) {
    Alert::error("Something went wrong " . $e->getMessage());
}


// report age and gender
$chartAgeData = [
    'labels' => [],
    'male' => [],
    'female' => [],
];


// report placement and platform
$chartPlacementData = [
    'labels' => [],
    'desktop' => [
        "audience_network" => 0,
        "facebook" => 0,
        "instagram" => 0,
        "messenger" => 0
    ],
    'mobile_app' => [
        "audience_network" => 0,
        "facebook" => 0,
        "instagram" => 0,
        "messenger" => 0
    ]
];
try {

    $ageImpressions = $app->getFacebookActiveAccount()
        ->getInsights(["impressions"], ["breakdowns" => "age,gender"])
        ->getLastResponse()->getContent()['data'];

    foreach ($ageImpressions as $impression) {
        if ($impression['gender'] == "male") {
            array_push($chartAgeData['male'], $impression["impressions"]);
        }
        if ($impression['gender'] == "female") {
            array_push($chartAgeData['female'], $impression["impressions"]);
        }
        $chartAgeData['labels'][$impression['age']] = $chartAgeData['labels'][$impression['age']] ?? $impression['age'];
    }
    unset($chartAgeData['labels']["Unknown"]);


// getting placement and  platform
    $placementPlatform = $app->getFacebookActiveAccount()
        ->getInsights(["impressions"], ["breakdowns" => "device_platform,publisher_platform"])
        ->getLastResponse()->getContent()['data'];
    foreach ($placementPlatform as $platform) {
        $chartPlacementData['labels'][$platform['publisher_platform']] = StringHelper::underscoreToCamelCase($platform['publisher_platform']);

        if (in_array($platform['device_platform'], ["desktop", "desktop_web"])) {
            $chartPlacementData['desktop'][$platform['publisher_platform']] += $platform["impressions"] ?? 0;
        }
        if (in_array($platform['device_platform'], ["mobile_app", "mobile_web"])) {
            $chartPlacementData['mobile_app'][$platform['publisher_platform']] += $platform["impressions"] ?? 0;
        }
    }
} catch (DataNotFoundException $e) {
    Alert::error("Something went wrong " . $e->getMessage());
}
?>
<h2 class="intro-y text-lg font-medium mt-10">
    Data List Reports
</h2>

<div class="grid grid-cols-12 gap-6 mt-5">
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
        <div class="intro-y flex flex-col-reverse sm:flex-row items-center">
            <div class="w-full sm:w-auto relative mr-auto mt-3 sm:mt-0">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                     stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
                     class="feather feather-search w-4 h-4 absolute my-auto inset-y-0 ml-3 left-0 z-10 text-gray-700">
                    <circle cx="11" cy="11" r="8"></circle>
                    <line x1="21" y1="21" x2="16.65" y2="16.65"></line>
                </svg>
                <input type="text" class="input w-full sm:w-64 box px-10 text-gray-700 placeholder-theme-13"
                       placeholder="Search files">
                <div class="inbox-filter dropdown absolute inset-y-0 mr-3 right-0 flex items-center">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                         stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
                         class="feather feather-chevron-down dropdown-toggle w-4 h-4 cursor-pointer text-gray-700">
                        <polyline points="6 9 12 15 18 9"></polyline>
                    </svg>
                    <div class="inbox-filter__dropdown-box dropdown-box mt-10 absolute top-0 left-0 z-20">
                        <div class="dropdown-box__content box p-5">
                            <form action="/campaigns/insights" method="get" class="mt-5">
                                <div class="grid grid-cols-12 gap-4 row-gap-3">
                                    <div class="col-span-6">
                                        <div class="text-xs">File Name</div>
                                        <input type="text" class="input w-full border mt-2 flex-1"
                                               placeholder="Type the file name">
                                    </div>
                                    <div class="col-span-6">
                                        <div class="text-xs">Shared With</div>
                                        <input type="text" class="input w-full border mt-2 flex-1"
                                               placeholder="example@gmail.com">
                                    </div>
                                    <div class="col-span-6">
                                        <div class="text-xs">Created At</div>
                                        <input type="text" class="input w-full border mt-2 flex-1"
                                               placeholder="Important Meeting">
                                    </div>
                                    <div class="col-span-6">
                                        <div class="mb-3">
                                            <label for="fields"> Select one</label>
                                            <select id="fields" name="fields[]" multiple
                                                    class="input w-full border mt-2 flex-1 select2 w-full"
                                                    data-auto-close="off">

                                                <?php foreach ($report as $key => $value) { ?>
                                                    <option <?= (isset($_GET['fields']) && in_array($value, $_GET['fields'])) ? "selected" : "" ?>
                                                            value="<?= $key ?>"><?= $value ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-span-12 flex items-center mt-3">
                                        <div class="text-right mt-5">
                                            <button type="reset" class="button w-24 border text-gray-700 mr-1">Cancel
                                            </button>
                                            <button type="submit" class="button w-24 bg-theme-1 text-white">Save
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="hidden md:block mx-auto text-gray-600">Showing 1 to 10 of 150 entries</div>
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <div class="w-56 relative text-gray-700">
                <input type="text" class="input w-56 box pr-10 placeholder-theme-13" placeholder="Search...">
                <i class="w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0" data-feather="search"></i>
            </div>
        </div>
    </div>
    <!-- BEGIN: Daily report by hourly -->
    <div class="col-span-6 lg:col-span-6 mt-8">
        <div class="intro-y box p-5 mt-12 sm:mt-5">
            <div class="flex flex-col xl:flex-row xl:items-center">
                <div class="flex">
                    <div>
                        <div class="text-theme-20 text-lg xl:text-xl font-bold">$ <?= number_format($chartData['ctr'], 2, '.', ',') ?></div>
                        <div class="text-gray-600">CTR</div>
                    </div>
                    <div class="w-px h-12 border border-r border-dashed border-gray-300 mx-4 xl:mx-6"></div>
                    <div>
                        <div class="text-gray-600 text-lg xl:text-xl font-medium">$ <?= number_format($chartData['cpc'], 2, '.', ',') ?></div>
                        <div class="text-gray-600">CPC</div>
                    </div>
                    <div class="w-px h-12 border border-r border-dashed border-gray-300 mx-4 xl:mx-6"></div>
                    <div>
                        <div class="text-gray-600 text-lg xl:text-xl font-medium"><?= $chartData['clicks']; ?></div>
                        <div class="text-gray-600">Click</div>
                    </div>
                </div>
                <div class="dropdown relative xl:ml-auto mt-5 xl:mt-0">
                    <button class="dropdown-toggle button font-normal border text-white relative flex items-center text-gray-700">
                        Filter by Category <i data-feather="chevron-down" class="w-4 h-4 ml-2"></i></button>
                    <div class="dropdown-box mt-10 absolute w-40 top-0 xl:right-0 z-20">
                        <div class="dropdown-box__content box p-2 overflow-y-auto h-32">
                            <a href=""
                               class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">PC
                                & Laptop</a>
                            <a href=""
                               class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">Smartphone</a>
                            <a href=""
                               class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">Electronic</a>
                            <a href=""
                               class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">Photography</a>
                            <a href=""
                               class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">Sport</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="report-chart">
                <canvas id="report-ctr-chart"
                        data-labels="<?= implode(",", $chartData['labels']); ?>"
                        data-cost="<?= implode(",", $dataHourly); ?>"
                        height="160" class="mt-6"></canvas>
            </div>
        </div>
    </div>
    <!-- END: Daily report by hourly -->


    <!-- BEGIN: Age and Gender Report -->
    <div class="col-span-6 lg:col-span-6 mt-8">
        <div class="intro-y box p-5 mt-12 sm:mt-5">
            <div class="flex flex-col xl:flex-row xl:items-center">
                <div class="intro-y block sm:flex items-center h-10">
                    <h2 class="text-lg font-medium truncate mr-5">
                        Age and Gender Distribution
                    </h2>
                </div>
            </div>
            <div class="report-chart">
                <canvas id="report-age-chart"
                        data-labels="<?= implode(",", $chartAgeData['labels']) ?>"
                        data-male="<?= implode(",", $chartAgeData['male']); ?>"
                        data-female="<?= implode(",", $chartAgeData['female']); ?>"
                        height="160" class="mt-6"></canvas>
            </div>
        </div>
    </div>
    <!-- END: Age and Gender Report -->

    <!-- BEGIN:  Placement per Platform Report -->
    <div class="col-span-6 lg:col-span-6 mt-8">
        <div class="intro-y box p-5 mt-12 sm:mt-5">
            <div class="flex flex-col xl:flex-row xl:items-center">
                <div class="intro-y block sm:flex items-center h-10">
                    <h2 class="text-lg font-medium truncate mr-5">
                        Placement per Platform impressions
                    </h2>
                </div>
            </div>
            <div class="report-chart">
                <canvas id="report-placement-chart"
                        data-labels="<?= implode(",", $chartPlacementData['labels']) ?>"
                        data-desktop="<?= implode(",", $chartPlacementData['desktop']); ?>"
                        data-mobile_app="<?= implode(",", $chartPlacementData['mobile_app']); ?>"
                        height="160" class="mt-6"></canvas>
            </div>
        </div>
    </div>
    <!-- END:  Placement per Platform Report -->
    <?php if (isset($data)) { ?>
        <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
            <table class="table table-report -mt-2">
                <thead>
                <tr>
                    <th scope="col">Field name</th>
                    <th scope="col">Field value</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($data as $field => $values) { ?>
                    <tr class="intro-x">
                        <td><?= StringHelper::underscoreToCamelCase($field); ?></td>
                        <td><?= StringHelper::underscoreToCamelCase($values) ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    <?php } ?>
    <!--    <div class="col-span-12 lg:col-span-6">-->
    <!-- BEGIN: Vertical Bar Chart -->
    <!--        <div class="intro-y box">-->
    <!--            <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200">-->
    <!--                <h2 class="font-medium text-base mr-auto">-->
    <!--                    Vertical Bar Chart-->
    <!--                </h2>-->
    <!---->
    <!--            </div>-->
    <!--            <div class="p-5" id="vertical-bar-chart">-->
    <!--                <div class="preview">-->
    <!--                    <canvas id="vertical-bar-chart-widget" height="200"></canvas>-->
    <!--                </div>-->
    <!--                <div class="source-code hidden">-->
    <!--                    <button data-target="#copy-vertical-bar-chart" class="copy-code button button--sm border flex items-center text-gray-700"> <i data-feather="file" class="w-4 h-4 mr-2"></i> Copy code </button>-->
    <!--                    <div class="overflow-y-auto h-64 mt-3">-->
    <!--                        <pre class="source-preview" id="copy-vertical-bar-chart"> <code class="text-xs p-0 rounded-md html pl-5 pt-8 pb-4 -mb-10 -mt-10"> HTMLOpenTagcanvas id=&quot;vertical-bar-chart-widget&quot; height=&quot;200&quot;HTMLCloseTagHTMLOpenTag/canvasHTMLCloseTag </code> </pre>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
    <!-- END: Vertical Bar Chart -->
</div>

