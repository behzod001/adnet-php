<?php


use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\Values\CampaignObjectiveValues;
use FacebookAds\Object\Values\CampaignSpecialAdCategoriesValues;
use FacebookAds\Object\Values\CampaignSpecialAdCategoryCountryValues;
use FacebookAds\Object\Values\CampaignStatusValues;
use helpers\Alert;
use helpers\StringHelper;

global $api, $app;

if (isset($_GET['service']) && $_GET['service'] == 'direct') {

}
if (isset($_GET['service']) && $_GET['service'] == 'fb') {
    $fields = [
        CampaignFields::ID,
        CampaignFields::NAME,
        CampaignFields::ACCOUNT_ID,
        CampaignFields::SPECIAL_AD_CATEGORY_COUNTRY,
        CampaignFields::SPEND_CAP,
        CampaignFields::OBJECTIVE,
        CampaignFields::STATUS,
        CampaignFields::UPDATED_TIME,
        CampaignFields::STOP_TIME,
        CampaignFields::SOURCE_CAMPAIGN_ID,
        CampaignFields::TOPLINE_ID,
        CampaignFields::RECOMMENDATIONS,
        CampaignFields::SPECIAL_AD_CATEGORIES
    ];

    $campaign = new Campaign($_GET['id'], null, $api);

    if (isset($_POST['Campaign'])) {
        $params = array(
            'name' => $_POST['Campaign'][CampaignFields::NAME],
            'objective' => $_POST['Campaign'][CampaignFields::OBJECTIVE],
            'status' => $_POST['Campaign'][CampaignFields::STATUS],
            'special_ad_categories' => [$_POST['Campaign'][CampaignFields::SPECIAL_AD_CATEGORIES]],
        );
        $campaignResponse = $campaign->updateSelf($fields, $params);
        Alert::success(' Campaign successfully updated for view <a href="/campaigns/view?id=' . $campaignResponse->{CampaignFields::ID} . '">checkout this page</a>');
    } else {
        $params = array(
            'effective_status' => array('ACTIVE', 'PAUSED'),
        );
        $campaignResponse = $campaign->getSelf($fields, $params);
    }
}

?>

<h2 class="intro-y text-lg font-medium mt-10">
    Update Campaign "<?= $campaignResponse->{CampaignFields::NAME}; ?>"
</h2>
<div class="grid grid-cols-12 gap-6 mt-5">
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
        <div class="hidden md:block mx-auto text-gray-600"></div>
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <a href="/campaigns" class="button text-white bg-theme-1 shadow-md mr-2">Go back</a>
        </div>
    </div>
    <div class="intro-y col-span-12 lg:col-span-6">
        <div class="intro-y box p-5">
            <!--    Form begin  -->
            <form method="post" action="/campaigns/update?id=<?= $_GET['id']; ?>">
                <div class="mb-3">
                    <label for="name" class="form-label">Company name</label>
                    <input type="text" name="Campaign[<?= CampaignFields::NAME ?>]" class="input w-full border mt-2"
                           id="name"
                           value="<?= $campaignResponse->{CampaignFields::NAME} ?>" aria-describedby="name">
                </div>
                <div class="mb-3">
                    <label for="status">Select campaign status</label>
                    <select name="Campaign[<?= CampaignFields::STATUS ?>]" class="select2 w-full"
                            aria-label="Default select example" id="status">
                        <option <?= (CampaignStatusValues::PAUSED === $campaignResponse->{CampaignFields::STATUS}) ? "selected" : ""; ?>
                                value="<?= CampaignStatusValues::PAUSED ?>"><?= StringHelper::underscoreToCamelCase(CampaignStatusValues::PAUSED )?></option>
                        <option <?= (CampaignStatusValues::ACTIVE === $campaignResponse->{CampaignFields::STATUS}) ? "selected" : ""; ?>
                                value="<?= CampaignStatusValues::ACTIVE ?>"><?= StringHelper::underscoreToCamelCase(CampaignStatusValues::ACTIVE) ?></option>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="objective">Select campaign objective</label>
                    <select name="Campaign[<?= CampaignFields::OBJECTIVE ?>]" class="select2 w-full"
                            aria-label="Default select example" id="objective">

                        <?php foreach (CampaignObjectiveValues::getInstance()->getValues() as $value) { ?>
                            <option data-subtext="Choose a Campaign Objective" value="<?= $value ?>" <?= ($value === $campaignResponse->{CampaignFields::OBJECTIVE}) ? "selected" : ""; ?> >
                                <?= StringHelper::underscoreToCamelCase($value)?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="category">Select campaign special ad categories</label>
                    <select name="Campaign[<?= CampaignFields::SPECIAL_AD_CATEGORIES ?>]" class="select2 w-full"
                            aria-label="Default select example" id="category">
                        <?php foreach (CampaignSpecialAdCategoriesValues::getInstance()->getValues() as $value) { ?>
                            <option value="<?= $value ?>" <?= ($value === $campaignResponse->{CampaignFields::SPECIAL_AD_CATEGORIES}) ? "selected" : ""; ?> >
                                <?= StringHelper::underscoreToCamelCase($value) ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="country">Select campaign special ad categories</label>
                    <select name="Campaign[<?= CampaignFields::SPECIAL_AD_CATEGORY_COUNTRY ?>]" class="select2 w-full"
                            aria-label="Default select example" id="country">
                        <?php foreach (CampaignSpecialAdCategoryCountryValues::getInstance()->getValues() as $value) { ?>
                            <option value="<?= $value ?>" <?= ($value === $campaignResponse->{CampaignFields::SPECIAL_AD_CATEGORY_COUNTRY}) ? "selected" : ""; ?> >
                                <?= StringHelper::underscoreToCamelCase($value) ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
                <div class="text-right mt-5">
                    <button type="reset" class="button w-24 border text-gray-700 mr-1">Cancel</button>
                    <button type="submit" class="button w-24 bg-theme-1 text-white">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>