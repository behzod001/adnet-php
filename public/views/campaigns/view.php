<?php


use app\exceptions\DataNotFoundException;
use directapi\services\campaigns\criterias\CampaignsSelectionCriteria;
use directapi\services\campaigns\enum\CampaignFieldEnum;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\Values\CampaignStatusValues;
use GuzzleHttp\Exception\GuzzleException;
use helpers\Alert;
use helpers\StringHelper;

global $api, $app;
$service = $_GET['service'];
$fields = [];
$campaignResponse = "";

//if (isset($_GET['active']) && $_GET['service'] == 'fb') {
//
//    if($_GET['active'] ){
//        $params=[
////            'effective_status' => 'ACTIVE',
//            'status' => 'ACTIVE'
//        ];
//    }else{
//        $params=[
////            'effective_status' => 'PAUSED',
//            'status' => 'PAUSED'
//        ];
//    }
//    $campaign = new Campaign($_GET['id'], null, $app->getFacebook());
//    $campaignResponse = $campaign->updateSelf($fields, $params);
//}
if (isset($_GET['service']) && $_GET['service'] == 'fb') {

    $fields = [
        CampaignFields::ID,
        CampaignFields::NAME,
        CampaignFields::ACCOUNT_ID,
        CampaignFields::SPECIAL_AD_CATEGORY_COUNTRY,
        CampaignFields::SPEND_CAP,
        CampaignFields::OBJECTIVE,
        CampaignFields::STATUS,
        CampaignFields::UPDATED_TIME,
        CampaignFields::STOP_TIME,
        CampaignFields::SOURCE_CAMPAIGN_ID,
        CampaignFields::TOPLINE_ID,
        CampaignFields::RECOMMENDATIONS,
        CampaignFields::SPECIAL_AD_CATEGORIES,
        CampaignFields::SPECIAL_AD_CATEGORY,
        CampaignFields::DAILY_BUDGET,
        CampaignFields::LAST_BUDGET_TOGGLING_TIME,
    ];

    $params = array(
        'effective_status' => array('ACTIVE', 'PAUSED'),
    );
    $campaign = new Campaign($_GET['id'], null, $api);
    $campaignResponse = $campaign->getSelf($fields, $params);
}
if (isset($_GET['service']) && $_GET['service'] == 'direct') {

    $fields = [
        CampaignFieldEnum::NAME,
        CampaignFieldEnum::ID,
        CampaignFieldEnum::STATUS,
        CampaignFieldEnum::DAILY_BUDGET,
        CampaignFieldEnum::START_DATE,
        CampaignFieldEnum::CURRENCY,
    ];

    $criteria = new CampaignsSelectionCriteria();
    $criteria->Ids = [$_GET['id']];
    try {
        $campaignResponse = $app->getDirect()->getCampaignsService()->get($criteria, $fields)[0];
    } catch (DataNotFoundException $e) {
        Alert::warning(' У вас есть Yandex Direct аккаунт ? Тогда выберите ваш аккаунт. Если нет создайте и  <a href="/accounts" class="text-theme-1 mx-1"> регистрируйте. </a> ');
    }

}
?>


<div class="grid grid-cols-12 gap-6 mt-5">
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
        <h2 class="intro-y text-lg font-medium ">
            View Campaign
            "<?= ($_GET['service'] == 'fb') ? $campaignResponse->{CampaignFields::NAME} : $campaignResponse->{CampaignFieldEnum::NAME}; ?>
            "
        </h2>
        <div class="hidden md:block mx-auto text-gray-600"></div>
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <div class="w-86 relative text-gray-700">
                <a class="button text-white bg-theme-1 shadow-md mr-2"
                   href="/campaigns/update?service=<?= $service ?>&id=<?= ($_GET['service'] == 'fb') ? $campaignResponse->{CampaignFields::ID} : $campaignResponse->{CampaignFieldEnum::ID}; ?>">Update</a>
                <?php if ($_GET['service'] == 'fb') { ?>
                    <?php if ($campaignResponse->{CampaignFields::STATUS} == CampaignStatusValues::ACTIVE) { ?>
                        <a class="button text-white bg-theme-9 shadow-md mr-2"
                           href="/campaigns/view?active=true&service=fb&id=<?= $_GET['id'] ?>">Publish</a>
                    <?php } ?>
                    <?php if ($campaignResponse->{CampaignFields::STATUS} == CampaignStatusValues::ACTIVE) { ?>
                        <a class="button text-white bg-theme-6 shadow-md mr-2"
                           href="/campaigns/view?active=true&service=fb&id=<?= $_GET['id'] ?>">Disactivate</a>
                    <?php } ?>
                <?php } ?>
                <a class="button text-white bg-theme-1 shadow-md mr-2" href="/campaigns">Go back</a>
            </div>
        </div>
    </div>
    <!-- BEGIN: Data List -->
    <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
        <table class="table table-report -mt-2">
            <thead>
            <tr>
                <th scope="col">Field name</th>
                <th scope="col">Field value</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($fields as $field) { ?>
                <tr class="intro-x">
                    <td><?= StringHelper::underscoreToCamelCase($field); ?></td>
                    <td><?= ($campaignResponse->{$field} == '' || $campaignResponse->{$field} == null) ? "Not defined " : StringHelper::underscoreToCamelCase($campaignResponse->{$field}) ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>

</div>
