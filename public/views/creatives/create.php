<?php

use FacebookAds\Object\AdAccount;
use FacebookAds\Object\AdCreativeLinkData;
use FacebookAds\Object\AdCreativeObjectStorySpec;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\Fields\AdCreativeFields;
use FacebookAds\Object\Fields\AdCreativeLinkDataFields;
use FacebookAds\Object\Fields\AdCreativeObjectStorySpecFields;
use FacebookAds\Object\Fields\AdImageFields;
use FacebookAds\Object\Values\AdCreativeCallToActionTypeValues;
use helpers\Alert;
use helpers\StringHelper;

global $api;

$images = (new AdAccount($_SESSION['user'][AdAccountFields::ID], null, $api))->getAdImages([
    AdImageFields::HASH,
    AdImageFields::NAME
], []);

if (isset($_POST['Ad'])) {
    $post = [
        "AdCreative" => $_POST["Ad"]["AdCreative"],
        "AdCreativeObjectStorySpec" => $_POST["Ad"]["AdCreativeObjectStorySpec"],
        "AdCreativeLinkData" => $_POST["Ad"]['AdCreativeLinkData'],
    ];

    $link_data = new AdCreativeLinkData();
    $link_data->setData([
        AdCreativeLinkDataFields::MESSAGE => $post["AdCreativeLinkData"][AdCreativeLinkDataFields::MESSAGE],
        AdCreativeLinkDataFields::LINK => $post["AdCreativeLinkData"][AdCreativeLinkDataFields::LINK],
        AdCreativeLinkDataFields::IMAGE_HASH => $post['AdCreativeLinkData'][AdCreativeLinkDataFields::IMAGE_HASH],
        AdCreativeLinkDataFields::CALL_TO_ACTION => [
            'type' => $post["AdCreativeLinkData"][AdCreativeLinkDataFields::CALL_TO_ACTION]["AdCreativeCallToActionType"],
            'value' => [
                'link' => $post["AdCreativeLinkData"][AdCreativeLinkDataFields::CALL_TO_ACTION]['url'],
            ]
        ]
    ]);

    $object_story_spec = new AdCreativeObjectStorySpec();
    $object_story_spec->setData(array(
        AdCreativeObjectStorySpecFields::PAGE_ID => $post['AdCreativeObjectStorySpec'][AdCreativeObjectStorySpecFields::PAGE_ID],
        AdCreativeObjectStorySpecFields::INSTAGRAM_ACTOR_ID => $post['AdCreativeObjectStorySpec'][AdCreativeObjectStorySpecFields::INSTAGRAM_ACTOR_ID],
        AdCreativeObjectStorySpecFields::LINK_DATA => $link_data,
    ));

    $params = [
        AdCreativeFields::NAME => $post["AdCreative"][AdCreativeFields::NAME],
        AdCreativeFields::OBJECT_STORY_SPEC => $object_story_spec,
    ];


    $fields = array(
        AdCreativeFields::THUMBNAIL_URL,
        AdCreativeFields::ID,
    );
    try {
        $adCreatives = (new AdAccount($_SESSION['user'][AdAccountFields::ID]))->createAdCreative(
            $fields,
            $params
        );
        Alert::success(' AdCreative successfully created for view <a class="w-auto mx-1 text-thee-1" href="/creatives/view?id=' . $adCreatives->{AdCreativeFields::ID} . '"> checkout this page ');
    } catch (Exception $e) {
        Alert::error($e->getMessage() . ' <a  class="w-auto"  href="/creatives">Back to home</a> ');
    }
}
$calToActions = [
    AdCreativeCallToActionTypeValues::FOLLOW_PAGE,
    AdCreativeCallToActionTypeValues::LIKE_PAGE,
    AdCreativeCallToActionTypeValues::APPLY_NOW,
    AdCreativeCallToActionTypeValues::FOLLOW_PAGE,
    AdCreativeCallToActionTypeValues::BUY_NOW,
    AdCreativeCallToActionTypeValues::WATCH_MORE,
    AdCreativeCallToActionTypeValues::CONTACT_US,
];

?>


<div class="intro-y flex items-center mt-8">
    <h2 class="text-lg font-medium mr-auto">
        Create Creative
    </h2>
</div>
<div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
    <div class="hidden md:block mx-auto text-gray-600"></div>
    <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
        <a href="/creatives" class="button text-white bg-theme-1 shadow-md mr-2">Go back</a>
    </div>
</div>
<form action="/creatives/create" method="post" enctype="multipart/form-data">
    <div class="grid grid-cols-12 gap-6 mt-5 box">

        <div class="intro-y col-span-12 lg:col-span-6">
            <div class="intro-y p-5">
                For create new name template
                <a target="_blank" class="text-theme-1"
                   href="https://www.facebook.com/ads/manager/account_settings/account_name_template/?act=810619469873809&pid=p1&page=account_settings&tab=account_name_template">
                    visit to this page</a></p>
                <div class="mb-3">
                    <label for="<?= AdCreativeFields::NAME ?>"
                    ><?= StringHelper::underscoreToCamelCase(AdCreativeFields::NAME) ?></label>
                    <input type="text"
                           name="<?= "Ad[AdCreative][" . AdCreativeFields::NAME . "]" ?>"
                           class="input w-full border mt-2"
                           id="<?= AdCreativeFields::NAME ?>" aria-describedby=" emailHelp">
                </div>
                <div class="my-3 ">
                    <div class="grid grid-cols-2 gap-6">
                        <div class="intro-y col-span-1">
                            <label for="<?= AdCreativeObjectStorySpecFields::PAGE_ID ?>">
                                <?= StringHelper::underscoreToCamelCase(AdCreativeObjectStorySpecFields::PAGE_ID) ?>
                            </label>
                            <input type="text"
                                   name="<?= "Ad[AdCreativeObjectStorySpec][" . AdCreativeObjectStorySpecFields::PAGE_ID . "]" ?>"
                                   class="input w-full border " value="103324265290960"
                                   id="<?= AdCreativeObjectStorySpecFields::PAGE_ID ?>" aria-describedby="pageid">
                        </div>
                        <div class="intro-y col-span-1">
                            <label for="<?= AdCreativeObjectStorySpecFields::INSTAGRAM_ACTOR_ID ?>">
                                <?= StringHelper::underscoreToCamelCase(AdCreativeObjectStorySpecFields::INSTAGRAM_ACTOR_ID) ?>
                            </label>
                            <input type="text"
                                   name="<?= "Ad[AdCreativeObjectStorySpec][" . AdCreativeObjectStorySpecFields::INSTAGRAM_ACTOR_ID . "]" ?>"
                                   class="input w-full border"
                                   id="<?= AdCreativeObjectStorySpecFields::INSTAGRAM_ACTOR_ID ?>"
                                   aria-describedby="INSTAGRAM_ACTOR_ID">
                        </div>
                        <div id="pageid" class="text-xs text-gray-600  col-span-2"><p>Your Facebook Page or Instagram
                                account
                                represents your business in
                                ads. You can also Create a Facebook Page</p>
                            <p>For Ad Network manager <strong>103324265290960</strong></p>
                        </div>
                    </div>
                </div>
                <!--                <div class="mb-3">-->
                <!--                    <label for="-->
                <? //= AdCreativeObjectStorySpecFields::INSTAGRAM_ACTOR_ID ?><!--"-->
                <!--                          >-->
                <? //= StringHelper::underscoreToCamelCase(AdCreativeObjectStorySpecFields::INSTAGRAM_ACTOR_ID) ?><!--</label>-->
                <!--                    <input type="text"-->
                <!--                           name="-->
                <? //= "Ad[AdCreativeObjectStorySpec][" . AdCreativeObjectStorySpecFields::INSTAGRAM_ACTOR_ID . "]" ?><!--"-->
                <!--                           class="input w-full border mt-2"-->
                <!--                           id="-->
                <? //= AdCreativeObjectStorySpecFields::INSTAGRAM_ACTOR_ID ?><!--"-->
                <!--                           aria-describedby="INSTAGRAM_ACTOR_ID">-->
                <!--                    <div id="INSTAGRAM_ACTOR_ID">You can add instagram page</div>-->
                <!--                </div>-->

                <div class="my-3">
                    <label for="<?= AdCreativeLinkDataFields::IMAGE_HASH ?>"
                    >Image for creative </label>
                    <select class="select2 w-full" data-show-subtext="true" data-live-search="true" data-size="8"
                            name="Ad[AdCreativeLinkData][<?= AdCreativeLinkDataFields::IMAGE_HASH ?>]"
                            aria-label="Default select example">
                        <option selected>Select one of the image for creative</option>
                        <?php foreach ($images as $value) { ?>
                            <option <?= $value->{AdImageFields::HASH} ?>>
                                <?= $value->{AdImageFields::NAME} ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>

            </div>
        </div>
        <div class="intro-y col-span-12 lg:col-span-6">
            <div class="intro-y p-5">
                <div class="mb-3">
                    <div class="mb-3">
                        <label for="<?= AdCreativeLinkDataFields::CALL_TO_ACTION ?>"
                        >Call to action button types </label>
                        <!--                        call to action AdCreativeCallToActionTypeValues -->
                        <select class="select2 w-full"
                                name="<?= "Ad[AdCreativeLinkData][" . AdCreativeLinkDataFields::CALL_TO_ACTION . "]" . "[AdCreativeCallToActionType]" ?>"
                                aria-label="Default select example">
                            <option selected>Select one of call to action type</option>
                            <?php foreach ($calToActions as $value) {
                                echo "<option value='" . $value . "'>" . StringHelper::underscoreToCamelCase($value) . "</option>";
                            } ?>
                        </select>
                    </div>
                    <label for="<?= AdCreativeLinkDataFields::CALL_TO_ACTION ?>"
                    >Call to action button endpoint url</label>
                    <input type="url"
                           name="<?= "Ad[AdCreativeLinkData][" . AdCreativeLinkDataFields::CALL_TO_ACTION . "]" ?>[url]"
                           class="input w-full border mt-2"
                           id="<?= AdCreativeLinkDataFields::CALL_TO_ACTION ?>" aria-describedby="weblink">
                    <div id="weblink">Enter the URL for the web page you want people to visit.</div>
                </div>


                <div class="mb-3">
                    <label for="<?= AdCreativeLinkDataFields::MESSAGE ?>"
                    ><?= StringHelper::underscoreToCamelCase(AdCreativeLinkDataFields::MESSAGE) ?></label>
                    <input type="text"
                           name="<?= "Ad[AdCreativeLinkData][" . AdCreativeLinkDataFields::MESSAGE . "]" ?>"
                           class="input w-full border mt-2"
                           id="<?= AdCreativeLinkDataFields::MESSAGE ?> " aria-describedby=" emailHelp">
                </div>

                <div class="mb-3">
                    <label for="<?= AdCreativeLinkDataFields::LINK ?>"
                    ><?= StringHelper::underscoreToCamelCase(AdCreativeLinkDataFields::LINK) ?>
                        for view </label>
                    <input type="text"
                           name="<?= "Ad[AdCreativeLinkData][" . AdCreativeLinkDataFields::LINK . "]" ?>"
                           class="input w-full border mt-2"
                           id="<?= AdCreativeLinkDataFields::LINK ?> " aria-describedby="displaylink">
                    <div id="displaylink">Show a shortened link instead of your full website URL in some
                        placements. The link should go to the same domain as your website URL. The position of
                        the display link varies by placement.
                    </div>
                </div>
            </div>

            <div class="text-right m-5">
                <button type="reset" class="button w-24 border text-gray-700 mr-1">Cancel</button>
                <button type="submit" class="button w-24 bg-theme-1 text-white">Save</button>
            </div>
        </div>
    </div>
</form>