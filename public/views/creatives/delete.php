<?php


use FacebookAds\Object\AdCreative;
use helpers\Alert;

global $api;

if (isset($_GET['id'])) {
    $creative = new AdCreative($_GET['id'], null, $api);

    try {
        $creative->deleteSelf();
        Alert::success(' AdCreative successfully deleted <a class="mx-1 text-theme-1" href="/creatives"> back to the index page</a> ');
    } catch (Exception $exception) {
        Alert::error($exception->getMessage());
    }
}

