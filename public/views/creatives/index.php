<?php

use app\exceptions\DataNotFoundException;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\Fields\AdCreativeFields;
use helpers\Alert;
use helpers\StringHelper;

global $app;


$fields = array(
    AdCreativeFields::ID,
    AdCreativeFields::NAME,
    AdCreativeFields::THUMBNAIL_URL,
    AdCreativeFields::STATUS,
    AdCreativeFields::IMAGE_URL,
    AdCreativeFields::ADLABELS,
    AdCreativeFields::BODY,
    AdCreativeFields::ACTOR_ID,
    AdCreativeFields::TITLE,
    AdCreativeFields::LINK_DESTINATION_DISPLAY_URL,
    AdCreativeFields::LINK_OG_ID,
    AdCreativeFields::LINK_URL,
    AdCreativeFields::ADLABELS,
    AdCreativeFields::CALL_TO_ACTION_TYPE,
//    AdCreativeFields::IMAGE_HASH,
);


try {
    $params = array(
        'thumbnail_width' => 150,
        'thumbnail_height' => 120,
    );
    $adCreatives = $app->getFacebookActiveAccount()->getAdCreatives(
        $fields,
        $params
    );
} catch (DataNotFoundException $e) {
    Alert::warning(' У вас есть Фасебоок аккаунт ? Тогда выберите ваш аккаунт. Если нет создайте и  <a href="/accounts" class="text-theme-1 mx-1"> регистрируйте. </a> ');
}
?>
<h2 class="intro-y text-lg font-medium mt-10">
    Data List Ad Creatives
    <div class="text-xs text-gray-600 mt-2">Создание креативов доступно только в
        <a class="text-theme-1"
           href="https://direct.yandex.ru/registered/main.pl?ulogin=bexzodxayrullayev&retpath=https%3A%2F%2Fdirect.yandex.ru%2Fregistered%2Fmain.pl%3Fulogin%3Dbexzodxayrullayev%26retpath%3Dhttps%3A%2F%2Fdirect.yandex.ru%2Fregistered%2Fmain.pl%3Fulogin%3Dbexzodxayrullayev%26cmd%3DshowRetargetingCond%26csrf_token%3D4_lUqeM19HyDJ6o6%26cmd%3DshowCreatives%26csrf_token%3DknNYRApZy33yraa_&cmd=showCreatives&csrf_token=PdbaALgSAJBdCdqJ">
            веб-интерфейсе Директа </a>. Редактирование креативов не предусмотрено.
    </div>
</h2>
<div class="grid grid-cols-12 gap-6 mt-5">
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
        <button class="button text-white bg-theme-1 shadow-md mr-2">Add New Creative</button>
        <div class="dropdown relative">
            <button class=" button px-2 box text-gray-700">
                <a href="/creatives/create">
                    <span class="w-5 h-5 flex items-center justify-center">
                        <i class="w-4 h-4" data-feather="plus"></i>
                    </span>
                </a>
            </button>
        </div>
        <div class="hidden md:block mx-auto text-gray-600">Showing 1 to 10 of 150 entries</div>
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <div class="w-56 relative text-gray-700">
                <input type="text" class="input w-56 box pr-10 placeholder-theme-13" placeholder="Search...">
                <i class="w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0" data-feather="search"></i>
            </div>
        </div>
    </div>
    <!-- BEGIN: Data List -->
    <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
        <table class="table table-report -mt-2">
            <thead>
            <tr>
                <th scope="col">Images</th>
                <th scope="col">Name of Ad creative</th>
                <?php unset($fields[0], $fields[1], $fields[2], $fields[3], $fields[4]);
                foreach ($fields as $field) { ?>
                    <th scope="col"><?= StringHelper::underscoreToCamelCase($field); ?></th>
                <?php } ?>
                <th scope="col">Status</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
            <?php if (isset($adCreatives)) {
                $idx = 0;
                foreach ($adCreatives as $adCreative) { ?>
                    <tr>
                        <td class="w-40">
                            <div class="flex">
                                <div class="w-10 h-10 image-fit zoom-in">
                                    <img alt="AdNet" class="tooltip rounded-full"
                                         src="<?= $adCreative->{AdCreativeFields::THUMBNAIL_URL} ?>"
                                         title="Uploaded at 17 July 2021">
                                </div>
                                <div class="w-10 h-10 image-fit zoom-in -ml-5">
                                    <img alt="AdNet" class="tooltip rounded-full"
                                         src="<?= $adCreative->{AdCreativeFields::THUMBNAIL_URL} ?>"
                                         title="Uploaded at 17 July 2021">
                                </div>
                            </div>
                        </td>
                        <td>
                            <a href="/creatives/view?id=<?= $adCreative->{AdAccountFields::ID} ?>"
                               class="font-medium whitespace-no-wrap">

                                <?= strlen($adCreative->{AdCreativeFields::NAME}) > 30 ? mb_substr($adCreative->{AdCreativeFields::NAME}, 0, 30, "utf-8") . "..." : $adCreative->{AdCreativeFields::NAME}; ?>
                            </a>
                            <div class="text-gray-600 text-xs whitespace-no-wrap">
                                <div class="text-left w-auto">
                                    <span class="bg-theme-14 text-theme-10 rounded px-2 mt-1 text-center mx-1"> Facebook </span>
                                    id <?= $adCreative->{AdCreativeFields::ID} ?>
                                </div>
                            </div>
                        </td>
                        <?php foreach ($fields as $field) { ?>
                            <td>
                                <?= strlen($adCreative->{$field}) > 80 ? mb_substr($adCreative->{$field}, 0, 80, "utf-8") . "..." : $adCreative->{$field}; ?>
                            </td>
                        <?php } ?>
                        <td class="w-40">
                            <div class="flex items-center justify-center text-theme-6">
                                <i data-feather="check-square" class="w-4 h-4 mr-2"></i>
                                <?= $adCreative->{AdCreativeFields::STATUS} ?>
                            </div>
                        </td>
                        <td class="table-report__action w-auto">
                            <div class="flex justify-center items-center">
                                <a href="/creatives/view?id=<?= $adCreative->{AdCreativeFields::ID} ?>"
                                   class="flex items-center mr-3">
                                    <i data-feather="eye" class=" w-4 h-4 mr-1"></i>
                                    View
                                </a>
                                <a class="flex items-center mr-3"
                                   href="/creatives/update?id=<?= $adCreative->{AdCreativeFields::ID} ?>">
                                    <i data-feather="check-square" class="w-4 h-4 mr-1"></i>
                                    Edit
                                </a>
                                <a class="flex items-center text-theme-6" href="javascript:"
                                   onclick="confirmDelete(this);"
                                   data-toggle="modal"
                                   data-target="#delete-confirmation-modal"
                                   data-delete="/creatives/delete?id=<?= $adCreative->{AdCreativeFields::ID} ?>">
                                    <i data-feather="trash-2" class="w-4 h-4 mr-1"></i>
                                    Delete
                                </a>
                            </div>
                        </td>
                    </tr>
                <?php }
            } ?>
            </tbody>
        </table>
    </div>
    <!-- END: Data List -->
    <!-- BEGIN: Pagination -->
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-row sm:flex-no-wrap items-center">
        <ul class="pagination">
            <li>
                <a class="pagination__link" href=""> <i class="w-4 h-4" data-feather="chevrons-left"></i> </a>
            </li>
            <li>
                <a class="pagination__link" href=""> <i class="w-4 h-4" data-feather="chevron-left"></i> </a>
            </li>
            <li><a class="pagination__link" href="">...</a></li>
            <li><a class="pagination__link pagination__link--active" href="">1</a></li>
            <li><a class="pagination__link " href="">2</a></li>
            <li><a class="pagination__link" href="">3</a></li>
            <li><a class="pagination__link" href="">...</a></li>
            <li>
                <a class="pagination__link" href=""> <i class="w-4 h-4" data-feather="chevron-right"></i> </a>
            </li>
            <li>
                <a class="pagination__link" href=""> <i class="w-4 h-4" data-feather="chevrons-right"></i> </a>
            </li>
        </ul>
        <select class="w-20 input box mt-3 sm:mt-0">
            <option>10</option>
            <option>25</option>
            <option>35</option>
            <option>50</option>
        </select>
    </div>
    <!-- END: Pagination -->
</div>
<!-- BEGIN: Delete Confirmation Modal -->
<div class="modal" id="delete-confirmation-modal">
    <div class="modal__content">
        <div class="p-5 text-center">
            <i data-feather="x-circle" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i>
            <div class="text-3xl mt-5">Are you sure?</div>
            <div class="text-gray-600 mt-2">Do you really want to delete these records? This process cannot be
                undone.
            </div>
        </div>
        <div class="px-5 pb-8 text-center">
            <button type="button" data-dismiss="modal" class="button w-24 border text-gray-700 mr-1">Cancel</button>
            <a href="" id="deleteBtn" type="button" class="button w-24 bg-theme-6 text-white">Delete</a>
        </div>
    </div>
</div>
<!-- END: Delete Confirmation Modal -->

