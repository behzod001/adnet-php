<?php

use FacebookAds\Object\AdCreative;
use FacebookAds\Object\Fields\AdCreativeFields;
use helpers\Alert;

global $api;
$creative = new AdCreative($_GET['id'], null, $api);
$adCreative = $creative->getSelf([
    AdCreativeFields::NAME,
    AdCreativeFields::ID,
]);
if (isset($_POST['AdCreative'])) {
    try {
        $creative->updateSelf([
            AdCreativeFields::NAME,
            AdCreativeFields::ID,
        ], [
            AdCreativeFields::NAME => $_POST['AdCreative'][AdCreativeFields::NAME],
        ]);
        Alert::success(' <i data-feather="check-square" class="w-6 h-6 mr-2"></i>  AdCreative successfully updated for view <a class="mx-1" href="/creatives/view?id=' . $creative->{AdCreativeFields::ID} . '"> checkout this page</a> ');
    } catch (Exception $exception) {
        Alert::error(' <i data-feather="alert-circle" class="w-6 h-6 mr-2"></i> ' . $exception->getMessage());
    }
}

?>
<div class="intro-y flex items-center mt-8">
    <h2 class="text-lg font-medium mr-auto">
        Update new Ad Creative "<?= $adCreative->{AdCreativeFields::NAME} ?>"
    </h2>
</div>
<div class="grid grid-cols-12 gap-6 mt-5">
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
        <div class="hidden md:block mx-auto text-gray-600"></div>
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <a href="/creatives" class="button text-white bg-theme-1 shadow-md mr-2">Go back</a>
        </div>
    </div>

    <div class="intro-y col-span-12 lg:col-span-6">
        <div class="intro-y box p-5">
            <form action="/creatives/update?id=<?= $adCreative->{AdCreativeFields::ID} ?>" method="post">
                <div class="mb-3">
                    <label for="<?= AdCreativeFields::NAME ?>"
                    ><?= ucfirst(str_replace("_", " ", AdCreativeFields::NAME)) ?></label>
                    <input type="text"
                           name="<?= "AdCreative[" . AdCreativeFields::NAME . "]" ?>"
                           class="input w-full border mt-2" value="<?= $adCreative->{AdCreativeFields::NAME} ?>"
                           id="<?= AdCreativeFields::NAME ?>" aria-describedby="nameHelp">
                    <div id="nameHelp">The name of the creative in the creative library. This field takes a string of up
                        to 100
                        characters.
                    </div>
                </div>
                <div class="text-right mt-5">
                    <button type="reset" class="button w-24 border text-gray-700 mr-1">Cancel</button>
                    <button type="submit" class="button w-24 bg-theme-1 text-white">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
