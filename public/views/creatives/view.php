<?php

use FacebookAds\Object\AdCreative;
use FacebookAds\Object\Fields\AdPreviewFields;
use FacebookAds\Object\Values\AdPreviewAdFormatValues;
use helpers\StringHelper;

global $api;


$fields = array(
    AdPreviewFields::BODY,
    AdPreviewFields::AD_FORMAT,
    AdPreviewFields::POST,
    AdPreviewFields::PRODUCT_ITEM_IDS,
);
$adCreative = new AdCreative($_GET['id'], null, $api);


$params = [
    AdPreviewAdFormatValues::FACEBOOK_STORY_STICKER_MOBILE,
    AdPreviewAdFormatValues::DESKTOP_FEED_STANDARD,
    AdPreviewAdFormatValues::MOBILE_FEED_STANDARD,
    AdPreviewAdFormatValues::INSTREAM_VIDEO_MOBILE,
    AdPreviewAdFormatValues::SUGGESTED_VIDEO_MOBILE,
    AdPreviewAdFormatValues::MARKETPLACE_MOBILE,
    AdPreviewAdFormatValues::SUGGESTED_VIDEO_MOBILE,
    AdPreviewAdFormatValues::MESSENGER_MOBILE_INBOX_MEDIA,
    AdPreviewAdFormatValues::MESSENGER_MOBILE_STORY_MEDIA,
    AdPreviewAdFormatValues::INSTREAM_VIDEO_IMAGE,
];
?>

<h2 class="intro-y text-lg font-medium mt-10">
    View Ad Creative
</h2>

<div class="grid grid-cols-12 gap-6 mt-5">
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
        <button class="button text-white bg-theme-1 shadow-md mr-2">Add New Creative</button>
        <div class="dropdown relative">
            <button class=" button px-2 box text-gray-700">
                <a href="/creatives/create">
                    <span class="w-5 h-5 flex items-center justify-center">
                        <i class="w-4 h-4" data-feather="plus"></i>
                    </span>
                </a>
            </button>
        </div>
        <div class="hidden md:block mx-auto text-gray-600"></div>
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <div class="w-56 relative text-gray-700">
                <a class="button text-white bg-theme-1 shadow-md mr-2"
                   href="/creatives/update?id=<?= $_GET['id'] ?>">Update</a>
                <a class="button text-white bg-theme-1 shadow-md mr-2" href="/creatives">Go back</a>
            </div>
        </div>
    </div>
    <!-- BEGIN: Data List -->
    <div class="intro-y col-span-12 md:col-span-6 lg:col-span-4 p-5">
        <div class="box py-5">
            <h3 class="font-medium m-5"><?= StringHelper::underscoreToCamelCase(AdPreviewAdFormatValues::MARKETPLACE_MOBILE) ?></h3>
            <div class="text-center lg:text-left p-5" id="<?= AdPreviewAdFormatValues::MARKETPLACE_MOBILE ?>">
                <?php
                $res = json_decode($adCreative->getPreviews($fields, ['ad_format' => AdPreviewAdFormatValues::MARKETPLACE_MOBILE])->getLastResponse()->getBody());
                $dom = new DOMDocument();
                $dom->loadHTML($res->data[0]->body);
                $elem = $dom->getElementsByTagName('iframe')->item(0);
                $elem->setAttribute('width', 400);
                $elem->setAttribute('height', 400);
                $elem->setAttribute('scrolling', 'no');
                echo $dom->saveHTML($elem);
                unset($dom, $elem, $res);
                ?>
            </div>
        </div>
    </div>
    <div class="intro-y col-span-12 md:col-span-6 lg:col-span-4 p-5">
        <div class="box py-5">
            <h3 class="font-medium m-5"><?= StringHelper::underscoreToCamelCase(AdPreviewAdFormatValues::FACEBOOK_STORY_STICKER_MOBILE) ?></h3>
            <div class="text-center lg:text-left p-5"
                 id="<?= AdPreviewAdFormatValues::FACEBOOK_STORY_STICKER_MOBILE ?>">

                <?php
                $res = json_decode($adCreative->getPreviews($fields, ['ad_format' => AdPreviewAdFormatValues::FACEBOOK_STORY_STICKER_MOBILE])->getLastResponse()->getBody());
                $dom = new DOMDocument();
                $dom->loadHTML($res->data[0]->body);
                $elem = $dom->getElementsByTagName('iframe')->item(0);
                $elem->setAttribute('width', 400);
                $elem->setAttribute('height', 580);
                $elem->setAttribute('scrolling', 'no');
                echo $dom->saveHTML($elem);
                unset($dom, $elem, $res);
                ?>
            </div>
        </div>
    </div>
    <div class="intro-y col-span-12 md:col-span-6 lg:col-span-4 p-5">
        <div class="box py-5">
            <h3 class="font-medium m-5"><?= StringHelper::underscoreToCamelCase(AdPreviewAdFormatValues::RIGHT_COLUMN_STANDARD) ?></h3>
            <div class="text-center lg:text-left p-5">
                <?php
                $res = json_decode($adCreative->getPreviews($fields, ['ad_format' => AdPreviewAdFormatValues::RIGHT_COLUMN_STANDARD])->getLastResponse()->getBody());
                $dom = new DOMDocument();
                $dom->loadHTML($res->data[0]->body);
                $elem = $dom->getElementsByTagName('iframe')->item(0);
                $elem->setAttribute('width', 259);
                $elem->setAttribute('height', 83);
                $elem->setAttribute('scrolling', 'no');
                echo $dom->saveHTML($elem);
                unset($dom, $elem, $res);
                ?>
            </div>
        </div>
    </div>
    <div class="intro-y col-span-12 md:col-span-6 lg:col-span-4 p-5">
        <div class="box py-5">
            <h3 class="font-medium m-5"><?= StringHelper::underscoreToCamelCase(AdPreviewAdFormatValues::INSTANT_ARTICLE_STANDARD) ?></h3>
            <div class="text-center lg:text-left p-5 ml-10">
                <?php
                $res = json_decode($adCreative->getPreviews($fields, ['ad_format' => AdPreviewAdFormatValues::INSTANT_ARTICLE_STANDARD])->getLastResponse()->getBody());
                $dom = new DOMDocument();
                $dom->loadHTML($res->data[0]->body);
                $elem = $dom->getElementsByTagName('iframe')->item(0);
                $elem->setAttribute('width', 320);
                $elem->setAttribute('height', 301);
                $elem->setAttribute('scrolling', 'no');
                echo $dom->saveHTML($elem);
                unset($dom, $elem, $res);
                ?>
            </div>
        </div>
    </div>
    <div class="intro-y col-span-12 md:col-span-6 lg:col-span-5 p-5">
        <div class="box py-5">
            <h3 class="font-medium m-5"><?= StringHelper::underscoreToCamelCase(AdPreviewAdFormatValues::DESKTOP_FEED_STANDARD) ?></h3>
            <div class="text-center lg:text-left p-5">
                <?php
                $res = json_decode($adCreative->getPreviews($fields, ['ad_format' => AdPreviewAdFormatValues::DESKTOP_FEED_STANDARD])->getLastResponse()->getBody());
                $dom = new DOMDocument();
                $dom->loadHTML($res->data[0]->body);
                $elem = $dom->getElementsByTagName('iframe')->item(0);
                $elem->setAttribute('width', 525);
                $elem->setAttribute('height', 485);
                $elem->setAttribute('scrolling', 'no');
                echo $dom->saveHTML($elem);
                unset($dom, $elem, $res);
                ?>
            </div>
        </div>
    </div>
    <div class="intro-y col-span-12 md:col-span-6 lg:col-span-4 p-5">
        <div class="box py-5">
            <h3 class="font-medium m-5"><?= StringHelper::underscoreToCamelCase(AdPreviewAdFormatValues::MOBILE_FEED_STANDARD) ?></h3>
            <div class="text-center lg:text-left p-4">
                <?php
                $res = json_decode($adCreative->getPreviews($fields, ['ad_format' => AdPreviewAdFormatValues::MOBILE_FEED_STANDARD])->getLastResponse()->getBody());
                $dom = new DOMDocument();
                $dom->loadHTML($res->data[0]->body);
                $elem = $dom->getElementsByTagName('iframe')->item(0);
                $elem->setAttribute('width', 320);
                $elem->setAttribute('height', 385);
                $elem->setAttribute('scrolling', 'no');
                echo $dom->saveHTML($elem);
                unset($dom, $elem, $res);
                ?>
            </div>
        </div>
    </div>
    <div class="intro-y col-span-12 md:col-span-6 lg:col-span-4 p-5">
        <div class="box py-5">
            <h3 class="font-medium m-5"><?= StringHelper::underscoreToCamelCase(AdPreviewAdFormatValues::SUGGESTED_VIDEO_MOBILE) ?></h3>
            <div class="text-center lg:text-left p-5">
                <?php
                $res = json_decode($adCreative->getPreviews($fields, ['ad_format' => AdPreviewAdFormatValues::SUGGESTED_VIDEO_MOBILE])->getLastResponse()->getBody());
                $dom = new DOMDocument();
                $dom->loadHTML($res->data[0]->body);
                $elem = $dom->getElementsByTagName('iframe')->item(0);
                $elem->setAttribute('width', 400);
                $elem->setAttribute('height', 400);
                $elem->setAttribute('scrolling', 'no');
                echo $dom->saveHTML($elem);
                unset($dom, $elem, $res);
                ?>
            </div>
        </div>
    </div>

    <div class="intro-y col-span-12 md:col-span-6 lg:col-span-4 p-5">
        <div class="box py-5">
            <h3 class="font-medium m-5"><?= StringHelper::underscoreToCamelCase(AdPreviewAdFormatValues::INSTREAM_VIDEO_MOBILE) ?></h3>
            <div class="text-center lg:text-left p-5 ml-8">
                <?php
                $res = json_decode($adCreative->getPreviews($fields, ['ad_format' => AdPreviewAdFormatValues::INSTREAM_VIDEO_MOBILE])->getLastResponse()->getBody());
                $dom = new DOMDocument();
                $dom->loadHTML($res->data[0]->body);
                $elem = $dom->getElementsByTagName('iframe')->item(0);
                $elem->setAttribute('width', 385);
                $elem->setAttribute('height', 375);
                $elem->setAttribute('scrolling', 'no');
                echo $dom->saveHTML($elem);
                unset($dom, $elem, $res);
                ?>
            </div>
        </div>
    </div>

    <div class="intro-y col-span-12 md:col-span-6 lg:col-span-4 p-5">
        <div class="box py-5">
            <h3 class="font-medium m-5"><?= StringHelper::underscoreToCamelCase(AdPreviewAdFormatValues::SUGGESTED_VIDEO_MOBILE) ?></h3>
            <div class="text-center lg:text-left p-5">
                <?php
                $res = json_decode($adCreative->getPreviews($fields, ['ad_format' => AdPreviewAdFormatValues::SUGGESTED_VIDEO_MOBILE])->getLastResponse()->getBody());
                $dom = new DOMDocument();
                $dom->loadHTML($res->data[0]->body);
                $elem = $dom->getElementsByTagName('iframe')->item(0);
                $elem->setAttribute('width', 385);
                $elem->setAttribute('height', 375);
                $elem->setAttribute('scrolling', 'no');
                echo $dom->saveHTML($elem);
                unset($dom, $elem, $res);
                ?>
            </div>
        </div>
    </div>
    <div class="intro-y col-span-12 md:col-span-6 lg:col-span-4 p-5">
        <div class="box py-5">
            <h3 class="font-medium m-5"><?= StringHelper::underscoreToCamelCase(AdPreviewAdFormatValues::WATCH_FEED_HOME) ?></h3>
            <div class="text-center lg:text-left p-5 ml-3">
                <?php
                $res = json_decode($adCreative->getPreviews($fields, ['ad_format' => AdPreviewAdFormatValues::WATCH_FEED_HOME])->getLastResponse()->getBody());
                $dom = new DOMDocument();
                $dom->loadHTML($res->data[0]->body);
                $elem = $dom->getElementsByTagName('iframe')->item(0);
                $elem->setAttribute('width', 385);
                $elem->setAttribute('height', 375);
                $elem->setAttribute('scrolling', 'no');
                echo $dom->saveHTML($elem);
                unset($dom, $elem, $res);
                ?>
            </div>
        </div>
    </div>
    <div class="intro-y col-span-12 md:col-span-6 lg:col-span-4 p-5">
        <div class="box py-5">
            <h3 class="font-medium m-5"><?= StringHelper::underscoreToCamelCase(AdPreviewAdFormatValues::WATCH_FEED_MOBILE) ?></h3>
            <div class="text-center lg:text-left p-5">
                <?php
                $res = json_decode($adCreative->getPreviews($fields, ['ad_format' => AdPreviewAdFormatValues::WATCH_FEED_MOBILE])->getLastResponse()->getBody());
                $dom = new DOMDocument();
                $dom->loadHTML($res->data[0]->body);
                $elem = $dom->getElementsByTagName('iframe')->item(0);
                $elem->setAttribute('width', 385);
                $elem->setAttribute('height', 375);
                $elem->setAttribute('scrolling', 'no');
                echo $dom->saveHTML($elem);
                unset($dom, $elem, $res);
                ?>
            </div>
        </div>
    </div>
    <div class="intro-y col-span-12 md:col-span-6 lg:col-span-5 p-5">
        <div class="box py-5">
            <h3 class="font-medium m-5"><?= StringHelper::underscoreToCamelCase(AdPreviewAdFormatValues::SUGGESTED_VIDEO_DESKTOP) ?></h3>
            <div class="text-center lg:text-left p-5">
                <?php
                $res = json_decode($adCreative->getPreviews($fields, ['ad_format' => AdPreviewAdFormatValues::SUGGESTED_VIDEO_DESKTOP])->getLastResponse()->getBody());
                $dom = new DOMDocument();
                $dom->loadHTML($res->data[0]->body);
                $elem = $dom->getElementsByTagName('iframe')->item(0);
                $elem->setAttribute('width', 385);
                $elem->setAttribute('height', 375);
                $elem->setAttribute('scrolling', 'no');
                echo $dom->saveHTML($elem);
                unset($dom, $elem, $res);
                ?>
            </div>
        </div>
    </div>
</div>
