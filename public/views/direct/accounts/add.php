<?php

use directapi\common\enum\clients\PrivilegeEnum;
use directapi\common\enum\CurrencyEnum;
use directapi\common\enum\LangEnum;
use directapi\common\enum\YesNoEnum;
use directapi\exceptions\DirectAccountNotExistException;
use directapi\exceptions\DirectApiException;
use directapi\exceptions\DirectApiNotEnoughUnitsException;
use directapi\exceptions\LoginIsUsedAlreadyException;
use directapi\exceptions\RequestValidationException;
use directapi\services\agencyclients\enum\ClientSettingAddEnum;
use directapi\services\agencyclients\enum\EmailSubscriptionEnum;
use directapi\services\agencyclients\models\AgencyClientAdd;
use directapi\services\agencyclients\models\ClientSettingAddItem;
use directapi\services\agencyclients\models\EmailSubscriptionItem;
use directapi\services\agencyclients\models\GrantItem;
use directapi\services\agencyclients\models\NotificationAdd;
use GuzzleHttp\Exception\GuzzleException;
use helpers\Alert;
use helpers\StringHelper;
use models\Account;

if (isset($_POST['Direct'])) {
    global $app;
    $post['login'] = $_POST['Direct']['login'];
    $post['firstName'] = $_POST['Direct']['first_name'];
    $post['lastName'] = $_POST['Direct']['last_name'];
    $post['currency'] = $_POST['Direct']['currency'];
    $post['grants'] = $_POST['Direct']['grants'];
    $post['notification'] = $_POST['Direct']['notification'];
    $post['settings'] = $_POST['Direct']['settings'];

    $agencyClientAdd = new AgencyClientAdd();
    $agencyClientAdd->FirstName = $post['firstName'];
    $agencyClientAdd->LastName = $post['lastName'];
    $agencyClientAdd->Login = $post['login'];
    $agencyClientAdd->Currency = $post['currency'];

    $agencyClientAdd->Grants = [];
    foreach (PrivilegeEnum::getValues() as $grantValue) {
        $grantItem = new GrantItem();
        $grantItem->Privilege = $grantValue;
        $grantItem->Value = in_array($grantValue, $post['grants']['privilege']) ? YesNoEnum::YES : YesNoEnum::NO;
        array_push($agencyClientAdd->Grants, $grantItem);
    }


    $notification = new NotificationAdd();
    $notification->Lang = $post['notification']['language'];
    $notification->Email = $post['notification']['email'];

    $notification->EmailSubscriptions = [];
    foreach (EmailSubscriptionEnum::getValues() as $subscribtionItemVal) {
        $subscriptionItem = new EmailSubscriptionItem();
        $subscriptionItem->Option = $subscribtionItemVal;
        $subscriptionItem->Value = in_array($subscribtionItemVal, $post['notification']['subscription']['option']) ? YesNoEnum::YES : YesNoEnum::NO;
        array_push($notification->EmailSubscriptions, $subscriptionItem);
    }

    $agencyClientAdd->Settings = [];
    foreach (ClientSettingAddEnum::getValues() as $settingsItem) {
        $settingsAddItem = new ClientSettingAddItem();
        $settingsAddItem->Option = $settingsItem;
        $settingsAddItem->Value = in_array($settingsItem, $post['settings']['option']) ? YesNoEnum::YES : YesNoEnum::NO;
        array_push($agencyClientAdd->Settings, $settingsAddItem);
    }
    $agencyClientAdd->Notification = $notification;
    try {
        $app->getDirect()->getAgencyClientsService()->add($agencyClientAdd);
        Alert::success(' <i data-feather="check-square" class="w-6 h-6 mr-2"></i> Service account successfully added to yandex direct account ');

        // add to database hostory
        $model = new Account();
        $newAccount[Account::USER_ID] = $app->getIdentity(true)->id;
        $newAccount[Account::ACCOUNT_ID] = $agencyClientAdd->Login;
        $newAccount[Account::SERVICE_ID] = Account::SERVICE_ID_YANDEX;
        $model->load($newAccount);

        if ($model->validate() && $model->create()) {
            Alert::success(' <i data-feather="check-square" class="w-6 h-6 mr-2"></i> Service account successfully added to user account ');
        } else {
            Alert::error(' <i data-feather="alert-triangle" class="w-6 h-6 mr-2"></i> Something went wrong. Service account not added.');
        }

    } catch (GuzzleException | JsonMapper_Exception | DirectAccountNotExistException | LoginIsUsedAlreadyException | RequestValidationException | DirectApiNotEnoughUnitsException | DirectApiException $e) {
        Alert::error(' <i data-feather="alert-triangle" class="w-6 h-6 mr-2"></i> Something went wrong. Service account not added.');
    }
}

?>


<div class="intro-y flex items-center mt-8">
    <h2 class="text-lg font-medium mr-auto">
        Add new service account
    </h2>
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
        <div class="hidden md:block mx-auto text-gray-600"></div>
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <a href="/accounts" class="button text-white bg-theme-1 shadow-md mr-2">Go back</a>
        </div>
    </div>
</div>
<form action="/direct/add" method="post">
    <div class="grid grid-cols-12 gap-6 mt-5 box">

        <div class="intro-y col-span-12 lg:col-span-6 ">
            <div class="intro-y  p-5">
                <p class="text-lg font-medium mr-auto">Fill the form</p>

                <div class="mb-3">
                    <label for="firstname">First name
                        <i data-feather="alert-circle" data-theme="light"
                           class="border-theme-1 text-gray-700 rounded-md w-4 h-4 mr-2  tooltip ro inline-block text-white"
                           title="Имя пользователя (не более 20 символов).">
                            Имя пользователя (не более 20 символов). Не допускаются символы &=<>.
                        </i>
                    </label>
                    <input type="text" class="input w-full border mt-2" name="Direct[first_name]"
                           id="first_name" maxlength="20">
                </div>
                <div class="mb-3">
                    <label for="last_name">Last name
                        <i data-feather="alert-circle" data-theme="light"
                           class="border-theme-1 text-gray-700 rounded-md w-4 h-4 mr-2  tooltip ro inline-block text-white"
                           title="Фамилия пользователя (не более 20 символов).">
                            Фамилия пользователя (не более 20 символов). Не допускаются символы &=<>.
                        </i>
                    </label>
                    <input type="text" class="input w-full border mt-2" name="Direct[last_name]"
                           id="last_name" maxlength="20">
                </div>
                <div class="mb-3">
                    <label for="login">Login
                        <i data-feather="alert-circle" data-theme="light"
                           class="border-theme-1 text-gray-700 rounded-md w-4 h-4 mr-2  tooltip ro inline-block text-white"
                           title="Логин может состоять из латинских символов, цифр, одинарного дефиса или точки. Он должен
                            начинаться с буквы, заканчиваться буквой или цифрой и содержать не более 30 символов.">
                        </i>
                    </label>
                    <input type="text" class="input w-full border mt-2" name="Direct[login]"
                           id="login" maxlength="30">

                </div>

                <div class="mb-3">
                    <label for="currency"> Select currency
                        <i data-feather="alert-circle" data-theme="light"
                           class="border-theme-1 text-gray-700 rounded-md w-4 h-4 mr-2  tooltip ro inline-block text-white"
                           title="Валюта рекламодателя.">
                            Валюта рекламодателя.
                        </i>
                    </label>
                    <select id="currency" name="Direct[currency]"
                            class="select2 w-full"
                            aria-label="Default select example">
                        <option disabled selected>Select currency</option>
                        <?php foreach (CurrencyEnum::getValues() as $currency) { ?>
                            <option value="<?= $currency ?>"><?= StringHelper::underscoreToCamelCase($currency) ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="text-xs text-gray-600 mt-2">Имя настройки:

                    <?= StringHelper::underscoreToCamelCase("CORRECT_TYPOS_AUTOMATICALLY") ?> — автоматически исправлять
                    ошибки и опечатки.<br/>

                    <?= StringHelper::underscoreToCamelCase("DISPLAY_STORE_RATING") ?> — дополнять объявления данными из
                    внешних источников (см. раздел Данные из внешних источников помощи Директа).<br/>

                    Если настройка не указана, она будет создана со значением NO.
                </div>
                <div class="mb-3">
                    <label for="settings_option"> Select account settings option
                        <i data-feather="alert-circle" data-theme="light"
                           class="border-theme-1 text-gray-700 rounded-md w-4 h-4 mr-2  tooltip ro inline-block text-white"
                           title="Типы уведомлений, отправляемых по электронной почте.">
                            Типы уведомлений, отправляемых по электронной почте.
                        </i>
                    </label>
                    <select id="settings_option" multiple name="Direct[settings][option][]"
                            class="select2 w-full" data-placeholder="Select email subscription option">
                        <?php foreach (ClientSettingAddEnum::getValues() as $settingOption) { ?>
                            <option value="<?= $settingOption ?>"><?= StringHelper::underscoreToCamelCase($settingOption) ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="intro-y col-span-12 lg:col-span-6 ">
            <div class="intro-y  p-5">
                <p class="text-lg font-medium mr-auto">Полномочия рекламодателя по управлению кампаниями. Если не заданы
                    — полномочия отсутствуют.</p>
                <div class="mb-3">
                    <div class="text-xs text-gray-600 mt-2">
                        <?= StringHelper::underscoreToCamelCase("EDIT_CAMPAIGNS") ?> — редактирование кампаний.<br/>

                        <?= StringHelper::underscoreToCamelCase("IMPORT_XLS") ?> — управление кампаниями с помощью
                        файлов (см. разделы Управление кампаниями с помощью
                        файлов формата XLS и XLSX и Загрузка кампаний из CSV-файлов помощи Директа).<br/>

                        <?= StringHelper::underscoreToCamelCase("TRANSFER_MONEY") ?> — перенос средств между кампаниями
                        (см. раздел Перенос средств между кампаниями).<br/>

                        Если полномочие не указано, оно будет создано со значением NO.<br/>

                        Для полномочия <?= StringHelper::underscoreToCamelCase("IMPORT_XLS") ?> можно указать значение
                        YES только при условии, что для полномочия<br/>
                        <?= StringHelper::underscoreToCamelCase("EDIT_CAMPAIGNS") ?> также указано значение YES, в
                        противном случае возвращается ошибка.
                    </div>
                    <label for="privilege"> Имя полномочия:<br/>

                    </label>
                    <select id="privilege" name="Direct[grants][privilege][]"
                            class="select2 w-full" multiple
                            data-placeholder="Select grant item">
                        <?php foreach (PrivilegeEnum::getValues() as $privilege) { ?>
                            <option value="<?= $privilege ?>"><?= StringHelper::underscoreToCamelCase($privilege) ?></option>
                        <?php } ?>
                    </select>
                </div>

                <p class="text-lg font-medium mr-auto">Fill out the email information form to receive a notification</p>
                <div class="mb-3">
                    <label for="email">Email for notification
                        <i data-feather="alert-circle" data-theme="light"
                           class="border-theme-1 text-gray-700 rounded-md w-4 h-4 mr-2  tooltip ro inline-block text-white"
                           title="Адрес электронной почты для отправки уведомлений, связанных с аккаунтом.">
                            Адрес электронной почты для отправки уведомлений, связанных с аккаунтом.
                        </i>
                    </label>
                    <input type="email" class="input w-full border mt-2" name="Direct[notification][email]"
                           id="email">
                </div>
                <div class="mb-3">
                    <label for="language"> Select notification language
                        <i data-feather="alert-circle" data-theme="light"
                           class="border-theme-1 text-gray-700 rounded-md w-4 h-4 mr-2  tooltip ro inline-block text-white"
                           title="Язык уведомлений.">
                            Язык уведомлений.
                        </i>
                    </label>
                    <select id="language" name="Direct[notification][language]"
                            class="select2 w-full" aria-label="Default select example">
                        <option disabled selected> Select notification language</option>
                        <?php foreach (LangEnum::getValues() as $language) { ?>
                            <option value="<?= $language ?>"><?= StringHelper::underscoreToCamelCase($language) ?></option>
                        <?php } ?>
                    </select>
                </div>

                <p class="text-lg font-medium mr-auto">
                    Fill out the email information form to subscribe to receive notification
                </p>
                <div class="mb-3">
                    <label for="subscribe_option"> Select email subscription option
                        <i data-feather="alert-circle" data-theme="light"
                           class="border-theme-1 text-gray-700 rounded-md w-4 h-4 mr-2  tooltip ro inline-block text-white"
                           title="Типы уведомлений, отправляемых по электронной почте.">
                            Типы уведомлений, отправляемых по электронной почте.
                        </i>
                    </label>
                    <select id="subscribe_option" name="Direct[notification][subscription][option][]"
                            class="select2 w-full" multiple
                            data-placeholder="Select email subscription option"
                    >
                        <?php foreach (EmailSubscriptionEnum::getValues() as $subscribeOptionItem) { ?>
                            <option value="<?= $subscribeOptionItem ?>"><?= StringHelper::underscoreToCamelCase($subscribeOptionItem); ?></option>
                        <?php } ?>
                    </select>
                </div>


                <div class="text-right mt-5">
                    <button type="reset" class="button w-24 border text-gray-700 mr-1">Cancel</button>
                    <button type="submit" class="button w-24 bg-theme-1 text-white">Save</button>
                </div>
            </div>
        </div>

    </div>
</form>

