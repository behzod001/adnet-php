<?php

global $direct;


use directapi\common\criterias\IdsCriteria;
use directapi\exceptions\DirectAccountNotExistException;
use directapi\exceptions\DirectApiException;
use directapi\exceptions\DirectApiNotEnoughUnitsException;
use directapi\exceptions\RequestValidationException;
use directapi\exceptions\UnknownPropertyException;
use GuzzleHttp\Exception\GuzzleException;
use helpers\Alert;

try {
    $ad = $direct->getAdsService()->delete(new IdsCriteria(["Ids" => [$_GET['id']]]));
    Alert::success('  <i data-feather="alert-triangle" class="w-6 h-6 mr-2"></i>  Ad successfully deleted for view ad <a href="/ads"> checkout this page</a>');
} catch (GuzzleException | DirectApiException | RequestValidationException | DirectAccountNotExistException | DirectApiNotEnoughUnitsException | UnknownPropertyException $e) {
    Alert::error($e->getMessage());

} 