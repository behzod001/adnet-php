<?php

use directapi\exceptions\DirectAccountNotExistException;
use directapi\exceptions\DirectApiException;
use directapi\exceptions\DirectApiNotEnoughUnitsException;
use directapi\exceptions\RequestValidationException;
use directapi\exceptions\UnknownPropertyException;
use directapi\services\adgroups\criterias\AdGroupsSelectionCriteria;
use directapi\services\adgroups\enum\AdGroupFieldEnum;
use directapi\services\ads\criterias\AdsSelectionCriteria;
use directapi\services\ads\enum\AdFieldEnum;
use directapi\services\ads\enum\TextAdFieldEnum;
use directapi\services\ads\enum\TextAdPriceExtensionFieldEnum;
use directapi\services\campaigns\criterias\CampaignsSelectionCriteria;
use directapi\services\campaigns\enum\CampaignFieldEnum;
use GuzzleHttp\Exception\GuzzleException;
use helpers\Alert;
use helpers\StringHelper;

global $direct;


$fieldNames = [
    AdFieldEnum::ID,
    AdFieldEnum::TYPE,
    AdFieldEnum::SUBTYPE,
    AdFieldEnum::CAMPAIGN_ID,
    AdFieldEnum::STATUS,
    AdFieldEnum::AD_CATEGORIES,
    AdFieldEnum::STATE,
    AdFieldEnum::STATUS_CLARIFICATION,
    AdFieldEnum::AD_GROUP_ID,
    AdFieldEnum::AGE_LABEL,
];

try {
    $adGroups = $ads = null;
    $campaignFieldNames = [
        CampaignFieldEnum::NAME,
        CampaignFieldEnum::ID
    ];
    $campaigns = $direct->getCampaignsService()->get(new CampaignsSelectionCriteria(), $campaignFieldNames);

    $criteria = new AdsSelectionCriteria();

    if (isset($_GET['campaign'])) {
        $criteria->CampaignIds = [$_GET['campaign']];
        $adGroups = $direct->getAdGroupsService()->get(
            new AdGroupsSelectionCriteria([
                "CampaignIds" => [$_GET['campaign']],
            ]),
            [AdGroupFieldEnum::ID, AdGroupFieldEnum::NAME,]
        );
    }

    if (isset($_GET['group']) && is_numeric($_GET['group'])) {
        $criteria->AdGroupIds = [$_GET['group']];
    }

    $ads = $direct->getAdsService()->get($criteria, $fieldNames, [
        TextAdFieldEnum::AD_IMAGE_HASH,
        TextAdFieldEnum::DISPLAY_URL_PATH,
        TextAdFieldEnum::HREF,
        TextAdFieldEnum::TEXT,
        TextAdFieldEnum::TITLE,
        TextAdFieldEnum::TITLE2
    ], [], [], [], [], [], [], [
        TextAdPriceExtensionFieldEnum::PRICE_CURRENCY,
        TextAdPriceExtensionFieldEnum::PRICE,
        TextAdPriceExtensionFieldEnum::PRICE_QUALIFIER,
        TextAdPriceExtensionFieldEnum::OLD_PRICE,
    ]);
} catch (UnknownPropertyException | GuzzleException | DirectAccountNotExistException | DirectApiNotEnoughUnitsException | RequestValidationException | DirectApiException $e) {
    Alert::error($e->getMessage());
}
?>


<div class="container">
    <form action="/ads" method="get">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="mb-3">
                        <label for="type-view" class=" form-label">
                            For view Ads you need select one of campaign from here:
                        </label>
                        <select required id="type-view" name="campaign" class="form-select"
                                aria-label="Default select example">
                            <option> Select campaign</option>
                            <?php foreach ($campaigns as $campaign) { ?>
                                <option <?= (isset($_GET['campaign']) && $_GET['campaign'] == $campaign->{CampaignFieldEnum::ID}) ? ' selected ' : '' ?>
                                        value="<?= $campaign->{CampaignFieldEnum::ID}; ?>"><?= $campaign->{CampaignFieldEnum::NAME}; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col">
                    <div class="mb-3">
                        <label for="type-view" class=" form-label">
                            For view Ads by group you need select one of ad group from here:
                        </label>
                        <select required id="type-view" name="group" class="form-select"
                                aria-label="Default select example">
                            <?php if (!is_null($adGroups)) {
                                echo '<option> Select ad group</option>';
                                foreach ($adGroups as $adGroup) { ?>
                                    <option <?= (isset($_GET['group']) && $_GET['group'] == $adGroup->{AdGroupFieldEnum::ID}) ? ' selected ' : '' ?>
                                            value="<?= $adGroup->{AdGroupFieldEnum::ID}; ?>"><?= $adGroup->{AdGroupFieldEnum::NAME}; ?></option>
                                <?php }
                            } else { ?>
                                <option> Select campaign</option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="mb-3">
            <button type="submit" class="btn btn-primary">Get Ads By filter</button>
            <a href="/ads/create" class="btn btn-primary">Create ad</a>
        </div>
    </form>

    <table class="table caption-top">
        <caption>List of Ads</caption>

        <thead>
        <tr>
            <th scope="col">#</th>
            <?php
            foreach ($fieldNames as $field) { ?>
                <th scope="col"><?= StringHelper::underscoreToCamelCase($field); ?></th>
            <?php } ?>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        <?php $idx = 0;
        if (!is_null($ads))
            foreach ($ads as $ad) { ?>
                <tr>
                    <td>  <?= ++$idx; ?> </td>
                    <?php foreach ($fieldNames as $field) { ?>
                        <td><?= is_null($ad->{$field}) ? " Not installed " : StringHelper::underscoreToCamelCase($ad->{$field}); ?></td>
                    <?php } ?>
                    <td>
                        <a href="/ads/view?id=<?= $ad->{AdFieldEnum::ID} ?>" class="btn btn-primary">View</a>
                        <a href="/ads/update?id=<?= $ad->{AdFieldEnum::ID} ?>" class="btn btn-warning">Update</a>
                        <a href="/ads/delete?id=<?= $ad->{AdFieldEnum::ID} ?>" class="btn btn-danger">Delete</a>
                    </td>
                </tr>
            <?php } ?>

        </tbody>
    </table>
</div>