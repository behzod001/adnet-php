<?php

use app\exceptions\DataNotFoundException;
use directapi\exceptions\DirectAccountNotExistException;
use directapi\exceptions\DirectApiException;
use directapi\exceptions\UnknownPropertyException;
use directapi\services\adextensions\models\PriceExtensionAddItem;
use directapi\services\adextensions\models\PriceExtensionUpdateItem;
use directapi\services\adgroups\models\AdGroupAddItem;
use directapi\services\ads\criterias\AdsSelectionCriteria;
use directapi\services\ads\enum\AdFieldEnum;
use directapi\services\ads\enum\TextAdFieldEnum;
use directapi\services\ads\enum\TextAdPriceExtensionFieldEnum;
use directapi\services\ads\models\AdUpdateItem;
use directapi\services\ads\models\TextAdUpdate;
use directapi\services\campaigns\models\CampaignAddItem;
use directapi\services\creatives\models\CreativeItems;
use GuzzleHttp\Exception\GuzzleException;
use helpers\Alert;
use helpers\StringHelper;

/**
 * @var $campaigns CampaignAddItem
 * @var $adSets AdGroupAddItem
 */

global $app;
$adGroups = null;
try {
// find ad by id
    if (isset($_GET['id'])) {
        $fieldNames = [
            AdFieldEnum::ID,
            AdFieldEnum::TYPE,
            AdFieldEnum::SUBTYPE,
            AdFieldEnum::CAMPAIGN_ID,
            AdFieldEnum::STATUS,
            AdFieldEnum::AD_CATEGORIES,
            AdFieldEnum::STATE,
            AdFieldEnum::STATUS_CLARIFICATION,
            AdFieldEnum::AD_GROUP_ID,
            AdFieldEnum::AGE_LABEL,
        ];

        $adGroups = $ads = null;

        $ad = $app->getDirect()->getAdsService()->get(new AdsSelectionCriteria([
            "Ids" => [$_GET['id']]
        ]), $fieldNames, [
            TextAdFieldEnum::AD_IMAGE_HASH,
            TextAdFieldEnum::DISPLAY_URL_PATH,
            TextAdFieldEnum::HREF,
            TextAdFieldEnum::TEXT,
            TextAdFieldEnum::TITLE,
            TextAdFieldEnum::TITLE2
        ], [], [], [], [], [], [], [
//            TextAdPriceExtensionFieldEnum::PRICE_CURRENCY,
            TextAdPriceExtensionFieldEnum::PRICE,
            TextAdPriceExtensionFieldEnum::PRICE_QUALIFIER,
            TextAdPriceExtensionFieldEnum::OLD_PRICE,
        ])[0];
    }

// update
    if (isset($_POST['Ad'])) {

        $post = $_POST['Ad'];
        $TextAdd = new TextAdUpdate([
            TextAdFieldEnum::TITLE => $post['TextAd'][TextAdFieldEnum::TITLE],
            TextAdFieldEnum::TITLE2 => $post['TextAd'][TextAdFieldEnum::TITLE2],
            TextAdFieldEnum::TEXT => $post['TextAd'][TextAdFieldEnum::TEXT],
            TextAdFieldEnum::HREF => $post['TextAd'][TextAdFieldEnum::HREF],
            TextAdFieldEnum::DISPLAY_URL_PATH => $post['TextAd'][TextAdFieldEnum::DISPLAY_URL_PATH],
            TextAdFieldEnum::AD_IMAGE_HASH => $post['TextAd'][TextAdFieldEnum::AD_IMAGE_HASH]
        ]);

        $TextAdd->PriceExtension = new PriceExtensionUpdateItem([
            TextAdPriceExtensionFieldEnum::PRICE => $post['TextAd'][TextAdPriceExtensionFieldEnum::PRICE],
            TextAdPriceExtensionFieldEnum::PRICE_CURRENCY => $post['TextAd'][TextAdPriceExtensionFieldEnum::PRICE_CURRENCY],
            TextAdPriceExtensionFieldEnum::PRICE_QUALIFIER => $post['TextAd'][TextAdPriceExtensionFieldEnum::PRICE_QUALIFIER],
            TextAdPriceExtensionFieldEnum::OLD_PRICE => $post['TextAd'][TextAdPriceExtensionFieldEnum::OLD_PRICE],
        ]);

        $addItem = new AdUpdateItem();
        $addItem->{AdFieldEnum::ID} = $_GET['id'];
        $addItem->TextAd = $TextAdd;
        $ad = $app->getDirect()->getAdsService()->update([$addItem]);
        if (!is_null($ad[0]->Errors)) {
            $e = $ad[0]->Errors[0];
            Alert::error($e->Message . ' ' . $e->Details . ' <a href="/ads" class="mx-1 ">checkout this page</a>');
        } else
            Alert::success(' Ad successfully created for view ad <a class="mx-1 " href="/ads/view?id=' . $ad[0]->Id . '"> checkout this page</a>');
        return;
    }

} catch (DataNotFoundException $e) {
    Alert::warning(' У вас есть Yandex Direct аккаунт ? Тогда выберите ваш аккаунт. Если нет создайте и  <a href="/accounts"  class="mx-1 text-theme-1 mx-1"> регистрируйте. </a> ');
}
catch (GuzzleException | DirectAccountNotExistException | DirectApiException | UnknownPropertyException $e) {
    Alert::error($e->getMessage() . ' <a href="/campaigns" class="wx-1">  checkout this page </a>');
}
?>

<div class="intro-y flex items-center mt-8">
    <h2 class="text-lg font-medium mr-auto">
        Create new Ads
        <div class="text-xs text-gray-600 mt-2">
            Параметры текстово-графического объявления. См. Тип объявления.
        </div>
    </h2>
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
        <div class="hidden md:block mx-auto text-gray-600"></div>
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <a href="/ads" class="button text-white bg-theme-1 shadow-md mr-2">Go back</a>
        </div>
    </div>
</div>
<form action="/direct/ads/update?id=<?= $_GET['id']; ?>" method="post">
    <div class="grid grid-cols-12 gap-6 mt-5 box">

        <div class="intro-y col-span-12 lg:col-span-6 ">
            <div class="intro-y  p-5">

                <div class="mb-3">
                    <label for="<?= TextAdFieldEnum::TITLE ?>"
                    ><?= StringHelper::underscoreToCamelCase(TextAdFieldEnum::TITLE); ?></label>
                    <input type="text" name="Ad[TextAd][<?= TextAdFieldEnum::TITLE ?>]"
                           class="input w-full border mt-2" value="<?= $ad->TextAd->{TextAdFieldEnum::TITLE} ?>"
                           id="<?= TextAdFieldEnum::TITLE ?>">
                    <div class="text-xs text-gray-600 mt-2">Заголовок 1.
                        Не более 35 символов без учета «узких» плюс не более 15 «узких» символов.
                        Каждое слово не более 22 символов. В случае использования шаблона символы #
                        не учитываются в длине.
                    </div>
                </div>

                <div class="mb-3">
                    <label for="<?= TextAdFieldEnum::TITLE2 ?>"
                    ><?= StringHelper::underscoreToCamelCase(TextAdFieldEnum::TITLE2); ?></label>
                    <input type="text" name="Ad[TextAd][<?= TextAdFieldEnum::TITLE2 ?>]"
                           class="input w-full border mt-2"
                           value="<?= $ad->TextAd->{TextAdFieldEnum::TITLE2} ?>"
                           aria-describedby="<?= TextAdFieldEnum::TITLE2 ?>label"
                           id="<?= TextAdFieldEnum::TITLE2 ?>">
                    <div class="text-xs text-gray-600 mt-2">Заголовок 2.
                        Не более 30 символов без учета «узких» плюс не более 15 «узких» символов.
                        Каждое слово не более 22 символов. В случае использования шаблона символы #
                        не учитываются в длине.
                    </div>
                </div>
                <div class="mb-3">
                    <label for="<?= TextAdFieldEnum::TEXT ?>"
                    ><?= StringHelper::underscoreToCamelCase(TextAdFieldEnum::TEXT); ?></label>
                    <textarea data-feature="basic" rows="4" name="Ad[TextAd][<?= TextAdFieldEnum::TEXT ?>]"
                              class="summernote"
                              id="<?= TextAdFieldEnum::TEXT ?>">
                    <?= $ad->TextAd->{TextAdFieldEnum::TEXT} ?>
                </textarea>
                    <div class="text-xs text-gray-600 mt-2">
                        Текст объявления. Не более 81 символа без учета «узких» плюс не более 15 «узких»
                        символов. Каждое слово не более 23 символов. В случае использования шаблона символы # не
                        учитываются в длине.
                    </div>
                </div>


                <div class="mb-3">
                    <label for="<?= TextAdFieldEnum::AD_IMAGE_HASH ?>"
                    ><?= StringHelper::underscoreToCamelCase(TextAdFieldEnum::AD_IMAGE_HASH); ?></label>
                    <input type="text" name="Ad[TextAd][<?= TextAdFieldEnum::AD_IMAGE_HASH ?>]"
                           class="input w-full border mt-2"
                           value="<?= $ad->TextAd->{TextAdFieldEnum::AD_IMAGE_HASH} ?>"
                           id="<?= TextAdFieldEnum::AD_IMAGE_HASH ?>">
                    <div class="text-xs text-gray-600 mt-2">Хэш изображения. Для
                        текстово-графических объявлений подходят только изображения с типом REGULAR и WIDE, см.
                        Тип изображения.
                    </div>
                </div>
            </div>
        </div>
        <div class="intro-y col-span-12 lg:col-span-6 ">
            <div class="intro-y  p-5">
                <div class="col">
                    <div class="mb-3">
                        <label for="<?= TextAdFieldEnum::HREF ?>"
                        ><?= StringHelper::underscoreToCamelCase(TextAdFieldEnum::HREF); ?></label>
                        <input type="text" name="Ad[TextAd][<?= TextAdFieldEnum::HREF ?>]"
                               class="input w-full border mt-2" value="<?= $ad->TextAd->{TextAdFieldEnum::HREF} ?>"

                               id="<?= TextAdFieldEnum::HREF ?>">
                        <div class="text-xs text-gray-600 mt-2">Ссылка на сайт рекламодателя. Не
                            более 1024 символов. В случае использования шаблона символы # не учитываются
                            в длине. Должна содержать протокол и доменное имя. Может содержать
                            подстановочные переменные.
                        </div>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="<?= TextAdFieldEnum::DISPLAY_URL_PATH ?>"
                    ><?= StringHelper::underscoreToCamelCase(TextAdFieldEnum::DISPLAY_URL_PATH); ?></label>
                    <input type="text" name="Ad[TextAd][<?= TextAdFieldEnum::DISPLAY_URL_PATH ?>]"
                           class="input w-full border mt-2"
                           value="<?= $ad->TextAd->{TextAdFieldEnum::DISPLAY_URL_PATH} ?>"
                           id="<?= TextAdFieldEnum::DISPLAY_URL_PATH ?>">
                    <div class="text-xs text-gray-600 mt-2">
                        Отображаемая ссылка. Допускается только при наличии параметра Href. Не более
                        20 символов. В случае использования шаблона символы # не учитываются в
                        длине. Может содержать буквы, цифры, символы -, №, /, %, #. Запрещены
                        пробел, символ _, двойные символы --, //. См. раздел Отображаемая ссылка
                        помощи Директа.
                    </div>
                </div>
                <div class="mb-3">
                    <label for="<?= TextAdPriceExtensionFieldEnum::PRICE_QUALIFIER ?>"
                    ><?= StringHelper::underscoreToCamelCase(TextAdPriceExtensionFieldEnum::PRICE_QUALIFIER); ?></label>
                    <select id="<?= TextAdPriceExtensionFieldEnum::PRICE_QUALIFIER ?>"
                            name="Ad[TextAd][<?= TextAdPriceExtensionFieldEnum::PRICE_QUALIFIER ?>]"
                            class="select2 w-full"
                            value="<?= $ad->TextAd->PriceExtension->{TextAdPriceExtensionFieldEnum::PRICE_QUALIFIER} ?>">
                        <?php foreach (["FROM", "UP_TO", "NONE"] as $qualifier) {
                            echo '<option value="' . $qualifier . '">' . $qualifier . '</option>';
                        } ?>
                    </select>
                    <div class="text-xs text-gray-600 mt-2">Текстовое
                        пояснение к цене:
                        FROM — «от».
                        UP_TO — «до».
                        NONE — нет пояснения.
                    </div>
                </div>
                <div class="mb-3">
                    <label for="<?= TextAdPriceExtensionFieldEnum::PRICE ?>"
                    ><?= StringHelper::underscoreToCamelCase(TextAdPriceExtensionFieldEnum::PRICE); ?></label>
                    <input type="text"
                           name="Ad[TextAd][<?= TextAdPriceExtensionFieldEnum::PRICE ?>]"
                           class="input w-full border mt-2"
                           value="<?= $ad->TextAd->PriceExtension->{TextAdPriceExtensionFieldEnum::PRICE} ?>"

                           id="<?= TextAdPriceExtensionFieldEnum::PRICE ?>">
                    <div class="text-xs text-gray-600 mt-2">Цена товара или
                        услуги, умноженная на 1 000 000. Целое число, кратное
                        10 000 (что соответствует цене с двумя знаками после запятой). Максимальное
                        значение — 10 000 000 000 000 000.
                    </div>
                </div>
                <div class="mb-3">
                    <label for="<?= TextAdPriceExtensionFieldEnum::OLD_PRICE ?>"
                    ><?= StringHelper::underscoreToCamelCase(TextAdPriceExtensionFieldEnum::OLD_PRICE); ?></label>
                    <input type="text"
                           name="Ad[TextAd][<?= TextAdPriceExtensionFieldEnum::OLD_PRICE ?>]"
                           value="<?= $ad->TextAd->PriceExtension->{TextAdPriceExtensionFieldEnum::OLD_PRICE} ?>"
                           class="input w-full border mt-2"
                           id="<?= TextAdPriceExtensionFieldEnum::OLD_PRICE ?>">
                    <div class="text-xs text-gray-600 mt-2">
                        Старая цена товара или услуги, умноженная на 1 000 000. Целое число, кратное
                        10 000. Старая цена должна быть строго больше цены.
                    </div>
                </div>
                <div class="mb-3">
                    <label for="<?= TextAdPriceExtensionFieldEnum::PRICE_CURRENCY ?>"
                    ><?= StringHelper::underscoreToCamelCase(TextAdPriceExtensionFieldEnum::PRICE_CURRENCY); ?></label>
                    <select id="<?= TextAdPriceExtensionFieldEnum::PRICE_CURRENCY ?>"
                            name="Ad[TextAd][<?= TextAdPriceExtensionFieldEnum::PRICE_CURRENCY ?>]"
                            class="select2 w-full"
                            value="<?= $ad->TextAd->PriceExtension->{TextAdPriceExtensionFieldEnum::PRICE_CURRENCY} ?>"
                            aria-describedby="<?= TextAdPriceExtensionFieldEnum::PRICE_CURRENCY ?>label"
                            aria-label="Default select example">
                        <?php foreach (["RUB", "BYN", "CHF", "EUR", "KZT", "TRY", "UAH"] as $currency) {
                            echo '<option value="' . $currency . '">' . $currency . '</option>';
                        } ?>
                    </select>
                    <div class="text-xs text-gray-600 mt-2">Валюта, в
                        которой указана цена.
                    </div>
                </div>
                <div class="text-right mt-5">
                    <button type="reset" class="button w-24 border text-gray-700 mr-1">Cancel</button>
                    <button type="submit" class="button w-24 bg-theme-1 text-white">Update</button>
                </div>
            </div>
        </div>
    </div>
</form>

