<?php


use app\exceptions\DataNotFoundException;
use directapi\services\ads\criterias\AdsSelectionCriteria;
use directapi\services\ads\enum\AdFieldEnum;
use directapi\services\ads\enum\TextAdFieldEnum;
use directapi\services\ads\enum\TextAdPriceExtensionFieldEnum;
use GuzzleHttp\Exception\GuzzleException;
use helpers\Alert;
use helpers\StringHelper;

global $app;

$fieldNames = [
    AdFieldEnum::ID,
    AdFieldEnum::TYPE,
    AdFieldEnum::SUBTYPE,
    AdFieldEnum::CAMPAIGN_ID,
    AdFieldEnum::STATUS,
    AdFieldEnum::AD_CATEGORIES,
    AdFieldEnum::STATE,
    AdFieldEnum::STATUS_CLARIFICATION,
    AdFieldEnum::AD_GROUP_ID,
    AdFieldEnum::AGE_LABEL,
];
$adNames = [
    TextAdFieldEnum::AD_IMAGE_HASH => TextAdFieldEnum::AD_IMAGE_HASH,
    TextAdFieldEnum::DISPLAY_URL_PATH => TextAdFieldEnum::DISPLAY_URL_PATH,
    TextAdFieldEnum::HREF => TextAdFieldEnum::HREF,
    TextAdFieldEnum::TEXT => TextAdFieldEnum::TEXT,
    TextAdFieldEnum::TITLE => TextAdFieldEnum::TITLE,
    TextAdFieldEnum::TITLE2 => TextAdFieldEnum::TITLE2,

];
$priceNames = [
    TextAdPriceExtensionFieldEnum::PRICE_CURRENCY => TextAdPriceExtensionFieldEnum::PRICE_CURRENCY,
    TextAdPriceExtensionFieldEnum::PRICE => TextAdPriceExtensionFieldEnum::PRICE,
    TextAdPriceExtensionFieldEnum::PRICE_QUALIFIER => TextAdPriceExtensionFieldEnum::PRICE_QUALIFIER,
    TextAdPriceExtensionFieldEnum::OLD_PRICE => TextAdPriceExtensionFieldEnum::OLD_PRICE
];

try {
    $adGroups = $ads = null;
    $ad = $app->getDirect()->getAdsService()->get(new AdsSelectionCriteria([
        "Ids" => [$_GET['id']]
    ]), $fieldNames, [
        TextAdFieldEnum::AD_IMAGE_HASH,
        TextAdFieldEnum::DISPLAY_URL_PATH,
        TextAdFieldEnum::HREF,
        TextAdFieldEnum::TEXT,
        TextAdFieldEnum::TITLE,
        TextAdFieldEnum::TITLE2
    ], [], [], [], [], [], [], [
        TextAdPriceExtensionFieldEnum::PRICE_CURRENCY,
        TextAdPriceExtensionFieldEnum::PRICE,
        TextAdPriceExtensionFieldEnum::PRICE_QUALIFIER,
        TextAdPriceExtensionFieldEnum::OLD_PRICE,
    ])[0];


}catch (DataNotFoundException $e) {
    Alert::warning(' У вас есть Yandex Direct аккаунт ? Тогда выберите ваш аккаунт. Если нет создайте и  <a href="/accounts"  class="mx-1 text-theme-1 mx-1"> регистрируйте. </a> ');
} catch (GuzzleException | Exception $e) {
    Alert::error($e->getMessage());
}
?>



<div class="grid grid-cols-12 gap-6 mt-5">
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
        <h2 class="intro-y text-lg font-medium mt-10">
            View Ads "<?= $ad->TextAd->{TextAdFieldEnum::TITLE}; ?>"
        </h2>
        <button class="hiddden button  "> </button>
        <div class="hidden md:block mx-auto text-gray-600"></div>
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <div class="w-56 relative text-gray-700">
                <a class="button text-white bg-theme-1 shadow-md mr-2"
                   href="/direct/ads/update?id=<?= $ad->{AdFieldEnum::ID} ?>">Update</a>
                <a class="button text-white bg-theme-1 shadow-md mr-2" href="/ads">Go back</a>
            </div>
        </div>
    </div>
    <!-- BEGIN: Data List -->
    <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
        <table class="table table-report -mt-2">
            <thead>
            <tr>
                <th scope="col">Field name</th>
                <th scope="col">Field value</th>
            </tr>
            </thead>
            <tbody>

            <tbody>
            <?php
            foreach ($fieldNames as $field) { ?>
                <tr>
                    <td><?= StringHelper::underscoreToCamelCase($field); ?></td>
                    <td><?= is_null($ad->{$field}) ? " Not installed " : StringHelper::underscoreToCamelCase($ad->{$field}); ?></td>
                </tr>
            <?php }
            foreach ($priceNames as $field) { ?>
                <tr>
                    <td><?= StringHelper::underscoreToCamelCase("TextAdPriceExtensionFieldNames") . " " . StringHelper::underscoreToCamelCase($field); ?></td>
                    <td><?= is_null($ad->TextAd->PriceExtension->{$field}) ? " Not installed " : $ad->TextAd->PriceExtension->{$field}; ?></td>
                </tr>
            <?php }
            foreach ($adNames as $field) { ?>
                <tr>
                    <td><?= StringHelper::underscoreToCamelCase("TextAdFieldNames") . " " . StringHelper::underscoreToCamelCase($field); ?></td>
                    <td><?= is_null($ad->TextAd->{$field}) ? " Not installed " : $ad->TextAd->{$field}; ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>
