<?php

use app\exceptions\DataNotFoundException;
use directapi\services\adgroups\enum\AdGroupFieldEnum;
use directapi\services\adgroups\models\AdGroupAddItem;
use directapi\services\campaigns\criterias\CampaignsSelectionCriteria;
use directapi\services\campaigns\enum\CampaignFieldEnum;
use helpers\Alert;
use helpers\StringHelper;

global $app;
//all campaigns for select
try {
    $campaignFieldNames = [
        CampaignFieldEnum::NAME,
        CampaignFieldEnum::ID
    ];
    $campaigns = $app->getDirect()->getCampaignsService()->get(new CampaignsSelectionCriteria(), $campaignFieldNames);
} catch (DataNotFoundException $e) {
    Alert::warning(' У вас есть Yandex Direct аккаунт ? Тогда выберите ваш аккаунт. Если нет создайте и  <a href="/accounts"  class="mx-1 text-theme-1 mx-1"> регистрируйте. </a> ');
} catch (Exception $e) {
    Alert::error($e->getMessage() . ' <a href="/adsets" class="mx-1"> checkout Ad Groups page </a>');
}

if (isset($_POST['AdGroups'])) {
    // crate targeting
    $adGroups = new AdGroupAddItem();
    $adGroups->{AdGroupFieldEnum::NAME} = $_POST['AdGroups'][AdGroupFieldEnum::NAME];
    $adGroups->{AdGroupFieldEnum::CAMPAIGN_ID} = $_POST['AdGroups'][AdGroupFieldEnum::CAMPAIGN_ID];
    $adGroups->{AdGroupFieldEnum::REGION_IDS} = explode(",", $_POST['AdGroups'][AdGroupFieldEnum::REGION_IDS]);
    $adGroups->{AdGroupFieldEnum::NEGATIVE_KEYWORDS} = ["Items" => explode(",", $_POST['AdGroups'][AdGroupFieldEnum::NEGATIVE_KEYWORDS])];

    try {
        $response = $app->getDirect()->getAdGroupsService()->add([$adGroups])[0];
        Alert::success(' Ad set successfully created for view <a class="mx-1" href="/dierct/adset/view?id=' . $response->{AdGroupFieldEnum::ID} . '">checkout this page</a>');
    } catch (DataNotFoundException $e) {
        Alert::warning(' У вас есть Yandex Direct аккаунт ? Тогда выберите ваш аккаунт. Если нет создайте и  <a href="/accounts"  class="mx-1 text-theme-1 mx-1"> регистрируйте. </a> ');
    } catch (Exception $e) {
        Alert::error($e->getMessage() . ' <a href="/adsets" class="mx-1"> checkout Ad Groups page</a>');
    }
}
?>


<div class="intro-y flex items-center mt-8">
    <h2 class="text-lg font-medium mr-auto">
        Create new Ad Group
    </h2>
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
        <div class="hidden md:block mx-auto text-gray-600"></div>
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <a href="/adsets" class="button text-white bg-theme-1 shadow-md mr-2">Go back</a>
        </div>
    </div>
</div>
<div class="grid grid-cols-12 gap-6 mt-5 box">

    <div class="intro-y col-span-12 lg:col-span-6 ">
        <div class="intro-y  p-5">
            <form action="/direct/adset/create" method="post">
                <div class="mb-3">
                    <label for="<?= AdGroupFieldEnum::NAME ?>">
                        <?= StringHelper::underscoreToCamelCase(AdGroupFieldEnum::NAME) ?>
                    </label>
                    <input type="text" name="<?= "AdGroups[" . AdGroupFieldEnum::NAME . "]" ?>"
                           id="<?= AdGroupFieldEnum::NAME ?>" class="input w-full border mt-2">
                    <div class="text-xs text-gray-600 mt-2">Название группы объявлений (от 1 до 255 символов).</div>
                </div>

                <div class="mb-3">
                    <label for="<?= AdGroupFieldEnum::CAMPAIGN_ID ?>">Select campaign</label>
                    <select name="AdGroups[<?= AdGroupFieldEnum::CAMPAIGN_ID ?>]"
                            id="<?= AdGroupFieldEnum::CAMPAIGN_ID ?>" class="select2 w-full">
                        <option selected>Select Campaign</option>
                        <?php foreach ($campaigns as $value) { ?>
                            <option value='<?= $value->{CampaignFieldEnum::ID} ?>'> <?= $value->{CampaignFieldEnum::NAME} ?> </option>
                        <?php } ?>
                    </select>
                    <div class="text-xs text-gray-600 mt-2">
                        Идентификатор кампании, в которую добавляется группа.
                    </div>
                </div>

                <div class="mb-3">
                    <label for="<?= AdGroupFieldEnum::REGION_IDS ?>"
                    ><?= StringHelper::underscoreToCamelCase(AdGroupFieldEnum::REGION_IDS) ?></label>
                    <input type="text" class="input w-full border mt-2"
                           name="<?= "AdGroups[" . AdGroupFieldEnum::REGION_IDS . "]" ?>"
                           id="<?= AdGroupFieldEnum::REGION_IDS ?>">
                    <div class="text-xs text-gray-600 mt-2">Массив идентификаторов регионов, для которых показы
                        включены или выключены. Массив должен содержать хотя бы один элемент. Идентификатор 0 —
                        показывать во
                        всех регионах. Минус перед идентификатором региона выключить показы в данном регионе. Например
                        [1,-219] — показывать для Москвы и Московской области, кроме
                        Черноголовки. Минус-регионы нельзя использовать, если указан 0. Массив не должен состоять только
                        из минус-регионов.
                    </div>
                </div>
                <div class="mb-3">
                    <label for="<?= AdGroupFieldEnum::NEGATIVE_KEYWORDS ?>">
                        <?= StringHelper::underscoreToCamelCase(AdGroupFieldEnum::NEGATIVE_KEYWORDS) ?>
                    </label>
                    <input type="text" class="input w-full border mt-2"
                           name="<?= "AdGroups[" . AdGroupFieldEnum::NEGATIVE_KEYWORDS . "]" ?>"
                           id="<?= AdGroupFieldEnum::NEGATIVE_KEYWORDS ?>">
                    <div class="text-xs text-gray-600 mt-2">
                        Массив минус-фраз, общих для всех ключевых фраз группы объявлений.
                        <div class="text-xs text-gray-600 mt-2">
                            <span class="text-danger"><i class="bi bi-info-square"></i> Ограничение.</span>
                            Минус-фразы не допускаются в группе медийных объявлений с условием нацеливания по профилю
                            пользователей.
                        </div>
                        Минус-фразу следует указывать без минуса перед первым словом. Не более 7 слов в минус-фразе.
                        Длина каждого слова не более 35 символов. Суммарная длина минус-фраз в массиве не более 4096
                        символов. Пробелы, дефисы и операторы не учитываются в суммарной длине.
                    </div>
                </div>
                <div class="text-right mt-5">
                    <button type="reset" class="button w-24 border text-gray-700 mr-1">Cancel</button>
                    <button type="submit" class="button w-24 bg-theme-1 text-white">Save</button>
                </div>
            </form>

        </div>
    </div>

    <div class="intro-y col-span-12 lg:col-span-6">

        <div class="intro-y p-5">


        </div>
    </div>
