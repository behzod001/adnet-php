<?php


global $app;

use app\exceptions\DataNotFoundException;
use directapi\exceptions\UnknownPropertyException;
use directapi\services\adgroups\criterias\AdGroupsSelectionCriteria;
use directapi\services\adgroups\enum\AdGroupFieldEnum;
use FacebookAds\Cursor;
use FacebookAds\Http\Exception\ClientException;
use FacebookAds\Http\Exception\RequestException;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Fields\TargetingFields;
use helpers\Alert;
use helpers\StringHelper;

/**
 * @var $adSet Cursor
 * @var $idx int index of array
 */
try {
    $selectionCriteria = new AdGroupsSelectionCriteria([
        "Ids" => [$_GET['id']],
        "Statuses" => ["ACCEPTED", "DRAFT", "MODERATION", "PREACCEPTED", "REJECTED"]
    ]);

    $fieldNames = [
        AdGroupFieldEnum::ID,
        AdGroupFieldEnum::NAME,
        AdGroupFieldEnum::CAMPAIGN_ID,
        AdGroupFieldEnum::REGION_IDS,
        AdGroupFieldEnum::STATUS,
        AdGroupFieldEnum::NEGATIVE_KEYWORDS,
        AdGroupFieldEnum::NEGATIVE_KEYWORD_SHARED_SET_IDS,

    ];


    $adGroups = $app->getDirect()->getAdGroupsService()->get($selectionCriteria, $fieldNames)[0];
} catch (DataNotFoundException $e) {
    Alert::warning(' У вас есть Yandex Direct аккаунт ? Тогда выберите ваш аккаунт. Если нет создайте и  <a href="/accounts"  class="mx-1 text-theme-1 mx-1"> регистрируйте. </a> ');
} catch (UnknownPropertyException $e) {
    Alert::error("Something went wrong. <a href='/accounts'  class='mx-1 text-theme-1 mx-1'> reload page </a> ");
}
$idx = 0;
?>


<div class="grid grid-cols-12 gap-6 mt-5">
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">

        <div class="dropdown relative">
            <h2 class="intro-y text-lg font-medium mt-3">
                View Ad Set "<?= $adGroups->{AdGroupFieldEnum::NAME}; ?>"
            </h2>
        </div>
        <div class="hidden md:block mx-auto text-gray-600"></div>
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <div class="w-56 relative text-gray-700">
                <a class="button text-white bg-theme-1 shadow-md mr-2"
                   href="/direct/adset/update?id=<?= $adGroups->{AdGroupFieldEnum::ID} ?>">Update</a>
                <a class="button text-white bg-theme-1 shadow-md mr-2" href="/adsets">Go back</a>
            </div>
        </div>
    </div>
    <!-- BEGIN: Data List -->
    <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
        <table class="table table-report -mt-2">
            <thead>
            <tr>
                <th scope="col">Field name</th>
                <th scope="col">Field value</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($fieldNames as $field) { ?>
                <tr>
                    <?php echo "<td>" . StringHelper::underscoreToCamelCase($field) . "</td>";
                    if ($field == AdGroupFieldEnum::REGION_IDS && is_array($adGroups->{$field})) {
                        echo " <td>";
                        $itemIdx = 0;
                        while (isset($adGroups->{$field}[$itemIdx])) {
                            $regionId = $adGroups->{$field}[$itemIdx];
                            echo " <span class='badge bg-warning text-dark' >" . $regionId . " <i class='bi bi-info-circle-fill' data-bs-toggle='tooltip' data-bs-placement='right' title='" . $regionId . "' ></i> </span> ";
                            $itemIdx++;
                        }
                        echo "</td>";
                        continue;
                    }
                    if ($field == AdGroupFieldEnum::NEGATIVE_KEYWORDS && !is_null($adGroups->{$field})) {
                        echo " <td>";
                        $items = $adGroups->{$field}->Items;
                        $keyIdx = 0;
                        while (isset($items[$keyIdx]))
                            echo " <span class='badge bg-warning text-dark' >" . $items[$keyIdx++] . " </span> ";
                        echo "</td>";
                        continue;
                    }
                    ?>
                    <td><?= is_null($adGroups->{$field}) ? " Not installed " : StringHelper::underscoreToCamelCase($adGroups->{$field}); ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>

</div>
