<?php


use app\exceptions\DataNotFoundException;
use directapi\common\results\ActionResult;
use directapi\exceptions\DirectAccountNotExistException;
use directapi\exceptions\DirectApiException;
use directapi\exceptions\DirectApiNotEnoughUnitsException;
use directapi\exceptions\RequestValidationException;
use directapi\services\campaigns\enum\CampaignFieldEnum;
use directapi\services\campaigns\models\CampaignAddItem;
use directapi\services\campaigns\models\CpmBannerCampaignItem;
use directapi\services\campaigns\models\DailyBudget;
use directapi\services\campaigns\models\DynamicTextCampaignItem;
use directapi\services\campaigns\models\MobileAppCampaignItem;
use directapi\services\campaigns\models\SmartCampaignItem;
use directapi\services\campaigns\models\TextCampaignItem;
use GuzzleHttp\Exception\GuzzleException;
use helpers\Alert;

global $app;
$campaigns = array(
    "TextCampaign" => array(
        "Search" => array(
            'HighestPosition' => [
                'NetworkDefault' =>
                    [
                        'LimitPercent' => '(int)',
                    ],
            ],
            'WbMaximumClicks' => [
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'WbMaximumConversionRate' => [
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
                'GoalId' => '(long)',
            ],
            'AverageCpc' => [
                'AverageCpc' => '(long)',
                'WeeklySpendLimit' => '(long)',
            ],
            'AverageCpa' => [
                'AverageCpa' => '(long)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'WeeklyClickPackage' => [
                'ClicksPerWeek' => '(long)',
                'AverageCpc' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'AverageRoi' => [
                'ReserveReturn' => '(int)',
                'RoiCoef' => '(long)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
                'Profitability' => '(long)',
            ],
            'AverageCrr' => [
                'Crr' => '(int)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
            ],
            'PayForConversion' => [
                'Cpa' => '(long)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
            ]
        ),
        "Network" => array(
            'NetworkDefault' =>
                [
                    'LimitPercent' => '(int)',
                ],
            'WbMaximumClicks' =>
                [
                    'WeeklySpendLimit' => '(long)',
                    'BidCeiling' => '(long)',
                ],
            'WbMaximumConversionRate' =>
                [
                    'WeeklySpendLimit' => '(long)',
                    'BidCeiling' => '(long)',
                    'GoalId' => '(long)',
                ],
            'AverageCpc' =>
                [
                    'AverageCpc' => '(long)',
                    'WeeklySpendLimit' => '(long)',
                ],
            'AverageCpa' =>
                [
                    'AverageCpa' => '(long)',
                    'GoalId' => '(long)',
                    'WeeklySpendLimit' => '(long)',
                    'BidCeiling' => '(long)',
                ],
            'WeeklyClickPackage' =>
                [
                    'ClicksPerWeek' => '(long)',
                    'AverageCpc' => '(long)',
                    'BidCeiling' => '(long)',
                ],
            'AverageRoi' =>
                [
                    'ReserveReturn' => '(int)',
                    'RoiCoef' => '(long)',
                    'GoalId' => '(long)',
                    'WeeklySpendLimit' => '(long)',
                    'BidCeiling' => '(long)',
                    'Profitability' => '(long)',
                ],
            'AverageCrr' =>
                [
                    'Crr' => '(int)',
                    'GoalId' => '(long)',
                    'WeeklySpendLimit' => '(long)',
                ],
            'PayForConversion' =>
                [
                    'Cpa' => '(long)',
                    'GoalId' => '(long)',
                    'WeeklySpendLimit' => '(long)',
                ],
        )
    ),
    "DynamicTextCampaign" => array(
        "Search" => array(
            'WbMaximumClicks' => [
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'WbMaximumConversionRate' => [
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
                'GoalId' => '(long)',
            ],
            'AverageCpc' => [
                'AverageCpc' => '(long)',
                'WeeklySpendLimit' => '(long)',
            ],
            'AverageCpa' => [
                'AverageCpa' => '(long)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'WeeklyClickPackage' => [
                'ClicksPerWeek' => '(long)',
                'AverageCpc' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'AverageRoi' => [
                'ReserveReturn' => '(int)',
                'RoiCoef' => '(long)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
                'Profitability' => '(long)',
            ],
            'AverageCrr' => [
                'Crr' => '(int)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
            ],
            'PayForConversion' => [
                'Cpa' => '(long)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
            ]
        ),
        "Network" => array(
            "BiddingStrategyType" => "SERVING_OFF"
        )
    ),
    "SmartCampaign" => array(
        "Network" => array(
            'AverageCpcPerCampaign' => [
                'AverageCpc' => '(long)',
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'AverageCpcPerFilter' => [
                'FilterAverageCpc' => '(long)',
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'AverageCpaPerCampaign' => [
                'AverageCpa' => '(long)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'AverageCpaPerFilter' => [
                'FilterAverageCpa' => '(long)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'AverageRoi' => [
                'ReserveReturn' => '(int)',
                'RoiCoef' => '(long)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
                'Profitability' => '(long)',
            ],
            'AverageCrr' => [
                'Crr' => '(int)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
            ],
            'PayForConversionPerCampaign' => [
                'Cpa' => '(long)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
            ],
            'PayForConversionPerFilter' => [
                'Cpa' => '(long)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
            ],
            'NetworkDefault' => [
            ]
        ),
        "Search" => array(
            'AverageCpcPerCampaign' => [
                'AverageCpc' => '(long)',
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'AverageCpcPerFilter' => [
                'FilterAverageCpc' => '(long)',
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'AverageCpaPerCampaign' => [
                'AverageCpa' => '(long)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'AverageCpaPerFilter' => [
                'FilterAverageCpa' => '(long)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'AverageRoi' => [
                'ReserveReturn' => '(int)',
                'RoiCoef' => '(long)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
                'Profitability' => '(long)',
            ],
            'AverageCrr' => [
                'Crr' => '(int)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
            ],
            'PayForConversionPerCampaign' => [
                'Cpa' => '(long)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
            ],
            'PayForConversionPerFilter' => [
                'Cpa' => '(long)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
            ]
        )
    ),
    "CpmBannerCampaign" => array(
        "Search" => array(
            "BiddingStrategyType" => "( 'MANUAL_CPM' | ... | 'WB_DECREASED_PRICE_FOR_REPEATED_IMPRESSIONS' )"
        ),
        "Network" => array(
            'WbMaximumImpressions' => [
                'AverageCpm' => '(long)',
                'SpendLimit' => '(long)',
            ],
            'CpMaximumImpressions' => [
                'AverageCpm' => '(long)',
                'SpendLimit' => '(long)',
                'StartDate' => '(string)',
                'EndDate' => '(string)',
                'AutoContinue' => '( \'YES\' | \'NO\' )',
            ],
            'WbDecreasedPriceForRepeatedImpressions' => [
                'AverageCpm' => '(long)',
                'SpendLimit' => '(long)',
            ],
            'CpDecreasedPriceForRepeatedImpressions' => [
                'AverageCpm' => '(long)',
                'SpendLimit' => '(long)',
                'StartDate' => '(string)',
                'EndDate' => '(string)',
                'AutoContinue' => '( \'YES\' | \'NO\' )',
            ],
            'WbAverageCpv' => [
                'AverageCpv' => '(long)',
                'SpendLimit' => '(long)',
            ],
            'CpAverageCpv' => [
                'AverageCpv' => '(long)',
                'SpendLimit' => '(long)',
                'StartDate' => '(string)',
                'EndDate' => '(string)',
                'AutoContinue' => '( \'YES\' | \'NO\' )',
            ]
        )
    ),
    "MobileAppCampaign" => array(
        "Search" => array(
            'WbMaximumClicks' => [
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'WbMaximumAppInstalls' => [
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'AverageCpc' => [
                'AverageCpc' => '(long)',
                'WeeklySpendLimit' => '(long)',
            ],
            'AverageCpi' => [
                'AverageCpi' => '(long)',
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'WeeklyClickPackage' => [
                'ClicksPerWeek' => '(long)',
                'AverageCpc' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'PayForInstall' => [
                'AverageCpi' => '(long)',
                'WeeklySpendLimit' => '(long)',
            ]
        ),
        "Network" => array(
            'NetworkDefault' => [
                'LimitPercent' => '(int)',
            ],
            'WbMaximumClicks' => [
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'WbMaximumAppInstalls' => [
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'AverageCpc' => [
                'AverageCpc' => '(long)',
                'WeeklySpendLimit' => '(long)',
            ],
            'AverageCpi' => [
                'AverageCpi' => '(long)',
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'WeeklyClickPackage' => [
                'ClicksPerWeek' => '(long)',
                'AverageCpc' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'PayForInstall' => [
                'AverageCpi' => '(long)',
                'WeeklySpendLimit' => '(long)',
            ]
        )
    )
);

$criteriasSearch = $criteriasNetwork = require "network.php";

$fields = [
    CampaignFieldEnum::NAME,
    CampaignFieldEnum::ID,
    CampaignFieldEnum::STATUS,
    CampaignFieldEnum::DAILY_BUDGET,
];

if (isset($_POST['Campaigns'])) {
    $cmp = new CampaignAddItem();
    // create daily budget
//    $dailyBudget = new DailyBudget([
//        "Mode" => $_POST['Campaigns'][CampaignFieldEnum::DAILY_BUDGET]['Mode'],
//        "Amount" => $_POST['Campaigns'][CampaignFieldEnum::DAILY_BUDGET]['Amount'],
//    ]);


    //detect campaign type  from post
    $campaignType = $_POST['Campaigns']['Type'];
    $campaignAddItem = getCampaign($campaignType);

    // detect strategy from post
    $campaignStrategyClassName = "directapi\services\campaigns\models\\" . $campaignType . "Strategy";
    $campaignStrategySearchClassName = "directapi\services\campaigns\models\strategies\\" . $campaignType . "SearchStrategy";
    $campaignStrategyNetworkClassName = "directapi\services\campaigns\models\strategies\\" . $campaignType . "NetworkStrategy";

    // detect strategy classes values
    $searchStrategyItemsArray = $_POST[$campaignType]['BiddingStrategy']['Search'][$_POST["Campaigns"]['Strategy']['Search']];
    $networkStrategyItemsArray = $_POST[$campaignType]['BiddingStrategy']['Network'][$_POST["Campaigns"]['Strategy']['Network']];


    // strategies constructors argumants
    $biddingStrategyTypesCamelCase = [
        "WbMaximumClicks" => 'WB_MAXIMUM_CLICKS',
        "WbMaximumConversionRate" => "WB_MAXIMUM_CONVERSION_RATE",
        "AverageCpc" => "AVERAGE_CPC",
        "AverageCpa" => 'AVERAGE_CPA',
        "WeeklyClickPackage" => 'WEEKLY_CLICK_PACKAGE',
        "AverageRoi" => 'AVERAGE_ROI',
        "AverageCrr" => 'AVERAGE_CRR',
        "PayForConversion" => 'PAY_FOR_CONVERSION',
        "NetworkDefault" => 'NETWORK_DEFAULT',
        "BiddingStrategyType" => 'BIDDING_STRATEGY_TYPE',
        "AverageCpcPerCampaign" => 'AVERAGE_CPC_PER_CAMPAIGN',
        "AverageCpcPerFilter" => 'AVERAGE_CPC_PER_FILTER',
        "AverageCpaPerCampaign" => 'AVERAGE_CPA_PER_CAMPAIGN',
        "AverageCpaPerFilter" => 'AVERAGE_CPA_PER_FILTER',
        "PayForConversionPerCampaign" => 'PAY_FOR_CONVERSION_PER_CAMPAIGN',
        "PayForConversionPerFilter" => 'PAY_FOR_CONVERSION_PER_FILTER',
        "WbMaximumImpressions" => 'WB_MAXIMUM_IMPRESSIONS',
        "CpMaximumImpressions" => 'CP_MAXIMUM_IMPRESSIONS',
        "WbDecreasedPriceForRepeatedImpressions" => 'WB_DECREASED_PRICE_FOR_REPEATED_IMPRESSIONS',
        "CpDecreasedPriceForRepeatedImpressions" => 'CP_DECREASED_PRICE_FOR_REPEATED_IMPRESSIONS',
        "WbAverageCpv" => 'WB_AVERAGE_CPV',
        "CpAverageCpv" => 'CP_AVERAGE_CPV',
        "WbMaximumAppInstalls" => 'WB_MAXIMUM_APP_INSTALLS',
        "AverageCpi" => 'AVERAGE_CPI',
        "PayForInstall" => 'PAY_FOR_INSTALL'
    ];
    $campaignStrategySearchClassConstructArgs = array_merge(['BiddingStrategyType' => $biddingStrategyTypesCamelCase[$_POST["Campaigns"]['Strategy']['Search']]], [$_POST["Campaigns"]['Strategy']['Search'] => $searchStrategyItemsArray]);
    $campaignStrategyNetworkClassConstructArgs = array_merge(['BiddingStrategyType' => $biddingStrategyTypesCamelCase[$_POST["Campaigns"]['Strategy']['Network']]], [$_POST["Campaigns"]['Strategy']['Network'] => $networkStrategyItemsArray]);

    //create BiddingStrategy Constructor args
    $search = new $campaignStrategySearchClassName($campaignStrategySearchClassConstructArgs);
    $network = new $campaignStrategyNetworkClassName($campaignStrategyNetworkClassConstructArgs);

    //create BiddingStrategy
    $biddingStrategy = new $campaignStrategyClassName($search, $network);

    $campaignAddItem->BiddingStrategy = $biddingStrategy;
    $cmp->{$campaignType} = $campaignAddItem;
    $cmp->Name = $_POST['Campaigns'][CampaignFieldEnum::NAME];
    $cmp->BlockedIps['Items'] = explode(" ", $_POST['Campaigns'][CampaignFieldEnum::BLOCKED_IPS]);
    $cmp->NegativeKeywords['Items'] = explode(" ", $_POST['Campaigns'][CampaignFieldEnum::NEGATIVE_KEYWORDS]['Items']);
    $cmp->ExcludedSites['Items'] = explode(" ", $_POST['Campaigns'][CampaignFieldEnum::EXCLUDED_SITES]['Items']);
    $cmp->StartDate = date("Y-m-d", strtotime($_POST['Campaigns'][CampaignFieldEnum::START_DATE]));
    $cmp->EndDate = date("Y-m-d", strtotime($_POST['Campaigns'][CampaignFieldEnum::END_DATE]));
//    $cmp->DailyBudget = $dailyBudget;

    echo json_encode($cmp, JSON_PRETTY_PRINT);

    try {
        $adResults = $app->getDirect()->call("campaigns", 'add', ["Campaigns" => [$cmp]], true);
        $actionResult = new ActionResult();
        $actionResult->Errors = (isset($response->AddResults[0]->Errors)) ?? null;
        $actionResult->Warnings = (isset($response->AddResults[0]->Errors)) ?? null;
        $actionResult->Id = (isset($response->AddResults[0]->Id)) ?? null;
        if ($actionResult->Errors != null)
            foreach ($actionResult->Errors as $item)
                Alert::error('Campaign not created for view <a href="/campaigns">' . $item->Message . ', ' . $item->Details . '</a>');
        else Alert::success(' Campaign successfully created for view <a href="/campaigns/view?id=' . $actionResult->Id . '">checkout this page</a>');

    } catch (DataNotFoundException $e) {
        Alert::warning(' У вас есть Yandex Direct аккаунт ? Тогда выберите ваш аккаунт. Если нет создайте и  <a href="/accounts" class="text-theme-1 mx-1"> регистрируйте. </a> ');
    } catch (DirectAccountNotExistException | RequestValidationException | DirectApiNotEnoughUnitsException | GuzzleException | DirectApiException $e) {
        Alert::error(' Campaign not created for view <a href="/campaigns">' . $e->getMessage() . '</a>');
    }

}

function getCampaign($type)
{
    return [
        "TextCampaign" => new TextCampaignItem(),
        "DynamicTextCampaign" => new DynamicTextCampaignItem(),
        "SmartCampaign" => new SmartCampaignItem(),
        "CpmBannerCampaign" => new CpmBannerCampaignItem(),
        "MobileAppCampaign" => new MobileAppCampaignItem()
    ][$type];
}

?>

<script>
    var criterias = {
        "WbMaximumClicks": {
            "WeeklySpendLimit": "(long)",
            "BidCeiling": "(long)"
        },
        "WbMaximumConversionRate": {
            "WeeklySpendLimit": "(long)",
            "BidCeiling": "(long)",
            "GoalId": "(long)"
        },
        "AverageCpc": {
            "AverageCpc": "(long)",
            "WeeklySpendLimit": "(long)"
        },
        "AverageCpa": {
            "AverageCpa": "(long)",
            "GoalId": "(long)",
            "WeeklySpendLimit": "(long)",
            "BidCeiling": "(long)"
        },
        "WeeklyClickPackage": {
            "ClicksPerWeek": "(long)",
            "AverageCpc": "(long)",
            "BidCeiling": "(long)"
        },
        "AverageRoi": {
            "ReserveReturn": "(int)",
            "RoiCoef": "(long)",
            "GoalId": "(long)",
            "WeeklySpendLimit": "(long)",
            "BidCeiling": "(long)",
            "Profitability": "(long)"
        },
        "AverageCrr": {
            "Crr": "(int)",
            "GoalId": "(long)",
            "WeeklySpendLimit": "(long)"
        },
        "PayForConversion": {
            "Cpa": "(long)",
            "GoalId": "(long)",
            "WeeklySpendLimit": "(long)"
        },
        "NetworkDefault": {
            "LimitPercent": "(int)"
        },
        "BiddingStrategyType": "SERVING_OFF",
        "AverageCpcPerCampaign": {
            "AverageCpc": "(long)",
            "WeeklySpendLimit": "(long)",
            "BidCeiling": "(long)"
        },
        "AverageCpcPerFilter": {
            "FilterAverageCpc": "(long)",
            "WeeklySpendLimit": "(long)",
            "BidCeiling": "(long)"
        },
        "AverageCpaPerCampaign": {
            "AverageCpa": "(long)",
            "GoalId": "(long)",
            "WeeklySpendLimit": "(long)",
            "BidCeiling": "(long)"
        },
        "AverageCpaPerFilter": {
            "FilterAverageCpa": "(long)",
            "GoalId": "(long)",
            "WeeklySpendLimit": "(long)",
            "BidCeiling": "(long)"
        },
        "PayForConversionPerCampaign": {
            "Cpa": "(long)",
            "GoalId": "(long)",
            "WeeklySpendLimit": "(long)"
        },
        "PayForConversionPerFilter": {
            "Cpa": "(long)",
            "GoalId": "(long)",
            "WeeklySpendLimit": "(long)"
        },
        "WbMaximumImpressions": {
            "AverageCpm": "(long)",
            "SpendLimit": "(long)"
        },
        "CpMaximumImpressions": {
            "AverageCpm": "(long)",
            "SpendLimit": "(long)",
            "StartDate": "(string)",
            "EndDate": "(string)",
            "AutoContinue": "( 'YES' | 'NO' )"
        },
        "WbDecreasedPriceForRepeatedImpressions": {
            "AverageCpm": "(long)",
            "SpendLimit": "(long)"
        },
        "CpDecreasedPriceForRepeatedImpressions": {
            "AverageCpm": "(long)",
            "SpendLimit": "(long)",
            "StartDate": "(string)",
            "EndDate": "(string)",
            "AutoContinue": "( 'YES' | 'NO' )"
        },
        "WbAverageCpv": {
            "AverageCpv": "(long)",
            "SpendLimit": "(long)"
        },
        "CpAverageCpv": {
            "AverageCpv": "(long)",
            "SpendLimit": "(long)",
            "StartDate": "(string)",
            "EndDate": "(string)",
            "AutoContinue": "( 'YES' | 'NO' )"
        },
        "WbMaximumAppInstalls": {
            "WeeklySpendLimit": "(long)",
            "BidCeiling": "(long)"
        },
        "AverageCpi": {
            "AverageCpi": "(long)",
            "WeeklySpendLimit": "(long)",
            "BidCeiling": "(long)"
        },
        "PayForInstall": {
            "AverageCpi": "(long)",
            "WeeklySpendLimit": "(long)"
        }
    };

    $(document).ready(function () {
        let campaigns = <?=json_encode($campaigns, JSON_FORCE_OBJECT)?>;
        // select campaign type
        $("#campaign-type").on("change", function () {
            console.log(campaigns[this.value]);
            let network = campaigns[this.value]["Network"];
            let search = campaigns[this.value]["Search"];
            if ($("#campaign-type").attr("data-campaign") !== this.value) {
                hideAllCriteria("Search");
                hideAllCriteria("Network");
            }
            let searchStrategySelect = $("#search-strategy");
            searchStrategySelect.attr("data-campaign", this.value);
            searchStrategySelect.children().remove();
            let networkStrategySelect = $("#network-strategy");
            networkStrategySelect.attr("data-campaign", this.value);
            networkStrategySelect.children().remove();
            $("#campaign-type").attr("data-campaign", this.value);

            searchStrategySelect.append(new Option("Select campaign search strategy", "searchOptions", true));
            networkStrategySelect.append(new Option("Select campaign network strategy", "networkOptions", true));
            for (let searchOptions in search) {
                searchStrategySelect.append(new Option(searchOptions, searchOptions, false));
            }
            for (let networkOptions in network) {
                networkStrategySelect.append(new Option(networkOptions, networkOptions, false));
            }
        })
        let strategy;
        let campaignType;
        let showBlock;
        let valueSelectedItem;
        let previousSelectedItem;
        let hideAllCriteria = (prefixCriteria) => {
            for (let criteriasKey in criterias) {
                previousSelectedItemCriteria = $("#" + prefixCriteria + criteriasKey.toLowerCase().replaceAll("_", ""));
                if (!previousSelectedItemCriteria.hasClass("hidden")) previousSelectedItemCriteria.addClass("hidden");
            }
        }
        $('select.strategy').on('change', function () {
            strategy = this.getAttribute("data-strategy");
            if (this.getAttribute("data-campaign") !== campaignType) {
                hideAllCriteria("Search");
                hideAllCriteria("Network");
            }
            campaignType = this.getAttribute("data-campaign");

            console.log("Strategy", strategy);
            valueSelectedItem = this.value.toLowerCase().replaceAll("_", "");
            if (this.getAttribute('data-prev').length > 0) {
                previousSelectedItem = $("#" + this.getAttribute('data-prev'));
                if (!previousSelectedItem.hasClass("hidden"))
                    previousSelectedItem.addClass("hidden");
            }
            showBlock = $("#" + strategy + valueSelectedItem);
            if (showBlock.hasClass("hidden"))
                showBlock.removeClass("hidden");
            this.setAttribute("data-prev", strategy + valueSelectedItem);

            let inputName = campaignType + "[BiddingStrategy][" + strategy + "]";
            let input = showBlock.find("input");
            input.each(function (item) {
                let fieldName = input[item].getAttribute('data-field');
                let criteria = input[item].getAttribute('data-criteria');
                let nameItem = inputName + "[" + criteria + "]" + "[" + fieldName + "]";
                input[item].setAttribute('name', nameItem);
                console.log("Block input ", input[item].getAttribute('data-field'))
            });
            // console.log("Block input ",);
        });
    })
</script>
<div class="intro-y flex items-center mt-8">
    <h2 class="text-lg font-medium mr-auto">
        Create new Campaign
    </h2>
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
        <div class="hidden md:block mx-auto text-gray-600"></div>
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <a href="/direct/campaigns/index" class="button text-white bg-theme-1 shadow-md mr-2">Go back</a>
        </div>
    </div>
</div>
<form method="post" action="/direct/campaigns/create">
    <div class="grid grid-cols-12 gap-6 mt-5 box">

        <div class="intro-y col-span-12 lg:col-span-6">

            <div class="intro-y p-5">
                <div class="mb-3">
                    <label for="name">Company name</label>
                    <input type="text" name="Campaigns[<?= CampaignFieldEnum::NAME ?>]" class="input w-full border mt-2"
                           id="name">
                    <div class="form-text">Название кампании (до 255 символов).</div>
                </div>
                <div class="p-5 grid grid-cols-2 gap-4 row-gap-3">
                    <div class="col-span-1 sm:col-span-1">
                        <label for="<?= CampaignFieldEnum::START_DATE ?>">From</label>
                        <input class="datepicker input w-full border mt-2" id="<?= CampaignFieldEnum::START_DATE ?>"
                               name="Campaigns[<?= CampaignFieldEnum::START_DATE ?>]">
                        <div class="text-xs text-gray-600 mt-2">Дата начала показов объявлений в формате YYYY-MM-DD.
                            Должна быть не меньше текущей даты. Показы объявлений начинаются в 00:00 по московскому
                            времени (независимо от значения параметра TimeZone).
                        </div>
                    </div>
                    <div class="col-span-1 sm:col-span-1">
                        <label for="<?= CampaignFieldEnum::END_DATE ?>">To</label>
                        <input id="<?= CampaignFieldEnum::END_DATE ?>"
                               name="Campaigns[<?= CampaignFieldEnum::END_DATE ?>]"
                               class="datepicker input w-full border mt-2">
                        <div class="text-xs text-gray-600 mt-2">Дата окончания показов объявлений в формате YYYY-MM-DD.
                            Показы объявлений прекращаются в 24:00 по московскому времени (независимо от значения
                            параметра
                            TimeZone).
                        </div>
                    </div>

                </div>
                <div class="text-xs text-gray-600 mt-2 mb-4">
                    На время начала показов влияют
                    настройки временного таргетинга (параметр TimeTargeting). Показы объявлений возможны при
                    условии, что хотя бы одно объявление принято модерацией и внесены средства на кампанию или
                    на общий счет.
                </div>
                <div class="mb-3">
                    <label for="<?= CampaignFieldEnum::BLOCKED_IPS ?>">Blocked Ips</label>
                    <input id="<?= CampaignFieldEnum::BLOCKED_IPS ?>" type="text"
                           name="Campaigns[<?= CampaignFieldEnum::BLOCKED_IPS ?>]" class="input w-full border mt-2">
                    <div class="text-xs text-gray-600 mt-2">Массив IP-адресов, которым не нужно показывать объявления.
                        Не более 25 элементов в массиве.
                    </div>
                </div>
                <div class="mb-3">
                    <label for="<?= CampaignFieldEnum::NEGATIVE_KEYWORDS ?>">Массив минус-фраз, общих
                        для всех ключевых фраз
                        кампании.</label>
                    <input id="<?= CampaignFieldEnum::NEGATIVE_KEYWORDS ?>" type="text"
                           name="Campaigns[<?= CampaignFieldEnum::NEGATIVE_KEYWORDS ?>][Items]"
                           class="input w-full border mt-2"
                           aria-describedby="name">
                    <div class="text-xs text-gray-600 mt-2">Минус-фразу следует указывать без минуса перед первым
                        словом. Не более 7 слов в минус-фразе. Длина каждого слова — не более 35 символов. Суммарная
                        длина
                        минус-фраз в массиве — 20000 символов. Пробелы, дефисы и операторы не учитываются в суммарной
                        длине.
                    </div>
                </div>
                <!--                <div class="mb-3">-->
                <!--                    <label for="-->
                <? //= CampaignFieldEnum::DAILY_BUDGET ?><!--_amount" >Дневной бюджет-->
                <!--                        кампании в валюте рекламодателя, умноженный на 1 000 000.</label>-->
                <!--                    <input id="-->
                <? //= CampaignFieldEnum::DAILY_BUDGET ?><!--_amount" type="text"-->
                <!--                           name="Campaigns[-->
                <? //= CampaignFieldEnum::DAILY_BUDGET ?><!--][Amount]"-->
                <!--                           class="input w-full border mt-2"-->
                <!--                           aria-describedby="name">-->
                <!--                    <div class="text-xs text-gray-600 mt-2">Минимальный дневной бюджет для каждой валюты представлен в-->
                <!--                        справочнике валют. Справочник валют можно получить с помощью метода Dictionaries.get.-->
                <!--                    </div>-->
                <!--                </div>-->
                <!--                <div class="mb-3">-->
                <!--                    <label for="-->
                <? //= CampaignFieldEnum::DAILY_BUDGET ?><!--_mode" >Режим показа-->
                <!--                        объявлений: </label>-->
                <!--                    <select id="--><? //= CampaignFieldEnum::DAILY_BUDGET ?><!--_mode"-->
                <!--                            name="Campaigns[-->
                <? //= CampaignFieldEnum::DAILY_BUDGET ?><!--][Mode]" class="select2 w-full">-->
                <!--                        <option selected disabled>Режим показа объявлений:</option>-->
                <!--                        <option value="STANDARD">STANDARD</option>-->
                <!--                        <option value="DISTRIBUTED">DISTRIBUTED</option>-->
                <!--                    </select>-->
                <!--                    <div class="text-xs text-gray-600 mt-2">-->
                <!--                        Режим показа объявлений:<br/>-->
                <!--                        STANDARD — стандартный.<br/>-->
                <!--                        DISTRIBUTED — распределенный.<br/>-->
                <!--                        См. подраздел Средний дневной бюджет раздела «Ручное управление ставками» помощи Директа.-->
                <!--                    </div>-->
                <!--                </div>-->
                <div class="mb-3">
                    <label for="<?= CampaignFieldEnum::EXCLUDED_SITES ?>">Массив мест показа, где не
                        нужно показывать объявления:</label>
                    <input type="text" id="<?= CampaignFieldEnum::EXCLUDED_SITES ?>"
                           name="Campaigns[<?= CampaignFieldEnum::EXCLUDED_SITES ?>][Items]"
                           class="input w-full border mt-2">
                    <div class="text-xs text-gray-600 mt-2">Массив мест показа, где не нужно показывать объявления:
                        доменные имена сайтов; идентификаторы мобильных приложений (bundle ID для iOS, package name для
                        Android); наименования внешних сетей (SSP). Список наименований можно получить с помощью метода
                        Dictionaries.get. Не более 1000 элементов в массиве. Не более 255 символов в каждом элементе
                        массива.
                    </div>
                </div>
                <div class="text-right mt-5">
                    <button type="reset" class="button w-24 border text-gray-700 mr-1">Cancel</button>
                    <button type="submit" class="button w-24 bg-theme-1 text-white">Save</button>
                </div>

            </div>
        </div>

        <div class="intro-y col-span-12 lg:col-span-6">
            <div class="intro-y p-5">
                <div class="mt-5">
                    <select id="campaign-type" name="Campaigns[Type]" class="select2 w-full"
                            aria-label="Default select example" data-campaign=""
                            data-placeholder="Select campaign type">
                        <option selected disabled value="Campaign">Select campaign type</option>
                        <option value="TextCampaign">Текстово-графические объявления</option>
                        <option value="MobileAppCampaign">Реклама мобильных приложений</option>
                        <option value="DynamicTextCampaign">Динамические объявления</option>
                        <option value="CpmBannerCampaign">Медийная кампания</option>
                        <option value="SmartCampaign">Смарт-баннеры</option>
                    </select>
                    <div class="text-xs text-gray-600 mt-2">Стратегия показа.</div>
                </div>
                <div class="mb-3">
                    <label for="search-strategy">Search Bidding Strategy Type</label>
                    <select id="search-strategy" name="Campaigns[Strategy][Search]" class="strategy select2 w-full"
                            data-prev="" data-campaign="" data-strategy="Search">
                        <option selected disabled>Select campaign search strategy</option>
                    </select>
                    <div class="text-xs text-gray-600 mt-2">«Ручное управление ставками с оптимизацией» См. описание в
                        помощи Директа
                    </div>
                </div>
                <div class="mb-3">
                    <label for="network-strategy">Network Bidding Strategy Type</label>
                    <select id="network-strategy" name="Campaigns[Strategy][Network]" class="strategy select2 w-full"
                            data-prev="" data-campaign="" data-strategy="Network">
                        <option selected disabled>Select campaign network strategy</option>
                    </select>
                    <div class="text-xs text-gray-600 mt-2">«Ручное управление ставками с оптимизацией», включено
                        раздельное управление ставками на поиске и в сетях См. описание в помощи Директа
                    </div>
                </div>
                <div class=" my-2 border p-2"><p class="font-medium text-base mr-auto">Selected Criteria Search</p>
                    <?php foreach ($criteriasSearch as $criteriaSearch => $criteriaSearchValues) { ?>
                        <div id="Search<?= strtolower(str_replace("_", "", $criteriaSearch)); ?>"
                             class="hidden border p-1 my-2">
                            <p class="font-medium text-base mr-auto"><?= $criteriaSearch; ?></p>
                            <?php if (is_array($criteriaSearchValues)) {
                                foreach ($criteriaSearchValues as $fieldSearch => $kSearch) { ?>
                                    <div class="mb-3">
                                        <label for="search_<?= $fieldSearch ?>"
                                        ><?= $fieldSearch ?></label>
                                        <input type="text" data-field="<?= $fieldSearch ?>"
                                               data-criteria="<?= $criteriaSearch ?>"
                                               class="input w-full border mt-2">
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
                <div class=" my-2 border p-2"><p class="font-medium text-base mr-auto">Selected Criteria Network</p>
                    <?php foreach ($criteriasNetwork as $criteria => $criteriaValues) { ?>
                    <div id="Network<?= strtolower(str_replace("_", "", $criteria)); ?>" class="hidden border p-1 my-2">
                        <p class="font-medium text-base mr-auto"><?= $criteria; ?></p>
                        <?php if (is_array($criteriaValues)) {
                            foreach ($criteriaValues as $field => $k) { ?>
                                <div class="mb-3">
                                    <label for="network_<?= $field ?>"><?= $field ?></label>
                                    <input type="text" data-field="<?= $field ?>" data-criteria="<?= $criteria ?>"
                                           class="input w-full border mt-2">
                                </div>
                            <?php } ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>

            </div>
        </div>
    </div>
</form>

