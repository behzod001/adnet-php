<?php
$campaigns = array(
    "TextCampaign" => array(
        "Search" => array(
            'WbMaximumClicks' => [
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'WbMaximumConversionRate' => [
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
                'GoalId' => '(long)',
            ],
            'AverageCpc' => [
                'AverageCpc' => '(long)',
                'WeeklySpendLimit' => '(long)',
            ],
            'AverageCpa' => [
                'AverageCpa' => '(long)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'WeeklyClickPackage' => [
                'ClicksPerWeek' => '(long)',
                'AverageCpc' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'AverageRoi' => [
                'ReserveReturn' => '(int)',
                'RoiCoef' => '(long)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
                'Profitability' => '(long)',
            ],
            'AverageCrr' => [
                'Crr' => '(int)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
            ],
            'PayForConversion' => [
                'Cpa' => '(long)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
            ]
        ),
        "Network" => array(
            'NetworkDefault' =>
                [
                    'LimitPercent' => '(int)',
                ],
            'WbMaximumClicks' =>
                [
                    'WeeklySpendLimit' => '(long)',
                    'BidCeiling' => '(long)',
                ],
            'WbMaximumConversionRate' =>
                [
                    'WeeklySpendLimit' => '(long)',
                    'BidCeiling' => '(long)',
                    'GoalId' => '(long)',
                ],
            'AverageCpc' =>
                [
                    'AverageCpc' => '(long)',
                    'WeeklySpendLimit' => '(long)',
                ],
            'AverageCpa' =>
                [
                    'AverageCpa' => '(long)',
                    'GoalId' => '(long)',
                    'WeeklySpendLimit' => '(long)',
                    'BidCeiling' => '(long)',
                ],
            'WeeklyClickPackage' =>
                [
                    'ClicksPerWeek' => '(long)',
                    'AverageCpc' => '(long)',
                    'BidCeiling' => '(long)',
                ],
            'AverageRoi' =>
                [
                    'ReserveReturn' => '(int)',
                    'RoiCoef' => '(long)',
                    'GoalId' => '(long)',
                    'WeeklySpendLimit' => '(long)',
                    'BidCeiling' => '(long)',
                    'Profitability' => '(long)',
                ],
            'AverageCrr' =>
                [
                    'Crr' => '(int)',
                    'GoalId' => '(long)',
                    'WeeklySpendLimit' => '(long)',
                ],
            'PayForConversion' =>
                [
                    'Cpa' => '(long)',
                    'GoalId' => '(long)',
                    'WeeklySpendLimit' => '(long)',
                ],
        )
    ),
    "DynamicTextCampaign" => array(
        "Search" => array(
            'WbMaximumClicks' => [
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'WbMaximumConversionRate' => [
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
                'GoalId' => '(long)',
            ],
            'AverageCpc' => [
                'AverageCpc' => '(long)',
                'WeeklySpendLimit' => '(long)',
            ],
            'AverageCpa' => [
                'AverageCpa' => '(long)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'WeeklyClickPackage' => [
                'ClicksPerWeek' => '(long)',
                'AverageCpc' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'AverageRoi' => [
                'ReserveReturn' => '(int)',
                'RoiCoef' => '(long)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
                'Profitability' => '(long)',
            ],
            'AverageCrr' => [
                'Crr' => '(int)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
            ],
            'PayForConversion' => [
                'Cpa' => '(long)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
            ]
        ),
        "Network" => array(
            "BiddingStrategyType" => "SERVING_OFF"
        )
    ),
    "SmartCampaign" => array(
        "Network" => array(
            'AverageCpcPerCampaign' => [
                'AverageCpc' => '(long)',
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'AverageCpcPerFilter' => [
                'FilterAverageCpc' => '(long)',
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'AverageCpaPerCampaign' => [
                'AverageCpa' => '(long)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'AverageCpaPerFilter' => [
                'FilterAverageCpa' => '(long)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'AverageRoi' => [
                'ReserveReturn' => '(int)',
                'RoiCoef' => '(long)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
                'Profitability' => '(long)',
            ],
            'AverageCrr' => [
                'Crr' => '(int)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
            ],
            'PayForConversionPerCampaign' => [
                'Cpa' => '(long)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
            ],
            'PayForConversionPerFilter' => [
                'Cpa' => '(long)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
            ],
            'NetworkDefault' => [
            ]
        ),
        "Search" => array(
            'AverageCpcPerCampaign' => [
                'AverageCpc' => '(long)',
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'AverageCpcPerFilter' => [
                'FilterAverageCpc' => '(long)',
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'AverageCpaPerCampaign' => [
                'AverageCpa' => '(long)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'AverageCpaPerFilter' => [
                'FilterAverageCpa' => '(long)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'AverageRoi' => [
                'ReserveReturn' => '(int)',
                'RoiCoef' => '(long)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
                'Profitability' => '(long)',
            ],
            'AverageCrr' => [
                'Crr' => '(int)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
            ],
            'PayForConversionPerCampaign' => [
                'Cpa' => '(long)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
            ],
            'PayForConversionPerFilter' => [
                'Cpa' => '(long)',
                'GoalId' => '(long)',
                'WeeklySpendLimit' => '(long)',
            ]
        )
    ),
    "CpmBannerCampaign" => array(
        "Search" => array(
            "BiddingStrategyType" => "( 'MANUAL_CPM' | ... | 'WB_DECREASED_PRICE_FOR_REPEATED_IMPRESSIONS' )"
        ),
        "Network" => array(
            'WbMaximumImpressions' => [
                'AverageCpm' => '(long)',
                'SpendLimit' => '(long)',
            ],
            'CpMaximumImpressions' => [
                'AverageCpm' => '(long)',
                'SpendLimit' => '(long)',
                'StartDate' => '(string)',
                'EndDate' => '(string)',
                'AutoContinue' => '( \'YES\' | \'NO\' )',
            ],
            'WbDecreasedPriceForRepeatedImpressions' => [
                'AverageCpm' => '(long)',
                'SpendLimit' => '(long)',
            ],
            'CpDecreasedPriceForRepeatedImpressions' => [
                'AverageCpm' => '(long)',
                'SpendLimit' => '(long)',
                'StartDate' => '(string)',
                'EndDate' => '(string)',
                'AutoContinue' => '( \'YES\' | \'NO\' )',
            ],
            'WbAverageCpv' => [
                'AverageCpv' => '(long)',
                'SpendLimit' => '(long)',
            ],
            'CpAverageCpv' => [
                'AverageCpv' => '(long)',
                'SpendLimit' => '(long)',
                'StartDate' => '(string)',
                'EndDate' => '(string)',
                'AutoContinue' => '( \'YES\' | \'NO\' )',
            ]
        )
    ),
    "MobileAppCampaign" => array(
        "Search" => array(
            'WbMaximumClicks' => [
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'WbMaximumAppInstalls' => [
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'AverageCpc' => [
                'AverageCpc' => '(long)',
                'WeeklySpendLimit' => '(long)',
            ],
            'AverageCpi' => [
                'AverageCpi' => '(long)',
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'WeeklyClickPackage' => [
                'ClicksPerWeek' => '(long)',
                'AverageCpc' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'PayForInstall' => [
                'AverageCpi' => '(long)',
                'WeeklySpendLimit' => '(long)',
            ]
        ),
        "Network" => array(
            'NetworkDefault' => [
                'LimitPercent' => '(int)',
            ],
            'WbMaximumClicks' => [
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'WbMaximumAppInstalls' => [
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'AverageCpc' => [
                'AverageCpc' => '(long)',
                'WeeklySpendLimit' => '(long)',
            ],
            'AverageCpi' => [
                'AverageCpi' => '(long)',
                'WeeklySpendLimit' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'WeeklyClickPackage' => [
                'ClicksPerWeek' => '(long)',
                'AverageCpc' => '(long)',
                'BidCeiling' => '(long)',
            ],
            'PayForInstall' => [
                'AverageCpi' => '(long)',
                'WeeklySpendLimit' => '(long)',
            ]
        )
    )
);

$criterias = [];

foreach ($campaigns as $campaign) {
    foreach ($campaign as $item) {
        foreach ($item as $criteria => $value) {
            if (!array_key_exists($criteria, $criterias)) $criterias[$criteria] = $value;
        }
    }
}


$keys = array_keys($criterias);
return $criterias;


