<?php


use directapi\common\containers\ArrayOfString;
use directapi\services\campaigns\criterias\CampaignsSelectionCriteria;
use directapi\services\campaigns\enum\CampaignFieldEnum;
use directapi\services\campaigns\models\CampaignUpdateItem;
use directapi\services\campaigns\models\strategies\TextCampaignNetworkStrategy;
use directapi\services\campaigns\models\strategies\TextCampaignSearchStrategy;
use directapi\services\campaigns\models\TextCampaignStrategy;
use directapi\services\campaigns\models\TextCampaignUpdateItem;
use helpers\Alert;

global $app;

$fields = [
    CampaignFieldEnum::NAME,
    CampaignFieldEnum::ID,
    CampaignFieldEnum::STATUS,
    CampaignFieldEnum::EXCLUDED_SITES,
    CampaignFieldEnum::START_DATE,
    CampaignFieldEnum::END_DATE,
    CampaignFieldEnum::BLOCKED_IPS,
    CampaignFieldEnum::NEGATIVE_KEYWORDS,
];

$criteria = new CampaignsSelectionCriteria();
$criteria->Ids = [$_GET['id']];
$app->getDirect()->setClientLogin("testinglogin2");
$campaign = $app->getDirect()->getCampaignsService()->get($criteria, $fields)[0];

if (isset($_POST['Campaigns'])) {
    $cmp = new CampaignUpdateItem();
    $cmp->Id = $_GET['id'];
    $cmp->Name = $_POST['Campaigns'][CampaignFieldEnum::NAME];
    $cmp->StartDate = date("Y-m-d", strtotime($_POST['Campaigns'][CampaignFieldEnum::START_DATE]));
    $cmp->EndDate = date("Y-m-d", strtotime($_POST['Campaigns'][CampaignFieldEnum::END_DATE]));
    $cmp->ExcludedSites = new ArrayOfString(explode(' ', $_POST['Campaigns'][CampaignFieldEnum::EXCLUDED_SITES]['Items']));
    $cmp->BlockedIps = new ArrayOfString(explode(' ', $_POST['Campaigns'][CampaignFieldEnum::BLOCKED_IPS]['Items']));
    $cmp->NegativeKeywords = new ArrayOfString(explode(' ', $_POST['Campaigns'][CampaignFieldEnum::NEGATIVE_KEYWORDS]['Items']));
    $response = $app->getDirect()->getCampaignsService()->update([$cmp]);

    if ($response[0]->Errors != null)
        foreach ($response[0]->Errors as $item)
            Alert::error(' <i data-feather="alert-circle" class="w-6 h-6 mr-2"></i> Campaign not updated for view <a href="/campaigns"> ' . $item->Message . ' </a>  ' . $item->Details);
    else
        Alert::success(' <i data-feather="check-square" class="w-6 h-6 mr-2"></i> Campaign successfully updated for view <a href="/campaigns/view?service=direct&id=' . $response[0]->Id . '"> checkout this page </a> ');
}
?>


<div class="intro-y flex items-center mt-8">
    <h2 class="text-lg font-medium mr-auto">
        Create new Campaign
    </h2>
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
        <div class="hidden md:block mx-auto text-gray-600"></div>
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <a href="/campaigns" class="button text-white bg-theme-1 shadow-md mr-2">Go back</a>
        </div>
    </div>
</div>
<form method="post" action="/direct/campaigns/update?id=<?= $_GET['id'] ?>">
    <div class="grid grid-cols-12 gap-6 mt-5 box">

        <div class="intro-y col-span-12 lg:col-span-6">

            <div class="intro-y p-5">
                <div class="mb-3">
                    <label for="name">Company name</label>
                    <input type="text" name="Campaigns[<?= CampaignFieldEnum::NAME ?>]" class="input w-full border mt-2"
                           id="name" value="<?= $campaign->{CampaignFieldEnum::NAME} ?>">
                    <div class="form-text">Название кампании (до 255 символов).</div>
                </div>
                <div class="p-5 grid grid-cols-2 gap-4 row-gap-3">
                    <div class="col-span-1 sm:col-span-1">
                        <label for="<?= CampaignFieldEnum::START_DATE ?>">From <?= date("d/m/Y", strtotime($campaign->{CampaignFieldEnum::START_DATE})) ?></label>
                        <input class="datepicker input w-full border mt-2" id="<?= CampaignFieldEnum::START_DATE ?>"
                               name="Campaigns[<?= CampaignFieldEnum::START_DATE ?>]">
                        <div class="text-xs text-gray-600 mt-2">Дата начала показов объявлений .
                            Должна быть не меньше текущей даты. Показы объявлений начинаются в 00:00 по московскому
                            времени (независимо от значения параметра TimeZone).
                        </div>
                    </div>
                    <div class="col-span-1 sm:col-span-1">
                        <label for="<?= CampaignFieldEnum::END_DATE ?>">To</label>
                        <input id="<?= CampaignFieldEnum::END_DATE ?>"
                               value="<?= date("m/d/Y", strtotime($campaign->{CampaignFieldEnum::END_DATE})) ?>"
                               name="Campaigns[<?= CampaignFieldEnum::END_DATE ?>]"
                               class="datepicker input w-full border mt-2">
                        <div class="text-xs text-gray-600 mt-2">Дата окончания показов объявлений.
                            Показы объявлений прекращаются в 24:00 по московскому времени (независимо от значения
                            параметра TimeZone).
                        </div>
                    </div>

                </div>
                <div class="text-xs text-gray-600 mt-2 mb-4">
                    На время начала показов влияют
                    настройки временного таргетинга (параметр TimeTargeting). Показы объявлений возможны при
                    условии, что хотя бы одно объявление принято модерацией и внесены средства на кампанию или
                    на общий счет.
                </div>


            </div>
        </div>

        <div class="intro-y col-span-12 lg:col-span-6">
            <div class="intro-y p-5">
                <div class="mb-3">
                    <label for="<?= CampaignFieldEnum::BLOCKED_IPS ?>">Blocked Ips</label>
                    <input id="<?= CampaignFieldEnum::BLOCKED_IPS ?>" type="text"
                           value="<?= implode(' ', $campaign->{CampaignFieldEnum::BLOCKED_IPS}->Items) ?>"
                           name="Campaigns[<?= CampaignFieldEnum::BLOCKED_IPS ?>][Items]"
                           class="input w-full border mt-2">
                    <div class="text-xs text-gray-600 mt-2">Массив IP-адресов, которым не нужно показывать объявления.
                        Не более 25 элементов в массиве.
                    </div>
                </div>

                <div class="mb-3">
                    <label for="<?= CampaignFieldEnum::NEGATIVE_KEYWORDS ?>">Массив минус-фраз, общих
                        для всех ключевых фраз
                        кампании.</label>
                    <input id="<?= CampaignFieldEnum::NEGATIVE_KEYWORDS ?>" type="text"
                           name="Campaigns[<?= CampaignFieldEnum::NEGATIVE_KEYWORDS ?>][Items]"
                           value="<?= implode(' ', $campaign->{CampaignFieldEnum::NEGATIVE_KEYWORDS}->Items) ?>"
                           class="input w-full border mt-2">
                    <div class="text-xs text-gray-600 mt-2">Минус-фразу следует указывать без минуса перед первым
                        словом. Не более 7 слов в минус-фразе. Длина каждого слова — не более 35 символов. Суммарная
                        длина
                        минус-фраз в массиве — 20000 символов. Пробелы, дефисы и операторы не учитываются в суммарной
                        длине.
                    </div>
                </div>

                <div class="mb-3">
                    <label for="<?= CampaignFieldEnum::EXCLUDED_SITES ?>">Массив мест показа, где не
                        нужно показывать объявления:</label>
                    <input type="text" id="<?= CampaignFieldEnum::EXCLUDED_SITES ?>"
                           value="<?= implode(' ', $campaign->{CampaignFieldEnum::EXCLUDED_SITES}->Items) ?>"
                           name="Campaigns[<?= CampaignFieldEnum::EXCLUDED_SITES ?>][Items]"
                           class="input w-full border mt-2">
                    <div class="text-xs text-gray-600 mt-2">Массив мест показа, где не нужно показывать объявления:
                        доменные имена сайтов; идентификаторы мобильных приложений (bundle ID для iOS, package name для
                        Android); наименования внешних сетей (SSP). Список наименований можно получить с помощью метода
                        Dictionaries.get. Не более 1000 элементов в массиве. Не более 255 символов в каждом элементе
                        массива.
                    </div>
                </div>
                <div class="text-right mt-5">
                    <button type="reset" class="button w-24 border text-gray-700 mr-1">Cancel</button>
                    <button type="submit" class="button w-24 bg-theme-1 text-white">Save</button>
                </div>
            </div>
        </div>
    </div>
</form>
