<?php

use app\helpers\ChartColors;
use app\models\GoogleReports;
use helpers\XmlParser;

/**
 * @var XmlParser $parser
 * @var $model GoogleReports
 */

if (isset($_GET['id'])) {
    $reportId = $_GET['id'];
    // getting report file item from db
    $model = new GoogleReports();
    $reportItem = $model->findById($reportId);

    // parsing report item by XmlParser
    $parser = (new XmlParser())->parseFileContent($reportItem->file);

}

$campaignClicks = $campaignViews = [];
?>
<h2 class="intro-y text-lg font-medium mt-10">
    <?=$parser->getReportName(); ?>:
    <?=$parser->getDateRange(); ?>
</h2>

<!-- BEGIN: Table -->
<div class="grid grid-cols-12 gap-6 mt-5">
    <!-- BEGIN: Data List -->
    <div class="intro-y col-span-12 overflow-auto">
        <table class="table table-report -mt-2 res">
            <thead>
            <tr>
                <?php
                foreach ($parser->getHeaderRows()->values() as $value) { ?>
                    <th> <?= $value; ?> </th>
                <?php } ?>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($parser->getRowColumnsData()->values() as $rowItem) {
                $campaignClicks["{$rowItem->get(md5("Кампания"))}"] = (int)preg_replace('/[\x00-\x1F\x7F, ]/u', '', $rowItem->get(md5("Kлики")));
                $campaignViews["{$rowItem->get(md5("Кампания"))}"] = (int)preg_replace('/[\x00-\x1F\x7F, ]/u', '', $rowItem->get(md5("Расходы")));
                $campaignCost["{$rowItem->get(md5("Кампания"))}"] = (int)preg_replace('/[\x00-\x1F\x7F, ]/u', '', $rowItem->get(md5("Расходы")));
                ?>
                <tr class="intro-x">
                    <?php foreach ($rowItem->values() as $item => $value) { ?>
                        <td class="w-40"><?= $value; ?> </td>
                    <?php } ?>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <!-- END: Data List -->
</div>
<!-- END: Table -->


<div class="col-span-12 xxl:col-span-9 grid grid-cols-12 gap-6">
    <!-- BEGIN: Weekly Top Seller -->
    <div class="col-span-12 sm:col-span-6 lg:col-span-3 mt-8">
        <div class="intro-y flex items-center h-10">
            <h2 class="text-lg font-medium truncate mr-5">
                Кампания Kлики
            </h2>
            <!--            <a href="" class="ml-auto text-theme-1 truncate">See all</a>-->
        </div>
        <div class="intro-y box p-5 mt-5">
            <canvas class="mt-3" id="report-pie-chart"
                    data-count="<?= implode(",", $campaignClicks) ?>"
                    data-labels="<?= implode(",", array_keys($campaignClicks)) ?>"
                    data-colors='#FF8B26,#FFC533,#285FD3,#ff9933,#ccff33,#a3a375'
                    height="280"></canvas>
        </div>
    </div>
    <!-- END: Weekly Top Seller -->
    <!-- BEGIN: Weekly Top Seller -->
    <div class="col-span-12 sm:col-span-6 lg:col-span-3 mt-8">
        <div class="intro-y flex items-center h-10">
            <h2 class="text-lg font-medium truncate mr-5">
                Кампания Показы
            </h2>
            <!--            <a href="" class="ml-auto text-theme-1 truncate">See all</a>-->
        </div>
        <div class="intro-y box p-5 mt-5">
            <canvas class="mt-3" id="report-pie-chart2"
                    data-count="<?= implode(",", $campaignViews) ?>"
                    data-labels="<?= implode(",", array_keys($campaignViews)) ?>"
                    data-colors='#FF8B26,#FFC533,#285FD3,#ff9933,#ccff33,#a3a375'
                    height="280"></canvas>
        </div>
    </div>
    <!-- END: Weekly Top Seller -->
    <!-- BEGIN: Weekly Top Seller -->
    <div class="col-span-12 sm:col-span-12 lg:col-span-6 mt-8">
        <div class="intro-y flex items-center h-10">
            <h2 class="text-lg font-medium truncate mr-5">
                Кампания Расходы
            </h2>
            <!--            <a href="" class="ml-auto text-theme-1 truncate">See all</a>-->
        </div>
        <div class="intro-y box p-5 mt-5">
            <canvas id="vertical-bar-chart-widget"
                    data-count="<?= implode(",", $campaignCost) ?>"
                    data-labels="<?= implode(",", array_keys($campaignCost)) ?>"
                    data-colors='#FF8B26,#FFC533,#285FD3,#ff9933,#ccff33,#a3a375'
                    height="280"></canvas>
        </div>
    </div>
</div>

