<?php


use app\models\GoogleReports;
use helpers\Alert;

if (isset($_GET['id'])) {
    $reportId = $_GET['id'];
    // getting report file item from db
    $model = new GoogleReports();
    if ($model->deleteById($reportId)) {
        Alert::setFlush("deleted", Alert::generateMessage(Alert::SUCCESS, "File successfully deleted"));
        header("Location: /google");
    } else
        Alert::setFlush("deleted", Alert::generateMessage(Alert::ERROR, "Something went wrong"));


}
