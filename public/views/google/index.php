<?php
/**
 * $fields [] array of string arrays
 * $uploadedFiles [] all uploaded files from google
 */

use app\exceptions\DataNotFoundException;
use app\helpers\LinkPager;
use app\models\GoogleReports;
use app\models\ReportTypes;
use helpers\Alert;

$viewLast = (isset($_GET['view']) && $_GET['view'] == "last") ? $viewLast = true : $viewLast = false;
$page = (isset($_GET['page'])) ? $_GET['page'] : 0;
$size = (isset($_GET['size'])) ? $_GET['size'] : 10;
$pageResult = null;
if (isset($_GET['val']) && $_GET['type']) {
    try {
        $type = ReportTypes::getTypesValue($_GET['val'], $_GET['type']);

        $pageResult = GoogleReports::findAllPageable($page, $size, $_GET['type']);
    } catch (DataNotFoundException $e) {
        Alert::error($_GET['val'] . " type not found");
    }
}

echo Alert::getFlush("deleted");
?>

<style>
    tr.bg-pink td, tr.bg-pink td a {
        --text-opacity: 1;
        color: #F78B00;
        background-color: rgba(255, 239, 217, var(--bg-opacity)) !important;
    }
</style>
<h2 class="intro-y text-lg font-medium mt-10">
    Google reports type <?= $type ?>
</h2>

<div class="grid grid-cols-12 gap-6 mt-5">
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
        <button class="button text-white bg-theme-1 shadow-md mr-2">Upload File</button>
        <div class="relative ">
            <a href="/google/upload?type=<?= isset($_GET['type']) ?? "" ?>&val=<?= isset($_GET['val']) ?? ""; ?>">
                <button class=" button px-2 box text-gray-700">
            <span class="w-5 h-5 flex items-center justify-center">
                <i class="w-4 h-4" data-feather="plus"></i>
            </span>
                </button>
            </a>
        </div>
        <div class="hidden md:block mx-auto text-gray-600"><?= sprintf("Showing %d to %d of %d", count($pageResult->getRows()), $size, $pageResult->getTotal()) ?>
            entries
        </div>
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <div class="w-56 relative text-gray-700">
                <input type="text" class="input w-56 box pr-10 placeholder-theme-13" placeholder="Search...">
                <i class="w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0" data-feather="search"></i>
            </div>
        </div>
    </div>
    <!-- BEGIN: Data List -->
    <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
        <table class="table table-report -mt-2">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name Campaign</th>
                <th scope="col"> Report Type</th>
                <th scope="col">Uploaded date</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $idx = 0;
            for ($i = 0; $i < count($pageResult->getRows()); $i++) {
                $file = $pageResult->getRows()[$i];

                ++$idx;
                $index = ($page) * (int)$size + $idx; ?>
                <tr class=" <?= $viewLast && $idx == 1 ? " bg-pink " : "" ?>">
                    <td> <?= $index ?></td>
                    <td><?= $file->name ?></td>
                    <td><?= ReportTypes::getTypesValue($_GET['val'], $_GET['type']) ?></td>
                    <td><?= date('Y-m-d', $file->created_at) ?></td>
                    <td class=" table-report__action w-56">
                        <div class="flex justify-center items-center">
                            <a class="flex items-center mr-3"
                               href="/google/chart?id=<?= $file->id ?>"> <i
                                        data-feather="eye" class="w-4 h-4 mr-1"></i> View </a>
                            <a class="flex items-center mr-3"
                               href="/google/update?id=<?= $file->id ?>"> <i
                                        data-feather="check-square" class="w-4 h-4 mr-1"></i> Edit </a>
                            <a class="flex items-center text-theme-6" href="javascript:" data-toggle="modal"
                               data-delete="/google/delete?id=<?= $file->id ?>"
                               onclick="confirmDelete(this);"
                               data-target="#delete-confirmation-modal">
                                <i data-feather="trash-2" class="w-4 h-4 mr-1"></i>
                                Delete </a>
                        </div>
                    </td>
                </tr>
            <?php } ?>

            </tbody>
        </table>
    </div>


    <?php

    $totalLink = ceil($pageResult->getTotal() / $size);
    ?>

    <!-- BEGIN: Pagination -->
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-row sm:flex-no-wrap items-center">
        <ul class="pagination">
            <?php
            $linkPager = new LinkPager([
                "total" => $pageResult->getTotal(),
                "linkTemplate" => "/google/index?type=" . $_GET['type'] . "&val=" . $_GET['val'] . "&page={page}&size={size}",
                "page" => $page,
                "size" => $size
            ]);
            $linkPager->print();
            ?>
        </ul>

        <select class="w-20 input box mt-3 sm:mt-0">
            <option>10</option>
            <option>25</option>
            <option>35</option>
            <option>50</option>
        </select>
    </div>
    <!-- END: Pagination -->
</div>
<!-- BEGIN: Delete Confirmation Modal -->
<div class="modal" id="delete-confirmation-modal">
    <div class="modal__content">
        <div class="p-5 text-center">
            <i data-feather="x-circle" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i>
            <div class="text-3xl mt-5">Are you sure?</div>
            <div class="text-gray-600 mt-2">Do you really want to delete these records? This process cannot be
                undone.
            </div>
        </div>
        <div class="px-5 pb-8 text-center">
            <button type="button" data-dismiss="modal" class="button w-24 border text-gray-700 mr-1">Cancel</button>
            <a href="" id="deleteBtn" type="button" class="button w-24 bg-theme-6 text-white">Delete</a>
        </div>
    </div>
</div>
<!-- END: Delete Confirmation Modal -->

