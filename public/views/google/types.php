<?php

use app\models\ReportTypes;

?>


<!-- END: Top Bar -->
<div class="intro-y flex items-center mt-8">
    <h2 class="text-lg font-medium mr-auto">
        Google Report
    </h2>
</div>
<div class="intro-y grid grid-cols-12 gap-6 mt-5">
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
        <button class="button text-white bg-theme-1 shadow-md mr-2">Upload File</button>
        <div class="relative ">
            <a href="/google/upload">
                <button class=" button px-2 box text-gray-700">
            <span class="w-5 h-5 flex items-center justify-center">
                <i class="w-4 h-4" data-feather="plus"></i>
            </span>
                </button>
            </a>
        </div>

    </div>
    <!-- BEGIN: Basic Accordion -->
    <div class="col-span-12 lg:col-span-12">
        <div class="intro-y box">
            <div class="p-5" id="basic-accordion">
                <div class="preview">
                    <div class="accordion">
                        <?php
                        $index = 0;
                        foreach (ReportTypes::getTypes() as $type => $items) { ?>
                        <div class="accordion__pane <?= (++$index == 1) ? " active " : "" ?> border-b border-gray-200 pb-4">
                            <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200">
                                <a href="javascript:;" class="accordion__pane__toggle font-medium block">
                                    <h2 class="font-medium text-base mr-auto">
                                        <?= $type ?>
                                    </h2>
                                </a>
                            </div>
                            <div class="accordion__pane__content mt-3 text-gray-700 leading-relaxed pl-20 pr-20">
                                <div class="grid grid-cols-12 gap-5 mt-5">
                                    <?php foreach ($items

                                    as $key => $val) { ?>
                                    <div class="col-span-4 sm:col-span-4 xxl:col-span-3 bg-theme-1 box p-5 cursor-pointer zoom-in">
                                        <div class="font-medium text-base text-white"
                                        "><?= $val ?></div>
                                    <div class="text-gray-600  text-white"
                                    ">
                                    <a href="/google/upload?type=<?= $key ?>&val=<?= $type ?>">Upload
                                        file</a>
                                    |
                                    <a href="/google/index?type=<?= $key ?>&val=<?= $type ?>">View
                                        files</a>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
</div>
<!-- END: Basic Accordion -->
</div>
</div>
<!-- END: Content -->