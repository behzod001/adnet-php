<?php


use app\helpers\FileUpload;
use app\models\GoogleReports;
use app\models\ReportTypes;
use helpers\Alert;

$model = new GoogleReports();

$val = "";
$type = "";
if (isset($_GET['val'])) {
    $val = "&val=" . $_GET['val'];
}
if (isset($_GET['type'])) {
    $type = "type=" . $_GET['type'];
}
if (isset( $_POST['Report'])) {
    if (isset($_POST['Report']['main'])) {
        $val = "&val=" . $_POST['Report']['main'];
    }
    if (isset($_POST['Report']['type'])) {
        $type = "type=" . $_POST['Report']['type'];
    }

    $post = $_POST['Report'];
    $post['type'] = (int)$_POST['Report']['type'];
    if (!isset($_FILES['Report'])) {
        $error = ["file" => "File must not be null"];
    }
    $tmpFile = $_FILES["Report"]['tmp_name']['file'];

    $fileUploader = new FileUpload($tmpFile);
    if ($fileUploader->uploadFile("/g_reports/", basename($_FILES['Report']['name']['file']))) {
        $post[GoogleReports::FILE] = $fileUploader->getUploadedPath();

    }
    $post[GoogleReports::USER] = (int)$_SESSION['identity']->id;
    $model->load($post);

    if ($model->validate() && $model->create()) {
        Alert::success(' File successfully uploaded. <a href="/google/index?' . $type  . $val . '&view=last"> Click here to view </a> ');
    } else {
        Alert::error(' Something went wrong. file not uploaded.');
    }


}


?>


<div class="intro-y flex items-center mt-8">
    <h2 class="text-lg font-medium mr-auto">
        Upload your report
    </h2>
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
        <div class="hidden md:block mx-auto text-gray-600"></div>
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <a href="/google/types" class="button text-white bg-theme-1 shadow-md mr-2">Go back</a>
        </div>
    </div>
</div>


<div class="grid grid-cols-12 gap-6 mt-5 box">

    <div class="intro-y col-span-12 lg:col-span-6 ">
        <div class="intro-y  p-5">

            <form action="/google/upload?<?= $type ?><?= $val ?>" method="post" enctype="multipart/form-data">

                <div class="mb-3">
                    <label for="fileName" class="form-label">File name</label>
                    <input type="text" name="Report[name]" class="input w-full border mt-2" id="fileName">
                </div>
                <?php if (!isset($_GET['type'])) { ?>
                    <div class="mb-3">
                        <label for="fileName" class="form-label">Report main type</label>
                        <div class="mt-2">
                            <select class="select2 w-full" name="Report[main]">
                                <?php foreach (ReportTypes::getTypes() as $type => $items) { ?>
                                    <option value="<?= $type ?>"><?= $type ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                <?php } ?>
                <div class="mb-3">
                    <label for="fileName" class="form-label">Report type</label>

                    <div class="mt-2">
                        <select class="select2 w-full" name="Report[type]">
                            <?php foreach (ReportTypes::getTypes() as $typeKey => $items) { ?>
                                <optgroup label="<?= $typeKey ?>">
                                    <?php foreach ($items as $key => $val) { ?>
                                        <option <?= $key == (int)$type ? " selected " : ""; ?>
                                                value="<?= $key ?>"><?= $val ?></option>
                                    <?php } ?>
                                </optgroup>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="file" class="form-label">File</label>
                    <input type="file" name="Report[file]" class="input w-full border mt-2" id="file">
                </div>
                <div class="text-right mt-5">
                    <button type="reset" class="button w-24 border text-gray-700 mr-1">Cancel</button>
                    <button type="submit" class="button w-24 bg-theme-1 text-white">Upload file</button>
                </div>
            </form>

        </div>
    </div>
</div>
