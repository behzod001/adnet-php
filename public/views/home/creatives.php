<div class="grid grid-cols-12 gap-6 mt-8">
    <?php require_once "help_menu.php" ?>
    <div class="col-span-12 lg:col-span-9 xxl:col-span-10">
        <!-- BEGIN: File Manager Filter -->
        <div class="intro-y flex flex-col-reverse sm:flex-row items-center">
            <div class="w-full sm:w-auto relative mr-auto mt-3 sm:mt-0">
                <i class="w-4 h-4 absolute my-auto inset-y-0 ml-3 left-0 z-10 text-gray-700" data-feather="search"></i>
                <input type="text" class="input w-full sm:w-64 box px-10 text-gray-700 placeholder-theme-13"
                       placeholder="Search files">
                <div class="inbox-filter dropdown absolute inset-y-0 mr-3 right-0 flex items-center">
                    <i class="dropdown-toggle w-4 h-4 cursor-pointer text-gray-700" data-feather="chevron-down"></i>
                    <div class="inbox-filter__dropdown-box dropdown-box mt-10 absolute top-0 left-0 z-20">
                        <div class="dropdown-box__content box p-5">
                            <div class="grid grid-cols-12 gap-4 row-gap-3">
                                <div class="col-span-6">
                                    <div class="text-xs">File Name</div>
                                    <input type="text" class="input w-full border mt-2 flex-1"
                                           placeholder="Type the file name">
                                </div>
                                <div class="col-span-6">
                                    <div class="text-xs">Shared With</div>
                                    <input type="text" class="input w-full border mt-2 flex-1"
                                           placeholder="example@gmail.com">
                                </div>
                                <div class="col-span-6">
                                    <div class="text-xs">Created At</div>
                                    <input type="text" class="input w-full border mt-2 flex-1"
                                           placeholder="Important Meeting">
                                </div>
                                <div class="col-span-6">
                                    <div class="text-xs">Size</div>
                                    <select class="input w-full border mt-2 flex-1">
                                        <option>10</option>
                                        <option>25</option>
                                        <option>35</option>
                                        <option>50</option>
                                    </select>
                                </div>
                                <div class="col-span-12 flex items-center mt-3">
                                    <button class="button w-32 justify-center block bg-gray-200 text-gray-600 ml-auto">
                                        Create Filter
                                    </button>
                                    <button class="button w-32 justify-center block bg-theme-1 text-white ml-2">Search
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-full sm:w-auto flex">
                <button class="button text-white bg-theme-1 shadow-md mr-2">Do you have questions?</button>
                <div class="dropdown relative">
                    <button class="dropdown-toggle button px-2 box text-gray-700">
                        <span class="w-5 h-5 flex items-center justify-center"> <i class="w-4 h-4"
                                                                                   data-feather="plus"></i> </span>
                    </button>
                    <div class="dropdown-box mt-10 absolute w-40 top-0 right-0 z-20">
                        <div class="dropdown-box__content box p-2">
                            <a href=""
                               class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                <i data-feather="file" class="w-4 h-4 mr-2"></i> Ask question </a>
                            <a href=""
                               class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                <i data-feather="settings" class="w-4 h-4 mr-2"></i> FAQ </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END: File Manager Filter -->
        <!-- BEGIN: Directory & Files -->
        <div class="intro-y grid grid-cols-1 gap-3 sm:gap-6 mt-5 ">
            <div class="col-span-9 border-l box py-10 px-10">
                <h1 class="intro-y text-2xl font-medium pb-8 mb-10 border-b border-gray-800">
                    Getting Started
                </h1>
                <h2 class="intro-y text-xl font-medium pb-5 mb-5 border-b border-gray-800">
                    Introduction
                </h2>
                <div class="intro-y leading-relaxed">
                    <p class="mb-3">AdNet gives you everything you need to create your Admin Dashboard. There are a few things like colors, components, and layouts that carefully chosen and designed for every single page. The template are made with TailwindCSS framework which has many usefull utility classes and great SASS support.</p>
                    <p>The download files will contains 3 directories, the project source files and documentation to help you starting to use this template. It is created to save your time so you can take care the other parts. Every components are completely modular and written with SASS.</p>
                    <p>Thank you for purchasing this template. Congratulations on using AdNet, Left4code Team.</p>
                </div>
                <h2 class="intro-y text-xl font-medium pb-5 mb-5 border-b border-gray-800 mt-10">
                    Features
                </h2>
                <div class="w-full grid grid-cols-12 gap-5">
                    <div class="intro-y col-span-6 sm:col-span-3 cursor-pointer">
                        <div class="zoom-in flex flex-col items-center p-5 rounded-md box">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-airplay mr-3"><path d="M5 17H4a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2h-1"></path><polygon points="12 15 17 21 7 21 12 15"></polygon></svg>
                            <div class="font-medium mt-3">Fully Responsive</div>
                        </div>
                    </div>
                    <div class="intro-y col-span-6 sm:col-span-3 cursor-pointer">
                        <div class="zoom-in flex flex-col items-center p-5 rounded-md box">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-tool mr-3"><path d="M14.7 6.3a1 1 0 0 0 0 1.4l1.6 1.6a1 1 0 0 0 1.4 0l3.77-3.77a6 6 0 0 1-7.94 7.94l-6.91 6.91a2.12 2.12 0 0 1-3-3l6.91-6.91a6 6 0 0 1 7.94-7.94l-3.76 3.76z"></path></svg>
                            <div class="font-medium mt-3">Built-in Tools</div>
                        </div>
                    </div>
                    <div class="intro-y col-span-6 sm:col-span-3 cursor-pointer">
                        <div class="zoom-in flex flex-col items-center p-5 rounded-md box">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-sidebar mr-3"><rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect><line x1="9" y1="3" x2="9" y2="21"></line></svg>
                            <div class="font-medium mt-3">Functional Dashboard</div>
                        </div>
                    </div>
                    <div class="intro-y col-span-6 sm:col-span-3 cursor-pointer">
                        <div class="zoom-in flex flex-col items-center p-5 rounded-md box">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-package mr-3"><line x1="16.5" y1="9.4" x2="7.5" y2="4.21"></line><path d="M21 16V8a2 2 0 0 0-1-1.73l-7-4a2 2 0 0 0-2 0l-7 4A2 2 0 0 0 3 8v8a2 2 0 0 0 1 1.73l7 4a2 2 0 0 0 2 0l7-4A2 2 0 0 0 21 16z"></path><polyline points="3.27 6.96 12 12.01 20.73 6.96"></polyline><line x1="12" y1="22.08" x2="12" y2="12"></line></svg>
                            <div class="font-medium mt-3">Apps Preview</div>
                        </div>
                    </div>
                    <div class="intro-y col-span-6 sm:col-span-3 cursor-pointer">
                        <div class="zoom-in flex flex-col items-center p-5 rounded-md box">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users mr-3"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>
                            <div class="font-medium mt-3">Users Preview</div>
                        </div>
                    </div>
                    <div class="intro-y col-span-6 sm:col-span-3 cursor-pointer">
                        <div class="zoom-in flex flex-col items-center p-5 rounded-md box">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-inbox mr-3"><polyline points="22 12 16 12 14 15 10 15 8 12 2 12"></polyline><path d="M5.45 5.11L2 12v6a2 2 0 0 0 2 2h16a2 2 0 0 0 2-2v-6l-3.45-6.89A2 2 0 0 0 16.76 4H7.24a2 2 0 0 0-1.79 1.11z"></path></svg>
                            <div class="font-medium mt-3">Easy to Customize</div>
                        </div>
                    </div>
                    <div class="intro-y col-span-6 sm:col-span-3 cursor-pointer">
                        <div class="zoom-in flex flex-col items-center p-5 rounded-md box">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text mr-3"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                            <div class="font-medium mt-3">Useful Pages</div>
                        </div>
                    </div>
                    <div class="intro-y col-span-6 sm:col-span-3 cursor-pointer">
                        <div class="zoom-in flex flex-col items-center p-5 rounded-md box">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-fast-forward mr-3"><polygon points="13 19 22 12 13 5 13 19"></polygon><polygon points="2 19 11 12 2 5 2 19"></polygon></svg>
                            <div class="font-medium mt-3">Fast Performance</div>
                        </div>
                    </div>
                    <div class="intro-y col-span-6 sm:col-span-3 cursor-pointer">
                        <div class="zoom-in flex flex-col items-center p-5 rounded-md box">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-box mr-3"><path d="M21 16V8a2 2 0 0 0-1-1.73l-7-4a2 2 0 0 0-2 0l-7 4A2 2 0 0 0 3 8v8a2 2 0 0 0 1 1.73l7 4a2 2 0 0 0 2 0l7-4A2 2 0 0 0 21 16z"></path><polyline points="3.27 6.96 12 12.01 20.73 6.96"></polyline><line x1="12" y1="22.08" x2="12" y2="12"></line></svg>
                            <div class="font-medium mt-3">Utility Based</div>
                        </div>
                    </div>
                    <div class="intro-y col-span-6 sm:col-span-3 cursor-pointer">
                        <div class="zoom-in flex flex-col items-center p-5 rounded-md box">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-code mr-3"><polyline points="16 18 22 12 16 6"></polyline><polyline points="8 6 2 12 8 18"></polyline></svg>
                            <div class="font-medium mt-3">Clean Code &amp; Structure</div>
                        </div>
                    </div>
                    <div class="intro-y col-span-6 sm:col-span-3 cursor-pointer">
                        <div class="zoom-in flex flex-col items-center p-5 rounded-md box">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bold mr-3"><path d="M6 4h8a4 4 0 0 1 4 4 4 4 0 0 1-4 4H6z"></path><path d="M6 12h9a4 4 0 0 1 4 4 4 4 0 0 1-4 4H6z"></path></svg>
                            <div class="font-medium mt-3">Coded with SASS</div>
                        </div>
                    </div>
                    <div class="intro-y col-span-6 sm:col-span-3 cursor-pointer">
                        <div class="zoom-in flex flex-col items-center p-5 rounded-md box">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-clock mr-3"><circle cx="12" cy="12" r="10"></circle><polyline points="12 6 12 12 16 14"></polyline></svg>
                            <div class="font-medium mt-3">Free Lifetime Updates</div>
                        </div>
                    </div>
                </div>
                <h2 class="intro-y text-xl font-medium pb-5 mb-5 border-b border-gray-800 mt-10">
                    Browser Support
                </h2>
                <div class="w-full grid grid-cols-12 gap-5">
                    <div class="intro-y col-span-6 sm:col-span-3 cursor-pointer">
                        <div class="zoom-in flex flex-col items-center p-5 rounded-md box">
                            <img class="w-10" alt="Browser" src="/resources/Documentation/dist/images/chrome.png">
                            <div class="font-medium mt-3">Chrome</div>
                        </div>
                    </div>
                    <div class="intro-y col-span-6 sm:col-span-3 cursor-pointer">
                        <div class="zoom-in flex flex-col items-center p-5 rounded-md box">
                            <img class="w-10" alt="Browser" src="/resources/Documentation/dist/images/edge.png">
                            <div class="font-medium mt-3">Edge</div>
                        </div>
                    </div>
                    <div class="intro-y col-span-6 sm:col-span-3 cursor-pointer">
                        <div class="zoom-in flex flex-col items-center p-5 rounded-md box">
                            <img class="w-10" alt="Browser" src="/resources/Documentation/dist/images/mozilla.png">
                            <div class="font-medium mt-3">Mozilla</div>
                        </div>
                    </div>
                    <div class="intro-y col-span-6 sm:col-span-3 cursor-pointer">
                        <div class="zoom-in flex flex-col items-center p-5 rounded-md box">
                            <img class="w-10" alt="Browser" src="/resources/Documentation/dist/images/opera.png">
                            <div class="font-medium mt-3">Opera</div>
                        </div>
                    </div>
                    <div class="intro-y col-span-6 sm:col-span-3 cursor-pointer">
                        <div class="zoom-in flex flex-col items-center p-5 rounded-md box">
                            <img class="w-10" alt="Browser" src="/resources/Documentation/dist/images/safari.png">
                            <div class="font-medium mt-3">Safari</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END: Directory & Files -->

    </div>
</div>