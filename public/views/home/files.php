<div class="grid grid-cols-12 gap-6 mt-8">
    <?php require_once "help_menu.php" ?>
    <div class="col-span-12 lg:col-span-9 xxl:col-span-10">
        <!-- BEGIN: File Manager Filter -->
        <div class="intro-y flex flex-col-reverse sm:flex-row items-center">
            <div class="w-full sm:w-auto relative mr-auto mt-3 sm:mt-0">
                <i class="w-4 h-4 absolute my-auto inset-y-0 ml-3 left-0 z-10 text-gray-700" data-feather="search"></i>
                <input type="text" class="input w-full sm:w-64 box px-10 text-gray-700 placeholder-theme-13"
                       placeholder="Search files">
                <div class="inbox-filter dropdown absolute inset-y-0 mr-3 right-0 flex items-center">
                    <i class="dropdown-toggle w-4 h-4 cursor-pointer text-gray-700" data-feather="chevron-down"></i>
                    <div class="inbox-filter__dropdown-box dropdown-box mt-10 absolute top-0 left-0 z-20">
                        <div class="dropdown-box__content box p-5">
                            <div class="grid grid-cols-12 gap-4 row-gap-3">
                                <div class="col-span-6">
                                    <div class="text-xs">File Name</div>
                                    <input type="text" class="input w-full border mt-2 flex-1"
                                           placeholder="Type the file name">
                                </div>
                                <div class="col-span-6">
                                    <div class="text-xs">Shared With</div>
                                    <input type="text" class="input w-full border mt-2 flex-1"
                                           placeholder="example@gmail.com">
                                </div>
                                <div class="col-span-6">
                                    <div class="text-xs">Created At</div>
                                    <input type="text" class="input w-full border mt-2 flex-1"
                                           placeholder="Important Meeting">
                                </div>
                                <div class="col-span-6">
                                    <div class="text-xs">Size</div>
                                    <select class="input w-full border mt-2 flex-1">
                                        <option>10</option>
                                        <option>25</option>
                                        <option>35</option>
                                        <option>50</option>
                                    </select>
                                </div>
                                <div class="col-span-12 flex items-center mt-3">
                                    <button class="button w-32 justify-center block bg-gray-200 text-gray-600 ml-auto">
                                        Create Filter
                                    </button>
                                    <button class="button w-32 justify-center block bg-theme-1 text-white ml-2">Search
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-full sm:w-auto flex">
                <button class="button text-white bg-theme-1 shadow-md mr-2">Do you have questions?</button>
                <div class="dropdown relative">
                    <button class="dropdown-toggle button px-2 box text-gray-700">
                        <span class="w-5 h-5 flex items-center justify-center"> <i class="w-4 h-4"
                                                                                   data-feather="plus"></i> </span>
                    </button>
                    <div class="dropdown-box mt-10 absolute w-40 top-0 right-0 z-20">
                        <div class="dropdown-box__content box p-2">
                            <a href=""
                               class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                <i data-feather="file" class="w-4 h-4 mr-2"></i> Ask question </a>
                            <a href=""
                               class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                <i data-feather="settings" class="w-4 h-4 mr-2"></i> FAQ </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END: File Manager Filter -->
        <!-- BEGIN: Directory & Files -->
        <div class="intro-y grid grid-cols-12 gap-3 sm:gap-6 mt-5">

            <div class="intro-y col-span-6 sm:col-span-4 md:col-span-3 xxl:col-span-2">
                <div class="file box rounded-md px-5 pt-8 pb-5 px-3 sm:px-5 relative zoom-in">
                    <div class="absolute left-0 top-0 mt-3 ml-3">
                        <input class="input border border-gray-500" type="checkbox">
                    </div>
                    <a href="" class="w-3/5 file__icon file__icon--image mx-auto">
                        <div class="file__icon--image__preview image-fit">
                            <img alt="AdNet Grow your business with us" src="/resources/dist/images/preview-1.jpg">
                        </div>
                    </a>
                    <a href="" class="block font-medium mt-4 text-center truncate">preview-1.jpg</a>
                    <div class="text-gray-600 text-xs text-center">1.4 MB</div>
                    <div class="absolute top-0 right-0 mr-2 mt-2 dropdown ml-auto">
                        <a class="dropdown-toggle w-5 h-5 block" href="javascript:"> <i data-feather="more-vertical"
                                                                                        class="w-5 h-5 text-gray-500"></i>
                        </a>
                        <div class="dropdown-box mt-5 absolute w-40 top-0 right-0 z-10">
                            <div class="dropdown-box__content box p-2">
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                    <i data-feather="users" class="w-4 h-4 mr-2"></i> Share File </a>
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                    <i data-feather="trash" class="w-4 h-4 mr-2"></i> Delete </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="intro-y col-span-6 sm:col-span-4 md:col-span-3 xxl:col-span-2">
                <div class="file box rounded-md px-5 pt-8 pb-5 px-3 sm:px-5 relative zoom-in">
                    <div class="absolute left-0 top-0 mt-3 ml-3">
                        <input class="input border border-gray-500" type="checkbox">
                    </div>
                    <a href="" class="w-3/5 file__icon file__icon--file mx-auto">
                        <div class="file__icon__file-name">MP4</div>
                    </a>
                    <a href="" class="block font-medium mt-4 text-center truncate">Create Account - Ashes.mp4</a>
                    <div class="text-gray-600 text-xs text-center">20 MB</div>
                    <div class="absolute top-0 right-0 mr-2 mt-2 dropdown ml-auto">
                        <a class="dropdown-toggle w-5 h-5 block" href="javascript:"> <i data-feather="more-vertical"
                                                                                        class="w-5 h-5 text-gray-500"></i>
                        </a>
                        <div class="dropdown-box mt-5 absolute w-40 top-0 right-0 z-10">
                            <div class="dropdown-box__content box p-2">
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                    <i data-feather="users" class="w-4 h-4 mr-2"></i> Share File </a>
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                    <i data-feather="trash" class="w-4 h-4 mr-2"></i> Delete </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="intro-y col-span-6 sm:col-span-4 md:col-span-3 xxl:col-span-2">
                <div class="file box rounded-md px-5 pt-8 pb-5 px-3 sm:px-5 relative zoom-in">
                    <div class="absolute left-0 top-0 mt-3 ml-3">
                        <input class="input border border-gray-500" type="checkbox">
                    </div>
                    <a href="" class="w-3/5 file__icon file__icon--image mx-auto">
                        <div class="file__icon--image__preview image-fit">
                            <img alt="AdNet Grow your business with us"
                                 src="/resources/Documentation/dist/images/chrome.png">
                        </div>
                    </a>
                    <a href="" class="block font-medium mt-4 text-center truncate">Create campaign</a>
                    <div class="text-gray-600 text-xs text-center">1 MB</div>
                    <div class="absolute top-0 right-0 mr-2 mt-2 dropdown ml-auto">
                        <a class="dropdown-toggle w-5 h-5 block" href="javascript:"> <i data-feather="more-vertical"
                                                                                        class="w-5 h-5 text-gray-500"></i>
                        </a>
                        <div class="dropdown-box mt-5 absolute w-40 top-0 right-0 z-10">
                            <div class="dropdown-box__content box p-2">
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                    <i data-feather="users" class="w-4 h-4 mr-2"></i> Share File </a>
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                    <i data-feather="trash" class="w-4 h-4 mr-2"></i> Delete </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="intro-y col-span-6 sm:col-span-4 md:col-span-3 xxl:col-span-2">
                <div class="file box rounded-md px-5 pt-8 pb-5 px-3 sm:px-5 relative zoom-in">
                    <div class="absolute left-0 top-0 mt-3 ml-3">
                        <input class="input border border-gray-500" type="checkbox">
                    </div>
                    <a href="" class="w-3/5 file__icon file__icon--image mx-auto">
                        <div class="file__icon--image__preview image-fit">
                            <img alt="AdNet Grow your business with us"
                                 src="/resources/Documentation/dist/images/logo.svg">
                        </div>
                    </a>
                    <a href="" class="block font-medium mt-4 text-center truncate">Remove ad group</a>
                    <div class="text-gray-600 text-xs text-center">1.2 MB</div>
                    <div class="absolute top-0 right-0 mr-2 mt-2 dropdown ml-auto">
                        <a class="dropdown-toggle w-5 h-5 block" href="javascript:"> <i data-feather="more-vertical"
                                                                                        class="w-5 h-5 text-gray-500"></i>
                        </a>
                        <div class="dropdown-box mt-5 absolute w-40 top-0 right-0 z-10">
                            <div class="dropdown-box__content box p-2">
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                    <i data-feather="users" class="w-4 h-4 mr-2"></i> Share File </a>
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                    <i data-feather="trash" class="w-4 h-4 mr-2"></i> Delete </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="intro-y col-span-6 sm:col-span-4 md:col-span-3 xxl:col-span-2">
                <div class="file box rounded-md px-5 pt-8 pb-5 px-3 sm:px-5 relative zoom-in">
                    <div class="absolute left-0 top-0 mt-3 ml-3">
                        <input class="input border border-gray-500" type="checkbox">
                    </div>
                    <a href="" class="w-3/5 file__icon file__icon--file mx-auto">
                        <div class="file__icon__file-name">MP4</div>
                    </a>
                    <a href="" class="block font-medium mt-4 text-center truncate">Facebook - AdSet.mp4</a>
                    <div class="text-gray-600 text-xs text-center">20 MB</div>
                    <div class="absolute top-0 right-0 mr-2 mt-2 dropdown ml-auto">
                        <a class="dropdown-toggle w-5 h-5 block" href="javascript:"> <i data-feather="more-vertical"
                                                                                        class="w-5 h-5 text-gray-500"></i>
                        </a>
                        <div class="dropdown-box mt-5 absolute w-40 top-0 right-0 z-10">
                            <div class="dropdown-box__content box p-2">
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                    <i data-feather="users" class="w-4 h-4 mr-2"></i> Share File </a>
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                    <i data-feather="trash" class="w-4 h-4 mr-2"></i> Delete </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="intro-y col-span-6 sm:col-span-4 md:col-span-3 xxl:col-span-2">
                <div class="file box rounded-md px-5 pt-8 pb-5 px-3 sm:px-5 relative zoom-in">
                    <div class="absolute left-0 top-0 mt-3 ml-3">
                        <input class="input border border-gray-500" type="checkbox">
                    </div>
                    <a href="" class="w-3/5 file__icon file__icon--empty-directory mx-auto"></a> <a href=""
                                                                                                    class="block font-medium mt-4 text-center truncate">Documentation</a>
                    <div class="text-gray-600 text-xs text-center">4 MB</div>
                    <div class="absolute top-0 right-0 mr-2 mt-2 dropdown ml-auto">
                        <a class="dropdown-toggle w-5 h-5 block" href="javascript:"> <i data-feather="more-vertical"
                                                                                        class="w-5 h-5 text-gray-500"></i>
                        </a>
                        <div class="dropdown-box mt-5 absolute w-40 top-0 right-0 z-10">
                            <div class="dropdown-box__content box p-2">
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                    <i data-feather="users" class="w-4 h-4 mr-2"></i> Share File </a>
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                    <i data-feather="trash" class="w-4 h-4 mr-2"></i> Delete </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="intro-y col-span-6 sm:col-span-4 md:col-span-3 xxl:col-span-2">
                <div class="file box rounded-md px-5 pt-8 pb-5 px-3 sm:px-5 relative zoom-in">
                    <div class="absolute left-0 top-0 mt-3 ml-3">
                        <input class="input border border-gray-500" type="checkbox">
                    </div>
                    <a href="" class="w-3/5 file__icon file__icon--directory mx-auto"></a> <a href=""
                                                                                              class="block font-medium mt-4 text-center truncate">Yandex
                        Direct</a>
                    <div class="text-gray-600 text-xs text-center">112 GB</div>
                    <div class="absolute top-0 right-0 mr-2 mt-2 dropdown ml-auto">
                        <a class="dropdown-toggle w-5 h-5 block" href="javascript:"> <i data-feather="more-vertical"
                                                                                        class="w-5 h-5 text-gray-500"></i>
                        </a>
                        <div class="dropdown-box mt-5 absolute w-40 top-0 right-0 z-10">
                            <div class="dropdown-box__content box p-2">
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                    <i data-feather="users" class="w-4 h-4 mr-2"></i> Share File </a>
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                    <i data-feather="trash" class="w-4 h-4 mr-2"></i> Delete </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="intro-y col-span-6 sm:col-span-4 md:col-span-3 xxl:col-span-2">
                <div class="file box rounded-md px-5 pt-8 pb-5 px-3 sm:px-5 relative zoom-in">
                    <div class="absolute left-0 top-0 mt-3 ml-3">
                        <input class="input border border-gray-500" type="checkbox">
                    </div>
                    <a href="" class="w-3/5 file__icon file__icon--empty-directory mx-auto"></a> <a href=""
                                                                                                    class="block font-medium mt-4 text-center truncate">Control
                        campaign</a>
                    <div class="text-gray-600 text-xs text-center">120 MB</div>
                    <div class="absolute top-0 right-0 mr-2 mt-2 dropdown ml-auto">
                        <a class="dropdown-toggle w-5 h-5 block" href="javascript:"> <i data-feather="more-vertical"
                                                                                        class="w-5 h-5 text-gray-500"></i>
                        </a>
                        <div class="dropdown-box mt-5 absolute w-40 top-0 right-0 z-10">
                            <div class="dropdown-box__content box p-2">
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                    <i data-feather="users" class="w-4 h-4 mr-2"></i> Share File </a>
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                    <i data-feather="trash" class="w-4 h-4 mr-2"></i> Delete </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="intro-y col-span-6 sm:col-span-4 md:col-span-3 xxl:col-span-2">
                <div class="file box rounded-md px-5 pt-8 pb-5 px-3 sm:px-5 relative zoom-in">
                    <div class="absolute left-0 top-0 mt-3 ml-3">
                        <input class="input border border-gray-500" type="checkbox">
                    </div>
                    <a href="" class="w-3/5 file__icon file__icon--image mx-auto">
                        <div class="file__icon--image__preview image-fit">
                            <img alt="AdNet Grow your business with us" src="/resources/dist/images/preview-3.jpg">
                        </div>
                    </a>
                    <a href="" class="block font-medium mt-4 text-center truncate">Ad images.jpg</a>
                    <div class="text-gray-600 text-xs text-center">1.2 MB</div>
                    <div class="absolute top-0 right-0 mr-2 mt-2 dropdown ml-auto">
                        <a class="dropdown-toggle w-5 h-5 block" href="javascript:"> <i data-feather="more-vertical"
                                                                                        class="w-5 h-5 text-gray-500"></i>
                        </a>
                        <div class="dropdown-box mt-5 absolute w-40 top-0 right-0 z-10">
                            <div class="dropdown-box__content box p-2">
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                    <i data-feather="users" class="w-4 h-4 mr-2"></i> Share File </a>
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                    <i data-feather="trash" class="w-4 h-4 mr-2"></i> Delete </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="intro-y col-span-6 sm:col-span-4 md:col-span-3 xxl:col-span-2">
                <div class="file box rounded-md px-5 pt-8 pb-5 px-3 sm:px-5 relative zoom-in">
                    <div class="absolute left-0 top-0 mt-3 ml-3">
                        <input class="input border border-gray-500" type="checkbox">
                    </div>
                    <a href="" class="w-3/5 file__icon file__icon--empty-directory mx-auto"></a> <a href=""
                                                                                                    class="block font-medium mt-4 text-center truncate">Documentation
                        Creatives</a>
                    <div class="text-gray-600 text-xs text-center">4 MB</div>
                    <div class="absolute top-0 right-0 mr-2 mt-2 dropdown ml-auto">
                        <a class="dropdown-toggle w-5 h-5 block" href="javascript:"> <i data-feather="more-vertical"
                                                                                        class="w-5 h-5 text-gray-500"></i>
                        </a>
                        <div class="dropdown-box mt-5 absolute w-40 top-0 right-0 z-10">
                            <div class="dropdown-box__content box p-2">
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                    <i data-feather="users" class="w-4 h-4 mr-2"></i> Share File </a>
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                    <i data-feather="trash" class="w-4 h-4 mr-2"></i> Delete </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="intro-y col-span-6 sm:col-span-4 md:col-span-3 xxl:col-span-2">
                <div class="file box rounded-md px-5 pt-8 pb-5 px-3 sm:px-5 relative zoom-in">
                    <div class="absolute left-0 top-0 mt-3 ml-3">
                        <input class="input border border-gray-500" type="checkbox">
                    </div>
                    <a href="" class="w-3/5 file__icon file__icon--directory mx-auto"></a> <a href=""
                                                                                              class="block font-medium mt-4 text-center truncate">Files
                        Repository</a>
                    <div class="text-gray-600 text-xs text-center">20 KB</div>
                    <div class="absolute top-0 right-0 mr-2 mt-2 dropdown ml-auto">
                        <a class="dropdown-toggle w-5 h-5 block" href="javascript:"> <i data-feather="more-vertical"
                                                                                        class="w-5 h-5 text-gray-500"></i>
                        </a>
                        <div class="dropdown-box mt-5 absolute w-40 top-0 right-0 z-10">
                            <div class="dropdown-box__content box p-2">
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                    <i data-feather="users" class="w-4 h-4 mr-2"></i> Share File </a>
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                    <i data-feather="trash" class="w-4 h-4 mr-2"></i> Delete </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="intro-y col-span-6 sm:col-span-4 md:col-span-3 xxl:col-span-2">
                <div class="file box rounded-md px-5 pt-8 pb-5 px-3 sm:px-5 relative zoom-in">
                    <div class="absolute left-0 top-0 mt-3 ml-3">
                        <input class="input border border-gray-500" type="checkbox">
                    </div>
                    <a href="" class="w-3/5 file__icon file__icon--empty-directory mx-auto"></a> <a href=""
                                                                                                    class="block font-medium mt-4 text-center truncate">Ad
                        existing account</a>
                    <div class="text-gray-600 text-xs text-center">120 MB</div>
                    <div class="absolute top-0 right-0 mr-2 mt-2 dropdown ml-auto">
                        <a class="dropdown-toggle w-5 h-5 block" href="javascript:"> <i data-feather="more-vertical"
                                                                                        class="w-5 h-5 text-gray-500"></i>
                        </a>
                        <div class="dropdown-box mt-5 absolute w-40 top-0 right-0 z-10">
                            <div class="dropdown-box__content box p-2">
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                    <i data-feather="users" class="w-4 h-4 mr-2"></i> Share File </a>
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                    <i data-feather="trash" class="w-4 h-4 mr-2"></i> Delete </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="intro-y col-span-6 sm:col-span-4 md:col-span-3 xxl:col-span-2">
                <div class="file box rounded-md px-5 pt-8 pb-5 px-3 sm:px-5 relative zoom-in">
                    <div class="absolute left-0 top-0 mt-3 ml-3">
                        <input class="input border border-gray-500" type="checkbox">
                    </div>
                    <a href="" class="w-3/5 file__icon file__icon--image mx-auto">
                        <div class="file__icon--image__preview image-fit">
                            <img alt="AdNet Grow your business with us" src="/resources/dist/images/preview-2.jpg">
                        </div>
                    </a>
                    <a href="" class="block font-medium mt-4 text-center truncate">preview-2.jpg</a>
                    <div class="text-gray-600 text-xs text-center">1 MB</div>
                    <div class="absolute top-0 right-0 mr-2 mt-2 dropdown ml-auto">
                        <a class="dropdown-toggle w-5 h-5 block" href="javascript:"> <i data-feather="more-vertical"
                                                                                        class="w-5 h-5 text-gray-500"></i>
                        </a>
                        <div class="dropdown-box mt-5 absolute w-40 top-0 right-0 z-10">
                            <div class="dropdown-box__content box p-2">
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                    <i data-feather="users" class="w-4 h-4 mr-2"></i> Share File </a>
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                    <i data-feather="trash" class="w-4 h-4 mr-2"></i> Delete </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="intro-y col-span-6 sm:col-span-4 md:col-span-3 xxl:col-span-2">
                <div class="file box rounded-md px-5 pt-8 pb-5 px-3 sm:px-5 relative zoom-in">
                    <div class="absolute left-0 top-0 mt-3 ml-3">
                        <input class="input border border-gray-500" type="checkbox">
                    </div>
                    <a href="" class="w-3/5 file__icon file__icon--image mx-auto">
                        <div class="file__icon--image__preview image-fit">
                            <img alt="AdNet Grow your business with us" src="/resources/dist/images/preview-1.jpg">
                        </div>
                    </a>
                    <a href="" class="block font-medium mt-4 text-center truncate">preview-1.jpg</a>
                    <div class="text-gray-600 text-xs text-center">1.4 MB</div>
                    <div class="absolute top-0 right-0 mr-2 mt-2 dropdown ml-auto">
                        <a class="dropdown-toggle w-5 h-5 block" href="javascript:"> <i data-feather="more-vertical"
                                                                                        class="w-5 h-5 text-gray-500"></i>
                        </a>
                        <div class="dropdown-box mt-5 absolute w-40 top-0 right-0 z-10">
                            <div class="dropdown-box__content box p-2">
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                    <i data-feather="users" class="w-4 h-4 mr-2"></i> Share File </a>
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                    <i data-feather="trash" class="w-4 h-4 mr-2"></i> Delete </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="intro-y col-span-6 sm:col-span-4 md:col-span-3 xxl:col-span-2">
                <div class="file box rounded-md px-5 pt-8 pb-5 px-3 sm:px-5 relative zoom-in">
                    <div class="absolute left-0 top-0 mt-3 ml-3">
                        <input class="input border border-gray-500" type="checkbox">
                    </div>
                    <a href="" class="w-3/5 file__icon file__icon--image mx-auto">
                        <div class="file__icon--image__preview image-fit">
                            <img alt="AdNet Grow your business with us" src="/resources/dist/images/preview-4.jpg">
                        </div>
                    </a>
                    <a href="" class="block font-medium mt-4 text-center truncate">preview-4.jpg</a>
                    <div class="text-gray-600 text-xs text-center">1 MB</div>
                    <div class="absolute top-0 right-0 mr-2 mt-2 dropdown ml-auto">
                        <a class="dropdown-toggle w-5 h-5 block" href="javascript:"> <i data-feather="more-vertical"
                                                                                        class="w-5 h-5 text-gray-500"></i>
                        </a>
                        <div class="dropdown-box mt-5 absolute w-40 top-0 right-0 z-10">
                            <div class="dropdown-box__content box p-2">
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                    <i data-feather="users" class="w-4 h-4 mr-2"></i> Share File </a>
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                    <i data-feather="trash" class="w-4 h-4 mr-2"></i> Delete </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="intro-y col-span-6 sm:col-span-4 md:col-span-3 xxl:col-span-2">
                <div class="file box rounded-md px-5 pt-8 pb-5 px-3 sm:px-5 relative zoom-in">
                    <div class="absolute left-0 top-0 mt-3 ml-3">
                        <input class="input border border-gray-500" type="checkbox">
                    </div>
                    <a href="" class="w-3/5 file__icon file__icon--empty-directory mx-auto"></a> <a href=""
                                                                                                    class="block font-medium mt-4 text-center truncate">Documentation</a>
                    <div class="text-gray-600 text-xs text-center">4 MB</div>
                    <div class="absolute top-0 right-0 mr-2 mt-2 dropdown ml-auto">
                        <a class="dropdown-toggle w-5 h-5 block" href="javascript:"> <i data-feather="more-vertical"
                                                                                        class="w-5 h-5 text-gray-500"></i>
                        </a>
                        <div class="dropdown-box mt-5 absolute w-40 top-0 right-0 z-10">
                            <div class="dropdown-box__content box p-2">
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                    <i data-feather="users" class="w-4 h-4 mr-2"></i> Share File </a>
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                    <i data-feather="trash" class="w-4 h-4 mr-2"></i> Delete </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="intro-y col-span-6 sm:col-span-4 md:col-span-3 xxl:col-span-2">
                <div class="file box rounded-md px-5 pt-8 pb-5 px-3 sm:px-5 relative zoom-in">
                    <div class="absolute left-0 top-0 mt-3 ml-3">
                        <input class="input border border-gray-500" type="checkbox">
                    </div>
                    <a href="" class="w-3/5 file__icon file__icon--empty-directory mx-auto"></a> <a href=""
                                                                                                    class="block font-medium mt-4 text-center truncate">Draft
                        campaigns</a>
                    <div class="text-gray-600 text-xs text-center">120 MB</div>
                    <div class="absolute top-0 right-0 mr-2 mt-2 dropdown ml-auto">
                        <a class="dropdown-toggle w-5 h-5 block" href="javascript:"> <i data-feather="more-vertical"
                                                                                        class="w-5 h-5 text-gray-500"></i>
                        </a>
                        <div class="dropdown-box mt-5 absolute w-40 top-0 right-0 z-10">
                            <div class="dropdown-box__content box p-2">
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                    <i data-feather="users" class="w-4 h-4 mr-2"></i> Share File </a>
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                    <i data-feather="trash" class="w-4 h-4 mr-2"></i> Delete </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="intro-y col-span-6 sm:col-span-4 md:col-span-3 xxl:col-span-2">
                <div class="file box rounded-md px-5 pt-8 pb-5 px-3 sm:px-5 relative zoom-in">
                    <div class="absolute left-0 top-0 mt-3 ml-3">
                        <input class="input border border-gray-500" type="checkbox">
                    </div>
                    <a href="" class="w-3/5 file__icon file__icon--directory mx-auto"></a> <a href=""
                                                                                              class="block font-medium mt-4 text-center truncate">Bidding
                        strategies</a>
                    <div class="text-gray-600 text-xs text-center">112 GB</div>
                    <div class="absolute top-0 right-0 mr-2 mt-2 dropdown ml-auto">
                        <a class="dropdown-toggle w-5 h-5 block" href="javascript:"> <i data-feather="more-vertical"
                                                                                        class="w-5 h-5 text-gray-500"></i>
                        </a>
                        <div class="dropdown-box mt-5 absolute w-40 top-0 right-0 z-10">
                            <div class="dropdown-box__content box p-2">
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                    <i data-feather="users" class="w-4 h-4 mr-2"></i> Share File </a>
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                                    <i data-feather="trash" class="w-4 h-4 mr-2"></i> Delete </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END: Directory & Files -->
    </div>
</div>