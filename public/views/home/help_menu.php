<?php
global $app;
$url = $app->url;


?>

<div class="col-span-12 lg:col-span-3 xxl:col-span-2">
    <h2 class="intro-y text-lg font-medium mr-auto mt-2">
        Гид пользователя
    </h2>
    <!-- BEGIN: File Manager Menu -->
    <div class="intro-y box p-5 mt-6">
        <div class="mt-1">
            <a href="/home/start"
               class="flex items-center px-3 py-2 rounded-md <?= (in_array("/start", $url)) ? "bg-theme-1 text-white" : "" ?> ">
                <i class="w-4 h-4 mr-2" data-feather="image"></i> Start work
            </a>
            <a href="/home/videos"
               class="flex items-center px-3 py-2 mt-2 rounded-md <?= (in_array("/videos", $url)) ? " bg-theme-1 text-white font-medium " : "" ?> ">
                <i class="w-4 h-4 mr-2" data-feather="video"></i> Video tutorials
            </a>
            <a href="/home/files"
               class="flex items-center px-3 py-2 mt-2 rounded-md <?= (in_array("/files", $url)) ? " bg-theme-1 text-white font-medium " : "" ?>">
                <i class="w-4 h-4 mr-2" data-feather="file"></i> Uploaded
                documents </a>
            <a href="/home/users"
               class="flex items-center px-3 py-2 mt-2 rounded-md  <?= (in_array("/users", $url)) ? " bg-theme-1 text-white font-medium " : "" ?>">
                <i class="w-4 h-4 mr-2" data-feather="users"></i> Accounts
            </a>
            <a href="/home/removed"
               class="flex items-center px-3 py-2 mt-2 rounded-md  <?= (in_array("/removed", $url)) ? " bg-theme-1 text-white font-medium " : "" ?>">
                <i class="w-4 h-4 mr-2" data-feather="trash"></i> Removed items
            </a>
        </div>
        <div class="border-t border-gray-200 mt-5 pt-5">
            <a href="/home/structure"
               class="flex items-center px-3 py-2 rounded-md  <?= (in_array("/structure", $url)) ? " bg-theme-1 text-white font-medium " : "" ?>">
                <div class="w-2 h-2 bg-theme-11 rounded-full mr-3"></div>
                Structures
            </a>
            <a href="/home/accounts"
               class="flex items-center px-3 py-2 mt-2 rounded-md  <?= (in_array("/accounts", $url)) ? " bg-theme-1 text-white font-medium " : "" ?>">
                <div class="w-2 h-2 bg-theme-9 rounded-full mr-3"></div>
                Accounts & Accounts managements
            </a>
            <a href="/home/campaigns"
               class="flex items-center px-3 py-2 mt-2 rounded-md  <?= (in_array("/campaigns", $url)) ? " bg-theme-1 text-white font-medium " : "" ?>">
                <div class="w-2 h-2 bg-theme-12 rounded-full mr-3"></div>
                Create Campaigns
            </a>
            <a href="/home/groups"
               class="flex items-center px-3 py-2 mt-2 rounded-md  <?= (in_array("/groups", $url)) ? " bg-theme-1 text-white font-medium " : "" ?>">
                <div class="w-2 h-2 bg-theme-11 rounded-full mr-3"></div>
                Control Ad Groups
            </a>
            <a href="/home/creatives"
               class="flex items-center px-3 py-2 mt-2 rounded-md <?= (in_array("/creatives", $url)) ? " bg-theme-1 text-white font-medium " : "" ?>">
                <div class="w-2 h-2 bg-theme-6 rounded-full mr-3"></div>
                Creatives Control
            </a>
            <a href="#"
               class="flex items-center px-3 py-2 mt-2 rounded-md <?= (in_array("#", $url)) ? " bg-theme-1 text-white font-medium " : "" ?>">
                <i class="w-4 h-4 mr-2"
                   data-feather="plus"></i> Add New Label
            </a>
        </div>
    </div>
    <!-- END: File Manager Menu -->
</div>