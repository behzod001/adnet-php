<?php

global $app;

// report placement and platform
use app\exceptions\DataNotFoundException;
use app\models\GoogleReports;
use app\models\ReportTypes;
use helpers\Alert;
use helpers\StringHelper;
use helpers\XmlParser;

$chartPlacementData = [
    'labels' => [],
    'desktop' => [
        "audience_network" => 0,
        "facebook" => 0,
        "instagram" => 0,
        "messenger" => 0
    ],
    'mobile_app' => [
        "audience_network" => 0,
        "facebook" => 0,
        "instagram" => 0,
        "messenger" => 0
    ]
];
// report age and gender
$chartAgeData = [
    'labels' => [],
    'male' => [],
    'female' => [],
];

// report single chart CTR hourly
$chartData = [
    'labels' => [
        "00-07" => 0,
        "07-12" => 0,
        "12-17" => 0,
        "17-24" => 0
    ],
    'clicks' => 0,
    'ctr' => 0,
    'cpc' => 0,

];
$dataHourly = [0, 0, 0, 0];
$generalReport = '';
try {
    //getting age  demographical chart data
    $ageImpressions = $app->getFacebookActiveAccount()
        ->getInsights(["impressions"], ["breakdowns" => "age,gender"])
        ->getLastResponse()->getContent()['data'];

    foreach ($ageImpressions as $impression) {
        if ($impression['gender'] == "male") {
            array_push($chartAgeData['male'], $impression["impressions"]);
        }
        if ($impression['gender'] == "female") {
            array_push($chartAgeData['female'], $impression["impressions"]);
        }
        $chartAgeData['labels'][$impression['age']] = $chartAgeData['labels'][$impression['age']] ?? $impression['age'];
    }
    unset($chartAgeData['labels']["Unknown"]);


    $generalReport = $app->getFacebookActiveAccount()
        ->getInsights(["clicks", "impressions", "reach", "cpm",
            "cost_per_inline_link_click", "cost_per_inline_post_engagement", "cost_per_unique_click", "cost_per_unique_inline_link_click",
            "social_spend", "canvas_avg_view_percent", "canvas_avg_view_time"], ['level' => 'account'])
        ->getLastResponse()
        ->getContent()['data'];
    $reportCtrChartData = $app->getFacebookActiveAccount()
        ->getInsights(["clicks", "cpc", "ctr", "reach", "cpm"], ["breakdowns" => "hourly_stats_aggregated_by_audience_time_zone", "level" => "account"])
        ->getLastResponse()->getContent()['data'];

//    $minCost = $maxCost = 0;
    $cost = [];
    foreach ($reportCtrChartData as $reportCtrChartDatum) {
        $chartData['clicks'] += $reportCtrChartDatum['clicks'];
        $chartData['ctr'] += $reportCtrChartDatum['ctr'] ?? 0;
        $chartData['cpc'] += $reportCtrChartDatum['cpc'] ?? 0;

        // getting times labels  from facebook response
        $pattern = '/(^[\d]{2}):[\d]{2}:[\d]{2} - ([\d]{2}):[\d]{2}:[\d]{2}/';
        $match = [];
        preg_match($pattern, $reportCtrChartDatum['hourly_stats_aggregated_by_audience_time_zone'], $match);
        array_push($chartData['labels'], $match[1]);


        if ($match[1] < 7) {
            $dataHourly[0] += $reportCtrChartDatum['ctr'];
        }
        if ($match[1] > 7 && $match[1] < 12) {
            $dataHourly[1] += $reportCtrChartDatum['ctr'];
        }

        if ($match[1] > 12 && $match[1] < 17) {
            $dataHourly[2] += $reportCtrChartDatum['ctr'];
        }
        if ($match[1] > 17 && $match[1] < 24) {
            $dataHourly[3] += $reportCtrChartDatum['ctr'];
        }
        // push ctr to cost labels array
        if (isset($reportCtrChartDatum['ctr'])) {
            array_push($cost, $reportCtrChartDatum['ctr']);
        }
    }

// getting placement and  platform
    $placementPlatform = $app->getFacebookActiveAccount()
        ->getInsights(["impressions"], ["breakdowns" => "device_platform,publisher_platform"])
        ->getLastResponse()->getContent()['data'];
    foreach ($placementPlatform as $platform) {
        $chartPlacementData['labels'][$platform['publisher_platform']] = StringHelper::underscoreToCamelCase($platform['publisher_platform']);

        if (in_array($platform['device_platform'], ["desktop", "desktop_web"])) {
            $chartPlacementData['desktop'][$platform['publisher_platform']] += $platform["impressions"] ?? 0;
        }
        if (in_array($platform['device_platform'], ["mobile_app", "mobile_web"])) {
            $chartPlacementData['mobile_app'][$platform['publisher_platform']] += $platform["impressions"] ?? 0;
        }
    }
} catch (DataNotFoundException $e) {
    Alert::warning(' У вас есть Фасебоок или Yandex Direct аккаунт ? Тогда выберите ваш аккаунт. Если нет создайте и  <a href="/accounts" class="text-theme-1 mx-1"> регистрируйте. </a> ');
}


$model = new GoogleReports();
$reportItem = $model->findLastByType(1);

// parsing report item by XmlParser
$parser = (new XmlParser())->parseFileContent($reportItem->file);


?>


<div class="grid grid-cols-12 gap-6">
    <div class="col-span-12 xxl:col-span-9 grid grid-cols-12 gap-6">
        <!-- BEGIN: General Report -->
        <div class="col-span-12 mt-8">
            <div class="intro-y flex items-center h-10">
                <h2 class="text-lg font-medium truncate mr-5">
                    General Report
                </h2>
                <a href="" class="ml-auto flex text-theme-1"> <i data-feather="refresh-ccw" class="w-4 h-4 mr-3"></i>
                    Reload Data </a>
            </div>
            <div class="grid grid-cols-12 gap-6 mt-5">
                <div class="col-span-12 sm:col-span-6 xl:col-span-3 intro-y">
                    <div class="report-box zoom-in">
                        <div class="box p-5">
                            <div class="flex">
                                <i data-feather="shopping-cart" class="report-box__icon text-theme-10"></i>
                                <div class="ml-auto">
                                    <div class="report-box__indicator bg-theme-9 tooltip cursor-pointer"
                                         title="33% Higher than last month"> 33% <i data-feather="chevron-up"
                                                                                    class="w-4 h-4"></i></div>
                                </div>
                            </div>
                            <div class="text-3xl font-bold leading-8 mt-6"><?= $generalReport['cpm'] ?? 0 ?></div>
                            <div class="text-base text-gray-600 mt-1">Average cost for 1,000 impressions.</div>
                        </div>
                    </div>
                </div>
                <div class="col-span-12 sm:col-span-6 xl:col-span-3 intro-y">
                    <div class="report-box zoom-in">
                        <div class="box p-5">
                            <div class="flex">
                                <i data-feather="credit-card" class="report-box__icon text-theme-11"></i>
                                <div class="ml-auto">
                                    <div class="report-box__indicator bg-theme-6 tooltip cursor-pointer"
                                         title="2% Lower than last month"> 2% <i data-feather="chevron-down"
                                                                                 class="w-4 h-4"></i></div>
                                </div>
                            </div>
                            <div class="text-3xl font-bold leading-8 mt-6"><?= $generalReport['impressions'] ?? 0 ?></div>
                            <div class="text-base text-gray-600 mt-1">Number of times your ads were on screen.</div>
                        </div>
                    </div>
                </div>
                <div class="col-span-12 sm:col-span-6 xl:col-span-3 intro-y">
                    <div class="report-box zoom-in">
                        <div class="box p-5">
                            <div class="flex">
                                <i data-feather="monitor" class="report-box__icon text-theme-12"></i>
                                <div class="ml-auto">
                                    <div class="report-box__indicator bg-theme-9 tooltip cursor-pointer"
                                         title="12% Higher than last month"> 12% <i data-feather="chevron-up"
                                                                                    class="w-4 h-4"></i></div>
                                </div>
                            </div>
                            <div class="text-3xl font-bold leading-8 mt-6"><?= $generalReport['clicks'] ?? 0 ?></div>
                            <div class="text-base text-gray-600 mt-1">Total Clicks</div>
                        </div>
                    </div>
                </div>
                <div class="col-span-12 sm:col-span-6 xl:col-span-3 intro-y">
                    <div class="report-box zoom-in">
                        <div class="box p-5">
                            <div class="flex">
                                <i data-feather="user" class="report-box__icon text-theme-9"></i>
                                <div class="ml-auto">
                                    <div class="report-box__indicator bg-theme-9 tooltip cursor-pointer"
                                         title="22% Higher than last month"> 22% <i data-feather="chevron-up"
                                                                                    class="w-4 h-4"></i></div>
                                </div>
                            </div>
                            <div class="text-3xl font-bold leading-8 mt-6"><?= $generalReport['reach'] ?? 0 ?></div>
                            <div class="text-base text-gray-600 mt-1">Reach</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END: General Report -->

        <!--        --><?php //if ($chartData['clicks'] > 0) { ?>
        <!-- BEGIN: Sales Report -->
        <div class="col-span-8 lg:col-span-8 mt-8">
            <div class="intro-y block sm:flex items-center h-10">
                <h2 class="text-lg font-medium truncate mr-5">
                    Daily Report
                </h2>
                <div class="sm:ml-auto mt-3 sm:mt-0 relative text-gray-700">
                    <i data-feather="calendar" class="w-4 h-4 z-10 absolute my-auto inset-y-0 ml-3 left-0"></i>
                    <input type="text" data-daterange="true" class="datepicker input w-full sm:w-56 box pl-10">
                </div>
            </div>
            <div class="intro-y box p-5 mt-12 sm:mt-5">
                <div class="flex flex-col xl:flex-row xl:items-center">
                    <div class="flex">
                        <div>
                            <div class="text-theme-20 text-lg xl:text-xl font-bold">
                                $ <?= number_format($chartData['ctr'], 2, '.', ',') ?></div>
                            <div class="text-gray-600">CTR</div>
                        </div>
                        <div class="w-px h-12 border border-r border-dashed border-gray-300 mx-4 xl:mx-6"></div>
                        <div>
                            <div class="text-gray-600 text-lg xl:text-xl font-medium">
                                $ <?= number_format($chartData['cpc'], 2, '.', ',') ?></div>
                            <div class="text-gray-600">CPC</div>
                        </div>
                        <div class="w-px h-12 border border-r border-dashed border-gray-300 mx-4 xl:mx-6"></div>
                        <div>
                            <div class="text-gray-600 text-lg xl:text-xl font-medium"><?= $chartData['clicks']; ?></div>
                            <div class="text-gray-600">Click</div>
                        </div>
                    </div>
                    <div class="dropdown relative xl:ml-auto mt-5 xl:mt-0">
                        <button class="dropdown-toggle button font-normal border text-white relative flex items-center text-gray-700">
                            Filter by Category <i data-feather="chevron-down" class="w-4 h-4 ml-2"></i></button>
                        <div class="dropdown-box mt-10 absolute w-40 top-0 xl:right-0 z-20">
                            <div class="dropdown-box__content box p-2 overflow-y-auto h-32"><a href=""
                                                                                               class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">PC
                                    & Laptop</a> <a href=""
                                                    class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">Smartphone</a>
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">Electronic</a>
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">Photography</a>
                                <a href=""
                                   class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">Sport</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="report-chart">
                    <canvas id="report-ctr-chart"
                            data-labels="<?= implode(",", $chartData['labels']); ?>"
                            data-cost="<?= implode(",", $dataHourly); ?>"
                            height="160" class="mt-6"></canvas>
                </div>
            </div>
        </div>
        <!-- END: Sales Report -->
        <!--        --><?php //} ?>
        <!--        --><?php //if (array_sum($chartPlacementData['mobile_app']) > 0) { ?>
        <!-- BEGIN: Mobile users -->
        <div class="col-span-4 sm:col-span-4 lg:col-span-4 mt-8">
            <div class="intro-y flex items-center h-10">
                <h2 class="text-lg font-medium truncate mr-5">
                    Mobile users
                </h2>
                <a href="" class="ml-auto text-theme-1 truncate">See all</a>
            </div>
            <div class="intro-y box p-5 mt-5">
                <canvas class="mt-3" id="report-device-mobile" height="280"
                        data-count="<?= implode(",", $chartPlacementData['mobile_app']) ?>"
                        data-labels="Audience network,Facebook,Instagram,Messenger"
                ></canvas>
                <div class="mt-8">
                    <div class="flex items-center">
                        <div class="w-2 h-2 bg-theme-11 rounded-full mr-3"></div>
                        <span class="truncate">Audience network</span>
                        <div class="h-px flex-1 border border-r border-dashed border-gray-300 mx-3 xl:hidden"></div>
                        <span class="font-medium xl:ml-auto"><?= $chartPlacementData['mobile_app']['audience_network'] ?></span>
                    </div>
                    <div class="flex items-center mt-4">
                        <div class="w-2 h-2 bg-theme-1 rounded-full mr-3"></div>
                        <span class="truncate">Facebook</span>
                        <div class="h-px flex-1 border border-r border-dashed border-gray-300 mx-3 xl:hidden"></div>
                        <span class="font-medium xl:ml-auto"><?= $chartPlacementData['mobile_app']['facebook'] ?></span>
                    </div>
                    <div class="flex items-center mt-4">
                        <div class="w-2 h-2 bg-theme-6 rounded-full mr-3"></div>
                        <span class="truncate">Instagram</span>
                        <div class="h-px flex-1 border border-r border-dashed border-gray-300 mx-3 xl:hidden"></div>
                        <span class="font-medium xl:ml-auto"><?= $chartPlacementData['mobile_app']['instagram'] ?></span>
                    </div>
                    <div class="flex items-center mt-4">
                        <div class="w-2 h-2 bg-theme-12 rounded-full mr-3"></div>
                        <span class="truncate">Messenger</span>
                        <div class="h-px flex-1 border border-r border-dashed border-gray-300 mx-3 xl:hidden"></div>
                        <span class="font-medium xl:ml-auto"><?= $chartPlacementData['mobile_app']['messenger'] ?></span>
                    </div>
                </div>
            </div>
        </div>
        <!-- END: Mobile users -->
        <!--        --><?php //} ?>

        <!--        --><?php //if (array_sum($chartPlacementData['desktop']) > 0) { ?>
        <!-- BEGIN: Desktop users-->
        <div class="col-span-4 sm:col-span-4 lg:col-span-4 mt-8">
            <div class="intro-y flex items-center h-10">
                <h2 class="text-lg font-medium truncate mr-5">
                    Desktop users
                </h2>
                <a href="" class="ml-auto text-theme-1 truncate">See all</a>
            </div>
            <div class="intro-y box p-5 mt-5">
                <canvas class="mt-3" id="report-device-desktop" height="280"
                        data-count="<?= implode(",", $chartPlacementData['desktop']) ?>"
                        data-labels="Audience network,Facebook,Instagram,Messenger"
                ></canvas>
                <div class="mt-8">
                    <div class="flex items-center">
                        <div class="w-2 h-2 bg-theme-11 rounded-full mr-3"></div>
                        <span class="truncate">Audience network</span>
                        <div class="h-px flex-1 border border-r border-dashed border-gray-300 mx-3 xl:hidden"></div>
                        <span class="font-medium xl:ml-auto"><?= $chartPlacementData['desktop']['audience_network'] ?></span>
                    </div>
                    <div class="flex items-center mt-4">
                        <div class="w-2 h-2 bg-theme-1 rounded-full mr-3"></div>
                        <span class="truncate">Facebook</span>
                        <div class="h-px flex-1 border border-r border-dashed border-gray-300 mx-3 xl:hidden"></div>
                        <span class="font-medium xl:ml-auto"><?= $chartPlacementData['desktop']['facebook'] ?></span>
                    </div>
                    <div class="flex items-center mt-4">
                        <div class="w-2 h-2 bg-theme-6 rounded-full mr-3"></div>
                        <span class="truncate">Instagram</span>
                        <div class="h-px flex-1 border border-r border-dashed border-gray-300 mx-3 xl:hidden"></div>
                        <span class="font-medium xl:ml-auto"><?= $chartPlacementData['desktop']['instagram'] ?></span>
                    </div>
                    <div class="flex items-center mt-4">
                        <div class="w-2 h-2 bg-theme-12 rounded-full mr-3"></div>
                        <span class="truncate">Messenger</span>
                        <div class="h-px flex-1 border border-r border-dashed border-gray-300 mx-3 xl:hidden"></div>
                        <span class="font-medium xl:ml-auto"><?= $chartPlacementData['desktop']['messenger'] ?></span>
                    </div>
                </div>
            </div>
        </div>
        <!-- END: Desktop users -->
        <!--        --><?php //} ?>
        <!--        --><?php //if (array_sum([$chartAgeData['female'], $chartAgeData['male']]) > 0) { ?>
        <!-- BEGIN: Age and Gender Report -->
        <div class="col-span-8 lg:col-span-8 mt-8">
            <div class="intro-y flex items-center h-10">
                <h2 class="text-lg font-medium truncate mr-5">
                    Desktop users
                </h2>
                <a href="" class="ml-auto text-theme-1 truncate">See all</a>
            </div>
            <div class="intro-y p-5 mt-12 sm:mt-5">
                <div class="intro-y box p-5 mt-5 report-chart">
                    <canvas id="report-age-chart"
                            data-labels="<?= implode(",", $chartAgeData['labels']) ?>"
                            data-male="<?= implode(",", $chartAgeData['male']); ?>"
                            data-female="<?= implode(",", $chartAgeData['female']); ?>"
                            height="160" class="mt-6"></canvas>
                </div>
            </div>
        </div>
        <!-- END: Age and Gender Report -->
        <!--        --><?php //} ?>
        <!-- BEGIN: Weekly Top Seller -->
        <div class="col-span-12 mt-6">
            <div class="intro-y block sm:flex items-center h-10">
                <h2 class="text-lg font-medium truncate mr-5">
                    Google Базовые Кампания
                </h2>
                <div class="flex items-center sm:ml-auto mt-3 sm:mt-0">
                    <a href="/google/index?type=1&val=Базовые" class="button box flex items-center text-gray-700"> <i
                                data-feather="file-text" class="hidden sm:block w-4 h-4 mr-2"></i> View All </a>
                    <a href="/google/types" class="ml-3 button box flex items-center text-gray-700"> <i
                                data-feather="file-text" class="hidden sm:block w-4 h-4 mr-2"></i> Report types </a>
                    <a href="/google/upload" class="ml-3 button box flex items-center text-gray-700"> <i
                                data-feather="file-text" class="hidden sm:block w-4 h-4 mr-2"></i> Upload report types
                    </a>
                </div>
            </div>
            <div class="intro-y overflow-auto  mt-8 sm:mt-0">
                <table class="table table-report sm:mt-2">
                    <thead>
                    <tr>
                        <?php
                        foreach ($parser->getHeaderRows()->values() as $value) { ?>
                            <th class="text-center whitespace-no-wrap"> <?= $value; ?> </th>
                        <?php } ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($parser->getRowColumnsData()->values() as $rowItem) {
                        $campaignClicks["{$rowItem->get(md5("Кампания"))}"] = (int)preg_replace('/[\x00-\x1F\x7F, ]/u', '', $rowItem->get(md5("Kлики")));
                        $campaignViews["{$rowItem->get(md5("Кампания"))}"] = (int)preg_replace('/[\x00-\x1F\x7F, ]/u', '', $rowItem->get(md5("Показы")));

                        ?>
                        <tr class="intro-x">
                            <?php foreach ($rowItem->values() as $item => $value) { ?>
                                <td class="w-40"><?= $value; ?> </td>
                            <?php } ?>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END: Weekly Top Seller -->
    </div>
    <div class="col-span-12 xxl:col-span-3 xxl:border-l border-theme-5 -mb-10 pb-10">
        <div class="xxl:pl-6 grid grid-cols-12 gap-6">

            <div class=" mt-5 col-span-12 sm:col-span-4 xxl:col-span-12 intro-y">
                <div class="mini-report-chart box p-5 zoom-in">
                    <div class="flex">
                        <div class="text-lg font-medium truncate mr-3">
                            <?= StringHelper::underscoreToCamelCase("cost_per_inline_link_click"); ?>
                        </div>
                        <div class="py-1 px-2 rounded-full text-xs bg-gray-200 text-gray-600 cursor-pointer ml-auto truncate">
                            <?= ' $ ' . isset($generalReport['cost_per_inline_link_click']) ?? 0; ?>
                        </div>
                    </div>
                    <div class="mt-4">
                        <canvas class="simple-line-chart-1 -ml-1" height="60"></canvas>
                    </div>
                </div>
            </div>

            <div class=" mt-5 col-span-12 sm:col-span-4 xxl:col-span-12 intro-y">
                <div class="mini-report-chart box p-5 zoom-in">
                    <div class="flex">
                        <div class="text-lg font-medium truncate mr-3">Social Media</div>
                        <div class="py-1 px-2 rounded-full text-xs bg-gray-200 text-gray-600 cursor-pointer ml-auto truncate">
                            <?= isset($generalReport['social_spend']) ?? 0; ?> Followers
                        </div>
                    </div>
                    <div class="mt-4">
                        <canvas class="simple-line-chart-1 -ml-1" height="60"></canvas>
                    </div>
                </div>
            </div>
            <div class=" mt-5 col-span-12 sm:col-span-4 xxl:col-span-12 intro-y">
                <div class="mini-report-chart box p-5 zoom-in">
                    <div class="flex">
                        <div class="text-lg font-medium truncate mr-3">
                            <?= StringHelper::underscoreToCamelCase("cost_per_inline_post_engagement"); ?>
                        </div>
                        <div class="py-1 px-2 rounded-full text-xs bg-gray-200 text-gray-600 cursor-pointer ml-auto truncate">
                            <?= ' $ ' . isset($generalReport['cost_per_inline_post_engagement']) ?? 0; ?>
                        </div>
                    </div>
                    <div class="mt-4">
                        <canvas class="simple-line-chart-1 -ml-1" height="60"></canvas>
                    </div>
                </div>
            </div>
            <div class=" mt-5 col-span-12 sm:col-span-4 xxl:col-span-12 intro-y">
                <div class="mini-report-chart box p-5 zoom-in">
                    <div class="flex">
                        <div class="text-lg font-medium truncate mr-3">
                            <?= StringHelper::underscoreToCamelCase("cost_per_unique_click"); ?>
                        </div>
                        <div class="py-1 px-2 rounded-full text-xs bg-gray-200 text-gray-600 cursor-pointer ml-auto truncate">
                            <?= ' $ ' . isset($generalReport['cost_per_unique_click']) ?? 0; ?>
                        </div>
                    </div>
                    <div class="mt-4">
                        <canvas class="simple-line-chart-1 -ml-1" height="60"></canvas>
                    </div>
                </div>
            </div>
            <div class=" mt-5 col-span-12 sm:col-span-4 xxl:col-span-12 intro-y">
                <div class="mini-report-chart box p-5 zoom-in">
                    <div class="flex">
                        <div class="text-lg font-medium truncate mr-3">
                            <?= StringHelper::underscoreToCamelCase("cost_per_unique_inline_link_click"); ?>
                        </div>
                        <div class="py-1 px-2 rounded-full text-xs bg-gray-200 text-gray-600 cursor-pointer ml-auto truncate">
                            <?= ' $ ' . isset($generalReport['cost_per_unique_inline_link_click']) ?? 0; ?>
                        </div>
                    </div>
                    <div class="mt-4">
                        <canvas class="simple-line-chart-1 -ml-1" height="60"></canvas>
                    </div>
                </div>
            </div>
            <div class=" mt-5 col-span-12 sm:col-span-4 xxl:col-span-12 intro-y">
                <div class="mini-report-chart box p-5 zoom-in">
                    <div class="flex items-center">
                        <div class="w-2/4 flex-none">
                            <div class="text-lg font-medium truncate">Instant Experience</div>
                            <div class="text-gray-600 mt-1"> Average percentage</div>
                        </div>
                        <div class="flex-none ml-auto relative">
                            <canvas id="report-donut-chart-2" width="90" height="90"></canvas>
                            <div class="font-medium absolute w-full h-full flex items-center justify-center top-0 left-0"
                                 title="The average percentage of the Instant Experience that people saw. An Instant Experience is a screen that opens after someone interacts with your ad on a mobile device. It may include a series of interactive or multimedia components, including video, images product catalog and more.">
                                <?= isset($generalReport['canvas_avg_view_percent']) ?? 0; ?>%
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class=" mt-5 col-span-12 sm:col-span-4 xxl:col-span-12 intro-y">
                <div class="mini-report-chart box p-5 zoom-in">
                    <div class="flex">
                        <div class="text-lg font-medium truncate mr-3"> Instant Experience</div>

                        <div class="py-1 px-2 rounded-full text-xs bg-gray-200 text-gray-600 cursor-pointer ml-auto truncate"
                             title="The average total time, in seconds, that people spent viewing an Instant Experience. An Instant Experience is a screen that opens after someone interacts with your ad on a mobile device. It may include a series of interactive or multimedia components, including video, images product catalog and more.">
                            <?= isset($generalReport['canvas_avg_view_time']) ?? 0; ?>
                        </div>
                    </div>
                    <div class="mt-4">
                        <canvas class="simple-line-chart-1 -ml-1" height="60"></canvas>
                    </div>
                </div>

            </div>
            <!-- END: Schedules -->
        </div>
    </div>
    <div class="col-span-12 xxl:col-span-9 grid grid-cols-12 gap-6">
        <!--        --><?php //if (array_sum($campaignClicks) > 0) { ?>
        <!-- BEGIN: Weekly Top Seller -->
        <div class="col-span-12 sm:col-span-6 lg:col-span-4 mt-8">
            <div class="intro-y flex items-center h-10">
                <h2 class="text-lg font-medium truncate mr-5">
                    Кампания Kлики
                </h2>
                <a href="/google/index?type=1&val=Базовые" class="ml-auto text-theme-1 truncate">See all</a>
            </div>
            <div class="intro-y box p-5 mt-5">
                <canvas class="mt-3" id="report-pie-chart"
                        data-count="<?= implode(",", $campaignClicks) ?>"
                        data-labels="<?= implode(",", array_keys($campaignClicks)) ?>"
                        data-colors='#FF8B26,#FFC533,#285FD3,#ff9933,#ccff33,#a3a375'
                        height="280"></canvas>
                <div class="mt-8">
                    <?php foreach ($campaignClicks as $campaignClick => $count) { ?>
                        <div class="flex items-center">
                            <div class="w-2 h-2 bg-theme-11 rounded-full mr-3"></div>
                            <span class="truncate"><?= $campaignClick; ?></span>
                            <div class="h-px flex-1 border border-r border-dashed border-gray-300 mx-3 xl:hidden"></div>
                            <span class="font-medium xl:ml-auto"><?= $count ?></span>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <!-- END: Weekly Top Seller -->
        <!--        --><?php //} ?>
        <!--        --><?php //if (array_sum($campaignViews) > 0){ ?>
        <!-- BEGIN: Weekly Top Seller -->
        <div class="col-span-12 sm:col-span-6 lg:col-span-4 mt-8">
            <div class="intro-y flex items-center h-10">
                <h2 class="text-lg font-medium truncate mr-5">
                    Кампания Показы
                </h2>
                <a href="/google/index?type=1&val=Базовые" class="ml-auto text-theme-1 truncate">See all</a>
            </div>
            <div class="intro-y box p-5 mt-5">
                <canvas class="mt-3" id="report-pie-chart2"
                        data-count="<?= implode(",", $campaignViews) ?>"
                        data-labels="<?= implode(",", array_keys($campaignViews)) ?>"
                        data-colors='#FF8B26,#FFC533,#285FD3,#ff9933,#ccff33,#a3a375'
                        height="280"></canvas>
                <div class="mt-8">
                    <div class="mt-8">
                        <?php foreach ($campaignViews as $campaignView => $counts) { ?>
                            <div class="flex items-center">
                                <div class="w-2 h-2 bg-theme-11 rounded-full mr-3"></div>
                                <span class="truncate"><?= $campaignView; ?></span>
                                <div class="h-px flex-1 border border-r border-dashed border-gray-300 mx-3 xl:hidden"></div>
                                <span class="font-medium xl:ml-auto"><?= $counts ?></span>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <!-- END: Weekly Top Seller -->
            <!--            --><?php //} ?>
        </div>
    </div>

</div>
