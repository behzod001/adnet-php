<?php


global $app;

use models\SignIn;

if (isset($_SESSION['identity'])) {
    header('Location: /home', true, 200);
}
$model = new SignIn();
if (isset($_POST['SignIn'])) {
    $model->load($_POST['SignIn']);
    if ($model->validate() && !$model->hasError()) {
        header('Location: /home', true, 200);
    }
}


?>
<div class="container sm:px-10">
    <div class="block xl:grid grid-cols-2 gap-4">
        <!-- BEGIN: Login Info -->
        <div class="hidden xl:flex flex-col min-h-screen">
            <a href="" class="-intro-x flex items-center pt-5">
                <img alt="AdNet" class="w-6" src="/resources/dist/images/logo.svg">
                <span class="text-white text-lg ml-3"> Ad<span class="font-medium">Net</span> </span>
            </a>
            <div class="my-auto">
                <img alt="AdNet" class="-intro-x w-1/2 -mt-16"
                     src="/resources/dist/images/illustration.svg">
                <div class="-intro-x text-white font-medium text-4xl leading-tight mt-10">
                    A few more clicks to
                    <br>
                    sign in to your account.
                </div>
                <div class="-intro-x mt-5 text-lg text-white">Manage all your e-commerce accounts in one place</div>
            </div>
        </div>
        <!-- END: Login Info -->
        <!-- BEGIN: Login Form -->
        <div class="h-screen xl:h-auto flex py-5 xl:py-0 my-10 xl:my-0">
            <div class="my-auto mx-auto xl:ml-20 bg-white xl:bg-transparent px-5 sm:px-8 py-8 xl:p-0 rounded-md shadow-md xl:shadow-none w-full sm:w-3/4 lg:w-2/4 xl:w-auto">
                <h2 class="intro-x font-bold text-2xl xl:text-3xl text-center xl:text-left">
                    Sign In
                </h2>
                <div class="intro-x mt-2 text-gray-500 xl:hidden text-center">A few more clicks to sign in to your
                    account. Manage all your e-commerce accounts in one place
                </div>
                <form action="/sign/in" method="post">
                    <div class="intro-x mt-8">
                        <input name="SignIn[login]" type="text"
                               class="intro-x login__input input input--lg border <?= ($model->errors[SignIn::LOGIN]) ? 'border-theme-6 ' : 'border-gray-300' ?> block"
                               placeholder="Login">
                        <?= ($model->errors[SignIn::LOGIN]) ? '<div class="text-theme-6 mt-2">' . $model->errors[SignIn::LOGIN] . '</div>' : '' ?>
                        <input name="SignIn[password]" type="password"
                               class="intro-x login__input input input--lg border <?= ($model->errors[SignIn::PASSWORD]) ? 'border-theme-6 ' : 'border-gray-300' ?>  block mt-4"
                               placeholder="Password">
                        <?= ($model->errors[SignIn::PASSWORD]) ? '<div class="text-theme-6 mt-2">' . $model->errors[SignIn::PASSWORD] . '</div>' : '' ?>
                    </div>
                    <div class="intro-x flex text-gray-700 text-xs sm:text-sm mt-4">
                        <div class="flex items-center mr-auto">
                            <input type="checkbox" class="input border mr-2" id="remember-me">
                            <label class="cursor-pointer select-none" for="remember-me">Remember me</label>
                        </div>
                        <a href="/sign/forgot">Forgot Password?</a>
                    </div>
                    <div class="intro-x mt-5 xl:mt-8 text-center xl:text-left">
                        <button type="submit" class="button button--lg w-full xl:w-32 text-white bg-theme-1 xl:mr-3">
                            Login
                        </button>
                        <a href="/sign/up"
                           class="button button--lg w-full xl:w-32 text-gray-700 border border-gray-300 mt-3 xl:mt-0">Sign
                            up</a>
                    </div>
                </form>
                <div class="intro-x mt-10 xl:mt-24 text-gray-700 text-center xl:text-left">
                    By signin up, you agree to our
                    <br>
                    <a class="text-theme-1" href="/sign/terms">Terms and Conditions</a> & <a class="text-theme-1"
                                                                                             href="/sign/privacy">Privacy
                        Policy</a>
                </div>
            </div>
        </div>
        <!-- END: Login Form -->
    </div>
</div>
