<?php

global $app;
$app->logout();
ob_start();
header("Location: /sign/in", true, 200);
ob_end_flush();