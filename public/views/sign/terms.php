<div class="container sm:px-10">
    <div class="block xl:grid grid-cols-2 gap-4">

        <div class="hidden xl:flex flex-col min-h-screen">
            <a href="" class="-intro-x flex items-center pt-5">
                <img alt="AdNet" class="w-6" src="/resources/dist/images/logo.svg">
                <span class="text-white text-lg ml-3"> Ad<span class="font-medium">Net</span> </span>
            </a>
            <div class="my-auto">
                <img alt="AdNet" class="-intro-x w-1/2 -mt-16" src="/resources/dist/images/illustration.svg">
                <div class="-intro-x text-white font-medium text-4xl leading-tight mt-10">
                    A few more clicks to
                    <br>
                    sign up to your account.
                </div>
                <div class="-intro-x mt-5 text-lg text-white">Manage all your e-commerce accounts in one place</div>
            </div>
        </div>
        <div class="h-screen xl:h-auto flex py-5 xl:py-0 my-10 xl:my-0">
            <div class="my-auto mx-auto xl:ml-20 bg-white xl:bg-transparent px-5 sm:px-8 py-8 xl:p-0 rounded-md shadow-md xl:shadow-none w-full sm:w-3/4 lg:w-2/4 xl:w-auto">
                <h2 class="intro-x font-bold text-2xl xl:text-3xl text-center xl:text-left">
                 Terms and Conditions
                </h2>
                <div class="intro-x mt-2 text-gray-500 ">

                    <p class="my-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus assumenda eum
                        hic ipsum, iste
                        nam
                        rem temporibus? Architecto atque consectetur consequatur culpa debitis doloremque, dolores et ex
                        excepturi harum illo itaque magni maiores molestiae nam neque nobis omnis possimus quae quidem
                        quis
                        rem sunt tempore ullam unde ut voluptatibus. Accusamus atque consectetur maiores molestiae nihil
                        non
                        quod rem tempora ut!</p>
                    <div class="grid grid-cols-12 gap-6 my-5">
                        <div class="intro-y col-span-12 lg:col-span-6">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus assumenda eum hic ipsum,
                            iste nam
                            rem temporibus? Architecto atque consectetur consequatur culpa debitis doloremque, dolores
                            et ex
                            excepturi harum illo itaque magni maiores molestiae nam neque nobis omnis possimus quae
                            quidem quis
                            rem sunt tempore ullam unde ut voluptatibus. Accusamus atque consectetur maiores molestiae
                            nihil non
                            quod rem tempora ut!
                        </div>
                        <div class="intro-y col-span-12 lg:col-span-6">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus assumenda eum hic ipsum,
                            iste nam
                            rem temporibus? Architecto atque consectetur consequatur culpa debitis doloremque, dolores
                            et ex
                            excepturi harum illo itaque magni maiores molestiae nam neque nobis omnis possimus quae
                            quidem quis
                            rem sunt tempore ullam unde ut voluptatibus. Accusamus atque consectetur maiores molestiae
                            nihil non
                            quod rem tempora ut!
                        </div>
                    </div>
                    <div class="intro-y col-span-1 lg:col-span-6">
                        <p class="my-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus assumenda
                            eum hic ipsum,
                            iste nam
                            rem temporibus? Architecto atque consectetur consequatur culpa debitis doloremque, dolores
                            et ex
                            excepturi harum illo itaque magni maiores molestiae nam neque nobis omnis possimus quae
                            quidem quis
                            rem sunt tempore ullam unde ut voluptatibus. Accusamus atque consectetur maiores molestiae
                            nihil non
                            quod rem tempora ut!</p>

                        <p class="my-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum ipsam magnam
                            natus non quisquam voluptas.</p>
                    </div>
                    <div class="intro-x mt-5 xl:mt-8 text-center xl:text-left">
                        <a href="/sign/up" type="submit"
                           class="button button--lg w-full xl:w-32 text-white bg-theme-1 xl:mr-3">
                            Register
                        </a>
                        <a href="/sign/in"
                           class="button button--lg w-full xl:w-32 text-gray-700 border border-gray-300 mt-3 xl:mt-0">
                            Sign in
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>