<?php


use FacebookAds\Api;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\AdSet;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\Fields\AdSetFields;

class GetAdGroupReport implements GetReport
{
    private Api $service;

    public function __construct()
    {
        global $app;
        $this->service = $app->getFacebook();
    }

    public function getReport()
    {
        $fields = array(
            AdSetFields::ID,
        );
        $params = array(
            'effective_status' => array('ACTIVE', 'PAUSED'),
        );
        if (isset($_SESSION['FACEBOOK']['ad_group_id'])) {
            $adSet = new AdSet($_SESSION['FACEBOOK']['ad_group_id']);
            $insights = $adSet->getInsights();
            return $insights->getLastResponse()->getContent();
        } else {
            $insights = [];
            $account = new AdAccount($_SESSION['FACEBOOK']['account_id'], null, $this->service);
            $adSet = $account->getAdSets($fields, $params);
            foreach ($adSet as $set) {
                $insights[$set->{AdSetFields::ID}] = $set->getInsights()->getLastResponse()->getContent();

            }
            return $insights;
        }
    }
}