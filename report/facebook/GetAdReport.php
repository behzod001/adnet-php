<?php


use FacebookAds\Api;
use FacebookAds\Object\Ad;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\AdSet;
use FacebookAds\Object\Fields\AdSetFields;

class GetAdReport implements GetReport
{
    private Api $service;

    public function __construct()
    {
        global $app;
        $this->service = $app->getFacebook();
    }

    public function getReport()
    {
        $fields = array(
            AdSetFields::ID,
        );
        $params = array(
            'effective_status' => array('ACTIVE', 'PAUSED'),
        );
        if (isset($_SESSION['FACEBOOK']['ad_id'])) {
            $ads = new Ad($_SESSION['FACEBOOK']['ad_id']);
            $insights = $ads->getInsights();
            return $insights->getLastResponse()->getContent();
        } else {
            $insights = [];
            $account = new AdAccount($_SESSION['FACEBOOK']['account_id'], null, $this->service);
            $ads = $account->getAds($fields, $params);
            foreach ($ads as $ad) {
                $insights[$ad->{AdSetFields::ID}] = $ad->getInsights()->getLastResponse()->getContent();

            }
            return $insights;
        }
    }
}