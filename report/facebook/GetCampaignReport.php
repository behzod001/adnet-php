<?php


use FacebookAds\Api;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\Fields\CampaignFields;

class GetCampaignReport implements GetReport
{
    private Api $service;

    public function __construct()
    {
        global $app;
        $this->service = $app->getFacebook();
    }

    public function getReport()
    {
        $fields = array(
            CampaignFields::ID,
            CampaignFields::NAME,
            CampaignFields::RECOMMENDATIONS,
            CampaignFields::CAN_USE_SPEND_CAP,
            CampaignFields::ACCOUNT_ID,
            CampaignFields::STATUS,
        );
        $params = array(
            'effective_status' => array('ACTIVE', 'PAUSED'),
        );
        if (isset($_SESSION['FACEBOOK']['campaign_id'])) {
            $campaigns = new Campaign($_SESSION['FACEBOOK']['campaign_id']);
            $insights = $campaigns->getInsights();
            return $insights->getLastResponse()->getContent();
        } else {
            $insights = [];
            $account = new AdAccount($_SESSION['FACEBOOK']['account_id'], null, $this->service);
            $campaigns = $account->getCampaigns($fields, $params);
            foreach ($campaigns as $campaign) {
                $insights[$campaign->{AdAccountFields::ID}] = $campaign->getInsights()->getLastResponse()->getContent();
            }
            return $insights;
        }
    }
}