function main() {

    // get token
    var token = authorize();
    Logger.log(token);

    // get campaign request
    var campaignIterator = AdsApp.campaigns()
        .forDateRange("LAST_MONTH")
        .get();

    // table header response in array
    var sheetArray = [[
        "name",
        "clicks",
        "impressions",
        "Average cost per click.",
        "Average cost per thousand impressions.",
        "Average cost per view.",
        "conversions",
        "Rate of conversions."
    ]];

    // collecting campaigns responses to array
    while (campaignIterator.hasNext()) {
        var campaign = campaignIterator.next();
        var stats = campaign.getStatsFor("LAST_MONTH");
        var impressions = stats.getImpressions();
        var clicks = stats.getClicks();

        sheetArray.push([
            campaign.getName(),
            clicks,
            impressions,
            stats.getAverageCpc(),
            stats.getAverageCpm(),
            stats.getAverageCpv(),
            stats.getConversions(),
            stats.getConversionRate()


        ]);
    }

    // send to adnets backend server
    sendBody(sheetArray);
}

function authorize() {

    var data = {
        "email": "xayrullayev001@gmail.com",
        "password": "manageradnetwork"
    };

    var options = {
        'method': 'post',
        'contentType': 'application/json',
        'payload': JSON.stringify(data)
    };

    var response = fetch('/authenticate', options);
    var jsonResponse = JSON.parse(response);
    Logger.log(jsonResponse.token);
    return jsonResponse.token;
}

function sendBody(body) {
    var requestPayload = {"load": body};
    Logger.log(requestPayload);

    var options = {
        'method': 'post',
        'contentType': 'application/json',
        'payload': JSON.stringify(requestPayload),
        "headers": {
            "Authorization": authHeader()
        }
    };

    Logger.log(options);
    var result = fetch('/google/report/load', options);
    Logger.log(result);
}

function authHeader() {
    return "Bearer " + authorize();
}

function fetch(url, options) {
    return UrlFetchApp.fetch('http://217.30.163.148:8080/api/v1' + url, options);
}
